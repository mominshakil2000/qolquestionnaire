$(document).ready(function(){
	// bind date control
	$('.input-group.date').datepicker({
		format: "dd-mm-yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});
	
});
function parseFloatSafe(input)
{
	if(isNaN(input))
	{
		return parseFloat(0);
	}
	else
	{
		return parseFloat(input);
	}
}