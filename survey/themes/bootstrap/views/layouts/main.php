<?php /* @var $this Controller */ ?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="en" lang="en">
<head>
	<meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
	<meta name="language" content="en" />
	<title><?php echo CHtml::encode($this->pageTitle); ?></title>
	
	<?php Yii::app()->bootstrap->register(); ?>
	
    <link rel="stylesheet" type="text/css" href="<?php echo Yii::app()->theme->baseUrl; ?>/css/styles.css" />	
    <script type="text/javascript" src="<?php echo Yii::app()->theme->baseUrl; ?>/js/common.js"></script>	
</head>

<body>

<?php $this->widget('bootstrap.widgets.TbNavbar',array(
	'fixed'=>false,
	'type'=>'default',
	'brandOptions'=>array('class'=>'navbar-brand'),
	'navbar'=>true,
	'collapse'=>false,
    'items'=>array(
        array(
            'class'=>'bootstrap.widgets.TbMenu',
            'navbar'=>true,
            'items'=>array(
                array('label'=>'Home', 'url'=>array('/site/index')),
				array(
					'label'=>'General',
					'class'=>'bootstrap.widgets.TbMenu',
					'navbar'=>true,
					'items'=>array(
						array('label'=>'QOL Survey Forms', 'url'=>array('/household/admin')),
						array('label'=>'User', 'url'=>array('/user/admin')),
						array('label'=>'Regional Council', 'url'=>array('/regionalCouncil/admin')),
						array('label'=>'Local Council', 'url'=>array('/localCouncil/admin')),
						array('label'=>'Jamat Khana', 'url'=>array('/jamatKhana/admin')),
						array('divider'=>true),
					),
				),				
                array('label'=>'Login', 'url'=>array('/site/login'), 'visible'=>Yii::app()->user->isGuest),
                array('label'=>'Logout ('.Yii::app()->user->name.')', 'url'=>array('/site/logout'), 'visible'=>!Yii::app()->user->isGuest)
            ),
        ),
    ),
)); ?>

<div class="container" id="page">

	<?php if(isset($this->breadcrumbs)):?>
		<?php $this->widget('bootstrap.widgets.TbBreadcrumbs', array(
			'links'=>$this->breadcrumbs,
			'separator'=>' ',
		)); ?><!-- breadcrumbs -->
	<?php endif?>

	<?php echo $content; ?>

	<div class="clear"></div>
	
	<?php
	/*
	<div id="footer">
		Copyright &copy; <?php echo date('Y'); ?> by My Company.<br/>
		All Rights Reserved.<br/>
		<?php echo Yii::powered(); ?>
	</div><!-- footer -->
	*/ 
	?>
</div><!-- page -->

</body>
</html>
