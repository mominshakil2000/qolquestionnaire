<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<!-- Fixed navbar -->
<div class="navbar navbar-default navbar-fixed-side navbar-fixed-side-left" role="navigation">
	<div class="container">
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
		</div>
		<div class="navbar-collapse collapse">
			<ul class="nav navbar-nav small">
				<li><a href="#household-detail-group1">Bio-data</a></li>
				<li><a href="#household-detail-group2">Jamat Khana &amp; Family Members</a></li>
				<li><a href="#household-detail-group3">Address</a></li>
				<li><a href="#household-detail-group4">Family Members</a></li>
				<li><a href="#household-detail-group5">Family Members Living Away</a></li>
				<li><a href="#household-detail-group11">Income / Deficit</a></li>
				<li><a href="#household-detail-group7">Living Facilities 1</a></li>
				<li><a href="#household-detail-group8">Living Facilities 2</a></li>
				<li><a href="#household-detail-group9">Seniors Count</a></li>
				<li><a href="#household-detail-group10">Seniors Bio-Data</a></li>
				<li><a href="#household-detail-group12">Entrepreneur </a></li>
				<li><a href="#household-detail-group13">Financial Assistance</a></li>
				<li><a href="#household-detail-group6">Maternal &amp; Child Health</a></li>
				<li><a href="#household-detail-group14">Assets</a></li>
				<li><a href="#household-detail-group15">Basic Facilities</a></li>
				<li><a href="#household-detail-group16">Safety &amp; Security</a></li>
				<li><a href="#household-detail-group17">Form Fill Details</a></li>
			</ul>
		</div>
		<!--/.nav-collapse -->
	</div>
</div>

<div class="row">
    <div class="col-xs-12 text-right">
        <?php
            foreach($this->menu as $item)
			{
				$this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'link',
					'type'=>'primary',
					'label'=>$item['label'],				
					'url'=>$item['url'],				
					'htmlOptions'=> isset($item['linkOptions']) ? array_merge(array('class'=>'btn-sm'), $item['linkOptions']) : array('class'=>'btn-sm topBtn'),				
					'icon'=>$item['icon'],				
				));
			}
        ?>
    </div>
    <div class="col-xs-12">
		<?php echo $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>