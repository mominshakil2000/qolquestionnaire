<?php

/**
 * This is the model class for table "household".
 *
 * The followings are the available columns in table 'household':
 * @property string $uid
 * @property string $formTypeUid
 * @property string $formNum
 * @property string $firstName
 * @property string $middleName
 * @property string $lastName
 * @property string $genderUid
 * @property string $nic
 * @property string $familyNumberNic
 * @property string $totalHouseHoldMember
 * @property string $isHouseShared
 * @property string $respondentName
 * @property string $respondentHeadRealtionUid
 * @property string $nearestJamatKhanaUid
 * @property string $localCouncilUid
 * @property string $reginalCouncilUid
 * @property string $numResidingBloodFamilies
 * @property string $numResidingUnbloodFamilies
 * @property string $numResidingUnbloodMember
 * @property string $totalFamilyMember
 * @property string $addressHouseNo
 * @property string $addressStreet
 * @property string $addressColony
 * @property string $addressArea
 * @property string $addressVillageCity
 * @property string $addressProvince
 * @property string $addressPrimaryPhone
 * @property string $addressSecondartPhone
 * @property string $houseConstructTypeUid
 * @property string $houseTotalRoom
 * @property string $cookingFuelUid
 * @property string $lightingSourceUid
 * @property string $winterHeatingSystemFuelUid
 * @property string $smokeOutletUid
 * @property string $drinkingWaterSourceUid
 * @property string $recDistanceUid
 * @property string $jkDistanceUid
 * @property string $toiletFacilityUid
 * @property string $physicalThreatInHouseHold
 * @property string $physicalThreatInNeighbourhood
 * @property string $satisfyWithLivingSpace
 * @property string $internetServiceAvailable
 * @property string $residenceStatusUid
 * @property string $confilictInHouseHoldUid
 * @property string $confilictResolutionUid
 * @property string $numberOfSeniorsInHome
 * @property string $numberOfSeniorsInOther
 * @property string $foodShortageInLastMonth
 * @property string $monthlyFamilyIncomeStatusUid
 * @property string $savingUid
 * @property string $deficitUid
 * @property string $savingAvailableUid
 * @property string $outstandingDebitUid
 * @property string $outstandingDebitInterestRateUid
 * @property string $loanPurposeUid
 * @property string $faJamatiHealth
 * @property string $faJamatiEducation
 * @property string $faJamatiHousing
 * @property string $faJamatiFood
 * @property string $faJamatiSeniorCitizen
 * @property string $faJamatiSpecialChild
 * @property string $faExternalHealth
 * @property string $faExternalEducation
 * @property string $faExternalHousing
 * @property string $faExternalFood
 * @property string $faExternalSeniorCitizen
 * @property string $faExternalSpecialChild
 * @property string $hhIsTelvision
 * @property string $hhQtyTelvision
 * @property string $hhIsDishOrCabel
 * @property string $hhQtyDishOrCabel
 * @property string $hhIsWashingMachine
 * @property string $hhQtyWashingMachine
 * @property string $hhIsElectricFan
 * @property string $hhQtyElectricFan
 * @property string $hhIsAc
 * @property string $hhQtyAc
 * @property string $hhIsRefrigerator
 * @property string $hhQtyRefrigerator
 * @property string $hhIsStove
 * @property string $hhQtyStove
 * @property string $hhIsOven
 * @property string $hhQtyOven
 * @property string $hhIsloadshadingBackup
 * @property string $hhQtyloadshadingBackup
 * @property string $hhIsGeyser
 * @property string $hhQtyGeyser
 * @property string $hhIsHeater
 * @property string $hhQtyHeater
 * @property string $hhIsMobilePhone
 * @property string $hhQtyMobilePhone
 * @property string $hhIsLivestock
 * @property string $hhQtyLivestock
 * @property string $hhIsHouse
 * @property string $hhQtyHouse
 * @property string $hhIsMotoryCycle
 * @property string $hhQtyMotoryCycle
 * @property string $hhIsCommercialProperty
 * @property string $hhQtyCommercialProperty
 * @property string $hhIsComputer
 * @property string $hhQtyComputer
 * @property string $hhIsFruitTrees
 * @property string $hhQtyFruitTrees
 * @property string $hhIsAgricultureAsset
 * @property string $hhQtyAgricultureAsset
 * @property string $hhIsCar
 * @property string $hhQtyCar
 * @property string $hhIsTruckBusVan
 * @property string $hhQtyTruckBusVan
 * @property string $hhIsTractor
 * @property string $hhQtyTractor
 * @property string $hhIsAgricultureLand
 * @property string $hhQtyAgricultureLand
 * @property string $hhQtyAgricultureLandUnitUid
 * @property string $hhIsNonAgricultureLand
 * @property string $hhQtyNonAgricultureLand
 * @property string $hhQtyNonAgricultureLandUnitUid
 * @property string $comments
 * @property string $filledBy
 * @property string $reviewedBy
 * @property string $filledDate
 * @property string $formFilledMinutes
 * @property string $surveyorEmail
 * @property string $surveyorContactNumber
 * @property string $isEducationInstituteAffordable
 * @property string $educationInstituteIsAffordable
 * @property string $educationInstituteIsAccessible
 * @property string $educationInstituteIsQuality
 * @property string $educationInstituteDistanceUid
 * @property string $primaryHealthServiceIsAffordable
 * @property string $primaryHealthServiceIsAccessible
 * @property string $primaryHealthServiceIsQuality
 * @property string $primaryHealthServiceDistanceUid
 * @property string $secondaryHealthServiceIsAffordable
 * @property string $secondaryHealthServiceIsAccessible
 * @property string $secondaryHealthServiceIsQuality
 * @property string $secondaryHealthServiceDistanceUid
 * @property string $drinkingWaterIsAffordable
 * @property string $drinkingWaterIsAccessible
 * @property string $drinkingWaterIsQuality
 * @property string $drinkingWaterDistanceUid
 * @property string $marketIsAffordable
 * @property string $marketIsAccessible
 * @property string $marketIsQuality
 * @property string $marketDistanceUid
 * @property string $parkIsAffordable
 * @property string $parkIsAccessible
 * @property string $parkIsQuality
 * @property string $parkDistanceUid
 * @property string $electricityIsAffordable
 * @property string $electricityIsAccessible
 * @property string $electricityIsQuality
 * @property string $electricityDistanceUid
 * @property string $gasIsAffordable
 * @property string $gasIsAccessible
 * @property string $gasIsQuality
 * @property string $gasDistanceUid
 * @property string $libraryIsAffordable
 * @property string $libraryIsAccessible
 * @property string $libraryIsQuality
 * @property string $libraryDistanceUid
 * @property string $cinemaIsAffordable
 * @property string $cinemaIsAccessible
 * @property string $cinemaIsQuality
 * @property string $cinemaDistanceUid
 * @property string $transportationIsAffordable
 * @property string $transportationIsAccessible
 * @property string $transportationIsQuality
 * @property string $transportationDistanceUid
 * @property string $telephoneIsAffordable
 * @property string $telephoneIsAccessible
 * @property string $telephoneIsQuality
 * @property string $telephoneDistanceUid
 * @property string $isRoadsafty
 * @property string $isUnsafeHouse
 * @property string $isNaturalDisaster
 * @property string $isLawOrder
 * @property string $isInappropriatePolitical
 * @property string $isChildAbuse
 * @property string $isReligiousIntolerance
 * @property string $isLandGrabbing
 * @property string $isRansom
 * @property string $isRiskBusinessWindup
 * @property string $formAddedByUser
 * @property string $formLastUpdatedByUser
 * @property string $formFillStatus
 * @property string $formPostStatus
 * @property string $createDate
 * @property string $lastUpdateDate
 *
 * The followings are the available model relations:
 * @property N28lightingsource $lightingSourceU
 * @property N30drinkingwatersource $drinkingWaterSourceU
 * @property N33residancestatus $residenceStatusU
 * @property N34confilict $confilictInHouseHoldU
 * @property N35disputeresolution $confilictResolutionU
 * @property N38incomestatus $monthlyFamilyIncomeStatusU
 * @property N39savingmode $savingU
 * @property N40deficitmanage $deficitU
 * @property N41savingavaliblity $savingAvailableU
 * @property N42outstandingdebt $outstandingDebitU
 * @property N43interestrate $outstandingDebitInterestRateU
 * @property N44loanpurpose $loanPurposeU
 * @property N1familyrelationship $respondentHeadRealtionU
 * @property Formtype $formTypeU
 * @property Gender $genderU
 * @property Localcouncil $localCouncilU
 * @property Regionalcouncil $reginalCouncilU
 * @property N26homeconstruction $houseConstructTypeU
 * @property N27cookingfuel $cookingFuelU
 * @property N27cookingfuel $winterHeatingSystemFuelU
 * @property N29smokeoutlet $smokeOutletU
 * @property N31facilitydistance $recDistanceU
 * @property N31facilitydistance $jkDistanceU
 * @property N32toiletfacility $toiletFacilityU
 */
class Household extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'household';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('formNum', 'numerical', 'integerOnly'=>true, 'max'=>25000),
			array('nic', 'match', 'pattern' => '/^([0-9]{5}-[0-9]{7}-[0-9]{1}|[0-9]{3}-[0-9]{2}-[0-9]{6})$/', 'message' => 'Invalid CNIC (12345-1234567-1) / NIC (123-45-123456).'),
			array('familyNumberNic', 'length', 'max'=>6),
			array('formNum, formTypeUid, firstName, middleName, lastName, genderUid, isHouseShared, respondentName, respondentHeadRealtionUid, reginalCouncilUid, localCouncilUid, nearestJamatKhanaUid,  numResidingBloodFamilies, numResidingUnbloodFamilies, totalFamilyMember, numResidingUnbloodMember, addressHouseNo, addressStreet, addressColony, addressArea, addressVillageCity, addressProvince, addressPrimaryPhone, addressSecondartPhone,houseConstructTypeUid, houseTotalRoom, cookingFuelUid, lightingSourceUid, winterHeatingSystemFuelUid, smokeOutletUid, drinkingWaterSourceUid, recDistanceUid, jkDistanceUid, toiletFacilityUid, physicalThreatInHouseHold, physicalThreatInNeighbourhood, satisfyWithLivingSpace, internetServiceAvailable, residenceStatusUid, confilictInHouseHoldUid, confilictResolutionUid, numberOfSeniorsInHome, numberOfSeniorsInOther, foodShortageInLastMonth, monthlyFamilyIncomeStatusUid, savingUid, deficitUid, savingAvailableUid, outstandingDebitUid, outstandingDebitInterestRateUid, loanPurposeUid, faJamatiHealth, faJamatiEducation, faJamatiHousing, faJamatiFood, faJamatiSeniorCitizen, faJamatiSpecialChild, faExternalHealth, faExternalEducation, faExternalHousing, faExternalFood, faExternalSeniorCitizen, faExternalSpecialChild, hhIsTelvision, hhQtyTelvision, hhIsDishOrCabel, hhQtyDishOrCabel, hhIsWashingMachine, hhQtyWashingMachine, hhIsElectricFan, hhQtyElectricFan, hhIsAc, hhQtyAc, hhIsRefrigerator, hhQtyRefrigerator, hhIsStove, hhQtyStove, hhIsOven, hhQtyOven, hhIsloadshadingBackup, hhQtyloadshadingBackup, hhIsGeyser, hhQtyGeyser, hhIsHeater, hhQtyHeater, hhIsMobilePhone, hhQtyMobilePhone, hhIsLivestock, hhQtyLivestock, hhIsHouse, hhQtyHouse, hhIsMotoryCycle, hhQtyMotoryCycle, hhIsCommercialProperty, hhQtyCommercialProperty, hhIsComputer, hhQtyComputer, hhIsFruitTrees, hhQtyFruitTrees, hhIsAgricultureAsset, hhQtyAgricultureAsset, hhIsCar, hhQtyCar, hhIsTruckBusVan, hhQtyTruckBusVan, hhIsTractor, hhQtyTractor, hhIsAgricultureLand, hhQtyAgricultureLand, hhQtyAgricultureLandUnitUid, hhIsNonAgricultureLand, hhQtyNonAgricultureLand, hhQtyNonAgricultureLandUnitUid, educationInstituteIsAffordable, educationInstituteIsAccessible, educationInstituteIsQuality, primaryHealthServiceIsAffordable, primaryHealthServiceIsAccessible, primaryHealthServiceIsQuality, secondaryHealthServiceIsAffordable, secondaryHealthServiceIsAccessible, secondaryHealthServiceIsQuality, drinkingWaterIsAffordable, drinkingWaterIsAccessible, drinkingWaterIsQuality, marketIsAffordable, marketIsAccessible, marketIsQuality, parkIsAffordable, parkIsAccessible, parkIsQuality, electricityIsAffordable, electricityIsAccessible, electricityIsQuality, gasIsAffordable, gasIsAccessible, gasIsQuality, libraryIsAffordable, libraryIsAccessible, libraryIsQuality, cinemaIsAffordable, cinemaIsAccessible, cinemaIsQuality, transportationIsAffordable, transportationIsAccessible, transportationIsQuality, telephoneIsAffordable, telephoneIsAccessible, telephoneIsQuality,educationInstituteDistanceUid, primaryHealthServiceDistanceUid, secondaryHealthServiceDistanceUid, drinkingWaterDistanceUid, marketDistanceUid, parkDistanceUid, electricityDistanceUid, gasDistanceUid, libraryDistanceUid, cinemaDistanceUid, transportationDistanceUid, telephoneDistanceUid, isRoadsafty, isUnsafeHouse, isNaturalDisaster, isLawOrder, isInappropriatePolitical, isChildAbuse, isReligiousIntolerance, isLandGrabbing, isRansom, isRiskBusinessWindup, comments, filledBy, reviewedBy, filledDate, formFilledMinutes, surveyorEmail, surveyorContactNumber', 'required'),
			array('formTypeUid, genderUid, totalHouseHoldMember, respondentHeadRealtionUid, localCouncilUid, reginalCouncilUid, numResidingBloodFamilies, numResidingUnbloodFamilies, numResidingUnbloodMember, totalFamilyMember, houseConstructTypeUid, houseTotalRoom, cookingFuelUid, lightingSourceUid, winterHeatingSystemFuelUid, smokeOutletUid, drinkingWaterSourceUid, recDistanceUid, jkDistanceUid, toiletFacilityUid, residenceStatusUid, confilictInHouseHoldUid, confilictResolutionUid, numberOfSeniorsInHome, numberOfSeniorsInOther, monthlyFamilyIncomeStatusUid, savingUid, deficitUid, savingAvailableUid, outstandingDebitUid, outstandingDebitInterestRateUid, loanPurposeUid, educationInstituteDistanceUid, primaryHealthServiceDistanceUid, secondaryHealthServiceDistanceUid, drinkingWaterDistanceUid, marketDistanceUid, parkDistanceUid, electricityDistanceUid, gasDistanceUid, libraryDistanceUid, cinemaDistanceUid, transportationDistanceUid, telephoneDistanceUid, formFillStatus, formPostStatus, formAddedByUser', 'length', 'max'=>11),
			// array('uid, formTypeUid, firstName, genderUid, isHouseShared, respondentName, respondentHeadRealtionUid, localCouncilUid, reginalCouncilUid, numResidingBloodFamilies, numResidingUnbloodMember, totalFamilyMember, houseConstructTypeUid, houseTotalRoom, cookingFuelUid, lightingSourceUid, winterHeatingSystemFuelUid, smokeOutletUid, drinkingWaterSourceUid, recDistanceUid, jkDistanceUid, toiletFacilityUid, physicalThreatInHouseHold, physicalThreatInNeighbourhood, satisfyWithLivingSpace, internetServiceAvailable, residenceStatusUid, confilictInHouseHoldUid, confilictResolutionUid, numberOfSeniorsInHome, numberOfSeniorsInOther, foodShortageInLastMonth, monthlyFamilyIncomeStatusUid, savingUid, deficitUid, savingAvailableUid, outstandingDebitUid, outstandingDebitInterestRateUid, loanPurposeUid, faJamatiHealth, faJamatiEducation, faJamatiHousing, faJamatiFood, faJamatiSeniorCitizen, faJamatiSpecialChild, faExternalHealth, faExternalEducation, faExternalHousing, faExternalFood, faExternalSeniorCitizen, faExternalSpecialChild, hhIsTelvision, hhQtyTelvision, hhIsDishOrCabel, hhQtyDishOrCabel, hhIsWashingMachine, hhQtyWashingMachine, hhIsElectricFan, hhQtyElectricFan, hhIsAc, hhQtyAc, hhIsRefrigerator, hhQtyRefrigerator, hhIsStove, hhQtyStove, hhIsOven, hhQtyOven, hhIsloadshadingBackup, hhQtyloadshadingBackup, hhIsGeyser, hhQtyGeyser, hhIsHeater, hhQtyHeater, hhIsMobilePhone, hhQtyMobilePhone, hhIsLivestock, hhQtyLivestock, hhIsHouse, hhQtyHouse, hhIsMotoryCycle, hhQtyMotoryCycle, hhIsCommercialProperty, hhQtyCommercialProperty, hhIsComputer, hhQtyComputer, hhIsFruitTrees, hhQtyFruitTrees, hhIsAgricultureAsset, hhQtyAgricultureAsset, hhIsCar, hhQtyCar, hhIsTruckBusVan, hhQtyTruckBusVan, hhIsTractor, hhQtyTractor, hhIsAgricultureLand, hhQtyAgricultureLand, hhQtyAgricultureLandUnitUid, hhIsNonAgricultureLand, hhQtyNonAgricultureLand, hhQtyNonAgricultureLandUnitUid, comments, filledBy, reviewedBy, filledDate, formFilledMinutes, surveyorEmail, surveyorContactNumber', 'required'),
			array('firstName, middleName, lastName, nic, familyNumberNic, respondentName, addressHouseNo, addressStreet, addressColony, addressArea, addressVillageCity, addressProvince, addressPrimaryPhone, addressSecondartPhone, filledBy, comments, filledDate, formFilledMinutes,  reviewedBy, surveyorEmail, surveyorContactNumber', 'length', 'max'=>255),
			array('isHouseShared, physicalThreatInHouseHold, physicalThreatInNeighbourhood, satisfyWithLivingSpace, internetServiceAvailable, foodShortageInLastMonth, faJamatiHealth, faJamatiEducation, faJamatiHousing, faJamatiFood, faJamatiSeniorCitizen, faJamatiSpecialChild, faExternalHealth, faExternalEducation, faExternalHousing, faExternalFood, faExternalSeniorCitizen, faExternalSpecialChild, hhIsTelvision, hhIsDishOrCabel, hhIsWashingMachine, hhIsElectricFan, hhIsAc, hhIsRefrigerator, hhIsStove, hhIsOven, hhIsloadshadingBackup, hhIsGeyser, hhIsHeater, hhIsMobilePhone, hhIsLivestock, hhIsHouse, hhIsMotoryCycle, hhIsCommercialProperty, hhIsComputer, hhIsFruitTrees, hhIsAgricultureAsset, hhIsCar, hhIsTruckBusVan, hhIsTractor, hhIsAgricultureLand, hhIsNonAgricultureLand, isEducationInstituteAffordable, educationInstituteIsAffordable, educationInstituteIsAccessible, educationInstituteIsQuality, primaryHealthServiceIsAffordable, primaryHealthServiceIsAccessible, primaryHealthServiceIsQuality, secondaryHealthServiceIsAffordable, secondaryHealthServiceIsAccessible, secondaryHealthServiceIsQuality, drinkingWaterIsAffordable, drinkingWaterIsAccessible, drinkingWaterIsQuality, marketIsAffordable, marketIsAccessible, marketIsQuality, parkIsAffordable, parkIsAccessible, parkIsQuality, electricityIsAffordable, electricityIsAccessible, electricityIsQuality, gasIsAffordable, gasIsAccessible, gasIsQuality, libraryIsAffordable, libraryIsAccessible, libraryIsQuality, cinemaIsAffordable, cinemaIsAccessible, cinemaIsQuality, transportationIsAffordable, transportationIsAccessible, transportationIsQuality, telephoneIsAffordable, telephoneIsAccessible, telephoneIsQuality, isRoadsafty, isUnsafeHouse, isNaturalDisaster, isLawOrder, isInappropriatePolitical, isChildAbuse, isReligiousIntolerance, isLandGrabbing, isRansom, isRiskBusinessWindup', 'length', 'max'=>1),
			array('hhQtyTelvision, hhQtyDishOrCabel, hhQtyWashingMachine, hhQtyElectricFan, hhQtyAc, hhQtyRefrigerator, hhQtyStove, hhQtyOven, hhQtyloadshadingBackup, hhQtyGeyser, hhQtyHeater, hhQtyMobilePhone, hhQtyLivestock, hhQtyHouse, hhQtyMotoryCycle, hhQtyCommercialProperty, hhQtyComputer, hhQtyFruitTrees, hhQtyAgricultureAsset, hhQtyCar, hhQtyTruckBusVan, hhQtyTractor, hhQtyAgricultureLand, hhQtyNonAgricultureLand', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, formTypeUid, firstName, middleName, lastName, genderUid, nic, familyNumberNic, totalHouseHoldMember, isHouseShared, respondentName, respondentHeadRealtionUid, nearestJamatKhanaUid, localCouncilUid, reginalCouncilUid, numResidingBloodFamilies, numResidingUnbloodFamilies, numResidingUnbloodMember, totalFamilyMember, addressHouseNo, addressStreet, addressColony, addressArea, addressVillageCity, addressProvince, addressPrimaryPhone, addressSecondartPhone, houseConstructTypeUid, houseTotalRoom, cookingFuelUid, lightingSourceUid, winterHeatingSystemFuelUid, smokeOutletUid, drinkingWaterSourceUid, recDistanceUid, jkDistanceUid, toiletFacilityUid, physicalThreatInHouseHold, physicalThreatInNeighbourhood, satisfyWithLivingSpace, internetServiceAvailable, residenceStatusUid, confilictInHouseHoldUid, confilictResolutionUid, numberOfSeniorsInHome, numberOfSeniorsInOther, foodShortageInLastMonth, monthlyFamilyIncomeStatusUid, savingUid, deficitUid, savingAvailableUid, outstandingDebitUid, outstandingDebitInterestRateUid, loanPurposeUid, faJamatiHealth, faJamatiEducation, faJamatiHousing, faJamatiFood, faJamatiSeniorCitizen, faJamatiSpecialChild, faExternalHealth, faExternalEducation, faExternalHousing, faExternalFood, faExternalSeniorCitizen, faExternalSpecialChild, hhIsTelvision, hhQtyTelvision, hhIsDishOrCabel, hhQtyDishOrCabel, hhIsWashingMachine, hhQtyWashingMachine, hhIsElectricFan, hhQtyElectricFan, hhIsAc, hhQtyAc, hhIsRefrigerator, hhQtyRefrigerator, hhIsStove, hhQtyStove, hhIsOven, hhQtyOven, hhIsloadshadingBackup, hhQtyloadshadingBackup, hhIsGeyser, hhQtyGeyser, hhIsHeater, hhQtyHeater, hhIsMobilePhone, hhQtyMobilePhone, hhIsLivestock, hhQtyLivestock, hhIsHouse, hhQtyHouse, hhIsMotoryCycle, hhQtyMotoryCycle, hhIsCommercialProperty, hhQtyCommercialProperty, hhIsComputer, hhQtyComputer, hhIsFruitTrees, hhQtyFruitTrees, hhIsAgricultureAsset, hhQtyAgricultureAsset, hhIsCar, hhQtyCar, hhIsTruckBusVan, hhQtyTruckBusVan, hhIsTractor, hhQtyTractor, hhIsAgricultureLand, hhQtyAgricultureLand, hhQtyAgricultureLandUnitUid, hhIsNonAgricultureLand, hhQtyNonAgricultureLand, hhQtyNonAgricultureLandUnitUid, comments, filledBy, reviewedBy, filledDate, formFilledMinutes, surveyorEmail, surveyorContactNumber, isEducationInstituteAffordable, educationInstituteIsAffordable, educationInstituteIsAccessible, educationInstituteIsQuality, educationInstituteDistanceUid, primaryHealthServiceIsAffordable, primaryHealthServiceIsAccessible, primaryHealthServiceIsQuality, primaryHealthServiceDistanceUid, secondaryHealthServiceIsAffordable, secondaryHealthServiceIsAccessible, secondaryHealthServiceIsQuality, secondaryHealthServiceDistanceUid, drinkingWaterIsAffordable, drinkingWaterIsAccessible, drinkingWaterIsQuality, drinkingWaterDistanceUid, marketIsAffordable, marketIsAccessible, marketIsQuality, marketDistanceUid, parkIsAffordable, parkIsAccessible, parkIsQuality, parkDistanceUid, electricityIsAffordable, electricityIsAccessible, electricityIsQuality, electricityDistanceUid, gasIsAffordable, gasIsAccessible, gasIsQuality, gasDistanceUid, libraryIsAffordable, libraryIsAccessible, libraryIsQuality, libraryDistanceUid, cinemaIsAffordable, cinemaIsAccessible, cinemaIsQuality, cinemaDistanceUid, transportationIsAffordable, transportationIsAccessible, transportationIsQuality, transportationDistanceUid, telephoneIsAffordable, telephoneIsAccessible, telephoneIsQuality, telephoneDistanceUid, isRoadsafty, isUnsafeHouse, isNaturalDisaster, isLawOrder, isInappropriatePolitical, isChildAbuse, isReligiousIntolerance, isLandGrabbing, isRansom, isRiskBusinessWindup, formAddedByUser, formLastUpdatedByUser, formPostStatus, formFillStatus,createDate,lastUpdateDate', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'lightingSourceU' => array(self::BELONGS_TO, 'N28lightingsource', 'lightingSourceUid'),
			'drinkingWaterSourceU' => array(self::BELONGS_TO, 'N30drinkingwatersource', 'drinkingWaterSourceUid'),
			'residenceStatusU' => array(self::BELONGS_TO, 'N33residancestatus', 'residenceStatusUid'),
			'confilictInHouseHoldU' => array(self::BELONGS_TO, 'N34confilict', 'confilictInHouseHoldUid'),
			'confilictResolutionU' => array(self::BELONGS_TO, 'N35disputeresolution', 'confilictResolutionUid'),
			'monthlyFamilyIncomeStatusU' => array(self::BELONGS_TO, 'N38incomestatus', 'monthlyFamilyIncomeStatusUid'),
			'savingU' => array(self::BELONGS_TO, 'N39savingmode', 'savingUid'),
			'deficitU' => array(self::BELONGS_TO, 'N40deficitmanage', 'deficitUid'),
			'savingAvailableU' => array(self::BELONGS_TO, 'N41savingavaliblity', 'savingAvailableUid'),
			'outstandingDebitU' => array(self::BELONGS_TO, 'N42outstandingdebt', 'outstandingDebitUid'),
			'outstandingDebitInterestRateU' => array(self::BELONGS_TO, 'N43interestrate', 'outstandingDebitInterestRateUid'),
			'loanPurposeU' => array(self::BELONGS_TO, 'N44loanpurpose', 'loanPurposeUid'),
			'respondentHeadRealtionU' => array(self::BELONGS_TO, 'N1familyrelationship', 'respondentHeadRealtionUid'),
			'formTypeU' => array(self::BELONGS_TO, 'Formtype', 'formTypeUid'),
			'genderU' => array(self::BELONGS_TO, 'Gender', 'genderUid'),
			'localCouncilU' => array(self::BELONGS_TO, 'Localcouncil', 'localCouncilUid'),
			'reginalCouncilU' => array(self::BELONGS_TO, 'Regionalcouncil', 'reginalCouncilUid'),
			'houseConstructTypeU' => array(self::BELONGS_TO, 'N26homeconstruction', 'houseConstructTypeUid'),
			'cookingFuelU' => array(self::BELONGS_TO, 'N27cookingfuel', 'cookingFuelUid'),
			'winterHeatingSystemFuelU' => array(self::BELONGS_TO, 'N27cookingfuel', 'winterHeatingSystemFuelUid'),
			'smokeOutletU' => array(self::BELONGS_TO, 'N29smokeoutlet', 'smokeOutletUid'),
			'recDistanceU' => array(self::BELONGS_TO, 'N31facilitydistance', 'recDistanceUid'),
			'jkDistanceU' => array(self::BELONGS_TO, 'N31facilitydistance', 'jkDistanceUid'),
			'toiletFacilityU' => array(self::BELONGS_TO, 'N32toiletfacility', 'toiletFacilityUid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'formTypeUid' => 'Form Type',
			'formNum' => 'Form Number',
			'firstName' => 'First Name',
			'middleName' => 'Middle Name',
			'lastName' => 'Last Name',
			'genderUid' => 'Gender',
			'nic' => 'C.N.I.C / N.I.C #:',
			'familyNumberNic' => 'Family number on NIC (head of HH)',
			'totalHouseHoldMember' => 'Total House Hold Member',
			'isHouseShared' => 'Is House Shared',
			'respondentName' => 'Name of Respondent',
			'respondentHeadRealtionUid' => 'Relation of Respondent with HH Head',
			'nearestJamatKhanaUid' => 'Nearest Jamat Khana',
			'localCouncilUid' => 'Local Council',
			'reginalCouncilUid' => 'Regional Council',
			'numResidingBloodFamilies' => 'No. of Blood related residing families',
			'numResidingUnbloodFamilies' => 'No. of unrelated residing families',
			'numResidingUnbloodMember' => 'No. of unrelated residing members',
			'totalFamilyMember' => 'No. of Blood related members',
			'addressHouseNo' => 'House / Apt. No.',
			'addressStreet' => 'Street',
			'addressColony' => 'Colony',
			'addressArea' => 'Area',
			'addressVillageCity' => 'Village/City',
			'addressProvince' => 'Province',
			'addressPrimaryPhone' => 'Primary Phone Number',
			'addressSecondartPhone' => 'Secondary Phone Number',
			'houseConstructTypeUid' => 'House Construct Type',
			'houseTotalRoom' => 'House Total Room',
			'cookingFuelUid' => 'Cooking Fuel',
			'lightingSourceUid' => 'Lighting Source',
			'winterHeatingSystemFuelUid' => 'Winter Heating System Fuel',
			'smokeOutletUid' => 'Smoke Outlet',
			'drinkingWaterSourceUid' => 'Drinking Water Source',
			'recDistanceUid' => 'Rec Distance',
			'jkDistanceUid' => 'Jk Distance',
			'toiletFacilityUid' => 'Toilet Facility',
			'physicalThreatInHouseHold' => 'Physical Threat In House Hold',
			'physicalThreatInNeighbourhood' => 'Physical Threat In Neighbourhood',
			'satisfyWithLivingSpace' => 'Satisfy With Living Space',
			'internetServiceAvailable' => 'Internet Service Available',
			'residenceStatusUid' => 'Residence Status',
			'confilictInHouseHoldUid' => 'Confilict In House Hold',
			'confilictResolutionUid' => 'Confilict Resolution',
			'numberOfSeniorsInHome' => 'Number Of Seniors (65+) residing with: In Home',
			'numberOfSeniorsInOther' => 'Number Of Seniors (65+) residing with: Others',
			'foodShortageInLastMonth' => 'Did you face any food shortage in last month?',
			'monthlyFamilyIncomeStatusUid' => 'What is your monthly family income status? ',
			'savingUid' => 'If family status is surplus? What is the mode of saving / Investment (Most / Preferred mode of the investment)',
			'deficitUid' => 'If family is experiencing deficit, how is it being managed? ',
			'savingAvailableUid' => 'Saving Available',
			'outstandingDebitUid' => 'Outstanding debt from? (Major borrowing)',
			'outstandingDebitInterestRateUid' => 'Average Interest rate on outstanding debt (Per anum)?',
			'loanPurposeUid' => 'What was the purpose of loan money? (For major borrowing)',
			'faJamatiHealth' => 'Fa Jamati Health',
			'faJamatiEducation' => 'Fa Jamati Education',
			'faJamatiHousing' => 'Fa Jamati Housing',
			'faJamatiFood' => 'Fa Jamati Food',
			'faJamatiSeniorCitizen' => 'Fa Jamati Senior Citizen',
			'faJamatiSpecialChild' => 'Fa Jamati Special Child',
			'faExternalHealth' => 'Fa External Health',
			'faExternalEducation' => 'Fa External Education',
			'faExternalHousing' => 'Fa External Housing',
			'faExternalFood' => 'Fa External Food',
			'faExternalSeniorCitizen' => 'Fa External Senior Citizen',
			'faExternalSpecialChild' => 'Fa External Special Child',
			'hhIsTelvision' => 'Is Telvision',
			'hhQtyTelvision' => 'Qty Telvision',
			'hhIsDishOrCabel' => 'Is Dish Or Cabel',
			'hhQtyDishOrCabel' => 'Qty Dish Or Cabel',
			'hhIsWashingMachine' => 'Is Washing Machine',
			'hhQtyWashingMachine' => 'Qty Washing Machine',
			'hhIsElectricFan' => 'Is Electric Fan',
			'hhQtyElectricFan' => 'Qty Electric Fan',
			'hhIsAc' => 'Is Ac',
			'hhQtyAc' => 'Qty Ac',
			'hhIsRefrigerator' => 'Is Refrigerator',
			'hhQtyRefrigerator' => 'Qty Refrigerator',
			'hhIsStove' => 'Is Stove',
			'hhQtyStove' => 'Qty Stove',
			'hhIsOven' => 'Is Oven',
			'hhQtyOven' => 'Qty Oven',
			'hhIsloadshadingBackup' => 'Isloadshading Backup',
			'hhQtyloadshadingBackup' => 'Qtyloadshading Backup',
			'hhIsGeyser' => 'Is Geyser',
			'hhQtyGeyser' => 'Qty Geyser',
			'hhIsHeater' => 'Is Heater',
			'hhQtyHeater' => 'Qty Heater',
			'hhIsMobilePhone' => 'Is Mobile Phone',
			'hhQtyMobilePhone' => 'Qty Mobile Phone',
			'hhIsLivestock' => 'Is Livestock',
			'hhQtyLivestock' => 'Qty Livestock',
			'hhIsHouse' => 'Is House',
			'hhQtyHouse' => 'Qty House',
			'hhIsMotoryCycle' => 'Is Motory Cycle',
			'hhQtyMotoryCycle' => 'Qty Motory Cycle',
			'hhIsCommercialProperty' => 'Is Commercial Property',
			'hhQtyCommercialProperty' => 'Qty Commercial Property',
			'hhIsComputer' => 'Is Computer',
			'hhQtyComputer' => 'Qty Computer',
			'hhIsFruitTrees' => 'Is Fruit Trees',
			'hhQtyFruitTrees' => 'Qty Fruit Trees',
			'hhIsAgricultureAsset' => 'Is Agriculture Asset',
			'hhQtyAgricultureAsset' => 'Qty Agriculture Asset',
			'hhIsCar' => 'Is Car',
			'hhQtyCar' => 'Qty Car',
			'hhIsTruckBusVan' => 'Is Truck Bus Van',
			'hhQtyTruckBusVan' => 'Qty Truck Bus Van',
			'hhIsTractor' => 'Is Tractor',
			'hhQtyTractor' => 'Qty Tractor',
			'hhIsAgricultureLand' => 'Is Agriculture Land',
			'hhQtyAgricultureLand' => 'Qty Agriculture Land',
			'hhQtyAgricultureLandUnitUid' => 'Qty Agriculture Land Unit',
			'hhIsNonAgricultureLand' => 'Is Non Agriculture Land',
			'hhQtyNonAgricultureLand' => 'Qty Non Agriculture Land',
			'hhQtyNonAgricultureLandUnitUid' => 'Qty Non Agriculture Land Unit',
			'comments' => 'Comments',
			'filledBy' => 'Filled By',
			'reviewedBy' => 'Reviewed By',
			'filledDate' => 'Filled Date',
			'formFilledMinutes' => 'Form Filled Minutes',
			'surveyorEmail' => 'Surveyor Email',
			'surveyorContactNumber' => 'Surveyor Contact Number',
			'isEducationInstituteAffordable' => 'Is Education Institute Affordable',
			'educationInstituteIsAffordable' => 'Education Institute Is Affordable',
			'educationInstituteIsAccessible' => 'Education Institute Is Accessible',
			'educationInstituteIsQuality' => 'Education Institute Is Quality',
			'educationInstituteDistanceUid' => 'Education Institute Distance',
			'primaryHealthServiceIsAffordable' => 'Primary Health Service Is Affordable',
			'primaryHealthServiceIsAccessible' => 'Primary Health Service Is Accessible',
			'primaryHealthServiceIsQuality' => 'Primary Health Service Is Quality',
			'primaryHealthServiceDistanceUid' => 'Primary Health Service Distance',
			'secondaryHealthServiceIsAffordable' => 'Secondary Health Service Is Affordable',
			'secondaryHealthServiceIsAccessible' => 'Secondary Health Service Is Accessible',
			'secondaryHealthServiceIsQuality' => 'Secondary Health Service Is Quality',
			'secondaryHealthServiceDistanceUid' => 'Secondary Health Service Distance',
			'drinkingWaterIsAffordable' => 'Drinking Water Is Affordable',
			'drinkingWaterIsAccessible' => 'Drinking Water Is Accessible',
			'drinkingWaterIsQuality' => 'Drinking Water Is Quality',
			'drinkingWaterDistanceUid' => 'Drinking Water Distance',
			'marketIsAffordable' => 'Market Is Affordable',
			'marketIsAccessible' => 'Market Is Accessible',
			'marketIsQuality' => 'Market Is Quality',
			'marketDistanceUid' => 'Market Distance',
			'parkIsAffordable' => 'Park Is Affordable',
			'parkIsAccessible' => 'Park Is Accessible',
			'parkIsQuality' => 'Park Is Quality',
			'parkDistanceUid' => 'Park Distance',
			'electricityIsAffordable' => 'Electricity Is Affordable',
			'electricityIsAccessible' => 'Electricity Is Accessible',
			'electricityIsQuality' => 'Electricity Is Quality',
			'electricityDistanceUid' => 'Electricity Distance',
			'gasIsAffordable' => 'Gas Is Affordable',
			'gasIsAccessible' => 'Gas Is Accessible',
			'gasIsQuality' => 'Gas Is Quality',
			'gasDistanceUid' => 'Gas Distance',
			'libraryIsAffordable' => 'Library Is Affordable',
			'libraryIsAccessible' => 'Library Is Accessible',
			'libraryIsQuality' => 'Library Is Quality',
			'libraryDistanceUid' => 'Library Distance',
			'cinemaIsAffordable' => 'Cinema Is Affordable',
			'cinemaIsAccessible' => 'Cinema Is Accessible',
			'cinemaIsQuality' => 'Cinema Is Quality',
			'cinemaDistanceUid' => 'Cinema Distance',
			'transportationIsAffordable' => 'Transportation Is Affordable',
			'transportationIsAccessible' => 'Transportation Is Accessible',
			'transportationIsQuality' => 'Transportation Is Quality',
			'transportationDistanceUid' => 'Transportation Distance',
			'telephoneIsAffordable' => 'Telephone Is Affordable',
			'telephoneIsAccessible' => 'Telephone Is Accessible',
			'telephoneIsQuality' => 'Telephone Is Quality',
			'telephoneDistanceUid' => 'Telephone Distance',
			'isRoadsafty' => 'Is Roadsafty',
			'isUnsafeHouse' => 'Is Unsafe House',
			'isNaturalDisaster' => 'Is Natural Disaster',
			'isLawOrder' => 'Is Law Order',
			'isInappropriatePolitical' => 'Is Inappropriate Political',
			'isChildAbuse' => 'Is Child Abuse',
			'isReligiousIntolerance' => 'Is Religious Intolerance',
			'isLandGrabbing' => 'Is Land Grabbing',
			'isRansom' => 'Is Ransom',
			'isRiskBusinessWindup' => 'Is Risk Business Windup',
			'formAddedByUser' => 'Added By User',
			'formLastUpdatedByUser' => 'Last Updated By User',
			'formFillStatus' => 'Form Fill Status',
			'formPostStatus' => 'Form Save As',
			'createDate' => 'Create Date',
			'lastUpdateDate' => 'Last Update Date',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,false);
		$criteria->compare('formTypeUid',$this->formTypeUid,false);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('middleName',$this->middleName,true);
		$criteria->compare('lastName',$this->lastName,true);
		$criteria->compare('genderUid',$this->genderUid,false);
		$criteria->compare('nic',$this->nic,true);
		$criteria->compare('familyNumberNic',$this->familyNumberNic,true);
		$criteria->compare('totalHouseHoldMember',$this->totalHouseHoldMember,false);
		$criteria->compare('isHouseShared',$this->isHouseShared,false);
		$criteria->compare('respondentName',$this->respondentName,true);
		$criteria->compare('respondentHeadRealtionUid',$this->respondentHeadRealtionUid,false);
		$criteria->compare('nearestJamatKhanaUid',$this->nearestJamatKhanaUid,false);
		$criteria->compare('localCouncilUid',$this->localCouncilUid,false);
		$criteria->compare('reginalCouncilUid',$this->reginalCouncilUid,false);
		$criteria->compare('numResidingBloodFamilies',$this->numResidingBloodFamilies,false);
		$criteria->compare('numResidingUnbloodFamilies',$this->numResidingUnbloodFamilies,false);
		$criteria->compare('numResidingUnbloodMember',$this->numResidingUnbloodMember,false);
		$criteria->compare('totalFamilyMember',$this->totalFamilyMember,false);
		$criteria->compare('addressHouseNo',$this->addressHouseNo,false);
		$criteria->compare('addressStreet',$this->addressStreet,false);
		$criteria->compare('addressColony',$this->addressColony,false);
		$criteria->compare('addressArea',$this->addressArea,false);
		$criteria->compare('addressVillageCity',$this->addressVillageCity,false);
		$criteria->compare('addressProvince',$this->addressProvince,false);
		$criteria->compare('addressPrimaryPhone',$this->addressPrimaryPhone,false);
		$criteria->compare('addressSecondartPhone',$this->addressSecondartPhone,false);
		$criteria->compare('houseConstructTypeUid',$this->houseConstructTypeUid,false);
		$criteria->compare('houseTotalRoom',$this->houseTotalRoom,false);
		$criteria->compare('cookingFuelUid',$this->cookingFuelUid,false);
		$criteria->compare('lightingSourceUid',$this->lightingSourceUid,false);
		$criteria->compare('winterHeatingSystemFuelUid',$this->winterHeatingSystemFuelUid,false);
		$criteria->compare('smokeOutletUid',$this->smokeOutletUid,false);
		$criteria->compare('drinkingWaterSourceUid',$this->drinkingWaterSourceUid,false);
		$criteria->compare('recDistanceUid',$this->recDistanceUid,false);
		$criteria->compare('jkDistanceUid',$this->jkDistanceUid,false);
		$criteria->compare('toiletFacilityUid',$this->toiletFacilityUid,false);
		$criteria->compare('physicalThreatInHouseHold',$this->physicalThreatInHouseHold,false);
		$criteria->compare('physicalThreatInNeighbourhood',$this->physicalThreatInNeighbourhood,false);
		$criteria->compare('satisfyWithLivingSpace',$this->satisfyWithLivingSpace,false);
		$criteria->compare('internetServiceAvailable',$this->internetServiceAvailable,false);
		$criteria->compare('residenceStatusUid',$this->residenceStatusUid,false);
		$criteria->compare('confilictInHouseHoldUid',$this->confilictInHouseHoldUid,false);
		$criteria->compare('confilictResolutionUid',$this->confilictResolutionUid,false);
		$criteria->compare('numberOfSeniorsInHome',$this->numberOfSeniorsInHome,false);
		$criteria->compare('numberOfSeniorsInOther',$this->numberOfSeniorsInOther,false);
		$criteria->compare('foodShortageInLastMonth',$this->foodShortageInLastMonth,false);
		$criteria->compare('monthlyFamilyIncomeStatusUid',$this->monthlyFamilyIncomeStatusUid,false);
		$criteria->compare('savingUid',$this->savingUid,false);
		$criteria->compare('deficitUid',$this->deficitUid,false);
		$criteria->compare('savingAvailableUid',$this->savingAvailableUid,false);
		$criteria->compare('outstandingDebitUid',$this->outstandingDebitUid,false);
		$criteria->compare('outstandingDebitInterestRateUid',$this->outstandingDebitInterestRateUid,false);
		$criteria->compare('loanPurposeUid',$this->loanPurposeUid,false);
		$criteria->compare('faJamatiHealth',$this->faJamatiHealth,false);
		$criteria->compare('faJamatiEducation',$this->faJamatiEducation,false);
		$criteria->compare('faJamatiHousing',$this->faJamatiHousing,false);
		$criteria->compare('faJamatiFood',$this->faJamatiFood,false);
		$criteria->compare('faJamatiSeniorCitizen',$this->faJamatiSeniorCitizen,false);
		$criteria->compare('faJamatiSpecialChild',$this->faJamatiSpecialChild,false);
		$criteria->compare('faExternalHealth',$this->faExternalHealth,false);
		$criteria->compare('faExternalEducation',$this->faExternalEducation,false);
		$criteria->compare('faExternalHousing',$this->faExternalHousing,false);
		$criteria->compare('faExternalFood',$this->faExternalFood,false);
		$criteria->compare('faExternalSeniorCitizen',$this->faExternalSeniorCitizen,false);
		$criteria->compare('faExternalSpecialChild',$this->faExternalSpecialChild,false);
		$criteria->compare('hhIsTelvision',$this->hhIsTelvision,false);
		$criteria->compare('hhQtyTelvision',$this->hhQtyTelvision,false);
		$criteria->compare('hhIsDishOrCabel',$this->hhIsDishOrCabel,false);
		$criteria->compare('hhQtyDishOrCabel',$this->hhQtyDishOrCabel,false);
		$criteria->compare('hhIsWashingMachine',$this->hhIsWashingMachine,false);
		$criteria->compare('hhQtyWashingMachine',$this->hhQtyWashingMachine,false);
		$criteria->compare('hhIsElectricFan',$this->hhIsElectricFan,false);
		$criteria->compare('hhQtyElectricFan',$this->hhQtyElectricFan,false);
		$criteria->compare('hhIsAc',$this->hhIsAc,false);
		$criteria->compare('hhQtyAc',$this->hhQtyAc,false);
		$criteria->compare('hhIsRefrigerator',$this->hhIsRefrigerator,false);
		$criteria->compare('hhQtyRefrigerator',$this->hhQtyRefrigerator,false);
		$criteria->compare('hhIsStove',$this->hhIsStove,false);
		$criteria->compare('hhQtyStove',$this->hhQtyStove,false);
		$criteria->compare('hhIsOven',$this->hhIsOven,false);
		$criteria->compare('hhQtyOven',$this->hhQtyOven,false);
		$criteria->compare('hhIsloadshadingBackup',$this->hhIsloadshadingBackup,false);
		$criteria->compare('hhQtyloadshadingBackup',$this->hhQtyloadshadingBackup,false);
		$criteria->compare('hhIsGeyser',$this->hhIsGeyser,false);
		$criteria->compare('hhQtyGeyser',$this->hhQtyGeyser,false);
		$criteria->compare('hhIsHeater',$this->hhIsHeater,false);
		$criteria->compare('hhQtyHeater',$this->hhQtyHeater,false);
		$criteria->compare('hhIsMobilePhone',$this->hhIsMobilePhone,false);
		$criteria->compare('hhQtyMobilePhone',$this->hhQtyMobilePhone,false);
		$criteria->compare('hhIsLivestock',$this->hhIsLivestock,false);
		$criteria->compare('hhQtyLivestock',$this->hhQtyLivestock,false);
		$criteria->compare('hhIsHouse',$this->hhIsHouse,false);
		$criteria->compare('hhQtyHouse',$this->hhQtyHouse,false);
		$criteria->compare('hhIsMotoryCycle',$this->hhIsMotoryCycle,false);
		$criteria->compare('hhQtyMotoryCycle',$this->hhQtyMotoryCycle,false);
		$criteria->compare('hhIsCommercialProperty',$this->hhIsCommercialProperty,false);
		$criteria->compare('hhQtyCommercialProperty',$this->hhQtyCommercialProperty,false);
		$criteria->compare('hhIsComputer',$this->hhIsComputer,false);
		$criteria->compare('hhQtyComputer',$this->hhQtyComputer,false);
		$criteria->compare('hhIsFruitTrees',$this->hhIsFruitTrees,false);
		$criteria->compare('hhQtyFruitTrees',$this->hhQtyFruitTrees,false);
		$criteria->compare('hhIsAgricultureAsset',$this->hhIsAgricultureAsset,false);
		$criteria->compare('hhQtyAgricultureAsset',$this->hhQtyAgricultureAsset,false);
		$criteria->compare('hhIsCar',$this->hhIsCar,false);
		$criteria->compare('hhQtyCar',$this->hhQtyCar,false);
		$criteria->compare('hhIsTruckBusVan',$this->hhIsTruckBusVan,false);
		$criteria->compare('hhQtyTruckBusVan',$this->hhQtyTruckBusVan,false);
		$criteria->compare('hhIsTractor',$this->hhIsTractor,false);
		$criteria->compare('hhQtyTractor',$this->hhQtyTractor,false);
		$criteria->compare('hhIsAgricultureLand',$this->hhIsAgricultureLand,false);
		$criteria->compare('hhQtyAgricultureLand',$this->hhQtyAgricultureLand,false);
		$criteria->compare('hhQtyAgricultureLandUnitUid',$this->hhQtyAgricultureLandUnitUid,false);
		$criteria->compare('hhIsNonAgricultureLand',$this->hhIsNonAgricultureLand,false);
		$criteria->compare('hhQtyNonAgricultureLand',$this->hhQtyNonAgricultureLand,false);
		$criteria->compare('hhQtyNonAgricultureLandUnitUid',$this->hhQtyNonAgricultureLandUnitUid,false);
		$criteria->compare('comments',$this->comments,false);
		$criteria->compare('filledBy',$this->filledBy,false);
		$criteria->compare('reviewedBy',$this->reviewedBy,false);
		$criteria->compare('filledDate',$this->filledDate,false);
		$criteria->compare('formFilledMinutes',$this->formFilledMinutes,false);
		$criteria->compare('surveyorEmail',$this->surveyorEmail,false);
		$criteria->compare('surveyorContactNumber',$this->surveyorContactNumber,false);
		$criteria->compare('isEducationInstituteAffordable',$this->isEducationInstituteAffordable,false);
		$criteria->compare('educationInstituteIsAffordable',$this->educationInstituteIsAffordable,false);
		$criteria->compare('educationInstituteIsAccessible',$this->educationInstituteIsAccessible,false);
		$criteria->compare('educationInstituteIsQuality',$this->educationInstituteIsQuality,false);
		$criteria->compare('educationInstituteDistanceUid',$this->educationInstituteDistanceUid,false);
		$criteria->compare('primaryHealthServiceIsAffordable',$this->primaryHealthServiceIsAffordable,false);
		$criteria->compare('primaryHealthServiceIsAccessible',$this->primaryHealthServiceIsAccessible,false);
		$criteria->compare('primaryHealthServiceIsQuality',$this->primaryHealthServiceIsQuality,false);
		$criteria->compare('primaryHealthServiceDistanceUid',$this->primaryHealthServiceDistanceUid,false);
		$criteria->compare('secondaryHealthServiceIsAffordable',$this->secondaryHealthServiceIsAffordable,false);
		$criteria->compare('secondaryHealthServiceIsAccessible',$this->secondaryHealthServiceIsAccessible,false);
		$criteria->compare('secondaryHealthServiceIsQuality',$this->secondaryHealthServiceIsQuality,false);
		$criteria->compare('secondaryHealthServiceDistanceUid',$this->secondaryHealthServiceDistanceUid,false);
		$criteria->compare('drinkingWaterIsAffordable',$this->drinkingWaterIsAffordable,false);
		$criteria->compare('drinkingWaterIsAccessible',$this->drinkingWaterIsAccessible,false);
		$criteria->compare('drinkingWaterIsQuality',$this->drinkingWaterIsQuality,false);
		$criteria->compare('drinkingWaterDistanceUid',$this->drinkingWaterDistanceUid,false);
		$criteria->compare('marketIsAffordable',$this->marketIsAffordable,false);
		$criteria->compare('marketIsAccessible',$this->marketIsAccessible,false);
		$criteria->compare('marketIsQuality',$this->marketIsQuality,false);
		$criteria->compare('marketDistanceUid',$this->marketDistanceUid,false);
		$criteria->compare('parkIsAffordable',$this->parkIsAffordable,false);
		$criteria->compare('parkIsAccessible',$this->parkIsAccessible,false);
		$criteria->compare('parkIsQuality',$this->parkIsQuality,false);
		$criteria->compare('parkDistanceUid',$this->parkDistanceUid,false);
		$criteria->compare('electricityIsAffordable',$this->electricityIsAffordable,false);
		$criteria->compare('electricityIsAccessible',$this->electricityIsAccessible,false);
		$criteria->compare('electricityIsQuality',$this->electricityIsQuality,false);
		$criteria->compare('electricityDistanceUid',$this->electricityDistanceUid,false);
		$criteria->compare('gasIsAffordable',$this->gasIsAffordable,false);
		$criteria->compare('gasIsAccessible',$this->gasIsAccessible,false);
		$criteria->compare('gasIsQuality',$this->gasIsQuality,false);
		$criteria->compare('gasDistanceUid',$this->gasDistanceUid,false);
		$criteria->compare('libraryIsAffordable',$this->libraryIsAffordable,false);
		$criteria->compare('libraryIsAccessible',$this->libraryIsAccessible,false);
		$criteria->compare('libraryIsQuality',$this->libraryIsQuality,false);
		$criteria->compare('libraryDistanceUid',$this->libraryDistanceUid,false);
		$criteria->compare('cinemaIsAffordable',$this->cinemaIsAffordable,false);
		$criteria->compare('cinemaIsAccessible',$this->cinemaIsAccessible,false);
		$criteria->compare('cinemaIsQuality',$this->cinemaIsQuality,false);
		$criteria->compare('cinemaDistanceUid',$this->cinemaDistanceUid,false);
		$criteria->compare('transportationIsAffordable',$this->transportationIsAffordable,false);
		$criteria->compare('transportationIsAccessible',$this->transportationIsAccessible,false);
		$criteria->compare('transportationIsQuality',$this->transportationIsQuality,false);
		$criteria->compare('transportationDistanceUid',$this->transportationDistanceUid,false);
		$criteria->compare('telephoneIsAffordable',$this->telephoneIsAffordable,false);
		$criteria->compare('telephoneIsAccessible',$this->telephoneIsAccessible,false);
		$criteria->compare('telephoneIsQuality',$this->telephoneIsQuality,false);
		$criteria->compare('telephoneDistanceUid',$this->telephoneDistanceUid,false);
		$criteria->compare('isRoadsafty',$this->isRoadsafty,false);
		$criteria->compare('isUnsafeHouse',$this->isUnsafeHouse,false);
		$criteria->compare('isNaturalDisaster',$this->isNaturalDisaster,false);
		$criteria->compare('isLawOrder',$this->isLawOrder,false);
		$criteria->compare('isInappropriatePolitical',$this->isInappropriatePolitical,false);
		$criteria->compare('isChildAbuse',$this->isChildAbuse,false);
		$criteria->compare('isReligiousIntolerance',$this->isReligiousIntolerance,false);
		$criteria->compare('isLandGrabbing',$this->isLandGrabbing,false);
		$criteria->compare('isRansom',$this->isRansom,false);
		$criteria->compare('isRiskBusinessWindup',$this->isRiskBusinessWindup,false);
		$criteria->compare('formLastUpdatedByUser',$this->formLastUpdatedByUser,false);
		$criteria->compare('formAddedByUser',$this->formAddedByUser,false);
		$criteria->compare('formPostStatus',$this->formPostStatus,false);
		$criteria->compare('formFillStatus',$this->formFillStatus,false);
		$criteria->compare('formNum',$this->formNum,false);
		$criteria->compare('createDate',$this->createDate,false);
		$criteria->compare('lastUpdateDate',$this->lastUpdateDate,false);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return Household the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function afterFind(){
		parent::afterFind();
		$this->createDate =  date('d-m-Y', CDateTimeParser::parse($this->createDate, 'yyyy-MM-dd')); 
		$this->lastUpdateDate =  date('d-m-Y', CDateTimeParser::parse($this->lastUpdateDate, 'yyyy-MM-dd')); 
	}
	
	protected function beforeSave(){
		if(parent::beforeSave())
		{
			
			foreach ($this->attributes as $key => $value)
            {
				if ($value == '')
				{
					$this->$key = NULL;
				}
			}
			
			$temp = date('d-m-Y', CDateTimeParser::parse($this->createDate, 'dd-MM-yyyy'));
			if($this->createDate == $temp)
				$this->createDate = date('Y-m-d', CDateTimeParser::parse($this->createDate, 'dd-MM-yyyy'));
			
			$temp = date('d-m-Y', CDateTimeParser::parse($this->lastUpdateDate, 'dd-MM-yyyy'));
			if($this->lastUpdateDate == $temp)
				$this->lastUpdateDate = date('Y-m-d', CDateTimeParser::parse($this->lastUpdateDate, 'dd-MM-yyyy'));
			
			return TRUE;
		}
		else 
			return FALSE;
	}
}
