<?php

/**
 * This is the model class for table "familymember".
 *
 * The followings are the available columns in table 'familymember':
 * @property string $uid
 * @property string $householdUid
 * @property string $firstName
 * @property string $relationshipWithHeadUid
 * @property integer $martialStatusUid
 * @property string $genderUid
 * @property string $birthYear
 * @property string $recEducationStatusUid
 * @property string $recCurrentGradeUid
 * @property string $recLastGradeUid
 * @property string $recEducationStatusReasonUid
 * @property string $isBenefitingJamatiInstitution
 * @property string $isBenefitingItreb
 * @property string $eveningJkAttendenceUid
 * @property string $eveningJkNotAttendenceReasonUid
 * @property string $morningJkAttendenceUid
 * @property string $morningJkNotAttendenceReasonUid
 * @property string $secularEducationStatusUid
 * @property string $secularCurrentGradeUid
 * @property string $secularEducationIsSatisfy
 * @property string $secularLastGradeUid
 * @property string $secularNotEnrolledReasonUid
 * @property string $secularDropoutYear
 * @property string $secularUpto10InstitueCategoryUid
 * @property string $secularAbove10InstitueCategoryUid
 * @property string $secularEducationGroupUid
 * @property string $englishProficiencyReadingUid
 * @property string $englishProficiencyWritingUid
 * @property string $englishProficiencySpeakingUid
 * @property string $itProficiencyOfficeSuitUid
 * @property string $itProficiencyInternetUid
 * @property string $occupationUid
 * @property string $monthlyIncomeUid
 * @property string $unemployedSinceUid
 * @property string $unemployedReasonUid
 * @property string $insuredSelfHealth
 * @property string $insuredSelfLife
 * @property string $insuredSelfAsset
 * @property string $insuredSelfVehicle
 * @property string $insuredSelfBusiness
 * @property string $insuredSelfEducation
 * @property string $insuredEmployerLife
 * @property string $insuredEmployerHealth
 * @property string $healthIssueStatusDiabetesUid
 * @property string $healthIssueArthritisUid
 * @property string $healthIssueStatusCancerUid
 * @property string $healthIssueStatusBpUid
 * @property string $healthIssueStatusHeartUid
 * @property string $healthIssueStatusMentalIllnessUid
 * @property string $healthIssueStatusTuberculosisUid
 * @property string $healthIssueStatusKidneyUid
 * @property string $healthIssueStatusStrokeUid
 * @property string $healthIssueStatusHepatitis
 * @property string $healthIssueStatusAsthmaUid
 * @property string $healthIssueStatusOthersUid
 * @property string $healthIssueDisabilityUid
 * @property string $healthIssueIllFrequencyUid
 * @property string $healthIssueNoHealthFacilityReasonUid
 * @property string $nutritionConsumptionFrequencyMeatUid
 * @property string $nutritionConsumptionFrequencyFruitUid
 * @property string $nutritionConsumptionFrequencyMilkUid
 * @property string $substanceAbuseAlcoholUid
 * @property string $substanceAbuseCigrateUid
 * @property string $substanceAbuseTobaccoUid
 * @property string $substanceAbuseIllicitDrugUid
 * @property string $substanceAbusePanUid
 * @property string $socialKhidmatUid
 * @property string $socialSportsHolidayUid
 * @property string $socialSportsWorkingDayUid
 * @property string $socialExcerciseUid
 * @property string $socialRecreationActivityUid
 * @property string $takSelfDecisionMaking
 * @property string $leisureTimeUid
 * @property string $qolUid
 * @property string $qolChangePast3YearsUid
 * @property string $qolChangeStatusUid
 */
class FamilyMember extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'familymember';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('firstName, relationshipWithHeadUid, martialStatusUid, genderUid, birthYear, recEducationStatusUid, recCurrentGradeUid, recLastGradeUid, recEducationStatusReasonUid, secularCurrentGradeUid, secularLastGradeUid, secularNotEnrolledReasonUid, secularDropoutYear, secularUpto10InstitueCategoryUid, secularAbove10InstitueCategoryUid, secularEducationStatusUid, secularEducationGroupUid, englishProficiencyReadingUid, englishProficiencyWritingUid, englishProficiencySpeakingUid, itProficiencyOfficeSuitUid, itProficiencyInternetUid, occupationUid, monthlyIncomeUid, unemployedSinceUid, unemployedReasonUid, healthIssueStatusDiabetesUid, healthIssueArthritisUid, healthIssueStatusCancerUid, healthIssueStatusBpUid, healthIssueStatusHeartUid, healthIssueStatusMentalIllnessUid, healthIssueStatusTuberculosisUid, healthIssueStatusKidneyUid, healthIssueStatusStrokeUid, healthIssueStatusHepatitis, healthIssueStatusAsthmaUid, healthIssueStatusOthersUid, healthIssueDisabilityUid, healthIssueIllFrequencyUid, healthIssueNoHealthFacilityReasonUid, nutritionConsumptionFrequencyMeatUid, nutritionConsumptionFrequencyFruitUid, nutritionConsumptionFrequencyMilkUid, substanceAbuseAlcoholUid, substanceAbuseTobaccoUid, substanceAbuseCigrateUid, substanceAbuseIllicitDrugUid, substanceAbusePanUid, socialKhidmatUid, socialSportsHolidayUid, socialSportsWorkingDayUid, socialExcerciseUid, socialRecreationActivityUid,isBenefitingJamatiInstitution, isBenefitingItreb, eveningJkAttendenceUid, eveningJkNotAttendenceReasonUid, morningJkAttendenceUid, morningJkNotAttendenceReasonUid, secularEducationStatusUid, secularEducationIsSatisfy, insuredSelfHealth, insuredSelfLife, insuredSelfAsset, insuredSelfVehicle, insuredSelfBusiness, insuredSelfEducation, insuredEmployerLife, insuredEmployerHealth, takSelfDecisionMaking, leisureTimeUid, qolUid, qolChangePast3YearsUid, qolChangeStatusUid', 'required'),
			array('relationshipWithHeadUid, martialStatusUid, genderUid', 'numerical', 'integerOnly'=>true),
			array('uid,genderUid, recEducationStatusUid, recCurrentGradeUid, recLastGradeUid, recEducationStatusReasonUid, secularCurrentGradeUid, secularLastGradeUid, secularNotEnrolledReasonUid, secularUpto10InstitueCategoryUid, secularAbove10InstitueCategoryUid, secularEducationGroupUid, englishProficiencyReadingUid, englishProficiencyWritingUid, englishProficiencySpeakingUid, itProficiencyOfficeSuitUid, itProficiencyInternetUid, occupationUid, monthlyIncomeUid, unemployedSinceUid, unemployedReasonUid, healthIssueStatusDiabetesUid, healthIssueArthritisUid, healthIssueStatusCancerUid, healthIssueStatusBpUid, healthIssueStatusHeartUid, healthIssueStatusMentalIllnessUid, healthIssueStatusTuberculosisUid, healthIssueStatusKidneyUid, healthIssueStatusStrokeUid, healthIssueStatusHepatitis, healthIssueStatusAsthmaUid, healthIssueStatusOthersUid, healthIssueDisabilityUid, healthIssueIllFrequencyUid, healthIssueNoHealthFacilityReasonUid, nutritionConsumptionFrequencyMeatUid, nutritionConsumptionFrequencyFruitUid, nutritionConsumptionFrequencyMilkUid, substanceAbuseAlcoholUid, substanceAbuseTobaccoUid, substanceAbuseCigrateUid, substanceAbuseIllicitDrugUid, substanceAbusePanUid, socialKhidmatUid, socialSportsHolidayUid, socialSportsWorkingDayUid, socialExcerciseUid, socialRecreationActivityUid', 'length', 'max'=>11),
			array('firstName', 'length', 'max'=>255),
			array('birthYear, secularDropoutYear', 'length', 'max'=>5),
			array('isBenefitingJamatiInstitution, isBenefitingItreb, secularEducationIsSatisfy, insuredSelfHealth, insuredSelfLife, insuredSelfAsset, insuredSelfVehicle, insuredSelfBusiness, insuredSelfEducation, insuredEmployerLife, insuredEmployerHealth, takSelfDecisionMaking', 'length', 'max'=>1),
			array('eveningJkAttendenceUid, eveningJkNotAttendenceReasonUid, morningJkAttendenceUid, morningJkNotAttendenceReasonUid, secularEducationStatusUid, leisureTimeUid, qolUid, qolChangePast3YearsUid, qolChangeStatusUid', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, firstName, relationshipWithHeadUid, martialStatusUid, genderUid, birthYear, recEducationStatusUid, recCurrentGradeUid, recLastGradeUid, recEducationStatusReasonUid, isBenefitingJamatiInstitution, isBenefitingItreb, eveningJkAttendenceUid, eveningJkNotAttendenceReasonUid, morningJkAttendenceUid, morningJkNotAttendenceReasonUid, secularEducationStatusUid, secularCurrentGradeUid, secularEducationIsSatisfy, secularLastGradeUid, secularNotEnrolledReasonUid, secularDropoutYear, secularUpto10InstitueCategoryUid, secularAbove10InstitueCategoryUid, secularEducationGroupUid, englishProficiencyReadingUid, englishProficiencyWritingUid, englishProficiencySpeakingUid, itProficiencyOfficeSuitUid, itProficiencyInternetUid, occupationUid, monthlyIncomeUid, unemployedSinceUid, unemployedReasonUid, insuredSelfHealth, insuredSelfLife, insuredSelfAsset, insuredSelfVehicle, insuredSelfBusiness, insuredSelfEducation, insuredEmployerLife, insuredEmployerHealth, healthIssueStatusDiabetesUid, healthIssueArthritisUid, healthIssueStatusCancerUid, healthIssueStatusBpUid, healthIssueStatusHeartUid, healthIssueStatusMentalIllnessUid, healthIssueStatusTuberculosisUid, healthIssueStatusKidneyUid, healthIssueStatusStrokeUid, healthIssueStatusHepatitis, healthIssueStatusAsthmaUid, healthIssueStatusOthersUid, healthIssueDisabilityUid, healthIssueIllFrequencyUid, healthIssueNoHealthFacilityReasonUid, nutritionConsumptionFrequencyMeatUid, nutritionConsumptionFrequencyFruitUid, nutritionConsumptionFrequencyMilkUid, substanceAbuseAlcoholUid, substanceAbuseTobaccoUid, substanceAbuseCigrateUid, substanceAbuseIllicitDrugUid, substanceAbusePanUid, socialKhidmatUid, socialSportsHolidayUid, socialSportsWorkingDayUid, socialExcerciseUid, socialRecreationActivityUid, takSelfDecisionMaking, leisureTimeUid, qolUid, qolChangePast3YearsUid, qolChangeStatusUid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'householdUid' => 'Household',
			'firstName' => 'First Name',
			'relationshipWithHeadUid' => 'Relationship With Head',
			'martialStatusUid' => 'Martial Status',
			'genderUid' => 'Gender',
			'birthYear' => 'Birth Year',
			'recEducationStatusUid' => 'Rec Education Status',
			'recCurrentGradeUid' => 'Rec Current Grade',
			'recLastGradeUid' => 'Rec Last Grade',
			'recEducationStatusReasonUid' => 'Rec Education Status Reason',
			'isBenefitingJamatiInstitution' => 'Is Benefiting Jamati Institution',
			'isBenefitingItreb' => 'Is Benefiting Itreb',
			'eveningJkAttendenceUid' => 'Evening Jk Attendence',
			'eveningJkNotAttendenceReasonUid' => 'Evening Jk Not Attendence Reason',
			'morningJkAttendenceUid' => 'Morning Jk Attendence',
			'morningJkNotAttendenceReasonUid' => 'Morning Jk Not Attendence Reason',
			'secularEducationStatusUid' => 'Secular Education Status',
			'secularCurrentGradeUid' => 'Secular Current Grade',
			'secularEducationIsSatisfy' => 'Secular Education Is Satisfy',
			'secularLastGradeUid' => 'Secular Last Grade',
			'secularNotEnrolledReasonUid' => 'Secular Not Enrolled Reason',
			'secularDropoutYear' => 'Secular Dropout Year',
			'secularUpto10InstitueCategoryUid' => 'Secular Upto10 Institue Category',
			'secularAbove10InstitueCategoryUid' => 'Secular Above10 Institue Category',
			'secularEducationGroupUid' => 'Secular Education Group',
			'englishProficiencyReadingUid' => 'English Proficiency Reading',
			'englishProficiencyWritingUid' => 'English Proficiency Writing',
			'englishProficiencySpeakingUid' => 'English Proficiency Speaking',
			'itProficiencyOfficeSuitUid' => 'It Proficiency Office Suit',
			'itProficiencyInternetUid' => 'It Proficiency Internet',
			'occupationUid' => 'Occupation',
			'monthlyIncomeUid' => 'Monthly Income',
			'unemployedSinceUid' => 'Unemployed Since',
			'unemployedReasonUid' => 'Unemployed Reason',
			'insuredSelfHealth' => 'Insured Self Health',
			'insuredSelfLife' => 'Insured Self Life',
			'insuredSelfAsset' => 'Insured Self Asset',
			'insuredSelfVehicle' => 'Insured Self Vehicle',
			'insuredSelfBusiness' => 'Insured Self Business',
			'insuredSelfEducation' => 'Insured Self Education',
			'insuredEmployerLife' => 'Insured Employer Life',
			'insuredEmployerHealth' => 'Insured Employer Health',
			'healthIssueStatusDiabetesUid' => 'Diabetes',
			'healthIssueArthritisUid' => ' Arthritis (Joint pain)',
			'healthIssueStatusCancerUid' => 'Cancer',
			'healthIssueStatusBpUid' => 'High Boold Pressure',
			'healthIssueStatusHeartUid' => 'Heart',
			'healthIssueStatusMentalIllnessUid' => 'Mental Illness (depression etc.)',
			'healthIssueStatusTuberculosisUid' => 'Tuberculosis',
			'healthIssueStatusKidneyUid' => 'Kidney',
			'healthIssueStatusStrokeUid' => 'Stroke',
			'healthIssueStatusHepatitis' => 'Hepatitis',
			'healthIssueStatusAsthmaUid' => 'Asthma',
			'healthIssueStatusOthersUid' => 'Others',
			'healthIssueDisabilityUid' => 'Do you have any disability?',
			'healthIssueIllFrequencyUid' => 'How frequently do you fall ill / unhealthy',
			'healthIssueNoHealthFacilityReasonUid' => 'Do you visit any health facilities during illness?',
			'nutritionConsumptionFrequencyMeatUid' => 'Red Meat or Chicken',
			'nutritionConsumptionFrequencyFruitUid' => 'Fruits',
			'nutritionConsumptionFrequencyMilkUid' => 'Milk / Milk products',
			'substanceAbuseAlcoholUid' => 'Alcohol / Mo?',
			'substanceAbuseTobaccoUid' => 'Chewing tobacco/Gutka / Naswar?',
			'substanceAbuseCigrateUid' => 'Cigarettes / Shisha?',
			'substanceAbuseIllicitDrugUid' => 'Illicit Drugs',
			'substanceAbusePanUid' => 'Pan',
			'socialKhidmatUid' => 'Social Services / Khidmat',
			'socialSportsHolidayUid' => 'Sports (During Holidays)',
			'socialSportsWorkingDayUid' => 'Social Sports Working Day',
			'socialExcerciseUid' => 'Physical exercise',
			'socialRecreationActivityUid' => 'Recreational activities',
			'takSelfDecisionMaking' => 'Do you feel empowered to make your own decisions?',
			'leisureTimeUid' => 'Leisure Time',
			'qolUid' => 'Qol',
			'qolChangePast3YearsUid' => 'Qol Change Past 3 Years',
			'qolChangeStatusUid' => 'Qol Change Status',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('firstName',$this->firstName,true);
		$criteria->compare('relationshipWithHeadUid',$this->relationshipWithHeadUid,true);
		$criteria->compare('martialStatusUid',$this->martialStatusUid);
		$criteria->compare('genderUid',$this->genderUid,true);
		$criteria->compare('birthYear',$this->birthYear,true);
		$criteria->compare('recEducationStatusUid',$this->recEducationStatusUid,true);
		$criteria->compare('recCurrentGradeUid',$this->recCurrentGradeUid,true);
		$criteria->compare('recLastGradeUid',$this->recLastGradeUid,true);
		$criteria->compare('recEducationStatusReasonUid',$this->recEducationStatusReasonUid,true);
		$criteria->compare('isBenefitingJamatiInstitution',$this->isBenefitingJamatiInstitution,true);
		$criteria->compare('isBenefitingItreb',$this->isBenefitingItreb,true);
		$criteria->compare('eveningJkAttendenceUid',$this->eveningJkAttendenceUid,true);
		$criteria->compare('eveningJkNotAttendenceReasonUid',$this->eveningJkNotAttendenceReasonUid,true);
		$criteria->compare('morningJkAttendenceUid',$this->morningJkAttendenceUid,true);
		$criteria->compare('morningJkNotAttendenceReasonUid',$this->morningJkNotAttendenceReasonUid,true);
		$criteria->compare('secularEducationStatusUid',$this->secularEducationStatusUid,true);
		$criteria->compare('secularCurrentGradeUid',$this->secularCurrentGradeUid,true);
		$criteria->compare('secularEducationIsSatisfy',$this->secularEducationIsSatisfy,true);
		$criteria->compare('secularLastGradeUid',$this->secularLastGradeUid,true);
		$criteria->compare('secularNotEnrolledReasonUid',$this->secularNotEnrolledReasonUid,true);
		$criteria->compare('secularDropoutYear',$this->secularDropoutYear,true);
		$criteria->compare('secularUpto10InstitueCategoryUid',$this->secularUpto10InstitueCategoryUid,true);
		$criteria->compare('secularAbove10InstitueCategoryUid',$this->secularAbove10InstitueCategoryUid,true);
		$criteria->compare('secularEducationGroupUid',$this->secularEducationGroupUid,true);
		$criteria->compare('englishProficiencyReadingUid',$this->englishProficiencyReadingUid,true);
		$criteria->compare('englishProficiencyWritingUid',$this->englishProficiencyWritingUid,true);
		$criteria->compare('englishProficiencySpeakingUid',$this->englishProficiencySpeakingUid,true);
		$criteria->compare('itProficiencyOfficeSuitUid',$this->itProficiencyOfficeSuitUid,true);
		$criteria->compare('itProficiencyInternetUid',$this->itProficiencyInternetUid,true);
		$criteria->compare('occupationUid',$this->occupationUid,true);
		$criteria->compare('monthlyIncomeUid',$this->monthlyIncomeUid,true);
		$criteria->compare('unemployedSinceUid',$this->unemployedSinceUid,true);
		$criteria->compare('unemployedReasonUid',$this->unemployedReasonUid,true);
		$criteria->compare('insuredSelfHealth',$this->insuredSelfHealth,true);
		$criteria->compare('insuredSelfLife',$this->insuredSelfLife,true);
		$criteria->compare('insuredSelfAsset',$this->insuredSelfAsset,true);
		$criteria->compare('insuredSelfVehicle',$this->insuredSelfVehicle,true);
		$criteria->compare('insuredSelfBusiness',$this->insuredSelfBusiness,true);
		$criteria->compare('insuredSelfEducation',$this->insuredSelfEducation,true);
		$criteria->compare('insuredEmployerLife',$this->insuredEmployerLife,true);
		$criteria->compare('insuredEmployerHealth',$this->insuredEmployerHealth,true);
		$criteria->compare('healthIssueStatusDiabetesUid',$this->healthIssueStatusDiabetesUid,true);
		$criteria->compare('healthIssueArthritisUid',$this->healthIssueArthritisUid,true);
		$criteria->compare('healthIssueStatusCancerUid',$this->healthIssueStatusCancerUid,true);
		$criteria->compare('healthIssueStatusBpUid',$this->healthIssueStatusBpUid,true);
		$criteria->compare('healthIssueStatusHeartUid',$this->healthIssueStatusHeartUid,true);
		$criteria->compare('healthIssueStatusMentalIllnessUid',$this->healthIssueStatusMentalIllnessUid,true);
		$criteria->compare('healthIssueStatusTuberculosisUid',$this->healthIssueStatusTuberculosisUid,true);
		$criteria->compare('healthIssueStatusKidneyUid',$this->healthIssueStatusKidneyUid,true);
		$criteria->compare('healthIssueStatusStrokeUid',$this->healthIssueStatusStrokeUid,true);
		$criteria->compare('healthIssueStatusHepatitis',$this->healthIssueStatusHepatitis,true);
		$criteria->compare('healthIssueStatusAsthmaUid',$this->healthIssueStatusAsthmaUid,true);
		$criteria->compare('healthIssueStatusOthersUid',$this->healthIssueStatusOthersUid,true);
		$criteria->compare('healthIssueDisabilityUid',$this->healthIssueDisabilityUid,true);
		$criteria->compare('healthIssueIllFrequencyUid',$this->healthIssueIllFrequencyUid,true);
		$criteria->compare('healthIssueNoHealthFacilityReasonUid',$this->healthIssueNoHealthFacilityReasonUid,true);
		$criteria->compare('nutritionConsumptionFrequencyMeatUid',$this->nutritionConsumptionFrequencyMeatUid,true);
		$criteria->compare('nutritionConsumptionFrequencyFruitUid',$this->nutritionConsumptionFrequencyFruitUid,true);
		$criteria->compare('nutritionConsumptionFrequencyMilkUid',$this->nutritionConsumptionFrequencyMilkUid,true);
		$criteria->compare('substanceAbuseAlcoholUid',$this->substanceAbuseAlcoholUid,true);
		$criteria->compare('substanceAbuseTobaccoUid',$this->substanceAbuseTobaccoUid,true);
		$criteria->compare('substanceAbuseCigrateUid',$this->substanceAbuseCigrateUid,true);
		$criteria->compare('substanceAbuseIllicitDrugUid',$this->substanceAbuseIllicitDrugUid,true);
		$criteria->compare('substanceAbusePanUid',$this->substanceAbusePanUid,true);
		$criteria->compare('socialKhidmatUid',$this->socialKhidmatUid,true);
		$criteria->compare('socialSportsHolidayUid',$this->socialSportsHolidayUid,true);
		$criteria->compare('socialSportsWorkingDayUid',$this->socialSportsWorkingDayUid,true);
		$criteria->compare('socialExcerciseUid',$this->socialExcerciseUid,true);
		$criteria->compare('socialRecreationActivityUid',$this->socialRecreationActivityUid,true);
		$criteria->compare('takSelfDecisionMaking',$this->takSelfDecisionMaking,true);
		$criteria->compare('leisureTimeUid',$this->leisureTimeUid,true);
		$criteria->compare('qolUid',$this->qolUid,true);
		$criteria->compare('qolChangePast3YearsUid',$this->qolChangePast3YearsUid,true);
		$criteria->compare('qolChangeStatusUid',$this->qolChangeStatusUid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamilyMember the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	function empty2null($value) {
		return $value === '' ? NULL : $value;
	}
	
	protected function beforeSave(){
		if(parent::beforeSave())
		{
			
			foreach ($this->attributes as $key => $value)
            {
				if ($value == '')
				{
					$this->$key = NULL;
				}
			}
			return TRUE;
		}
		else 
			return FALSE;
	}
}
