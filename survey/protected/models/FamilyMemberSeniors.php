<?php

/**
 * This is the model class for table "familymemberseniors".
 *
 * The followings are the available columns in table 'familymemberseniors':
 * @property string $uid
 * @property string $householdUid
 * @property string $livingWithUid
 * @property string $livingOtherUid
 * @property string $involveSocialOrEconomicActivities
 * @property string $notInvolveSocialOrEconomicActivitiesUid
 *
 * The followings are the available model relations:
 * @property N37seniorsinvolmentreason $notInvolveSocialOrEconomicActivitiesU
 * @property Household $householdU
 * @property N36seniorsliving $livingOtherU
 */
class FamilyMemberSeniors extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'familymemberseniors';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('livingWithUid, livingOtherUid, involveSocialOrEconomicActivities, notInvolveSocialOrEconomicActivitiesUid', 'required'),
			array('householdUid, livingWithUid, livingOtherUid, involveSocialOrEconomicActivities, notInvolveSocialOrEconomicActivitiesUid', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, householdUid, livingWithUid, livingOtherUid, involveSocialOrEconomicActivities, notInvolveSocialOrEconomicActivitiesUid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'notInvolveSocialOrEconomicActivitiesU' => array(self::BELONGS_TO, 'N37seniorsinvolmentreason', 'notInvolveSocialOrEconomicActivitiesUid'),
			'householdU' => array(self::BELONGS_TO, 'Household', 'householdUid'),
			'livingOtherU' => array(self::BELONGS_TO, 'N36seniorsliving', 'livingOtherUid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'householdUid' => 'Household',
			'livingWithUid' => 'Living with in Home/Others',
			'livingOtherUid' => 'Living Other',
			'involveSocialOrEconomicActivities' => 'Involve Social Or Economic Activities',
			'notInvolveSocialOrEconomicActivitiesUid' => 'Not Involve Social Or Economic Activities',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('householdUid',$this->householdUid,true);
		$criteria->compare('livingOtherUid', $this->livingOtherUid,true);
		$criteria->compare('involveSocialOrEconomicActivities',$this->involveSocialOrEconomicActivities,true);
		$criteria->compare('notInvolveSocialOrEconomicActivitiesUid',$this->notInvolveSocialOrEconomicActivitiesUid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamilyMemberSeniors the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function beforeSave(){
		if(parent::beforeSave())
		{
			
			foreach ($this->attributes as $key => $value)
            {
				if ($value == '')
				{
					$this->$key = NULL;
				}
			}
			return TRUE;
		}
		else 
			return FALSE;
	}
}
