<?php

/**
 * This is the model class for table "familymembermaternalhealth".
 *
 * The followings are the available columns in table 'familymembermaternalhealth':
 * @property string $uid
 * @property string $householdUid
 * @property string $familyMemberMotherUid
 * @property string $familyMemberChildUid
 * @property string $deliveryYear
 * @property string $deliveryCarriedByUid
 * @property string $duringDeliveryChildAlive
 * @property integer $duringDeliveryMotherAlive
 * @property string $nowChildAlive
 * @property string $nowMotherAlive
 * @property string $miscarriaged
 * @property string $motherDiedAge
 * @property string $childDiedAge
 * @property string $childGenderUid
 *
 * The followings are the available model relations:
 * @property Familymember $familyMemberChildU
 * @property N45deliverycarriedby $deliveryCarriedByU
 * @property Gender $childGenderU
 * @property Familymember $familyMemberMotherU
 */
class FamilyMemberMaternalHealth extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'familymembermaternalhealth';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('householdUid, deliveryYear, deliveryCarriedByUid, duringDeliveryChildAlive, duringDeliveryMotherAlive, nowChildAlive, nowMotherAlive, miscarriaged,  childGenderUid', 'required'),
			array('duringDeliveryMotherAlive', 'numerical', 'integerOnly'=>true),
			array('uid, householdUid, familyMemberMotherUid, familyMemberChildUid, deliveryCarriedByUid, childGenderUid', 'length', 'max'=>11),
			array('deliveryYear', 'length', 'max'=>4),
			array('duringDeliveryChildAlive, nowChildAlive, nowMotherAlive, miscarriaged', 'length', 'max'=>1),
			array('motherDiedAge, childDiedAge', 'length', 'max'=>5),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, householdUid, familyMemberMotherUid, familyMemberChildUid, deliveryYear, deliveryCarriedByUid, duringDeliveryChildAlive, duringDeliveryMotherAlive, nowChildAlive, nowMotherAlive, miscarriaged, motherDiedAge, childDiedAge, childGenderUid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'familyMemberChildU' => array(self::BELONGS_TO, 'Familymember', 'familyMemberChildUid'),
			'deliveryCarriedByU' => array(self::BELONGS_TO, 'N45deliverycarriedby', 'deliveryCarriedByUid'),
			'childGenderU' => array(self::BELONGS_TO, 'Gender', 'childGenderUid'),
			'familyMemberMotherU' => array(self::BELONGS_TO, 'Familymember', 'familyMemberMotherUid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'householdUid' => 'Household Uid',
			'familyMemberMotherUid' => 'Family Member Mother Uid',
			'familyMemberChildUid' => 'Family Member Child Uid',
			'deliveryYear' => 'Delivery Year',
			'deliveryCarriedByUid' => 'Delivery Carried By Uid',
			'duringDeliveryChildAlive' => 'During Delivery Child Alive',
			'duringDeliveryMotherAlive' => 'During Delivery Mother Alive',
			'nowChildAlive' => 'Now Child Alive',
			'nowMotherAlive' => 'Now Mother Alive',
			'miscarriaged' => 'Miscarriaged',
			'motherDiedAge' => 'Mother Died Age',
			'childDiedAge' => 'Child Died Age',
			'childGenderUid' => 'Child Gender Uid',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('householdUid',$this->householdUid,true);
		$criteria->compare('familyMemberMotherUid',$this->familyMemberMotherUid,true);
		$criteria->compare('familyMemberChildUid',$this->familyMemberChildUid,true);
		$criteria->compare('deliveryYear',$this->deliveryYear,true);
		$criteria->compare('deliveryCarriedByUid',$this->deliveryCarriedByUid,true);
		$criteria->compare('duringDeliveryChildAlive',$this->duringDeliveryChildAlive,true);
		$criteria->compare('duringDeliveryMotherAlive',$this->duringDeliveryMotherAlive);
		$criteria->compare('nowChildAlive',$this->nowChildAlive,true);
		$criteria->compare('nowMotherAlive',$this->nowMotherAlive,true);
		$criteria->compare('miscarriaged',$this->miscarriaged,true);
		$criteria->compare('motherDiedAge',$this->motherDiedAge,true);
		$criteria->compare('childDiedAge',$this->childDiedAge,true);
		$criteria->compare('childGenderUid',$this->childGenderUid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamilyMemberMaternalHealth the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function beforeSave(){
		if(parent::beforeSave())
		{
			foreach ($this->attributes as $key => $value)
            {
				if ($value == '')
				{
					$this->$key = NULL;
				}
			}
			return TRUE;
		}
		else 
			return FALSE;
	}
}
