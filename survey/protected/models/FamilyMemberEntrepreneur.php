<?php

/**
 * This is the model class for table "familymemberentrepreneur".
 *
 * The followings are the available columns in table 'familymemberentrepreneur':
 * @property string $uid
 * @property string $householdUid
 * @property string $businessTypeUid
 * @property string $businessSustainabilityUid
 * @property string $financialRiskUid
 * @property string $physicalRiskUid
 *
 * The followings are the available model relations:
 * @property N48physicalrisk $physicalRiskU
 * @property N47financialrisk $financialRiskU
 * @property Household $householdU
 * @property Familymember $familyMemberU
 * @property N46businesstype $businessTypeU
 * @property N38incomestatus $businessSustainabilityU
 */
class FamilyMemberEntrepreneur extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'familymemberentrepreneur';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('businessTypeUid, businessSustainabilityUid, financialRiskUid, physicalRiskUid', 'required'),
			array('householdUid, businessTypeUid, businessSustainabilityUid, financialRiskUid, physicalRiskUid', 'length', 'max'=>11),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, householdUid, businessTypeUid, businessSustainabilityUid, financialRiskUid, physicalRiskUid', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'physicalRiskU' => array(self::BELONGS_TO, 'N48physicalrisk', 'physicalRiskUid'),
			'financialRiskU' => array(self::BELONGS_TO, 'N47financialrisk', 'financialRiskUid'),
			'householdU' => array(self::BELONGS_TO, 'Household', 'householdUid'),
			'businessTypeU' => array(self::BELONGS_TO, 'N46businesstype', 'businessTypeUid'),
			'businessSustainabilityU' => array(self::BELONGS_TO, 'N38incomestatus', 'businessSustainabilityUid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'householdUid' => 'Household',
			'businessTypeUid' => 'Business Type',
			'businessSustainabilityUid' => 'Business Sustainability',
			'financialRiskUid' => 'Financial Risk',
			'physicalRiskUid' => 'Physical Risk',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('householdUid',$this->householdUid,true);
		$criteria->compare('businessTypeUid',$this->businessTypeUid,true);
		$criteria->compare('businessSustainabilityUid',$this->businessSustainabilityUid,true);
		$criteria->compare('financialRiskUid',$this->financialRiskUid,true);
		$criteria->compare('physicalRiskUid',$this->physicalRiskUid,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamilyMemberEntrepreneur the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}

	protected function beforeSave(){
		if(parent::beforeSave())
		{
			
			foreach ($this->attributes as $key => $value)
            {
				if ($value == '')
				{
					$this->$key = NULL;
				}
			}
			return TRUE;
		}
		else 
			return FALSE;
	}
}
