<?php

/**
 * Lookup class.
 * Lookup is the data structure for keeping
 * contact form data. It is used by the 'contact' action of 'SiteController'.
 */
class Lookup extends CFormModel
{

	/**
	 * Declares the validation rules.
	 */
	public function rules()
	{
		return array(
		);
	}

	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function yesNoNa()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Yes');
		$data[] = array('uid'=>2, 'uidLabel' => '2: No');
		$data[] = array('uid'=>3, 'uidLabel' => '3: Not available/applicable');
		return $data; 
	}
	
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function yesNoNi()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Yes');
		$data[] = array('uid'=>2, 'uidLabel' => '2: No');
		$data[] = array('uid'=>3, 'uidLabel' => '3: Never investigated');
		return $data; 
	}
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function yesNo()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Yes');
		$data[] = array('uid'=>2, 'uidLabel' => '2: No');
		return $data; 
	}
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function yesNoOnly()
	{
		$data = array();
		$data[] = array('uid'=>1, 'uidLabel' => 'Yes');
		$data[] = array('uid'=>0, 'uidLabel' => 'No');
		return $data; 
	}
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function residenceStatus()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Permanent(P)');
		$data[] = array('uid'=>2, 'uidLabel' => '2: Temporary (T)');
		return $data; 
	}
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function residencePurpose()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Study(S)');
		$data[] = array('uid'=>2, 'uidLabel' => '2: Work(W)');
		$data[] = array('uid'=>3, 'uidLabel' => '3: Relocated (R) ');
		return $data; 
	}
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function livingWith()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Living separately(S)');
		$data[] = array('uid'=>2, 'uidLabel' => '2: Relatives(R)');
		$data[] = array('uid'=>3, 'uidLabel' => '3: Other(O)');
		return $data; 
	}
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function aliveStatus()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Alive (A)');
		$data[] = array('uid'=>2, 'uidLabel' => '2: Died (D)');
		return $data; 
	}
	
	/**
	 * Return formated dropdownlist.
	 * @return List
	 */
	public static function seniorsLivingWith()
	{
		$data = array();
		$data[] = array('uid'=>0, 'uidLabel' => '0: Not Answered');
		$data[] = array('uid'=>1, 'uidLabel' => '1: Home');
		$data[] = array('uid'=>2, 'uidLabel' => '2: Others');
		return $data; 
	}
}