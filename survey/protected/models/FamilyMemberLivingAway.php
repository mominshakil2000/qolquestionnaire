<?php

/**
 * This is the model class for table "familymemberlivingaway".
 *
 * The followings are the available columns in table 'familymemberlivingaway':
 * @property string $uid
 * @property string $householdUid
 * @property string $familyMemberUid
 * @property string $residenceCountry
 * @property string $currentCity
 * @property string $currentJamatKhana
 * @property string $residenceStatus
 * @property string $workStatus
 * @property string $residenceRelativesStatus
 * @property string $year
 *
 * The followings are the available model relations:
 * @property Familymember $familyMemberU
 */
class FamilyMemberLivingAway extends CActiveRecord
{
	/**
	 * @return string the associated database table name
	 */
	public function tableName()
	{
		return 'familymemberlivingaway';
	}

	/**
	 * @return array validation rules for model attributes.
	 */
	public function rules()
	{
		// NOTE: you should only define rules for those attributes that
		// will receive user inputs.
		return array(
			array('residenceCountry, currentCity, currentJamatKhana, residenceStatus, workStatus, residenceRelativesStatus, year', 'required'),
			array('uid, householdUid, familyMemberUid', 'length', 'max'=>11),
			array('residenceCountry, currentCity, currentJamatKhana', 'length', 'max'=>255),
			array('residenceStatus, workStatus, residenceRelativesStatus', 'length', 'max'=>1),
			array('year', 'length', 'max'=>4),
			// The following rule is used by search().
			// @todo Please remove those attributes that should not be searched.
			array('uid, householdUid, familyMemberUid, residenceCountry, currentCity, currentJamatKhana, residenceStatus, workStatus, residenceRelativesStatus, year', 'safe', 'on'=>'search'),
		);
	}

	/**
	 * @return array relational rules.
	 */
	public function relations()
	{
		// NOTE: you may need to adjust the relation name and the related
		// class name for the relations automatically generated below.
		return array(
			'familyMemberU' => array(self::BELONGS_TO, 'Familymember', 'familyMemberUid'),
		);
	}

	/**
	 * @return array customized attribute labels (name=>label)
	 */
	public function attributeLabels()
	{
		return array(
			'uid' => 'Uid',
			'householdUid' => 'Household Uid',
			'familyMemberUid' => 'Family Member Uid',
			'residenceCountry' => 'Residence Country',
			'currentCity' => 'Current City',
			'currentJamatKhana' => 'Current Jamat Khana',
			'residenceStatus' => 'Residence Status',
			'workStatus' => 'Work Status',
			'residenceRelativesStatus' => 'Residence Relatives Status',
			'year' => 'Year',
		);
	}

	/**
	 * Retrieves a list of models based on the current search/filter conditions.
	 *
	 * Typical usecase:
	 * - Initialize the model fields with values from filter form.
	 * - Execute this method to get CActiveDataProvider instance which will filter
	 * models according to data in model fields.
	 * - Pass data provider to CGridView, CListView or any similar widget.
	 *
	 * @return CActiveDataProvider the data provider that can return the models
	 * based on the search/filter conditions.
	 */
	public function search()
	{
		// @todo Please modify the following code to remove attributes that should not be searched.

		$criteria=new CDbCriteria;

		$criteria->compare('uid',$this->uid,true);
		$criteria->compare('householdUid',$this->householdUid,true);
		$criteria->compare('familyMemberUid',$this->familyMemberUid,true);
		$criteria->compare('residenceCountry',$this->residenceCountry,true);
		$criteria->compare('currentCity',$this->currentCity,true);
		$criteria->compare('currentJamatKhana',$this->currentJamatKhana,true);
		$criteria->compare('residenceStatus',$this->residenceStatus,true);
		$criteria->compare('workStatus',$this->workStatus,true);
		$criteria->compare('residenceRelativesStatus',$this->residenceRelativesStatus,true);
		$criteria->compare('year',$this->year,true);

		return new CActiveDataProvider($this, array(
			'criteria'=>$criteria,
		));
	}

	/**
	 * Returns the static model of the specified AR class.
	 * Please note that you should have this exact method in all your CActiveRecord descendants!
	 * @param string $className active record class name.
	 * @return FamilyMemberLivingAway the static model class
	 */
	public static function model($className=__CLASS__)
	{
		return parent::model($className);
	}
	
	protected function beforeSave(){
		if(parent::beforeSave())
		{
			
			foreach ($this->attributes as $key => $value)
            {
				if ($value == '')
				{
					$this->$key = NULL;
				}
			}
			return TRUE;
		}
		else 
			return FALSE;
	}
}
