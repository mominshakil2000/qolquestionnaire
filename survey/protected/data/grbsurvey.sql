-- phpMyAdmin SQL Dump
-- version 3.4.5
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Dec 30, 2014 at 01:36 PM
-- Server version: 5.5.16
-- PHP Version: 5.3.8

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `grbsurvey`
--

-- --------------------------------------------------------

--
-- Table structure for table `familymember`
--

CREATE TABLE IF NOT EXISTS `familymember` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `householdUid` int(11) unsigned DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `relationshipWithHeadUid` int(11) unsigned DEFAULT NULL,
  `martialStatusUid` int(11) unsigned DEFAULT NULL,
  `genderUid` int(11) unsigned DEFAULT NULL,
  `birthYear` int(5) unsigned DEFAULT NULL,
  `recEducationStatusUid` int(11) unsigned DEFAULT NULL,
  `recCurrentGradeUid` int(11) unsigned DEFAULT NULL,
  `recLastGradeUid` int(11) unsigned DEFAULT NULL,
  `recEducationStatusReasonUid` int(11) unsigned DEFAULT NULL,
  `isBenefitingJamatiInstitution` int(1) unsigned DEFAULT NULL,
  `isBenefitingItreb` int(1) unsigned DEFAULT NULL,
  `eveningJkAttendenceUid` int(1) unsigned DEFAULT NULL,
  `eveningJkNotAttendenceReasonUid` int(1) unsigned DEFAULT NULL,
  `morningJkAttendenceUid` int(1) unsigned DEFAULT NULL,
  `morningJkNotAttendenceReasonUid` int(1) unsigned DEFAULT NULL,
  `secularEducationStatusUid` int(1) unsigned DEFAULT NULL,
  `secularCurrentGradeUid` int(11) unsigned DEFAULT NULL,
  `secularEducationIsSatisfy` int(1) unsigned DEFAULT NULL,
  `secularLastGradeUid` int(11) unsigned DEFAULT NULL,
  `secularNotEnrolledReasonUid` int(11) unsigned DEFAULT NULL,
  `secularDropoutYear` int(5) unsigned DEFAULT NULL,
  `secularUpto10InstitueCategoryUid` int(11) unsigned DEFAULT NULL,
  `secularAbove10InstitueCategoryUid` int(11) unsigned DEFAULT NULL,
  `secularEducationGroupUid` int(11) unsigned DEFAULT NULL,
  `englishProficiencyReadingUid` int(11) unsigned DEFAULT NULL,
  `englishProficiencyWritingUid` int(11) unsigned DEFAULT NULL,
  `englishProficiencySpeakingUid` int(11) unsigned DEFAULT NULL,
  `itProficiencyOfficeSuitUid` int(11) unsigned DEFAULT NULL,
  `itProficiencyInternetUid` int(11) unsigned DEFAULT NULL,
  `occupationUid` int(11) unsigned DEFAULT NULL,
  `monthlyIncomeUid` int(11) unsigned DEFAULT NULL,
  `unemployedSinceUid` int(11) unsigned DEFAULT NULL,
  `unemployedReasonUid` int(11) unsigned DEFAULT NULL,
  `insuredSelfHealth` int(1) unsigned DEFAULT NULL,
  `insuredSelfLife` int(1) unsigned DEFAULT NULL,
  `insuredSelfAsset` int(1) unsigned DEFAULT NULL,
  `insuredSelfVehicle` int(1) unsigned DEFAULT NULL,
  `insuredSelfBusiness` int(1) unsigned DEFAULT NULL,
  `insuredSelfEducation` int(1) unsigned DEFAULT NULL,
  `insuredEmployerLife` int(1) unsigned DEFAULT NULL,
  `insuredEmployerHealth` int(1) unsigned DEFAULT NULL,
  `healthIssueStatusDiabetesUid` int(11) unsigned DEFAULT NULL,
  `healthIssueArthritisUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusCancerUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusBpUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusHeartUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusMentalIllnessUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusTuberculosisUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusKidneyUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusStrokeUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusHepatitis` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusAsthmaUid` int(11) unsigned DEFAULT NULL,
  `healthIssueStatusOthersUid` int(11) unsigned DEFAULT NULL,
  `healthIssueDisabilityUid` int(11) unsigned DEFAULT NULL,
  `healthIssueIllFrequencyUid` int(11) unsigned DEFAULT NULL,
  `healthIssueNoHealthFacilityReasonUid` int(11) unsigned DEFAULT NULL,
  `nutritionConsumptionFrequencyMeatUid` int(11) unsigned DEFAULT NULL,
  `nutritionConsumptionFrequencyFruitUid` int(11) unsigned DEFAULT NULL,
  `nutritionConsumptionFrequencyMilkUid` int(11) unsigned DEFAULT NULL,
  `substanceAbuseAlcoholUid` int(11) unsigned DEFAULT NULL,
  `substanceAbuseTobaccoUid` int(11) unsigned DEFAULT NULL,
  `substanceAbuseCigrateUid` int(11) unsigned DEFAULT NULL,
  `substanceAbuseIllicitDrugUid` int(11) unsigned DEFAULT NULL,
  `substanceAbusePanUid` int(11) unsigned DEFAULT NULL,
  `socialKhidmatUid` int(11) unsigned DEFAULT NULL,
  `socialSportsHolidayUid` int(11) unsigned DEFAULT NULL,
  `socialSportsWorkingDayUid` int(11) unsigned DEFAULT NULL,
  `socialExcerciseUid` int(11) unsigned DEFAULT NULL,
  `socialRecreationActivityUid` int(11) unsigned DEFAULT NULL,
  `takSelfDecisionMaking` int(1) unsigned DEFAULT NULL,
  `leisureTimeUid` int(1) unsigned DEFAULT NULL,
  `qolUid` int(1) unsigned DEFAULT NULL,
  `qolChangePast3YearsUid` int(1) unsigned DEFAULT NULL,
  `qolChangeStatusUid` int(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `relationshipWithHeadUid` (`relationshipWithHeadUid`),
  KEY `martialStatusUid` (`martialStatusUid`),
  KEY `recEducationStatusUid` (`recEducationStatusUid`),
  KEY `recCurrentGradeUid` (`recCurrentGradeUid`),
  KEY `recLastGradeUid` (`recLastGradeUid`),
  KEY `recEducationStatusReasonUid` (`recEducationStatusReasonUid`),
  KEY `eveningJkAttendenceUid` (`eveningJkAttendenceUid`),
  KEY `eveningJkNotAttendenceReasonUid` (`eveningJkNotAttendenceReasonUid`),
  KEY `morningJkAttendenceUid` (`morningJkAttendenceUid`),
  KEY `morningJkNotAttendenceReasonUid` (`morningJkNotAttendenceReasonUid`),
  KEY `secularEducationStatusUid` (`secularEducationStatusUid`),
  KEY `secularCurrentGradeUid` (`secularCurrentGradeUid`),
  KEY `secularLastGradeUid` (`secularLastGradeUid`),
  KEY `secularNotEnrolledReasonUid` (`secularNotEnrolledReasonUid`),
  KEY `secularUpto10InstitueCategoryUid` (`secularUpto10InstitueCategoryUid`),
  KEY `secularAbove10InstitueCategoryUid` (`secularAbove10InstitueCategoryUid`),
  KEY `secularEducationGroupUid` (`secularEducationGroupUid`),
  KEY `englishProficiencyReadingUid` (`englishProficiencyReadingUid`),
  KEY `englishProficiencyWritingUid` (`englishProficiencyWritingUid`),
  KEY `englishProficiencySpeakingUid` (`englishProficiencySpeakingUid`),
  KEY `itProficiencyOfficeSuitUid` (`itProficiencyOfficeSuitUid`),
  KEY `itProficiencyInternetUid` (`itProficiencyInternetUid`),
  KEY `occupationUid` (`occupationUid`),
  KEY `monthlyIncomeUid` (`monthlyIncomeUid`),
  KEY `unemployedSinceUid` (`unemployedSinceUid`),
  KEY `unemployedReasonUid` (`unemployedReasonUid`),
  KEY `healthIssueArthritisUid` (`healthIssueArthritisUid`),
  KEY `healthIssueStatusCancerUid` (`healthIssueStatusCancerUid`),
  KEY `healthIssueStatusBpUid` (`healthIssueStatusBpUid`),
  KEY `healthIssueStatusHeartUid` (`healthIssueStatusHeartUid`),
  KEY `healthIssueStatusTuberculosisUid` (`healthIssueStatusTuberculosisUid`),
  KEY `healthIssueStatusKidneyUid` (`healthIssueStatusKidneyUid`),
  KEY `healthIssueStatusStrokeUid` (`healthIssueStatusStrokeUid`),
  KEY `healthIssueStatusAsthmaUid` (`healthIssueStatusAsthmaUid`),
  KEY `healthIssueStatusOthersUid` (`healthIssueStatusOthersUid`),
  KEY `healthIssueDisabilityUid` (`healthIssueDisabilityUid`),
  KEY `healthIssueIllFrequencyUid` (`healthIssueIllFrequencyUid`),
  KEY `healthIssueNoHealthFacilityReasonUid` (`healthIssueNoHealthFacilityReasonUid`),
  KEY `nutritionConsumptionFrequencyMeatUid` (`nutritionConsumptionFrequencyMeatUid`),
  KEY `nutritionConsumptionFrequencyFruitUid` (`nutritionConsumptionFrequencyFruitUid`),
  KEY `nutritionConsumptionFrequencyMilkUid` (`nutritionConsumptionFrequencyMilkUid`),
  KEY `substanceAbuseAlcoholUid` (`substanceAbuseAlcoholUid`),
  KEY `substanceAbuseTobaccoUid` (`substanceAbuseTobaccoUid`),
  KEY `substanceAbuseIllicitDrugUid` (`substanceAbuseIllicitDrugUid`),
  KEY `substanceAbusePanUid` (`substanceAbusePanUid`),
  KEY `socialKhidmatUid` (`socialKhidmatUid`),
  KEY `socialSportsHolidayUid` (`socialSportsHolidayUid`),
  KEY `socialSportsWorkingDayUid` (`socialSportsWorkingDayUid`),
  KEY `socialExcerciseUid` (`socialExcerciseUid`),
  KEY `socialRecreationActivityUid` (`socialRecreationActivityUid`),
  KEY `leisureTimeUid` (`leisureTimeUid`),
  KEY `qolUid` (`qolUid`),
  KEY `qolChangePast3YearsUid` (`qolChangePast3YearsUid`),
  KEY `qolChangeStatusUid` (`qolChangeStatusUid`),
  KEY `genderUid` (`genderUid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=131 ;

--
-- Dumping data for table `familymember`
--

INSERT INTO `familymember` (`uid`, `householdUid`, `firstName`, `relationshipWithHeadUid`, `martialStatusUid`, `genderUid`, `birthYear`, `recEducationStatusUid`, `recCurrentGradeUid`, `recLastGradeUid`, `recEducationStatusReasonUid`, `isBenefitingJamatiInstitution`, `isBenefitingItreb`, `eveningJkAttendenceUid`, `eveningJkNotAttendenceReasonUid`, `morningJkAttendenceUid`, `morningJkNotAttendenceReasonUid`, `secularEducationStatusUid`, `secularCurrentGradeUid`, `secularEducationIsSatisfy`, `secularLastGradeUid`, `secularNotEnrolledReasonUid`, `secularDropoutYear`, `secularUpto10InstitueCategoryUid`, `secularAbove10InstitueCategoryUid`, `secularEducationGroupUid`, `englishProficiencyReadingUid`, `englishProficiencyWritingUid`, `englishProficiencySpeakingUid`, `itProficiencyOfficeSuitUid`, `itProficiencyInternetUid`, `occupationUid`, `monthlyIncomeUid`, `unemployedSinceUid`, `unemployedReasonUid`, `insuredSelfHealth`, `insuredSelfLife`, `insuredSelfAsset`, `insuredSelfVehicle`, `insuredSelfBusiness`, `insuredSelfEducation`, `insuredEmployerLife`, `insuredEmployerHealth`, `healthIssueStatusDiabetesUid`, `healthIssueArthritisUid`, `healthIssueStatusCancerUid`, `healthIssueStatusBpUid`, `healthIssueStatusHeartUid`, `healthIssueStatusMentalIllnessUid`, `healthIssueStatusTuberculosisUid`, `healthIssueStatusKidneyUid`, `healthIssueStatusStrokeUid`, `healthIssueStatusHepatitis`, `healthIssueStatusAsthmaUid`, `healthIssueStatusOthersUid`, `healthIssueDisabilityUid`, `healthIssueIllFrequencyUid`, `healthIssueNoHealthFacilityReasonUid`, `nutritionConsumptionFrequencyMeatUid`, `nutritionConsumptionFrequencyFruitUid`, `nutritionConsumptionFrequencyMilkUid`, `substanceAbuseAlcoholUid`, `substanceAbuseTobaccoUid`, `substanceAbuseCigrateUid`, `substanceAbuseIllicitDrugUid`, `substanceAbusePanUid`, `socialKhidmatUid`, `socialSportsHolidayUid`, `socialSportsWorkingDayUid`, `socialExcerciseUid`, `socialRecreationActivityUid`, `takSelfDecisionMaking`, `leisureTimeUid`, `qolUid`, `qolChangePast3YearsUid`, `qolChangeStatusUid`) VALUES
(126, 6, 'ali', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0),
(127, 6, 'akber', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 10, 0, 0, 0, 0, 0, 0),
(128, 6, 'aamir', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 6, 0, 0, 0, 0, 0, 0),
(129, 7, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0),
(130, 7, '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `familymemberentrepreneur`
--

CREATE TABLE IF NOT EXISTS `familymemberentrepreneur` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `householdUid` int(11) unsigned DEFAULT NULL,
  `businessTypeUid` int(11) unsigned DEFAULT NULL,
  `businessSustainabilityUid` int(11) unsigned DEFAULT NULL,
  `financialRiskUid` int(11) unsigned DEFAULT NULL,
  `physicalRiskUid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `businessTypeUid` (`businessTypeUid`),
  KEY `financialRiskUid` (`financialRiskUid`),
  KEY `physicalRiskUid` (`physicalRiskUid`),
  KEY `businessSustainabilityUid` (`businessSustainabilityUid`),
  KEY `householdUid` (`householdUid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=2 ;

--
-- Dumping data for table `familymemberentrepreneur`
--

INSERT INTO `familymemberentrepreneur` (`uid`, `householdUid`, `businessTypeUid`, `businessSustainabilityUid`, `financialRiskUid`, `physicalRiskUid`) VALUES
(1, 6, 0, 1, 3, 3);

-- --------------------------------------------------------

--
-- Table structure for table `familymemberlivingaway`
--

CREATE TABLE IF NOT EXISTS `familymemberlivingaway` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `householdUid` int(11) unsigned DEFAULT NULL,
  `familyMemberUid` int(11) unsigned DEFAULT NULL,
  `residenceCountry` varchar(255) DEFAULT NULL,
  `currentCity` varchar(255) DEFAULT NULL,
  `currentJamatKhana` varchar(255) DEFAULT NULL,
  `residenceStatus` char(1) DEFAULT NULL,
  `workStatus` char(1) DEFAULT NULL,
  `residenceRelativesStatus` char(1) DEFAULT NULL,
  `year` year(4) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `familyMemberUid` (`familyMemberUid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=7 ;

--
-- Dumping data for table `familymemberlivingaway`
--

INSERT INTO `familymemberlivingaway` (`uid`, `householdUid`, `familyMemberUid`, `residenceCountry`, `currentCity`, `currentJamatKhana`, `residenceStatus`, `workStatus`, `residenceRelativesStatus`, `year`) VALUES
(2, 6, 126, '', '', '', '', '', '', 0000),
(3, 6, 127, '', '', '', '', '', '', 0000);

-- --------------------------------------------------------

--
-- Table structure for table `familymembermaternalhealth`
--

CREATE TABLE IF NOT EXISTS `familymembermaternalhealth` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `householdUid` int(11) unsigned DEFAULT NULL,
  `familyMemberMotherUid` int(11) unsigned DEFAULT NULL,
  `familyMemberChildUid` int(11) unsigned DEFAULT NULL,
  `deliveryYear` year(4) DEFAULT NULL,
  `deliveryCarriedByUid` int(11) unsigned DEFAULT NULL,
  `duringDeliveryChildAlive` int(1) unsigned DEFAULT NULL,
  `duringDeliveryMotherAlive` int(1) DEFAULT NULL,
  `nowChildAlive` int(1) unsigned DEFAULT NULL,
  `nowMotherAlive` int(1) unsigned DEFAULT NULL,
  `miscarriaged` int(1) unsigned DEFAULT NULL,
  `motherDiedAge` int(5) unsigned DEFAULT NULL,
  `childDiedAge` int(5) unsigned DEFAULT NULL,
  `childGenderUid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `childGenderUid` (`childGenderUid`),
  KEY `deliveryCarriedByUid` (`deliveryCarriedByUid`),
  KEY `familyMemberMotherUid` (`familyMemberMotherUid`),
  KEY `familyMemberChildUid` (`familyMemberChildUid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=3 ;

--
-- Dumping data for table `familymembermaternalhealth`
--

INSERT INTO `familymembermaternalhealth` (`uid`, `householdUid`, `familyMemberMotherUid`, `familyMemberChildUid`, `deliveryYear`, `deliveryCarriedByUid`, `duringDeliveryChildAlive`, `duringDeliveryMotherAlive`, `nowChildAlive`, `nowMotherAlive`, `miscarriaged`, `motherDiedAge`, `childDiedAge`, `childGenderUid`) VALUES
(1, 6, NULL, NULL, 0000, 0, 0, NULL, 0, 0, 0, 0, 0, 0),
(2, 6, NULL, NULL, 0000, 0, 0, NULL, 0, 0, 0, 0, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `familymemberseniors`
--

CREATE TABLE IF NOT EXISTS `familymemberseniors` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `householdUid` int(11) unsigned DEFAULT NULL,
  `livingWithUid` int(11) unsigned DEFAULT NULL,
  `livingOtherUid` int(11) unsigned DEFAULT NULL,
  `involveSocialOrEconomicActivities` int(11) unsigned DEFAULT NULL,
  `notInvolveSocialOrEconomicActivitiesUid` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `notInvolveSocialOrEconomicActivitiesUid` (`notInvolveSocialOrEconomicActivitiesUid`),
  KEY `livingOtherUid` (`livingOtherUid`),
  KEY `householdUid` (`householdUid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

-- --------------------------------------------------------

--
-- Table structure for table `formfillstatus`
--

CREATE TABLE IF NOT EXISTS `formfillstatus` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formfillstatus`
--

INSERT INTO `formfillstatus` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Complete', '1 : Complete'),
(2, 'Incomplete', '2: Incomplete');

-- --------------------------------------------------------

--
-- Table structure for table `formpoststatus`
--

CREATE TABLE IF NOT EXISTS `formpoststatus` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formpoststatus`
--

INSERT INTO `formpoststatus` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0: Not Answered'),
(1, 'Save As Draft', '1: Save As Draft'),
(2, 'Completed', '2: Completed'),
(3, 'Reviewed and Posted', '3: Reviewed and Posted');

-- --------------------------------------------------------

--
-- Table structure for table `formtype`
--

CREATE TABLE IF NOT EXISTS `formtype` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `formtype`
--

INSERT INTO `formtype` (`uid`, `label`, `uidLabel`) VALUES
(1, 'Bachelor/Individual living alone', '1 : Bachelor/Individual living alone'),
(2, 'Household', '2 : Household');

-- --------------------------------------------------------

--
-- Table structure for table `gender`
--

CREATE TABLE IF NOT EXISTS `gender` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `gender`
--

INSERT INTO `gender` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Male', '1 : Male'),
(2, 'Female', '2 : Female'),
(3, 'Not Applicable', '3 : Not Applicable');

-- --------------------------------------------------------

--
-- Table structure for table `healthissuestatus`
--

CREATE TABLE IF NOT EXISTS `healthissuestatus` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `healthissuestatus`
--

INSERT INTO `healthissuestatus` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered');

-- --------------------------------------------------------

--
-- Table structure for table `household`
--

CREATE TABLE IF NOT EXISTS `household` (
  `uid` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `formTypeUid` int(11) unsigned DEFAULT NULL,
  `formNum` int(11) unsigned DEFAULT NULL,
  `firstName` varchar(255) DEFAULT NULL,
  `middleName` varchar(255) DEFAULT NULL,
  `lastName` varchar(255) DEFAULT NULL,
  `genderUid` int(11) unsigned DEFAULT NULL,
  `nic` varchar(255) DEFAULT NULL,
  `familyNumberNic` varchar(10) DEFAULT NULL,
  `totalHouseHoldMember` int(11) unsigned DEFAULT NULL,
  `isHouseShared` int(1) unsigned DEFAULT NULL,
  `respondentName` varchar(255) DEFAULT NULL,
  `respondentHeadRealtionUid` int(11) unsigned DEFAULT NULL,
  `nearestJamatKhanaUid` int(11) DEFAULT NULL,
  `localCouncilUid` int(11) unsigned DEFAULT NULL,
  `reginalCouncilUid` int(11) unsigned DEFAULT NULL,
  `numResidingBloodFamilies` int(11) unsigned DEFAULT NULL,
  `numResidingUnbloodFamilies` int(11) unsigned DEFAULT NULL,
  `numResidingUnbloodMember` int(11) unsigned DEFAULT NULL,
  `totalFamilyMember` int(11) unsigned DEFAULT NULL,
  `addressHouseNo` varchar(255) DEFAULT NULL,
  `addressStreet` varchar(255) DEFAULT NULL,
  `addressColony` varchar(255) DEFAULT NULL,
  `addressArea` varchar(255) DEFAULT NULL,
  `addressVillageCity` varchar(255) DEFAULT NULL,
  `addressProvince` varchar(255) DEFAULT NULL,
  `addressPrimaryPhone` varchar(255) DEFAULT NULL,
  `addressSecondartPhone` varchar(255) DEFAULT NULL,
  `houseConstructTypeUid` int(11) unsigned DEFAULT NULL,
  `houseTotalRoom` int(11) unsigned DEFAULT NULL,
  `cookingFuelUid` int(11) unsigned DEFAULT NULL,
  `lightingSourceUid` int(11) unsigned DEFAULT NULL,
  `winterHeatingSystemFuelUid` int(11) unsigned DEFAULT NULL,
  `smokeOutletUid` int(11) unsigned DEFAULT NULL,
  `drinkingWaterSourceUid` int(11) unsigned DEFAULT NULL,
  `recDistanceUid` int(11) unsigned DEFAULT NULL,
  `jkDistanceUid` int(11) unsigned DEFAULT NULL,
  `toiletFacilityUid` int(11) unsigned DEFAULT NULL,
  `physicalThreatInHouseHold` int(1) unsigned DEFAULT NULL,
  `physicalThreatInNeighbourhood` int(1) unsigned DEFAULT NULL,
  `satisfyWithLivingSpace` int(1) unsigned DEFAULT NULL,
  `internetServiceAvailable` int(1) unsigned DEFAULT NULL,
  `residenceStatusUid` int(11) unsigned DEFAULT NULL,
  `confilictInHouseHoldUid` int(11) unsigned DEFAULT NULL,
  `confilictResolutionUid` int(11) unsigned DEFAULT NULL,
  `numberOfSeniorsInHome` int(11) unsigned DEFAULT NULL,
  `numberOfSeniorsInOther` int(11) unsigned DEFAULT NULL,
  `foodShortageInLastMonth` int(1) unsigned DEFAULT NULL,
  `monthlyFamilyIncomeStatusUid` int(11) unsigned DEFAULT NULL,
  `savingUid` int(11) unsigned DEFAULT NULL,
  `deficitUid` int(11) unsigned DEFAULT NULL,
  `savingAvailableUid` int(11) unsigned DEFAULT NULL,
  `outstandingDebitUid` int(11) unsigned DEFAULT NULL,
  `outstandingDebitInterestRateUid` int(11) unsigned DEFAULT NULL,
  `loanPurposeUid` int(11) unsigned DEFAULT NULL,
  `faJamatiHealth` int(1) unsigned DEFAULT NULL,
  `faJamatiEducation` int(1) unsigned DEFAULT NULL,
  `faJamatiHousing` int(1) unsigned DEFAULT NULL,
  `faJamatiFood` int(1) unsigned DEFAULT NULL,
  `faJamatiSeniorCitizen` int(1) unsigned DEFAULT NULL,
  `faJamatiSpecialChild` int(1) unsigned DEFAULT NULL,
  `faExternalHealth` int(1) unsigned DEFAULT NULL,
  `faExternalEducation` int(1) unsigned DEFAULT NULL,
  `faExternalHousing` int(1) unsigned DEFAULT NULL,
  `faExternalFood` int(1) unsigned DEFAULT NULL,
  `faExternalSeniorCitizen` int(1) unsigned DEFAULT NULL,
  `faExternalSpecialChild` int(1) unsigned DEFAULT NULL,
  `hhIsTelvision` int(1) unsigned DEFAULT NULL,
  `hhQtyTelvision` int(5) unsigned DEFAULT NULL,
  `hhIsDishOrCabel` int(1) unsigned DEFAULT NULL,
  `hhQtyDishOrCabel` int(5) unsigned DEFAULT NULL,
  `hhIsWashingMachine` int(1) unsigned DEFAULT NULL,
  `hhQtyWashingMachine` int(5) unsigned DEFAULT NULL,
  `hhIsElectricFan` int(1) unsigned DEFAULT NULL,
  `hhQtyElectricFan` int(5) unsigned DEFAULT NULL,
  `hhIsAc` int(1) unsigned DEFAULT NULL,
  `hhQtyAc` int(5) unsigned DEFAULT NULL,
  `hhIsRefrigerator` int(1) unsigned DEFAULT NULL,
  `hhQtyRefrigerator` int(5) unsigned DEFAULT NULL,
  `hhIsStove` int(1) unsigned DEFAULT NULL,
  `hhQtyStove` int(5) unsigned DEFAULT NULL,
  `hhIsOven` int(1) unsigned DEFAULT NULL,
  `hhQtyOven` int(5) unsigned DEFAULT NULL,
  `hhIsloadshadingBackup` int(1) unsigned DEFAULT NULL,
  `hhQtyloadshadingBackup` int(5) unsigned DEFAULT NULL,
  `hhIsGeyser` int(1) unsigned DEFAULT NULL,
  `hhQtyGeyser` int(5) unsigned DEFAULT NULL,
  `hhIsHeater` int(1) unsigned DEFAULT NULL,
  `hhQtyHeater` int(5) unsigned DEFAULT NULL,
  `hhIsMobilePhone` int(1) unsigned DEFAULT NULL,
  `hhQtyMobilePhone` int(5) unsigned DEFAULT NULL,
  `hhIsLivestock` int(1) unsigned DEFAULT NULL,
  `hhQtyLivestock` int(5) unsigned DEFAULT NULL,
  `hhIsHouse` int(1) unsigned DEFAULT NULL,
  `hhQtyHouse` int(5) unsigned DEFAULT NULL,
  `hhIsMotoryCycle` int(1) unsigned DEFAULT NULL,
  `hhQtyMotoryCycle` int(5) unsigned DEFAULT NULL,
  `hhIsCommercialProperty` int(1) unsigned DEFAULT NULL,
  `hhQtyCommercialProperty` int(5) unsigned DEFAULT NULL,
  `hhIsComputer` int(1) unsigned DEFAULT NULL,
  `hhQtyComputer` int(5) unsigned DEFAULT NULL,
  `hhIsFruitTrees` int(1) unsigned DEFAULT NULL,
  `hhQtyFruitTrees` int(5) unsigned DEFAULT NULL,
  `hhIsAgricultureAsset` int(1) unsigned DEFAULT NULL,
  `hhQtyAgricultureAsset` int(5) unsigned DEFAULT NULL,
  `hhIsCar` int(1) unsigned DEFAULT NULL,
  `hhQtyCar` int(5) unsigned DEFAULT NULL,
  `hhIsTruckBusVan` int(1) unsigned DEFAULT NULL,
  `hhQtyTruckBusVan` int(5) unsigned DEFAULT NULL,
  `hhIsTractor` int(1) unsigned DEFAULT NULL,
  `hhQtyTractor` int(5) unsigned DEFAULT NULL,
  `hhIsAgricultureLand` int(1) unsigned DEFAULT NULL,
  `hhQtyAgricultureLand` int(5) unsigned DEFAULT NULL,
  `hhQtyAgricultureLandUnitUid` int(11) unsigned DEFAULT NULL,
  `hhIsNonAgricultureLand` int(1) unsigned DEFAULT NULL,
  `hhQtyNonAgricultureLand` int(5) unsigned DEFAULT NULL,
  `hhQtyNonAgricultureLandUnitUid` int(11) unsigned DEFAULT NULL,
  `comments` text,
  `filledBy` varchar(255) DEFAULT NULL,
  `reviewedBy` varchar(255) DEFAULT NULL,
  `filledDate` date DEFAULT NULL,
  `formFilledMinutes` int(6) unsigned DEFAULT NULL,
  `surveyorEmail` varchar(255) DEFAULT NULL,
  `surveyorContactNumber` varchar(255) DEFAULT NULL,
  `isEducationInstituteAffordable` int(1) unsigned DEFAULT NULL,
  `educationInstituteIsAffordable` int(1) unsigned DEFAULT NULL,
  `educationInstituteIsAccessible` int(1) unsigned DEFAULT NULL,
  `educationInstituteIsQuality` int(1) unsigned DEFAULT NULL,
  `educationInstituteDistanceUid` int(11) unsigned DEFAULT NULL,
  `primaryHealthServiceIsAffordable` int(1) unsigned DEFAULT NULL,
  `primaryHealthServiceIsAccessible` int(1) unsigned DEFAULT NULL,
  `primaryHealthServiceIsQuality` int(1) unsigned DEFAULT NULL,
  `primaryHealthServiceDistanceUid` int(11) unsigned DEFAULT NULL,
  `secondaryHealthServiceIsAffordable` int(1) unsigned DEFAULT NULL,
  `secondaryHealthServiceIsAccessible` int(1) unsigned DEFAULT NULL,
  `secondaryHealthServiceIsQuality` int(1) unsigned DEFAULT NULL,
  `secondaryHealthServiceDistanceUid` int(11) unsigned DEFAULT NULL,
  `drinkingWaterIsAffordable` int(1) unsigned DEFAULT NULL,
  `drinkingWaterIsAccessible` int(1) unsigned DEFAULT NULL,
  `drinkingWaterIsQuality` int(1) unsigned DEFAULT NULL,
  `drinkingWaterDistanceUid` int(11) unsigned DEFAULT NULL,
  `marketIsAffordable` int(1) unsigned DEFAULT NULL,
  `marketIsAccessible` int(1) unsigned DEFAULT NULL,
  `marketIsQuality` int(1) unsigned DEFAULT NULL,
  `marketDistanceUid` int(11) unsigned DEFAULT NULL,
  `parkIsAffordable` int(1) unsigned DEFAULT NULL,
  `parkIsAccessible` int(1) unsigned DEFAULT NULL,
  `parkIsQuality` int(1) unsigned DEFAULT NULL,
  `parkDistanceUid` int(11) unsigned DEFAULT NULL,
  `electricityIsAffordable` int(1) unsigned DEFAULT NULL,
  `electricityIsAccessible` int(1) unsigned DEFAULT NULL,
  `electricityIsQuality` int(1) unsigned DEFAULT NULL,
  `electricityDistanceUid` int(11) unsigned DEFAULT NULL,
  `gasIsAffordable` int(1) unsigned DEFAULT NULL,
  `gasIsAccessible` int(1) unsigned DEFAULT NULL,
  `gasIsQuality` int(1) unsigned DEFAULT NULL,
  `gasDistanceUid` int(11) unsigned DEFAULT NULL,
  `libraryIsAffordable` int(1) unsigned DEFAULT NULL,
  `libraryIsAccessible` int(1) unsigned DEFAULT NULL,
  `libraryIsQuality` int(1) unsigned DEFAULT NULL,
  `libraryDistanceUid` int(11) unsigned DEFAULT NULL,
  `cinemaIsAffordable` int(1) unsigned DEFAULT NULL,
  `cinemaIsAccessible` int(1) unsigned DEFAULT NULL,
  `cinemaIsQuality` int(1) unsigned DEFAULT NULL,
  `cinemaDistanceUid` int(11) unsigned DEFAULT NULL,
  `transportationIsAffordable` int(1) unsigned DEFAULT NULL,
  `transportationIsAccessible` int(1) unsigned DEFAULT NULL,
  `transportationIsQuality` int(1) unsigned DEFAULT NULL,
  `transportationDistanceUid` int(11) unsigned DEFAULT NULL,
  `telephoneIsAffordable` int(1) unsigned DEFAULT NULL,
  `telephoneIsAccessible` int(1) unsigned DEFAULT NULL,
  `telephoneIsQuality` int(1) unsigned DEFAULT NULL,
  `telephoneDistanceUid` int(11) unsigned DEFAULT NULL,
  `isRoadsafty` int(1) unsigned DEFAULT NULL,
  `isUnsafeHouse` int(1) unsigned DEFAULT NULL,
  `isNaturalDisaster` int(1) unsigned DEFAULT NULL,
  `isLawOrder` int(1) unsigned DEFAULT NULL,
  `isInappropriatePolitical` int(1) unsigned DEFAULT NULL,
  `isChildAbuse` int(1) unsigned DEFAULT NULL,
  `isReligiousIntolerance` int(1) unsigned DEFAULT NULL,
  `isLandGrabbing` int(1) unsigned DEFAULT NULL,
  `isRansom` int(1) unsigned DEFAULT NULL,
  `isRiskBusinessWindup` int(1) unsigned DEFAULT NULL,
  `formLastUpdatedByUser` int(11) unsigned DEFAULT NULL,
  `formAddedByUser` int(11) unsigned DEFAULT NULL,
  `formFillStatus` int(11) unsigned DEFAULT NULL,
  `formPostStatus` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`uid`),
  UNIQUE KEY `formNum` (`formNum`),
  KEY `formTypeUid` (`formTypeUid`),
  KEY `genderUid` (`genderUid`),
  KEY `respondentHeadRealtionUid` (`respondentHeadRealtionUid`),
  KEY `localCouncilUid` (`localCouncilUid`),
  KEY `reginalCouncilUid` (`reginalCouncilUid`),
  KEY `houseConstructTypeUid` (`houseConstructTypeUid`),
  KEY `cookingFuelUid` (`cookingFuelUid`),
  KEY `lightingSourceUid` (`lightingSourceUid`),
  KEY `winterHeatingSystemFuelUid` (`winterHeatingSystemFuelUid`),
  KEY `smokeOutletUid` (`smokeOutletUid`),
  KEY `drinkingWaterSourceUid` (`drinkingWaterSourceUid`),
  KEY `recDistanceUid` (`recDistanceUid`),
  KEY `jkDistanceUid` (`jkDistanceUid`),
  KEY `toiletFacilityUid` (`toiletFacilityUid`),
  KEY `residenceStatusUid` (`residenceStatusUid`),
  KEY `confilictInHouseHoldUid` (`confilictInHouseHoldUid`),
  KEY `confilictResolutionUid` (`confilictResolutionUid`),
  KEY `monthlyFamilyIncomeStatusUid` (`monthlyFamilyIncomeStatusUid`),
  KEY `savingUid` (`savingUid`),
  KEY `deficitUid` (`deficitUid`),
  KEY `savingAvailableUid` (`savingAvailableUid`),
  KEY `outstandingDebitUid` (`outstandingDebitUid`),
  KEY `outstandingDebitInterestRateUid` (`outstandingDebitInterestRateUid`),
  KEY `loanPurposeUid` (`loanPurposeUid`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=8 ;

--
-- Dumping data for table `household`
--

INSERT INTO `household` (`uid`, `formTypeUid`, `formNum`, `firstName`, `middleName`, `lastName`, `genderUid`, `nic`, `familyNumberNic`, `totalHouseHoldMember`, `isHouseShared`, `respondentName`, `respondentHeadRealtionUid`, `nearestJamatKhanaUid`, `localCouncilUid`, `reginalCouncilUid`, `numResidingBloodFamilies`, `numResidingUnbloodFamilies`, `numResidingUnbloodMember`, `totalFamilyMember`, `addressHouseNo`, `addressStreet`, `addressColony`, `addressArea`, `addressVillageCity`, `addressProvince`, `addressPrimaryPhone`, `addressSecondartPhone`, `houseConstructTypeUid`, `houseTotalRoom`, `cookingFuelUid`, `lightingSourceUid`, `winterHeatingSystemFuelUid`, `smokeOutletUid`, `drinkingWaterSourceUid`, `recDistanceUid`, `jkDistanceUid`, `toiletFacilityUid`, `physicalThreatInHouseHold`, `physicalThreatInNeighbourhood`, `satisfyWithLivingSpace`, `internetServiceAvailable`, `residenceStatusUid`, `confilictInHouseHoldUid`, `confilictResolutionUid`, `numberOfSeniorsInHome`, `numberOfSeniorsInOther`, `foodShortageInLastMonth`, `monthlyFamilyIncomeStatusUid`, `savingUid`, `deficitUid`, `savingAvailableUid`, `outstandingDebitUid`, `outstandingDebitInterestRateUid`, `loanPurposeUid`, `faJamatiHealth`, `faJamatiEducation`, `faJamatiHousing`, `faJamatiFood`, `faJamatiSeniorCitizen`, `faJamatiSpecialChild`, `faExternalHealth`, `faExternalEducation`, `faExternalHousing`, `faExternalFood`, `faExternalSeniorCitizen`, `faExternalSpecialChild`, `hhIsTelvision`, `hhQtyTelvision`, `hhIsDishOrCabel`, `hhQtyDishOrCabel`, `hhIsWashingMachine`, `hhQtyWashingMachine`, `hhIsElectricFan`, `hhQtyElectricFan`, `hhIsAc`, `hhQtyAc`, `hhIsRefrigerator`, `hhQtyRefrigerator`, `hhIsStove`, `hhQtyStove`, `hhIsOven`, `hhQtyOven`, `hhIsloadshadingBackup`, `hhQtyloadshadingBackup`, `hhIsGeyser`, `hhQtyGeyser`, `hhIsHeater`, `hhQtyHeater`, `hhIsMobilePhone`, `hhQtyMobilePhone`, `hhIsLivestock`, `hhQtyLivestock`, `hhIsHouse`, `hhQtyHouse`, `hhIsMotoryCycle`, `hhQtyMotoryCycle`, `hhIsCommercialProperty`, `hhQtyCommercialProperty`, `hhIsComputer`, `hhQtyComputer`, `hhIsFruitTrees`, `hhQtyFruitTrees`, `hhIsAgricultureAsset`, `hhQtyAgricultureAsset`, `hhIsCar`, `hhQtyCar`, `hhIsTruckBusVan`, `hhQtyTruckBusVan`, `hhIsTractor`, `hhQtyTractor`, `hhIsAgricultureLand`, `hhQtyAgricultureLand`, `hhQtyAgricultureLandUnitUid`, `hhIsNonAgricultureLand`, `hhQtyNonAgricultureLand`, `hhQtyNonAgricultureLandUnitUid`, `comments`, `filledBy`, `reviewedBy`, `filledDate`, `formFilledMinutes`, `surveyorEmail`, `surveyorContactNumber`, `isEducationInstituteAffordable`, `educationInstituteIsAffordable`, `educationInstituteIsAccessible`, `educationInstituteIsQuality`, `educationInstituteDistanceUid`, `primaryHealthServiceIsAffordable`, `primaryHealthServiceIsAccessible`, `primaryHealthServiceIsQuality`, `primaryHealthServiceDistanceUid`, `secondaryHealthServiceIsAffordable`, `secondaryHealthServiceIsAccessible`, `secondaryHealthServiceIsQuality`, `secondaryHealthServiceDistanceUid`, `drinkingWaterIsAffordable`, `drinkingWaterIsAccessible`, `drinkingWaterIsQuality`, `drinkingWaterDistanceUid`, `marketIsAffordable`, `marketIsAccessible`, `marketIsQuality`, `marketDistanceUid`, `parkIsAffordable`, `parkIsAccessible`, `parkIsQuality`, `parkDistanceUid`, `electricityIsAffordable`, `electricityIsAccessible`, `electricityIsQuality`, `electricityDistanceUid`, `gasIsAffordable`, `gasIsAccessible`, `gasIsQuality`, `gasDistanceUid`, `libraryIsAffordable`, `libraryIsAccessible`, `libraryIsQuality`, `libraryDistanceUid`, `cinemaIsAffordable`, `cinemaIsAccessible`, `cinemaIsQuality`, `cinemaDistanceUid`, `transportationIsAffordable`, `transportationIsAccessible`, `transportationIsQuality`, `transportationDistanceUid`, `telephoneIsAffordable`, `telephoneIsAccessible`, `telephoneIsQuality`, `telephoneDistanceUid`, `isRoadsafty`, `isUnsafeHouse`, `isNaturalDisaster`, `isLawOrder`, `isInappropriatePolitical`, `isChildAbuse`, `isReligiousIntolerance`, `isLandGrabbing`, `isRansom`, `isRiskBusinessWindup`, `formLastUpdatedByUser`, `formAddedByUser`, `formFillStatus`, `formPostStatus`) VALUES
(6, 1, NULL, '', '', '', 0, '', '', NULL, 0, '', 0, NULL, 8, 2, 0, 0, 0, 3, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '0000-00-00', 0, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 0),
(7, 1, NULL, '', '', '', 0, '', '', NULL, 0, '', 0, NULL, 0, 0, 0, 0, 0, 2, '', '', '', '', '', '', '', '', 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, '', '', '', '0000-00-00', 0, '', '', NULL, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, NULL, NULL, 0, 1);

-- --------------------------------------------------------

--
-- Table structure for table `jamatkhana`
--

CREATE TABLE IF NOT EXISTS `jamatkhana` (
  `uid` int(11) unsigned NOT NULL,
  `regionalCouncilUid` int(11) unsigned DEFAULT NULL,
  `localCouncilUid` int(11) unsigned DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `regionalCouncilUid` (`regionalCouncilUid`),
  KEY `localCouncilUid` (`localCouncilUid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `jamatkhana`
--

INSERT INTO `jamatkhana` (`uid`, `regionalCouncilUid`, `localCouncilUid`, `label`, `uidLabel`) VALUES
(1, 6, 25, 'DANYORE1a', 'DANYORE1a'),
(2, 6, 25, 'DANYORE10', 'DANYORE10'),
(3, 6, 25, 'DANYORE11', 'DANYORE11'),
(4, 6, 25, 'DANYORE12', 'DANYORE12'),
(5, 6, 25, 'DANYORE2', 'DANYORE2'),
(6, 6, 25, 'DANYORE3', 'DANYORE3'),
(7, 6, 25, 'DANYORE4', 'DANYORE4'),
(8, 6, 25, 'DANYORE5', 'DANYORE5'),
(9, 6, 25, 'DANYORE6', 'DANYORE6'),
(10, 6, 25, 'DANYORE7', 'DANYORE7'),
(11, 6, 25, 'DANYORE8', 'DANYORE8'),
(12, 6, 25, 'DANYORE9', 'DANYORE9'),
(13, 6, 25, 'OSHIKHANDAS1', 'OSHIKHANDAS1'),
(14, 6, 25, 'OSHIKHANDAS2', 'OSHIKHANDAS2'),
(15, 6, 25, 'OSHIKHANDAS3', 'OSHIKHANDAS3'),
(16, 6, 25, 'OSHIKHANDAS4', 'OSHIKHANDAS4'),
(17, 6, 25, 'OSHIKHANDAS5', 'OSHIKHANDAS5'),
(18, 6, 25, 'OSHIKHANDAS6', 'OSHIKHANDAS6'),
(19, 6, 25, 'OSHIKHANDAS7', 'OSHIKHANDAS7'),
(20, 6, 25, 'OSHIKHANDAS8', 'OSHIKHANDAS8'),
(21, 6, 25, 'RAHIMABAD1', 'RAHIMABAD1'),
(22, 6, 25, 'RAHIMABAD2', 'RAHIMABAD2'),
(23, 6, 25, 'RAHIMABAD3', 'RAHIMABAD3'),
(24, 6, 25, 'RAHIMABAD4', 'RAHIMABAD4'),
(25, 6, 25, 'SULTANABAD1', 'SULTANABAD1'),
(26, 6, 25, 'SULTANABAD2', 'SULTANABAD2'),
(27, 6, 25, 'SULTANABAD3', 'SULTANABAD3'),
(28, 6, 25, 'SULTANABAD4', 'SULTANABAD4'),
(29, 6, 24, 'AMYNABAD Jutiyal', 'AMYNABAD Jutiyal'),
(30, 6, 24, 'AMYNABAD1 NOMAL', 'AMYNABAD1 NOMAL'),
(31, 6, 24, 'AMYNABAD2 NOMAL', 'AMYNABAD2 NOMAL'),
(32, 6, 24, 'Baladul Karim JK', 'Baladul Karim JK'),
(33, 6, 24, 'GILGIT Central', 'GILGIT Central'),
(34, 6, 24, 'Hussainabad', 'Hussainabad'),
(35, 6, 24, 'JUTIYAL2 UPPER', 'JUTIYAL2 UPPER'),
(36, 6, 24, 'KHOMER', 'KHOMER'),
(37, 6, 24, 'MADINATUL KARIM Nomal-4', 'MADINATUL KARIM Nomal-4'),
(38, 6, 24, 'MUJAHID COLONY', 'MUJAHID COLONY'),
(39, 6, 24, 'NOOR COLONY', 'NOOR COLONY'),
(40, 6, 24, 'NOORABAD', 'NOORABAD'),
(41, 6, 24, 'SADRUDDINABAD Nomal-3', 'SADRUDDINABAD Nomal-3'),
(42, 6, 24, 'Satellite Colony', 'Satellite Colony'),
(43, 6, 24, 'SONIKOTE', 'SONIKOTE'),
(44, 6, 24, 'YASIN COLONY', 'YASIN COLONY'),
(45, 6, 24, 'ZULFIQARABAD', 'ZULFIQARABAD'),
(46, 6, 26, 'KHAPLU, Baltistan', 'KHAPLU, Baltistan'),
(47, 6, 26, 'SATELLITE TOWN Skardu', 'SATELLITE TOWN Skardu'),
(48, 6, 26, 'SKARDU', 'SKARDU'),
(49, 8, 36, 'ALYABAD Hundrap-3', 'ALYABAD Hundrap-3'),
(50, 8, 36, 'BARSET-1', 'BARSET-1'),
(51, 8, 36, 'BARSET-2', 'BARSET-2'),
(52, 8, 36, 'BARSET-3', 'BARSET-3'),
(53, 8, 36, 'BARSET-4 Brundan', 'BARSET-4 Brundan'),
(54, 8, 36, 'BARSET-5 Akhtobaring', 'BARSET-5 Akhtobaring'),
(55, 8, 36, 'Barset-6 Uthali', 'Barset-6 Uthali'),
(56, 8, 36, 'BOYANDEH', 'BOYANDEH'),
(57, 8, 36, 'GHOLAGHMULLI Central', 'GHOLAGHMULLI Central'),
(58, 8, 36, 'GHOLAGHTORI', 'GHOLAGHTORI'),
(59, 8, 36, 'HELTY', 'HELTY'),
(60, 8, 36, 'HUNDRAP-1 Paien', 'HUNDRAP-1 Paien'),
(61, 8, 36, 'HUNDRAP-2 Bala', 'HUNDRAP-2 Bala'),
(62, 8, 36, 'HUSSAINABAD', 'HUSSAINABAD'),
(63, 8, 36, 'KARIMABAD Teru-4', 'KARIMABAD Teru-4'),
(64, 8, 36, 'KHONANDEH', 'KHONANDEH'),
(65, 8, 36, 'SHAHIMAL', 'SHAHIMAL'),
(66, 8, 36, 'TERECH', 'TERECH'),
(67, 8, 36, 'TERU-1 Bahachi', 'TERU-1 Bahachi'),
(68, 8, 36, 'TERU-2 Paien', 'TERU-2 Paien'),
(69, 8, 36, 'TERU-3 Bala', 'TERU-3 Bala'),
(70, 8, 33, 'ALi BASTI', 'ALi BASTI'),
(71, 8, 33, 'ALYABAD', 'ALYABAD'),
(72, 8, 33, 'CHIRMAYUN', 'CHIRMAYUN'),
(73, 8, 33, 'DALTI', 'DALTI'),
(74, 8, 33, 'DODOSHOT', 'DODOSHOT'),
(75, 8, 33, 'DROTE', 'DROTE'),
(76, 8, 33, 'DURNAK', 'DURNAK'),
(77, 8, 33, 'GAMIS', 'GAMIS'),
(78, 8, 33, 'GUPIS Central', 'GUPIS Central'),
(79, 8, 33, 'GUPIS-1 Daas', 'GUPIS-1 Daas'),
(80, 8, 33, 'GUPIS2 BALA', 'GUPIS2 BALA'),
(81, 8, 33, 'HAKIS', 'HAKIS'),
(82, 8, 33, 'HAMARDAS', 'HAMARDAS'),
(83, 8, 33, 'KARIMABAD-1 Sub Centre', 'KARIMABAD-1 Sub Centre'),
(84, 8, 33, 'KARIMABAD-2 Bala', 'KARIMABAD-2 Bala'),
(85, 8, 33, 'KARIMABAD3 PAEEN', 'KARIMABAD3 PAEEN'),
(86, 8, 33, 'KASHBAT HAJIABAD', 'KASHBAT HAJIABAD'),
(87, 8, 33, 'KHALTI', 'KHALTI'),
(88, 8, 33, 'MAULAABAD', 'MAULAABAD'),
(89, 8, 33, 'NOORABAD', 'NOORABAD'),
(90, 8, 33, 'RAUSHAN', 'RAUSHAN'),
(91, 8, 33, 'SUMAL CENTRE', 'SUMAL CENTRE'),
(92, 8, 33, 'TALIDAS', 'TALIDAS'),
(93, 8, 33, 'YANGAL1 PAEEN', 'YANGAL1 PAEEN'),
(94, 8, 33, 'YANGAL2 BALA', 'YANGAL2 BALA'),
(95, 8, 33, 'Zulfiqar Abad', 'Zulfiqar Abad'),
(96, 8, 35, 'ALYABAD Anotek', 'ALYABAD Anotek'),
(97, 8, 35, 'ALYABAD Shamaran-4', 'ALYABAD Shamaran-4'),
(98, 8, 35, 'BEREST', 'BEREST'),
(99, 8, 35, 'CHASHI-1 Bala', 'CHASHI-1 Bala'),
(100, 8, 35, 'CHASHI-2 Paien', 'CHASHI-2 Paien'),
(101, 8, 35, 'CHASHI-3 Tologh', 'CHASHI-3 Tologh'),
(102, 8, 35, 'DALOMAL', 'DALOMAL'),
(103, 8, 35, 'DALOMAL - 2', 'DALOMAL - 2'),
(104, 8, 35, 'DERBARKOLTI-1 Paien', 'DERBARKOLTI-1 Paien'),
(105, 8, 35, 'DERBARKOLTI-2 Bala', 'DERBARKOLTI-2 Bala'),
(106, 8, 35, 'GHOLOGH', 'GHOLOGH'),
(107, 8, 35, 'JAFFARABAD Joharbabad', 'JAFFARABAD Joharbabad'),
(108, 8, 35, 'KINOTE', 'KINOTE'),
(109, 8, 35, 'MOMEENABAD Shamaran-5', 'MOMEENABAD Shamaran-5'),
(110, 8, 35, 'PHUNDER Central', 'PHUNDER Central'),
(111, 8, 35, 'RAHIMABAD Derberkulti-3', 'RAHIMABAD Derberkulti-3'),
(112, 8, 35, 'RAWAT - 2', 'RAWAT - 2'),
(113, 8, 35, 'RAWAT Shamaran-3', 'RAWAT Shamaran-3'),
(114, 8, 35, 'SERBAL', 'SERBAL'),
(115, 8, 35, 'SHAHABAD', 'SHAHABAD'),
(116, 8, 35, 'SHAMARAN-1', 'SHAMARAN-1'),
(117, 8, 35, 'SHAMARAN-2', 'SHAMARAN-2'),
(118, 8, 35, 'SHINGLAT', 'SHINGLAT'),
(119, 8, 34, 'DAHIMAL', 'DAHIMAL'),
(120, 8, 34, 'HAMER Rahimabad', 'HAMER Rahimabad'),
(121, 8, 34, 'JIKARUM', 'JIKARUM'),
(122, 8, 34, 'JOLIJAL', 'JOLIJAL'),
(123, 8, 34, 'KHALTI - 2 Paien', 'KHALTI - 2 Paien'),
(124, 8, 34, 'KHASUNDER', 'KHASUNDER'),
(125, 8, 34, 'MATUJ LASHT', 'MATUJ LASHT'),
(126, 8, 34, 'NOGHOOR LASHT', 'NOGHOOR LASHT'),
(127, 8, 34, 'NOLTY', 'NOLTY'),
(128, 8, 34, 'PAYUKHUSH', 'PAYUKHUSH'),
(129, 8, 34, 'PINGAL', 'PINGAL'),
(130, 8, 34, 'SARALO KHOTU', 'SARALO KHOTU'),
(131, 8, 34, 'SOSOT', 'SOSOT'),
(132, 8, 34, 'THANGAI Sub-Centre', 'THANGAI Sub-Centre'),
(133, 8, 39, 'BERKULTEY-1', 'BERKULTEY-1'),
(134, 8, 39, 'BERKULTEY-2', 'BERKULTEY-2'),
(135, 8, 39, 'BERKULTEY-3', 'BERKULTEY-3'),
(136, 8, 39, 'BERKULTEY-4', 'BERKULTEY-4'),
(137, 8, 39, 'BERKULTEY-5', 'BERKULTEY-5'),
(138, 8, 39, 'BERKULTEY-6', 'BERKULTEY-6'),
(139, 8, 39, 'BERKULTEY-7 Shaghitan', 'BERKULTEY-7 Shaghitan'),
(140, 8, 39, 'BERKULTEY-8', 'BERKULTEY-8'),
(141, 8, 39, 'DARKUT-1', 'DARKUT-1'),
(142, 8, 39, 'DARKUT-2', 'DARKUT-2'),
(143, 8, 39, 'DARKUT-3', 'DARKUT-3'),
(144, 8, 39, 'DARKUT-4 Gamelti', 'DARKUT-4 Gamelti'),
(145, 8, 39, 'DARKUT-5 Gartenz', 'DARKUT-5 Gartenz'),
(146, 8, 39, 'DARKUT-6 Ghasum', 'DARKUT-6 Ghasum'),
(147, 8, 39, 'HUNDUR Central', 'HUNDUR Central'),
(148, 8, 39, 'HUNDUR-2', 'HUNDUR-2'),
(149, 8, 39, 'HUNDUR-3', 'HUNDUR-3'),
(150, 8, 39, 'HUNDUR-4', 'HUNDUR-4'),
(151, 8, 39, 'HUNDUR-5', 'HUNDUR-5'),
(152, 8, 39, 'HUNDUR-6 Burakot', 'HUNDUR-6 Burakot'),
(153, 8, 39, 'HUNDUR-7 Terset', 'HUNDUR-7 Terset'),
(154, 8, 39, 'ISTORDANI', 'ISTORDANI'),
(155, 8, 39, 'UMALSAT-1', 'UMALSAT-1'),
(156, 8, 39, 'UMALSAT-2', 'UMALSAT-2'),
(157, 8, 38, 'CHIKMOJOOR', 'CHIKMOJOOR'),
(158, 8, 38, 'DALSANDEY', 'DALSANDEY'),
(159, 8, 38, 'GOJALTI-1', 'GOJALTI-1'),
(160, 8, 38, 'GOJALTI-2', 'GOJALTI-2'),
(161, 8, 38, 'MAWLAABAD Sandey-4', 'MAWLAABAD Sandey-4'),
(162, 8, 38, 'QARKULTEY-1 Paien', 'QARKULTEY-1 Paien'),
(163, 8, 38, 'QARKULTEY-2 Bala', 'QARKULTEY-2 Bala'),
(164, 8, 38, 'QARKULTEY-3 Bardass', 'QARKULTEY-3 Bardass'),
(165, 8, 38, 'SANDEY - 1', 'SANDEY - 1'),
(166, 8, 38, 'SANDEY - 2 Paien', 'SANDEY - 2 Paien'),
(167, 8, 38, 'SANDEY - 3', 'SANDEY - 3'),
(168, 8, 38, 'SANDEY - 5', 'SANDEY - 5'),
(169, 8, 38, 'SULTANABAD-1 SubCentre', 'SULTANABAD-1 SubCentre'),
(170, 8, 38, 'SULTANABAD-2', 'SULTANABAD-2'),
(171, 8, 38, 'SULTANABAD-3', 'SULTANABAD-3'),
(172, 8, 38, 'SULTANABAD-4', 'SULTANABAD-4'),
(173, 8, 38, 'TAOUS-1', 'TAOUS-1'),
(174, 8, 38, 'TAOUS-2', 'TAOUS-2'),
(175, 8, 38, 'TAOUS-3', 'TAOUS-3'),
(176, 8, 38, 'TAOUS-4', 'TAOUS-4'),
(177, 8, 38, 'TAOUS-5', 'TAOUS-5'),
(178, 8, 38, 'TAOUS-6', 'TAOUS-6'),
(179, 8, 38, 'TAOUS-7', 'TAOUS-7'),
(180, 8, 40, 'CHIRYAT', 'CHIRYAT'),
(181, 8, 40, 'DALKOI', 'DALKOI'),
(182, 8, 40, 'DAPIS', 'DAPIS'),
(183, 8, 40, 'DASS-1 Paien', 'DASS-1 Paien'),
(184, 8, 40, 'DASS-2 Bala', 'DASS-2 Bala'),
(185, 8, 40, 'DRACH', 'DRACH'),
(186, 8, 40, 'DRASKIN', 'DRASKIN'),
(187, 8, 40, 'GHAINCHEL', 'GHAINCHEL'),
(188, 8, 40, 'HARF-2 Bala', 'HARF-2 Bala'),
(189, 8, 40, 'HARF-3 Paien', 'HARF-3 Paien'),
(190, 8, 40, 'ISHKAIBAR', 'ISHKAIBAR'),
(191, 8, 40, 'ISHKAMDAS', 'ISHKAMDAS'),
(192, 8, 40, 'KARIMABAD', 'KARIMABAD'),
(193, 8, 40, 'KUNO', 'KUNO'),
(194, 8, 40, 'NALTY - 1', 'NALTY - 1'),
(195, 8, 40, 'NALTY -2', 'NALTY -2'),
(196, 8, 40, 'QADAMABAD', 'QADAMABAD'),
(197, 8, 40, 'RAHIMABAD', 'RAHIMABAD'),
(198, 8, 40, 'SHOTE', 'SHOTE'),
(199, 8, 40, 'THELTI', 'THELTI'),
(200, 8, 40, 'THOI Central Harf-1', 'THOI Central Harf-1'),
(201, 8, 37, 'ATKASH', 'ATKASH'),
(202, 8, 37, 'BARKHAI', 'BARKHAI'),
(203, 8, 37, 'BOJAYOUT', 'BOJAYOUT'),
(204, 8, 37, 'DAMALGUN', 'DAMALGUN'),
(205, 8, 37, 'Gindai Bala', 'Gindai Bala'),
(206, 8, 37, 'GINDAI-1', 'GINDAI-1'),
(207, 8, 37, 'GINDAI-2', 'GINDAI-2'),
(208, 8, 37, 'HELTER', 'HELTER'),
(209, 8, 37, 'MAHSHER', 'MAHSHER'),
(210, 8, 37, 'MURKAH', 'MURKAH'),
(211, 8, 37, 'NAZBAR-1', 'NAZBAR-1'),
(212, 8, 37, 'NAZBAR-2', 'NAZBAR-2'),
(213, 8, 37, 'NAZBAR-3', 'NAZBAR-3'),
(214, 8, 37, 'NAZBAR-4', 'NAZBAR-4'),
(215, 8, 37, 'NOOH-1 Bala', 'NOOH-1 Bala'),
(216, 8, 37, 'NOOH-2 Paien', 'NOOH-2 Paien'),
(217, 8, 37, 'YASIN Central', 'YASIN Central'),
(218, 8, 37, 'YASIN-2', 'YASIN-2'),
(219, 8, 37, 'YASIN-3', 'YASIN-3'),
(220, 8, 37, 'YASIN-4 Manich', 'YASIN-4 Manich'),
(221, 5, 18, 'AGAKHANABAD', 'AGAKHANABAD'),
(222, 5, 18, 'ALYABAD Central', 'ALYABAD Central'),
(223, 5, 18, 'BERBER', 'BERBER'),
(224, 5, 18, 'BROONGSHAL', 'BROONGSHAL'),
(225, 5, 18, 'CHUMER KHAND', 'CHUMER KHAND'),
(226, 5, 18, 'DOOR KHUN', 'DOOR KHUN'),
(227, 5, 18, 'FARMANABAD MURTAZAABAD-3', 'FARMANABAD MURTAZAABAD-3'),
(228, 5, 18, 'GARELTH', 'GARELTH'),
(229, 5, 18, 'HASSANABAD1', 'HASSANABAD1'),
(230, 5, 18, 'HASSANABAD2', 'HASSANABAD2'),
(231, 5, 18, 'HASSANABAD3', 'HASSANABAD3'),
(232, 5, 18, 'HASSANABAD4', 'HASSANABAD4'),
(233, 5, 18, 'HYDERABAD Central', 'HYDERABAD Central'),
(234, 5, 18, 'KHURU KHUSHAL', 'KHURU KHUSHAL'),
(235, 5, 18, 'MURTAZAABAD1', 'MURTAZAABAD1'),
(236, 5, 18, 'MURTAZAABAD2', 'MURTAZAABAD2'),
(237, 5, 18, 'MURTAZAABAD4', 'MURTAZAABAD4'),
(238, 5, 18, 'RAHIMABAD', 'RAHIMABAD'),
(239, 5, 18, 'SHAHABAD', 'SHAHABAD'),
(240, 5, 18, 'SHERAZ', 'SHERAZ'),
(241, 5, 18, 'SINA KHAN', 'SINA KHAN'),
(242, 5, 18, 'SULTANABAD', 'SULTANABAD'),
(243, 5, 17, 'AGA KHAN TEKKERY Doiker', 'AGA KHAN TEKKERY Doiker'),
(244, 5, 17, 'AHMEDABAD1 PAYEEN', 'AHMEDABAD1 PAYEEN'),
(245, 5, 17, 'AHMEDABAD2 BALA', 'AHMEDABAD2 BALA'),
(246, 5, 17, 'ALMUSTAFABAD  BRUMBUN', 'ALMUSTAFABAD  BRUMBUN'),
(247, 5, 17, 'ALTIT Central', 'ALTIT Central'),
(248, 5, 17, 'AMYNABAD', 'AMYNABAD'),
(249, 5, 17, 'ATTAABAD1 BALA', 'ATTAABAD1 BALA'),
(250, 5, 17, 'ATTAABAD2 PAYEEN', 'ATTAABAD2 PAYEEN'),
(251, 5, 17, 'BARASHAL', 'BARASHAL'),
(252, 5, 17, 'BROONGSHAL', 'BROONGSHAL'),
(253, 5, 17, 'CHOBI KHUSHAL', 'CHOBI KHUSHAL'),
(254, 5, 17, 'EMANABAD', 'EMANABAD'),
(255, 5, 17, 'FAIZABAD', 'FAIZABAD'),
(256, 5, 17, 'GANESH Kalan', 'GANESH Kalan'),
(257, 5, 17, 'GARA BEREZ', 'GARA BEREZ'),
(258, 5, 17, 'GHAMESH', 'GHAMESH'),
(259, 5, 17, 'HERCHI', 'HERCHI'),
(260, 5, 17, 'IMAMYARABAD', 'IMAMYARABAD'),
(261, 5, 17, 'KARIMABAD Central', 'KARIMABAD Central'),
(262, 5, 17, 'KARIMABAD2', 'KARIMABAD2'),
(263, 5, 17, 'MOHAMMEDABAD', 'MOHAMMEDABAD'),
(264, 5, 17, 'MOMINABAD', 'MOMINABAD'),
(265, 5, 17, 'RAHIMABAD', 'RAHIMABAD'),
(266, 5, 17, 'SALMANABAD1 BALA', 'SALMANABAD1 BALA'),
(267, 5, 17, 'SALMANABAD-2 Paien', 'SALMANABAD-2 Paien'),
(268, 5, 17, 'SARAT SUB CENTRE', 'SARAT SUB CENTRE'),
(269, 5, 17, 'SHANO KHUSHAL', 'SHANO KHUSHAL'),
(270, 5, 17, 'SULTANABAD', 'SULTANABAD'),
(271, 5, 17, 'ZIARAT Astana', 'ZIARAT Astana'),
(272, 5, 23, 'AMYNABAD Yezerich', 'AMYNABAD Yezerich'),
(273, 5, 23, 'ISPANGE', 'ISPANGE'),
(274, 5, 23, 'KHAIRABAD', 'KHAIRABAD'),
(275, 5, 23, 'KHIL1', 'KHIL1'),
(276, 5, 23, 'KHIL-2 Chupersan', 'KHIL-2 Chupersan'),
(277, 5, 23, 'KIRMIN1 RAHIMABAD', 'KIRMIN1 RAHIMABAD'),
(278, 5, 23, 'KIRMIN2 AMYNABAD', 'KIRMIN2 AMYNABAD'),
(279, 5, 23, 'KIRMIN3 NOORABAD', 'KIRMIN3 NOORABAD'),
(280, 5, 23, 'RESHIT Central', 'RESHIT Central'),
(281, 5, 23, 'SHAR-E-SABZ', 'SHAR-E-SABZ'),
(282, 5, 23, 'SHET MERG', 'SHET MERG'),
(283, 5, 23, 'ZOOD KHUN', 'ZOOD KHUN'),
(284, 5, 20, 'AYEENABAD', 'AYEENABAD'),
(285, 5, 20, 'BORETH1 PAEEN', 'BORETH1 PAEEN'),
(286, 5, 20, 'BORETH-2 Bala', 'BORETH-2 Bala'),
(287, 5, 20, 'CHAMAN GUL', 'CHAMAN GUL'),
(288, 5, 20, 'DALGIRAM', 'DALGIRAM'),
(289, 5, 20, 'GHULKIN', 'GHULKIN'),
(290, 5, 20, 'GOZE', 'GOZE'),
(291, 5, 20, 'GULMIT Central', 'GULMIT Central'),
(292, 5, 20, 'HUSSAINI', 'HUSSAINI'),
(293, 5, 20, 'KAMARIS', 'KAMARIS'),
(294, 5, 20, 'MURADABAD', 'MURADABAD'),
(295, 5, 20, 'NAZIMABAD1 BALA', 'NAZIMABAD1 BALA'),
(296, 5, 20, 'NAZIMABAD2 DARMIYAN', 'NAZIMABAD2 DARMIYAN'),
(297, 5, 20, 'NAZIMABAD3 PAEEN', 'NAZIMABAD3 PAEEN'),
(298, 5, 20, 'PASSU Janabad', 'PASSU Janabad'),
(299, 5, 20, 'UDBER', 'UDBER'),
(300, 5, 19, 'HOON KOOT', 'HOON KOOT'),
(301, 5, 19, 'HUSSAINABAD1', 'HUSSAINABAD1'),
(302, 5, 19, 'HUSSAINABAD2', 'HUSSAINABAD2'),
(303, 5, 19, 'HUSSAINABAD3', 'HUSSAINABAD3'),
(304, 5, 19, 'HUSSAINABAD4', 'HUSSAINABAD4'),
(305, 5, 19, 'KHANABAD1', 'KHANABAD1'),
(306, 5, 19, 'KHANABAD2 TEKRY', 'KHANABAD2 TEKRY'),
(307, 5, 19, 'KHANABAD3 MASHRAQI', 'KHANABAD3 MASHRAQI'),
(308, 5, 19, 'KHIZARABAD', 'KHIZARABAD'),
(309, 5, 19, 'MAYOON', 'MAYOON'),
(310, 5, 19, 'NASIRABAD CENTRAL', 'NASIRABAD CENTRAL'),
(311, 5, 19, 'NASIRABAD-2 Bala', 'NASIRABAD-2 Bala'),
(312, 5, 19, 'REHMATABAD', 'REHMATABAD'),
(313, 5, 21, 'AMYNABAD Shemshal', 'AMYNABAD Shemshal'),
(314, 5, 21, 'FARMANABAD Shemshal', 'FARMANABAD Shemshal'),
(315, 5, 21, 'KHIZARABAD Shemshal', 'KHIZARABAD Shemshal'),
(316, 5, 21, 'SHEMSHAL Sub-Centre', 'SHEMSHAL Sub-Centre'),
(317, 5, 22, 'ABGARCH', 'ABGARCH'),
(318, 5, 22, 'AMYNABAD Sost Paien', 'AMYNABAD Sost Paien'),
(319, 5, 22, 'DHI KUNJERAB', 'DHI KUNJERAB'),
(320, 5, 22, 'GALAPAN', 'GALAPAN'),
(321, 5, 22, 'GIRCHA', 'GIRCHA'),
(322, 5, 22, 'HUSSAINABAD', 'HUSSAINABAD'),
(323, 5, 22, 'JAMALABAD', 'JAMALABAD'),
(324, 5, 22, 'KHUDAABAD Central', 'KHUDAABAD Central'),
(325, 5, 22, 'KHUDAABAD2 KARIMABAD', 'KHUDAABAD2 KARIMABAD'),
(326, 5, 22, 'KHUDAABAD3 IMAMABAD', 'KHUDAABAD3 IMAMABAD'),
(327, 5, 22, 'KHYBER1', 'KHYBER1'),
(328, 5, 22, 'KHYBER-2 Imamabad', 'KHYBER-2 Imamabad'),
(329, 5, 22, 'MISGAR1', 'MISGAR1'),
(330, 5, 22, 'MISGAR2', 'MISGAR2'),
(331, 5, 22, 'MOORKHUN', 'MOORKHUN'),
(332, 5, 22, 'NAZIMABAD Sost Bala', 'NAZIMABAD Sost Bala'),
(333, 5, 22, 'SERTEZ', 'SERTEZ'),
(334, 7, 27, 'BUBAR4 CENTER', 'BUBAR4 CENTER'),
(335, 7, 27, 'BUBUR1 PAIEN', 'BUBUR1 PAIEN'),
(336, 7, 27, 'BUBUR-2 Bala', 'BUBUR-2 Bala'),
(337, 7, 27, 'GEECH', 'GEECH'),
(338, 7, 27, 'GOHARABAD', 'GOHARABAD'),
(339, 7, 27, 'GULMUTI', 'GULMUTI'),
(340, 7, 27, 'GURUNJER1 PAIEN', 'GURUNJER1 PAIEN'),
(341, 7, 27, 'GURUNJER2 BALA', 'GURUNJER2 BALA'),
(342, 7, 27, 'GURUNJER-3 Bala', 'GURUNJER-3 Bala'),
(343, 7, 27, 'HUSSAINABAD Bubur-3 Bala', 'HUSSAINABAD Bubur-3 Bala'),
(344, 7, 27, 'KUTOOM1', 'KUTOOM1'),
(345, 7, 27, 'KUTOOM2', 'KUTOOM2'),
(346, 7, 27, 'SINGAL Central', 'SINGAL Central'),
(347, 7, 27, 'THINGDAS', 'THINGDAS'),
(348, 7, 30, 'AMYNABAD', 'AMYNABAD'),
(349, 7, 30, 'BIRGALE', 'BIRGALE'),
(350, 7, 30, 'CHATOORKHAND Central', 'CHATOORKHAND Central'),
(351, 7, 30, 'CHATOORKHAND PAEEN', 'CHATOORKHAND PAEEN'),
(352, 7, 30, 'CHATOORKHAND-3', 'CHATOORKHAND-3'),
(353, 7, 30, 'CHINAR', 'CHINAR'),
(354, 7, 30, 'DAIEN - 6 Paien', 'DAIEN - 6 Paien'),
(355, 7, 30, 'DAIEN1 PAEEN', 'DAIEN1 PAEEN'),
(356, 7, 30, 'DAIEN2 BALA', 'DAIEN2 BALA'),
(357, 7, 30, 'DAIEN3 GOCHAR', 'DAIEN3 GOCHAR'),
(358, 7, 30, 'DAIEN4 DOOK', 'DAIEN4 DOOK'),
(359, 7, 30, 'DAIEN5 GOOL', 'DAIEN5 GOOL'),
(360, 7, 30, 'Dain Sholja', 'Dain Sholja'),
(361, 7, 30, 'FAMANI', 'FAMANI'),
(362, 7, 30, 'KOCHDEH', 'KOCHDEH'),
(363, 7, 30, 'KOCHDEH - 2', 'KOCHDEH - 2'),
(364, 7, 30, 'MAWLAABAD, Daien', 'MAWLAABAD, Daien'),
(365, 7, 30, 'PAKHORA1', 'PAKHORA1'),
(366, 7, 30, 'PAKHORA2', 'PAKHORA2'),
(367, 7, 30, 'PAKORA - 3', 'PAKORA - 3'),
(368, 7, 30, 'RAHIMABAD Shunas', 'RAHIMABAD Shunas'),
(369, 7, 29, 'AIYESH1', 'AIYESH1'),
(370, 7, 29, 'AIYESH2', 'AIYESH2'),
(371, 7, 29, 'AIYESH3', 'AIYESH3'),
(372, 7, 29, 'ANWERABAD', 'ANWERABAD'),
(373, 7, 29, 'DAMAS Central', 'DAMAS Central'),
(374, 7, 29, 'DAMAS2 BALA', 'DAMAS2 BALA'),
(375, 7, 29, 'GAHKUCH1 BALA', 'GAHKUCH1 BALA'),
(376, 7, 29, 'GAHKUCH2 BALA', 'GAHKUCH2 BALA'),
(377, 7, 29, 'GAHKUCH3 PAEEN', 'GAHKUCH3 PAEEN'),
(378, 7, 29, 'GAHKUCH4 PAEEN', 'GAHKUCH4 PAEEN'),
(379, 7, 29, 'GAHKUCH5 BALA', 'GAHKUCH5 BALA'),
(380, 7, 29, 'HASIS1 PAIEN', 'HASIS1 PAIEN'),
(381, 7, 29, 'HASIS2 BALA', 'HASIS2 BALA'),
(382, 7, 29, 'HASIS2 BIDIRI KHARI', 'HASIS2 BIDIRI KHARI'),
(383, 7, 29, 'HATOON1', 'HATOON1'),
(384, 7, 29, 'HATOON2 SUBCENTRE', 'HATOON2 SUBCENTRE'),
(385, 7, 29, 'HATOON3', 'HATOON3'),
(386, 7, 29, 'HATOON4 DASS', 'HATOON4 DASS'),
(387, 7, 29, 'HATOON5 MAYOON', 'HATOON5 MAYOON'),
(388, 7, 29, 'JAMALABAD', 'JAMALABAD'),
(389, 7, 29, 'JK 1 Golodass', 'JK 1 Golodass'),
(390, 7, 29, 'JK 2 Golodass', 'JK 2 Golodass'),
(391, 7, 29, 'KUTOOLKEY', 'KUTOOLKEY'),
(392, 7, 29, 'NOORABAD Damas', 'NOORABAD Damas'),
(393, 7, 29, 'SARAYOT', 'SARAYOT'),
(394, 7, 29, 'SILPI1', 'SILPI1'),
(395, 7, 29, 'SILPI2', 'SILPI2'),
(396, 7, 32, 'ALYABAD', 'ALYABAD'),
(397, 7, 32, 'BARSWAT', 'BARSWAT'),
(398, 7, 32, 'BAZAR KUTOO', 'BAZAR KUTOO'),
(399, 7, 32, 'BILHANZ-1', 'BILHANZ-1'),
(400, 7, 32, 'BILHANZ-2', 'BILHANZ-2'),
(401, 7, 32, 'BOARTH', 'BOARTH'),
(402, 7, 32, 'BOOQ SHAMSABAD', 'BOOQ SHAMSABAD'),
(403, 7, 32, 'DOWERDAS', 'DOWERDAS'),
(404, 7, 32, 'GISH GISH1', 'GISH GISH1'),
(405, 7, 32, 'GISH GISH2', 'GISH GISH2'),
(406, 7, 32, 'IMMIT Central', 'IMMIT Central'),
(407, 7, 32, 'KUNJ Ganjabad', 'KUNJ Ganjabad'),
(408, 7, 32, 'MOHTRAM DAS', 'MOHTRAM DAS'),
(409, 7, 32, 'MUJAWAR', 'MUJAWAR'),
(410, 7, 32, 'NASIRABAD', 'NASIRABAD'),
(411, 7, 32, 'NAUBAHAR', 'NAUBAHAR'),
(412, 7, 32, 'RAHIMABAD Bar Jungle', 'RAHIMABAD Bar Jungle'),
(413, 7, 32, 'REHMATABAD-1 Mujawar', 'REHMATABAD-1 Mujawar'),
(414, 7, 32, 'REHMATABAD-2 Paien', 'REHMATABAD-2 Paien'),
(415, 7, 32, 'SHAMSABAD Kakunekay', 'SHAMSABAD Kakunekay'),
(416, 7, 32, 'SULTANABAD', 'SULTANABAD'),
(417, 7, 32, 'TESHNALOT', 'TESHNALOT'),
(418, 7, 32, 'TUNG SHAMSABAD (Seasonal)', 'TUNG SHAMSABAD (Seasonal)'),
(419, 7, 31, 'ALYBASTI TUSHKIN - 2', 'ALYBASTI TUSHKIN - 2'),
(420, 7, 31, 'DALTI', 'DALTI'),
(421, 7, 31, 'DAWOODABAD', 'DAWOODABAD'),
(422, 7, 31, 'FAIZABAD', 'FAIZABAD'),
(423, 7, 31, 'FAIZABAD - 2 Bala', 'FAIZABAD - 2 Bala'),
(424, 7, 31, 'ISKHOMAN Central', 'ISKHOMAN Central'),
(425, 7, 31, 'JALALABAD', 'JALALABAD'),
(426, 7, 31, 'MOMINABAD', 'MOMINABAD'),
(427, 7, 31, 'RAHIMABAD Gotolti', 'RAHIMABAD Gotolti'),
(428, 7, 31, 'SHENIKEY', 'SHENIKEY'),
(429, 7, 31, 'TAPUSHKIN', 'TAPUSHKIN'),
(430, 7, 31, 'TUSHKIN', 'TUSHKIN'),
(431, 7, 28, 'DAS JAPUK AMYNABAD', 'DAS JAPUK AMYNABAD'),
(432, 7, 28, 'DERYANI', 'DERYANI'),
(433, 7, 28, 'HAMUCHAL', 'HAMUCHAL'),
(434, 7, 28, 'JAPUKY', 'JAPUKY'),
(435, 7, 28, 'KARIMABAD Sherqilla-Sub-Centre', 'KARIMABAD Sherqilla-Sub-Centre'),
(436, 7, 28, 'Pray Hall Ghullapur', 'Pray Hall Ghullapur'),
(437, 7, 28, 'SHERQILLA2 HUSSAINABAD', 'SHERQILLA2 HUSSAINABAD'),
(438, 7, 28, 'SHERQILLA3 RAHIMABAD', 'SHERQILLA3 RAHIMABAD'),
(439, 7, 28, 'SHERQILLA4 SULTANABAD', 'SHERQILLA4 SULTANABAD'),
(440, 1, 4, 'GWADAR', 'GWADAR'),
(441, 1, 4, 'ORMARA', 'ORMARA'),
(442, 1, 4, 'PASNI', 'PASNI'),
(443, 1, 1, 'AMYNABAD', 'AMYNABAD'),
(444, 1, 1, 'DARKHANA', 'DARKHANA'),
(445, 1, 1, 'EVERSHINE', 'EVERSHINE'),
(446, 1, 1, 'GULZAR-E-RAHIM', 'GULZAR-E-RAHIM'),
(447, 1, 1, 'KARIM NAGAR', 'KARIM NAGAR'),
(448, 1, 1, 'MALIR', 'MALIR'),
(449, 1, 1, 'NAZIMABAD', 'NAZIMABAD'),
(450, 1, 1, 'NOORABAD', 'NOORABAD'),
(451, 1, 1, 'PASK COLONY', 'PASK COLONY'),
(452, 1, 1, 'PLATINUM', 'PLATINUM'),
(453, 1, 1, 'REHMANI GARDEN', 'REHMANI GARDEN'),
(454, 1, 1, 'SULTANABAD', 'SULTANABAD'),
(455, 1, 2, 'AL AZHAR GARDEN', 'AL AZHAR GARDEN'),
(456, 1, 2, 'AL NOOR', 'AL NOOR'),
(457, 1, 2, 'ALI JIWANI', 'ALI JIWANI'),
(458, 1, 2, 'ALYABAD', 'ALYABAD'),
(459, 1, 2, 'GULSHAN E NOOR', 'GULSHAN E NOOR'),
(460, 1, 2, 'KARIMABAD', 'KARIMABAD'),
(461, 1, 2, 'NIZARI', 'NIZARI'),
(462, 1, 2, 'RAHIMABAD', 'RAHIMABAD'),
(463, 1, 2, 'SALIMAHABAD', 'SALIMAHABAD'),
(464, 1, 3, 'CHASHMA GOTH', 'CHASHMA GOTH'),
(465, 1, 3, 'CLIFTON', 'CLIFTON'),
(466, 1, 3, 'EBRAHIM HAIDERY No. 1', 'EBRAHIM HAIDERY No. 1'),
(467, 1, 3, 'KHADIJAHABAD', 'KHADIJAHABAD'),
(468, 1, 3, 'KHARADAR', 'KHARADAR'),
(469, 1, 3, 'KORANGI', 'KORANGI'),
(470, 1, 3, 'LASSI', 'LASSI'),
(471, 1, 3, 'RANCHORELINE', 'RANCHORELINE'),
(472, 9, 44, 'AFZALABAD', 'AFZALABAD'),
(473, 9, 44, 'ALYABAD Arkari-Central', 'ALYABAD Arkari-Central'),
(474, 9, 44, 'BESTI - 1 Paien', 'BESTI - 1 Paien'),
(475, 9, 44, 'BESTI - 2 Bala', 'BESTI - 2 Bala'),
(476, 9, 44, 'DIR GOOL', 'DIR GOOL'),
(477, 9, 44, 'OWEER-1', 'OWEER-1'),
(478, 9, 44, 'OWEER-2 Lasht', 'OWEER-2 Lasht'),
(479, 9, 44, 'PURPUNI', 'PURPUNI'),
(480, 9, 44, 'RABAT', 'RABAT'),
(481, 9, 44, 'RABAT MOKHI', 'RABAT MOKHI'),
(482, 9, 44, 'RAHIMABAD', 'RAHIMABAD'),
(483, 9, 44, 'SADDAM', 'SADDAM'),
(484, 9, 44, 'SAFEED ARKARI', 'SAFEED ARKARI'),
(485, 9, 44, 'SHOL', 'SHOL'),
(486, 9, 44, 'SHUNJUR GHUCH', 'SHUNJUR GHUCH'),
(487, 9, 44, 'SULTANABAD', 'SULTANABAD'),
(488, 9, 45, 'ALYABAD Amerdin-2', 'ALYABAD Amerdin-2'),
(489, 9, 45, 'AMERDIN', 'AMERDIN'),
(490, 9, 45, 'BASHQIR-3 PaienSub Centre', 'BASHQIR-3 PaienSub Centre'),
(491, 9, 45, 'BEGHUSHT1', 'BEGHUSHT1'),
(492, 9, 45, 'BEGHUSHT2', 'BEGHUSHT2'),
(493, 9, 45, 'BEGHUSHT3', 'BEGHUSHT3'),
(494, 9, 45, 'BEGHUSHT4', 'BEGHUSHT4'),
(495, 9, 45, 'BEGHUSHT5', 'BEGHUSHT5'),
(496, 9, 45, 'BEHMI', 'BEHMI'),
(497, 9, 45, 'BEKHI1', 'BEKHI1'),
(498, 9, 45, 'BEKHI2', 'BEKHI2'),
(499, 9, 45, 'BESHQER1 BALA', 'BESHQER1 BALA'),
(500, 9, 45, 'BESHQER2 BALA', 'BESHQER2 BALA'),
(501, 9, 45, 'BESIRI', 'BESIRI'),
(502, 9, 45, 'BIRZEEN1', 'BIRZEEN1'),
(503, 9, 45, 'BIRZEEN2', 'BIRZEEN2'),
(504, 9, 45, 'BIRZEEN3', 'BIRZEEN3'),
(505, 9, 45, 'BURBUNO', 'BURBUNO'),
(506, 9, 45, 'CHERWIL', 'CHERWIL'),
(507, 9, 45, 'DIRI', 'DIRI'),
(508, 9, 45, 'DROSHP', 'DROSHP'),
(509, 9, 45, 'EIZH', 'EIZH'),
(510, 9, 45, 'ERJIEK', 'ERJIEK'),
(511, 9, 45, 'GAJAL', 'GAJAL'),
(512, 9, 45, 'GHOLAKI', 'GHOLAKI'),
(513, 9, 45, 'GISTINI', 'GISTINI'),
(514, 9, 45, 'GOHIK', 'GOHIK'),
(515, 9, 45, 'GUFTI', 'GUFTI'),
(516, 9, 45, 'GULUGH', 'GULUGH'),
(517, 9, 45, 'HAZAI (NEW)', 'HAZAI (NEW)'),
(518, 9, 45, 'JITUR SANIQ', 'JITUR SANIQ'),
(519, 9, 45, 'JITUR-1', 'JITUR-1'),
(520, 9, 45, 'JITUR-2', 'JITUR-2'),
(521, 9, 45, 'KANDUJAL1', 'KANDUJAL1'),
(522, 9, 45, 'KANDUJAL2', 'KANDUJAL2'),
(523, 9, 45, 'KANDUJAL3', 'KANDUJAL3'),
(524, 9, 45, 'KHATINJ', 'KHATINJ'),
(525, 9, 45, 'KHOGHIK', 'KHOGHIK'),
(526, 9, 45, 'KOCH', 'KOCH'),
(527, 9, 45, 'LOHOK', 'LOHOK'),
(528, 9, 45, 'MAIDAN', 'MAIDAN'),
(529, 9, 45, 'MAIDAN-2', 'MAIDAN-2'),
(530, 9, 45, 'MEGIGRAM1', 'MEGIGRAM1'),
(531, 9, 45, 'MEGIGRAM2', 'MEGIGRAM2'),
(532, 9, 45, 'MEGIGRAM3', 'MEGIGRAM3'),
(533, 9, 45, 'MOGH1', 'MOGH1'),
(534, 9, 45, 'MOGH2 NIZH', 'MOGH2 NIZH'),
(535, 9, 45, 'MUNOOR1', 'MUNOOR1'),
(536, 9, 45, 'MUNOOR2', 'MUNOOR2'),
(537, 9, 45, 'MUNOOR3', 'MUNOOR3'),
(538, 9, 45, 'MUNOOR-4 Phalam', 'MUNOOR-4 Phalam'),
(539, 9, 45, 'MURDAN', 'MURDAN'),
(540, 9, 45, 'NARKURAT1', 'NARKURAT1'),
(541, 9, 45, 'NARKURAT2', 'NARKURAT2'),
(542, 9, 45, 'NARKURAT3', 'NARKURAT3'),
(543, 9, 45, 'NARKURAT4 ERGO', 'NARKURAT4 ERGO'),
(544, 9, 45, 'NASIRABAD Central Ziarat', 'NASIRABAD Central Ziarat'),
(545, 9, 45, 'NIWEST', 'NIWEST'),
(546, 9, 45, 'NOGH PATI', 'NOGH PATI'),
(547, 9, 45, 'OCHIO LASHT', 'OCHIO LASHT'),
(548, 9, 45, 'OCHU GOOL-1', 'OCHU GOOL-1'),
(549, 9, 45, 'OSHIAK MUSHEEN', 'OSHIAK MUSHEEN'),
(550, 9, 45, 'OUGHUTI', 'OUGHUTI'),
(551, 9, 45, 'OVIRK1', 'OVIRK1'),
(552, 9, 45, 'OVIRK2', 'OVIRK2'),
(553, 9, 45, 'PARABEG Central', 'PARABEG Central'),
(554, 9, 45, 'Postaki 3', 'Postaki 3'),
(555, 9, 45, 'POSTAKI1', 'POSTAKI1'),
(556, 9, 45, 'POSTAKI2', 'POSTAKI2'),
(557, 9, 45, 'PURTO', 'PURTO'),
(558, 9, 45, 'ROYEE1', 'ROYEE1'),
(559, 9, 45, 'ROYEE2', 'ROYEE2'),
(560, 9, 45, 'ROYEE3', 'ROYEE3'),
(561, 9, 45, 'SANIK', 'SANIK'),
(562, 9, 45, 'Sanik Payeen', 'Sanik Payeen'),
(563, 9, 45, 'SEPOKHT', 'SEPOKHT'),
(564, 9, 45, 'SHAGRAM1', 'SHAGRAM1'),
(565, 9, 45, 'SHAGRAM2', 'SHAGRAM2'),
(566, 9, 45, 'TUNIK', 'TUNIK'),
(567, 9, 45, 'UCHO GOAL1', 'UCHO GOAL1'),
(568, 9, 45, 'UCHO GOAL2', 'UCHO GOAL2'),
(569, 9, 45, 'UTRAIE-1', 'UTRAIE-1'),
(570, 9, 45, 'UTRAIE-2', 'UTRAIE-2'),
(571, 9, 45, 'WAHAT', 'WAHAT'),
(572, 9, 45, 'YOURJOGH', 'YOURJOGH'),
(573, 9, 43, 'AJARANDEH', 'AJARANDEH'),
(574, 9, 43, 'BISH NOGHOR', 'BISH NOGHOR'),
(575, 9, 43, 'BRESHGRAM Sub Centre', 'BRESHGRAM Sub Centre'),
(576, 9, 43, 'DALMEER', 'DALMEER'),
(577, 9, 43, 'DARDAI', 'DARDAI'),
(578, 9, 43, 'DARDAI KULUM', 'DARDAI KULUM'),
(579, 9, 43, 'DOOK DUR', 'DOOK DUR'),
(580, 9, 43, 'DRONIL', 'DRONIL'),
(581, 9, 43, 'GALEH', 'GALEH'),
(582, 9, 43, 'GREE Sub-Centre', 'GREE Sub-Centre'),
(583, 9, 43, 'HINJIL-1', 'HINJIL-1'),
(584, 9, 43, 'HINJIL-2', 'HINJIL-2'),
(585, 9, 43, 'HINJIL-3', 'HINJIL-3'),
(586, 9, 43, 'KARIMABAD Susum Central', 'KARIMABAD Susum Central'),
(587, 9, 43, 'KIYAR', 'KIYAR'),
(588, 9, 43, 'KULUM BRESHGRAM', 'KULUM BRESHGRAM'),
(589, 9, 43, 'LORIGRAM', 'LORIGRAM'),
(590, 9, 43, 'MADASHIL-1', 'MADASHIL-1'),
(591, 9, 43, 'MADASHIL-2', 'MADASHIL-2'),
(592, 9, 43, 'ORGHUCH', 'ORGHUCH'),
(593, 9, 43, 'ORLAGH', 'ORLAGH'),
(594, 9, 43, 'PASTURAGH', 'PASTURAGH'),
(595, 9, 43, 'PITRAGRAM-1', 'PITRAGRAM-1'),
(596, 9, 43, 'PITRAGRAM-2', 'PITRAGRAM-2'),
(597, 9, 43, 'SHAH', 'SHAH'),
(598, 9, 43, 'SHAHNURUN', 'SHAHNURUN'),
(599, 9, 43, 'SHUTH-1', 'SHUTH-1'),
(600, 9, 43, 'SHUTH-2', 'SHUTH-2'),
(601, 9, 43, 'SHUTH-3', 'SHUTH-3'),
(602, 9, 43, 'SUSUM LASHT', 'SUSUM LASHT'),
(603, 9, 43, 'TASHQAR-1', 'TASHQAR-1'),
(604, 9, 43, 'TASHQAR-2', 'TASHQAR-2'),
(605, 9, 43, 'THOK JAL', 'THOK JAL'),
(606, 9, 42, 'MADAKLASHT-1', 'MADAKLASHT-1'),
(607, 9, 42, 'MADAKLASHT-2, Sub-Centre', 'MADAKLASHT-2, Sub-Centre'),
(608, 9, 42, 'MADAKLASHT-3', 'MADAKLASHT-3'),
(609, 9, 42, 'MADAKLASHT-4', 'MADAKLASHT-4'),
(610, 9, 42, 'MADAKLASHT5 KASHANKOAL REC', 'MADAKLASHT5 KASHANKOAL REC'),
(611, 9, 41, 'ARGESH Parsan', 'ARGESH Parsan'),
(612, 9, 41, 'AWI', 'AWI'),
(613, 9, 41, 'BALACH', 'BALACH'),
(614, 9, 41, 'BILBIL Parsan', 'BILBIL Parsan'),
(615, 9, 41, 'BILPUK', 'BILPUK'),
(616, 9, 41, 'BODYOUGH', 'BODYOUGH'),
(617, 9, 41, 'BUHTULI DEH', 'BUHTULI DEH'),
(618, 9, 41, 'BUHTULI DOOK', 'BUHTULI DOOK'),
(619, 9, 41, 'BUHTULI GOOL-1', 'BUHTULI GOOL-1'),
(620, 9, 41, 'BUHTULI GOOL-2', 'BUHTULI GOOL-2'),
(621, 9, 41, 'CHINAR SOCIETY', 'CHINAR SOCIETY'),
(622, 9, 41, 'GHORO GOOL', 'GHORO GOOL'),
(623, 9, 41, 'HASANABAD', 'HASANABAD'),
(624, 9, 41, 'HERENI', 'HERENI'),
(625, 9, 41, 'HUSSAINABAD', 'HUSSAINABAD'),
(626, 9, 41, 'ISHPADER', 'ISHPADER'),
(627, 9, 41, 'KASETH', 'KASETH'),
(628, 9, 41, 'KILISHP', 'KILISHP'),
(629, 9, 41, 'KIRCHUM', 'KIRCHUM'),
(630, 9, 41, 'KRING', 'KRING'),
(631, 9, 41, 'KUCH GAH Shuljah', 'KUCH GAH Shuljah'),
(632, 9, 41, 'LASHT DEH Parsan', 'LASHT DEH Parsan'),
(633, 9, 41, 'MAJIMI Parsan', 'MAJIMI Parsan'),
(634, 9, 41, 'MIRLAN DEH Parsan', 'MIRLAN DEH Parsan'),
(635, 9, 41, 'MOMI-1 Paien', 'MOMI-1 Paien'),
(636, 9, 41, 'MOMI-2 Bala', 'MOMI-2 Bala'),
(637, 9, 41, 'MOMI-3 Khamjenandeh', 'MOMI-3 Khamjenandeh'),
(638, 9, 41, 'MOMOON', 'MOMOON'),
(639, 9, 41, 'OGHDER', 'OGHDER'),
(640, 9, 41, 'PECHUCH', 'PECHUCH'),
(641, 9, 41, 'RUJI', 'RUJI'),
(642, 9, 41, 'RUNI', 'RUNI'),
(643, 9, 41, 'SEEN LASHT', 'SEEN LASHT'),
(644, 9, 41, 'SHALEE', 'SHALEE'),
(645, 9, 41, 'SHERSHAL', 'SHERSHAL'),
(646, 9, 41, 'SHOGHORE Central', 'SHOGHORE Central'),
(647, 9, 41, 'SHOGHORE GHARI', 'SHOGHORE GHARI'),
(648, 9, 41, 'SIWAHTH-2', 'SIWAHTH-2'),
(649, 9, 41, 'SIWATH-1', 'SIWATH-1'),
(650, 9, 41, 'SUNICH', 'SUNICH'),
(651, 9, 41, 'Teyoli', 'Teyoli'),
(652, 9, 41, 'TILGRAM Parsan', 'TILGRAM Parsan'),
(653, 9, 41, 'UCHETUR', 'UCHETUR'),
(654, 3, 14, 'AHMEDPUR EAST', 'AHMEDPUR EAST'),
(655, 3, 14, 'ALLAHABAD', 'ALLAHABAD'),
(656, 3, 14, 'BAHAWALPUR', 'BAHAWALPUR'),
(657, 3, 14, 'KHANPUR', 'KHANPUR'),
(658, 3, 14, 'RAHIMYAR KHAN', 'RAHIMYAR KHAN'),
(659, 3, 14, 'SADIQABAD', 'SADIQABAD'),
(660, 3, 14, 'UCH SHARIF', 'UCH SHARIF'),
(661, 3, 11, 'CHINYOUT', 'CHINYOUT'),
(662, 3, 11, 'HAFIZABAD', 'HAFIZABAD'),
(663, 3, 11, 'JALALPUR BHATIAN', 'JALALPUR BHATIAN'),
(664, 3, 11, 'KARIMABAD Hafizabad', 'KARIMABAD Hafizabad'),
(665, 3, 11, 'KHANPUR KALAY, Sheikupur', 'KHANPUR KALAY, Sheikupur'),
(666, 3, 11, 'KOLO TARAR', 'KOLO TARAR'),
(667, 3, 11, 'PINDI BHATIAN', 'PINDI BHATIAN'),
(668, 3, 11, 'SALOKI', 'SALOKI'),
(669, 3, 11, 'THATI BHILOLPUR', 'THATI BHILOLPUR'),
(670, 3, 9, 'BADOMALI', 'BADOMALI'),
(671, 3, 9, 'FAISALABAD', 'FAISALABAD'),
(672, 3, 9, 'GUJRANWALA', 'GUJRANWALA'),
(673, 3, 9, 'LAHORE', 'LAHORE'),
(674, 3, 9, 'LALA MUSA', 'LALA MUSA'),
(675, 3, 9, 'SHADRA', 'SHADRA'),
(676, 3, 9, 'SHAH ALAM', 'SHAH ALAM'),
(677, 3, 9, 'TALWANDI', 'TALWANDI'),
(678, 3, 13, 'BAKHAR', 'BAKHAR'),
(679, 3, 13, 'KARIMABAD, Multan', 'KARIMABAD, Multan'),
(680, 3, 13, 'MULTAN', 'MULTAN'),
(681, 3, 13, 'MUMTAZABAD', 'MUMTAZABAD'),
(682, 3, 13, 'SAHIWAL', 'SAHIWAL'),
(683, 3, 13, 'VEHARI', 'VEHARI'),
(684, 3, 12, 'ATHAR', 'ATHAR'),
(685, 3, 12, 'BHAGTANWALA', 'BHAGTANWALA'),
(686, 3, 12, 'BHALWAL', 'BHALWAL'),
(687, 3, 12, 'BHEKO', 'BHEKO'),
(688, 3, 12, 'BHERA', 'BHERA'),
(689, 3, 12, 'CHAK NO.47', 'CHAK NO.47'),
(690, 3, 12, 'CHOHA SAIDEN SHAH', 'CHOHA SAIDEN SHAH'),
(691, 3, 12, 'HARANPUR', 'HARANPUR'),
(692, 3, 12, 'JAWARIAN', 'JAWARIAN'),
(693, 3, 12, 'KALRA ESTATE', 'KALRA ESTATE'),
(694, 3, 12, 'KHOTI ALLAH SHEIKIAN', 'KHOTI ALLAH SHEIKIAN'),
(695, 3, 12, 'KORRAY KOT', 'KORRAY KOT'),
(696, 3, 12, 'KOT MOMIN', 'KOT MOMIN'),
(697, 3, 12, 'LILYANI', 'LILYANI'),
(698, 3, 12, 'MANDI BAHAUDIN', 'MANDI BAHAUDIN'),
(699, 3, 12, 'MIANWAL', 'MIANWAL'),
(700, 3, 12, 'MIDH RANJHA', 'MIDH RANJHA'),
(701, 3, 12, 'NASEERPUR KALAN', 'NASEERPUR KALAN'),
(702, 3, 12, 'PHULARWAN', 'PHULARWAN'),
(703, 3, 12, 'PIND DADAN KHAN', 'PIND DADAN KHAN'),
(704, 3, 12, 'PINDI SAIDPUR', 'PINDI SAIDPUR'),
(705, 3, 12, 'SARGODHA', 'SARGODHA'),
(706, 3, 10, 'SIALKOT', 'SIALKOT'),
(707, 4, 15, 'ABBOTABAD', 'ABBOTABAD'),
(708, 4, 15, 'ATTOCK', 'ATTOCK'),
(709, 4, 15, 'BURBAN', 'BURBAN'),
(710, 4, 15, 'ISLAMABAD', 'ISLAMABAD'),
(711, 4, 15, 'KARIMABAD, Islamabad', 'KARIMABAD, Islamabad'),
(712, 4, 15, 'Muzaffarabad', 'Muzaffarabad'),
(713, 4, 15, 'RAWALPINDI', 'RAWALPINDI'),
(714, 4, 15, 'TAXILA', 'TAXILA'),
(715, 4, 16, 'AMIN COLONY', 'AMIN COLONY'),
(716, 4, 16, 'HAZRO', 'HAZRO'),
(717, 4, 16, 'MARDAN', 'MARDAN'),
(718, 4, 16, 'SIKANDER TOWN', 'SIKANDER TOWN'),
(719, 2, 5, 'ALYABAD', 'ALYABAD'),
(720, 2, 5, 'AMYNABAD', 'AMYNABAD'),
(721, 2, 5, 'JHUDO', 'JHUDO'),
(722, 2, 5, 'KHYBER', 'KHYBER'),
(723, 2, 5, 'MUBARAK', 'MUBARAK'),
(724, 2, 5, 'PRINCE ALY ROAD', 'PRINCE ALY ROAD'),
(725, 2, 5, 'RAHIMABAD, Tando Allahyar', 'RAHIMABAD, Tando Allahyar'),
(726, 2, 5, 'SHAH ALY', 'SHAH ALY'),
(727, 2, 5, 'SUKKUR', 'SUKKUR'),
(728, 2, 5, 'SULTANABAD 1', 'SULTANABAD 1'),
(729, 2, 5, 'SULTANABAD 2', 'SULTANABAD 2'),
(730, 2, 5, 'SULTANABAD 3', 'SULTANABAD 3'),
(731, 2, 5, 'TANDO ALLAHYAR', 'TANDO ALLAHYAR'),
(732, 2, 7, 'HUSSAINABAD', 'HUSSAINABAD'),
(733, 2, 7, 'JATI', 'JATI'),
(734, 2, 7, 'SUJAWAL', 'SUJAWAL'),
(735, 2, 7, 'TARR KHAWAJA', 'TARR KHAWAJA'),
(736, 2, 8, 'BADIN', 'BADIN'),
(737, 2, 8, 'KHADO', 'KHADO'),
(738, 2, 8, 'NAUABAD', 'NAUABAD'),
(739, 2, 8, 'SHAH KAPOOR', 'SHAH KAPOOR'),
(740, 2, 8, 'SONDA', 'SONDA'),
(741, 2, 8, 'TALHAR', 'TALHAR'),
(742, 2, 8, 'TANDO BAGO', 'TANDO BAGO'),
(743, 2, 8, 'TANDO MOHAMMED KHAN', 'TANDO MOHAMMED KHAN'),
(744, 2, 6, 'ALIJAH RAJAB ALI GOTH', 'ALIJAH RAJAB ALI GOTH'),
(745, 2, 6, 'ALIJAH WALI GOTH', 'ALIJAH WALI GOTH'),
(746, 2, 6, 'ALIJAHA BACHAL GOTH', 'ALIJAHA BACHAL GOTH'),
(747, 2, 6, 'ALYABAD', 'ALYABAD'),
(748, 2, 6, 'BOHARA', 'BOHARA'),
(749, 2, 6, 'CHACH SARKO(KAKO VILLAGE)', 'CHACH SARKO(KAKO VILLAGE)'),
(750, 2, 6, 'DINABAD NO.2', 'DINABAD NO.2'),
(751, 2, 6, 'GHULAMULLAH', 'GHULAMULLAH'),
(752, 2, 6, 'KARIMABAD', 'KARIMABAD'),
(753, 2, 6, 'MIRPUR SAKRO', 'MIRPUR SAKRO'),
(754, 2, 6, 'MOHAMMEDABAD', 'MOHAMMEDABAD'),
(755, 2, 6, 'NAU GOTH', 'NAU GOTH'),
(756, 2, 6, 'RAHIMABAD', 'RAHIMABAD'),
(757, 2, 6, 'VUR', 'VUR'),
(758, 10, 46, 'AWI BAND-1', 'AWI BAND-1'),
(759, 10, 46, 'AWI BAND-2', 'AWI BAND-2'),
(760, 10, 46, 'AWI GUCHDUR', 'AWI GUCHDUR'),
(761, 10, 46, 'AWI LASHT Sub Centre', 'AWI LASHT Sub Centre'),
(762, 10, 46, 'AWI LURANDEH', 'AWI LURANDEH'),
(763, 10, 46, 'AWI MANDAGH', 'AWI MANDAGH'),
(764, 10, 46, 'AWI SINGANDEH', 'AWI SINGANDEH'),
(765, 10, 46, 'BAZANDEH', 'BAZANDEH'),
(766, 10, 46, 'BOLAN LASHT Mulgram', 'BOLAN LASHT Mulgram'),
(767, 10, 46, 'BOONI Centre', 'BOONI Centre'),
(768, 10, 46, 'BOZIR', 'BOZIR'),
(769, 10, 46, 'BUTAKHAN DEH', 'BUTAKHAN DEH'),
(770, 10, 46, 'Centre JK Awi', 'Centre JK Awi'),
(771, 10, 46, 'CHARUN OVEER', 'CHARUN OVEER'),
(772, 10, 46, 'CHARUN Sub Centre', 'CHARUN Sub Centre'),
(773, 10, 46, 'CHARVELANDEH', 'CHARVELANDEH'),
(774, 10, 46, 'DARIYANO', 'DARIYANO'),
(775, 10, 46, 'DEH AVI CENTRE', 'DEH AVI CENTRE'),
(776, 10, 46, 'DUKANDEH', 'DUKANDEH'),
(777, 10, 46, 'GOOL GUCH', 'GOOL GUCH'),
(778, 10, 46, 'GREEN LASHT', 'GREEN LASHT'),
(779, 10, 46, 'HUSSAINABAD Junali Koch', 'HUSSAINABAD Junali Koch'),
(780, 10, 46, 'ISHPARDER Krui Junali', 'ISHPARDER Krui Junali'),
(781, 10, 46, 'JINALIKOACH BALA 1', 'JINALIKOACH BALA 1'),
(782, 10, 46, 'JUNALI KUCH-1 Paien', 'JUNALI KUCH-1 Paien'),
(783, 10, 46, 'JUNALI KUCH-2 Bala', 'JUNALI KUCH-2 Bala'),
(784, 10, 46, 'JUNALI KUCH-3 Paien', 'JUNALI KUCH-3 Paien'),
(785, 10, 46, 'KHORA GOOL', 'KHORA GOOL'),
(786, 10, 46, 'KRUI GOLOGH AWI', 'KRUI GOLOGH AWI'),
(787, 10, 46, 'KRUI JUNALI HOON', 'KRUI JUNALI HOON'),
(788, 10, 46, 'KRUICHAT PARWAK Sub Centre', 'KRUICHAT PARWAK Sub Centre'),
(789, 10, 46, 'KRUIJINALI 2', 'KRUIJINALI 2'),
(790, 10, 46, 'KURAGH', 'KURAGH'),
(791, 10, 46, 'LASHT BOONI', 'LASHT BOONI'),
(792, 10, 46, 'LUQANDEH Lutdur', 'LUQANDEH Lutdur'),
(793, 10, 46, 'Meragram Sonoghur', 'Meragram Sonoghur'),
(794, 10, 46, 'MERAGRAM-1', 'MERAGRAM-1'),
(795, 10, 46, 'MERAGRAM-2 Gool', 'MERAGRAM-2 Gool'),
(796, 10, 46, 'MUGHLANDEH', 'MUGHLANDEH'),
(797, 10, 46, 'NISUR GOOL Bala', 'NISUR GOOL Bala'),
(798, 10, 46, 'NIYATABAD sakolasht', 'NIYATABAD sakolasht'),
(799, 10, 46, 'PANAN DEH', 'PANAN DEH'),
(800, 10, 46, 'PARSANIK', 'PARSANIK'),
(801, 10, 46, 'Parwak Bala', 'Parwak Bala'),
(802, 10, 46, 'PARWAK-1 Paien', 'PARWAK-1 Paien'),
(803, 10, 46, 'PARWAK-2 Paien', 'PARWAK-2 Paien'),
(804, 10, 46, 'QASIMANDEH', 'QASIMANDEH'),
(805, 10, 46, 'QAZIANDEH', 'QAZIANDEH'),
(806, 10, 46, 'QUDRATABAD Nisur Gool', 'QUDRATABAD Nisur Gool'),
(807, 10, 46, 'RAHIMABAD PARWAK Bala', 'RAHIMABAD PARWAK Bala'),
(808, 10, 46, 'RAISANDEH Hisandeh', 'RAISANDEH Hisandeh'),
(809, 10, 46, 'RESHUN RAGHEEN Sub Centre', 'RESHUN RAGHEEN Sub Centre'),
(810, 10, 46, 'SHADER', 'SHADER'),
(811, 10, 46, 'SHAGUNAN DEH', 'SHAGUNAN DEH'),
(812, 10, 46, 'SHAKARANDEH', 'SHAKARANDEH'),
(813, 10, 46, 'SONOGHOR GOOL', 'SONOGHOR GOOL'),
(814, 10, 46, 'SONOGHORE Sub Centre', 'SONOGHORE Sub Centre'),
(815, 10, 46, 'SULTANABAD Morigar', 'SULTANABAD Morigar'),
(816, 10, 46, 'SYEDABAD PARWAK Bala', 'SYEDABAD PARWAK Bala'),
(817, 10, 46, 'TEK-LASHT', 'TEK-LASHT'),
(818, 10, 46, 'ZAIET', 'ZAIET'),
(819, 10, 52, 'ALYABAD Bang-3', 'ALYABAD Bang-3'),
(820, 10, 52, 'ALYABAD Brep', 'ALYABAD Brep'),
(821, 10, 52, 'BANG-1 Darmiyan SubCentre', 'BANG-1 Darmiyan SubCentre'),
(822, 10, 52, 'BANG-2 Paien', 'BANG-2 Paien'),
(823, 10, 52, 'BIRZOZE-1', 'BIRZOZE-1'),
(824, 10, 52, 'CHATU DOOK', 'CHATU DOOK'),
(825, 10, 52, 'DEH BREP', 'DEH BREP'),
(826, 10, 52, 'DEWSEER', 'DEWSEER'),
(827, 10, 52, 'DEWSEER - 2 Paien', 'DEWSEER - 2 Paien'),
(828, 10, 52, 'DIWAN GOOL', 'DIWAN GOOL'),
(829, 10, 52, 'DIZG', 'DIZG'),
(830, 10, 52, 'DONICH', 'DONICH'),
(831, 10, 52, 'DOOK Brep', 'DOOK Brep'),
(832, 10, 52, 'EMIT', 'EMIT'),
(833, 10, 52, 'GAZAN', 'GAZAN'),
(834, 10, 52, 'GAZEEN-1', 'GAZEEN-1'),
(835, 10, 52, 'GAZEEN-2', 'GAZEEN-2'),
(836, 10, 52, 'HUSSAINABAD - 2', 'HUSSAINABAD - 2'),
(837, 10, 52, 'HUSSAINABAD Birzoze-2', 'HUSSAINABAD Birzoze-2'),
(838, 10, 52, 'HYDERABAD', 'HYDERABAD'),
(839, 10, 52, 'ISTACH', 'ISTACH'),
(840, 10, 52, 'JAMLASHT-1', 'JAMLASHT-1'),
(841, 10, 52, 'JAMLASHT-2  Zandrandeh', 'JAMLASHT-2  Zandrandeh'),
(842, 10, 52, 'KARIMABAD Hoon-Bap', 'KARIMABAD Hoon-Bap'),
(843, 10, 52, 'KHANJARADEH', 'KHANJARADEH'),
(844, 10, 52, 'Khudabad Pashk', 'Khudabad Pashk'),
(845, 10, 52, 'KHURUZG', 'KHURUZG'),
(846, 10, 52, 'LASHDAN', 'LASHDAN'),
(847, 10, 52, 'MAHTING Paien', 'MAHTING Paien'),
(848, 10, 52, 'MAHTING-1 Bala', 'MAHTING-1 Bala'),
(849, 10, 52, 'MERAGRAM-1 Paien', 'MERAGRAM-1 Paien'),
(850, 10, 52, 'MERAGRAM-2 Bala', 'MERAGRAM-2 Bala'),
(851, 10, 52, 'MOMEENABAD Bang', 'MOMEENABAD Bang'),
(852, 10, 52, 'MOULAABAD', 'MOULAABAD'),
(853, 10, 52, 'MUHAMMADABAD', 'MUHAMMADABAD'),
(854, 10, 52, 'MULDEH', 'MULDEH'),
(855, 10, 52, 'MUSTAFAABAD', 'MUSTAFAABAD'),
(856, 10, 52, 'NICHHAGH', 'NICHHAGH'),
(857, 10, 52, 'NURABAD', 'NURABAD'),
(858, 10, 52, 'PARDAN', 'PARDAN'),
(859, 10, 52, 'PASHK', 'PASHK'),
(860, 10, 52, 'PATRANGAZ', 'PATRANGAZ'),
(861, 10, 52, 'PAWER-1 Uchu Hoon', 'PAWER-1 Uchu Hoon'),
(862, 10, 52, 'PAWER-2', 'PAWER-2'),
(863, 10, 52, 'RAHIMABAD Brep Central', 'RAHIMABAD Brep Central'),
(864, 10, 52, 'RAHMANABAD', 'RAHMANABAD'),
(865, 10, 52, 'RATHENI', 'RATHENI'),
(866, 10, 52, 'RUSTAMANDEH Kuch', 'RUSTAMANDEH Kuch'),
(867, 10, 52, 'SHICH', 'SHICH'),
(868, 10, 52, 'SHOL KUCH', 'SHOL KUCH'),
(869, 10, 52, 'SULTANABAD (KHOTAN LASHT)', 'SULTANABAD (KHOTAN LASHT)'),
(870, 10, 52, 'URKAN', 'URKAN'),
(871, 10, 52, 'Waghalabad Bang', 'Waghalabad Bang'),
(872, 10, 52, 'WASUM Bala Sub Centre', 'WASUM Bala Sub Centre'),
(873, 10, 52, 'WASUM-2 Paien', 'WASUM-2 Paien'),
(874, 10, 52, 'ZUPU', 'ZUPU'),
(875, 10, 48, 'ANDERGHECH', 'ANDERGHECH'),
(876, 10, 48, 'BARKATABAD Boop', 'BARKATABAD Boop'),
(877, 10, 48, 'BUZUND Sub Centre', 'BUZUND Sub Centre'),
(878, 10, 48, 'CHAT GAZ Sub Centre', 'CHAT GAZ Sub Centre'),
(879, 10, 48, 'DERGHERANDUR', 'DERGHERANDUR'),
(880, 10, 48, 'DUKAN', 'DUKAN'),
(881, 10, 48, 'FARDAGH', 'FARDAGH'),
(882, 10, 48, 'GOAL KHOT', 'GOAL KHOT'),
(883, 10, 48, 'HOON KHOT', 'HOON KHOT'),
(884, 10, 48, 'Jumbai Shal', 'Jumbai Shal'),
(885, 10, 48, 'KISH WAHT', 'KISH WAHT'),
(886, 10, 48, 'KISHEK', 'KISHEK'),
(887, 10, 48, 'Kishim', 'Kishim'),
(888, 10, 48, 'Kishiwaht 2', 'Kishiwaht 2'),
(889, 10, 48, 'LAMKUSHUM-1', 'LAMKUSHUM-1'),
(890, 10, 48, 'LAMKUSHUM-2', 'LAMKUSHUM-2'),
(891, 10, 48, 'LASHTDUR-1 Sub Centre', 'LASHTDUR-1 Sub Centre'),
(892, 10, 48, 'LASHTDUR-2', 'LASHTDUR-2'),
(893, 10, 48, 'LOOGAR', 'LOOGAR'),
(894, 10, 48, 'LUNGAR', 'LUNGAR'),
(895, 10, 48, 'Mahtin 2', 'Mahtin 2'),
(896, 10, 48, 'MAHTING Washich SubCentre', 'MAHTING Washich SubCentre'),
(897, 10, 48, 'MALANDUR', 'MALANDUR'),
(898, 10, 48, 'MARANDEH', 'MARANDEH'),
(899, 10, 48, 'MAYAR', 'MAYAR'),
(900, 10, 48, 'NICHAGH-1', 'NICHAGH-1'),
(901, 10, 48, 'NICHAGH-2', 'NICHAGH-2'),
(902, 10, 48, 'NOGHORAN TEK', 'NOGHORAN TEK'),
(903, 10, 48, 'PUCHUNG-1', 'PUCHUNG-1'),
(904, 10, 48, 'PUCHUNG-2', 'PUCHUNG-2'),
(905, 10, 48, 'PURKHOT-1', 'PURKHOT-1'),
(906, 10, 48, 'PURKHOT-2', 'PURKHOT-2'),
(907, 10, 48, 'RABAT-1', 'RABAT-1'),
(908, 10, 48, 'RABAT-2', 'RABAT-2'),
(909, 10, 48, 'SHAGRAM-1 Budandur', 'SHAGRAM-1 Budandur'),
(910, 10, 48, 'SHAGRAM-2 Uch Waht', 'SHAGRAM-2 Uch Waht'),
(911, 10, 48, 'SHAGRAM-3 (Mayar)', 'SHAGRAM-3 (Mayar)'),
(912, 10, 48, 'SHALANSUR', 'SHALANSUR'),
(913, 10, 48, 'SHARASTOON', 'SHARASTOON'),
(914, 10, 48, 'SHOOL', 'SHOOL'),
(915, 10, 48, 'SHOT MELP', 'SHOT MELP'),
(916, 10, 48, 'SHOTKHAR Central', 'SHOTKHAR Central'),
(917, 10, 48, 'SHOTKHAR-2', 'SHOTKHAR-2'),
(918, 10, 48, 'SHUCHAN LASHT', 'SHUCHAN LASHT'),
(919, 10, 48, 'WAZUM', 'WAZUM'),
(920, 10, 48, 'YAKDIZ-4', 'YAKDIZ-4'),
(921, 10, 48, 'YAKHDIZ-1', 'YAKHDIZ-1'),
(922, 10, 48, 'YAKHDIZ-2', 'YAKHDIZ-2'),
(923, 10, 48, 'YAKHDIZ-3', 'YAKHDIZ-3'),
(924, 10, 50, 'BALIM-1', 'BALIM-1'),
(925, 10, 50, 'BALIM-2', 'BALIM-2'),
(926, 10, 50, 'BALIM-3 Dook', 'BALIM-3 Dook'),
(927, 10, 50, 'BALIM-4 Tordeh', 'BALIM-4 Tordeh'),
(928, 10, 50, 'BROOK-1', 'BROOK-1'),
(929, 10, 50, 'BROOK-2', 'BROOK-2'),
(930, 10, 50, 'CHATORGHONI', 'CHATORGHONI'),
(931, 10, 50, 'Dok Dur', 'Dok Dur'),
(932, 10, 50, 'DOWKHAN Balim-5', 'DOWKHAN Balim-5'),
(933, 10, 50, 'GADAR SULASPUR', 'GADAR SULASPUR'),
(934, 10, 50, 'GASHT PAEEN', 'GASHT PAEEN'),
(935, 10, 50, 'GASHT-1', 'GASHT-1'),
(936, 10, 50, 'GASHT-2', 'GASHT-2'),
(937, 10, 50, 'GUCHGALL', 'GUCHGALL'),
(938, 10, 50, 'HARCHIN3', 'HARCHIN3'),
(939, 10, 50, 'HERCHIN Central', 'HERCHIN Central'),
(940, 10, 50, 'HUJUNE', 'HUJUNE'),
(941, 10, 50, 'IMAMABAD BALIM-6,', 'IMAMABAD BALIM-6,'),
(942, 10, 50, 'JUBILEEABAD Balim - 7', 'JUBILEEABAD Balim - 7'),
(943, 10, 50, 'LASHT-1 Herchin', 'LASHT-1 Herchin'),
(944, 10, 50, 'LASHT-2 Herchin', 'LASHT-2 Herchin'),
(945, 10, 50, 'MAYOON LASHT', 'MAYOON LASHT'),
(946, 10, 50, 'NOORABAD Ownshot', 'NOORABAD Ownshot'),
(947, 10, 50, 'OWNSHOT', 'OWNSHOT'),
(948, 10, 50, 'PARGRAM-1', 'PARGRAM-1'),
(949, 10, 50, 'PARGRAM-2', 'PARGRAM-2'),
(950, 10, 50, 'PORTH-1', 'PORTH-1'),
(951, 10, 50, 'PORTH-2', 'PORTH-2'),
(952, 10, 50, 'RAMAN-1 Guchdur', 'RAMAN-1 Guchdur'),
(953, 10, 50, 'RAMAN-2 Kochdur', 'RAMAN-2 Kochdur'),
(954, 10, 50, 'RAMAN-3 Doshandeh', 'RAMAN-3 Doshandeh'),
(955, 10, 50, 'RAMAN-4 Shaghnandeh', 'RAMAN-4 Shaghnandeh'),
(956, 10, 50, 'REJUNE', 'REJUNE'),
(957, 10, 50, 'REJUNE - 2', 'REJUNE - 2'),
(958, 10, 50, 'SHAIDAS', 'SHAIDAS'),
(959, 10, 50, 'SHAIDAS-2', 'SHAIDAS-2'),
(960, 10, 50, 'SUR-LASPUR', 'SUR-LASPUR'),
(961, 10, 50, 'Toor Jal', 'Toor Jal'),
(962, 10, 51, 'ALI ABAD LAKHAB', 'ALI ABAD LAKHAB'),
(963, 10, 51, 'Aliabad Hundur', 'Aliabad Hundur'),
(964, 10, 51, 'ALYABAD Chapali', 'ALYABAD Chapali'),
(965, 10, 51, 'ALYABAD Chunj-2 Bala', 'ALYABAD Chunj-2 Bala'),
(966, 10, 51, 'CHAPALI Bala', 'CHAPALI Bala'),
(967, 10, 51, 'Chapali Guch', 'Chapali Guch'),
(968, 10, 51, 'CHINAR Central', 'CHINAR Central'),
(969, 10, 51, 'CHUINJ-1 Paien', 'CHUINJ-1 Paien'),
(970, 10, 51, 'DARALOT Mastuj', 'DARALOT Mastuj'),
(971, 10, 51, 'GHORU BALA', 'GHORU BALA'),
(972, 10, 51, 'GHORU PAIEN', 'GHORU PAIEN'),
(973, 10, 51, 'Hasratabad Parkusap', 'Hasratabad Parkusap'),
(974, 10, 51, 'HOON Mastuj', 'HOON Mastuj'),
(975, 10, 51, 'ISTURDENI', 'ISTURDENI'),
(976, 10, 51, 'KARGIN-1 Bala', 'KARGIN-1 Bala'),
(977, 10, 51, 'KARGIN-2', 'KARGIN-2'),
(978, 10, 51, 'KHUZ-1 Paien', 'KHUZ-1 Paien'),
(979, 10, 51, 'KHUZ-2 Darmiyan', 'KHUZ-2 Darmiyan'),
(980, 10, 51, 'KHUZ-3 Bala', 'KHUZ-3 Bala'),
(981, 10, 51, 'LAKAP', 'LAKAP'),
(982, 10, 51, 'MUHAMMADABAD GhoruLasht', 'MUHAMMADABAD GhoruLasht'),
(983, 10, 51, 'NISUR', 'NISUR'),
(984, 10, 51, 'PARKUSAB-1 Muzhdeh', 'PARKUSAB-1 Muzhdeh'),
(985, 10, 51, 'PARKUSAB-2 Parimali', 'PARKUSAB-2 Parimali'),
(986, 10, 51, 'PASUM', 'PASUM'),
(987, 10, 51, 'RAHIMABAD Chuinj-3', 'RAHIMABAD Chuinj-3'),
(988, 10, 51, 'SARGHOZE-1', 'SARGHOZE-1'),
(989, 10, 51, 'SARGHOZE-2', 'SARGHOZE-2'),
(990, 10, 51, 'TOOQ - 2 Bala', 'TOOQ - 2 Bala'),
(991, 10, 51, 'TOOQ Mastuj', 'TOOQ Mastuj'),
(992, 10, 47, 'ALYABAD Morder-3', 'ALYABAD Morder-3'),
(993, 10, 47, 'AWNERDER', 'AWNERDER'),
(994, 10, 47, 'BAHRIANDUR', 'BAHRIANDUR'),
(995, 10, 47, 'BARCHAT', 'BARCHAT'),
(996, 10, 47, 'BLOGH', 'BLOGH'),
(997, 10, 47, 'BODO DOK Saht', 'BODO DOK Saht'),
(998, 10, 47, 'BUMBAGH', 'BUMBAGH'),
(999, 10, 47, 'DILSHAD KHUM', 'DILSHAD KHUM'),
(1000, 10, 47, 'DONO Saht', 'DONO Saht'),
(1001, 10, 47, 'DOOKDUR Ghat, Bala', 'DOOKDUR Ghat, Bala'),
(1002, 10, 47, 'GAHT PAEEN', 'GAHT PAEEN'),
(1003, 10, 47, 'ISTARI Saht', 'ISTARI Saht'),
(1004, 10, 47, 'KHALANI', 'KHALANI'),
(1005, 10, 47, 'KHOL BRUNZK Saht', 'KHOL BRUNZK Saht'),
(1006, 10, 47, 'KHORA GOLE', 'KHORA GOLE'),
(1007, 10, 47, 'KOHDOKE', 'KOHDOKE'),
(1008, 10, 47, 'KUSHUM', 'KUSHUM'),
(1009, 10, 47, 'LOONE-1 Sub Centre', 'LOONE-1 Sub Centre'),
(1010, 10, 47, 'LOONE-2 Krui Deru', 'LOONE-2 Krui Deru'),
(1011, 10, 47, 'LOONE-3 Auzakh', 'LOONE-3 Auzakh'),
(1012, 10, 47, 'LOONE-4 Pishili', 'LOONE-4 Pishili'),
(1013, 10, 47, 'MORDER-1 Paien', 'MORDER-1 Paien'),
(1014, 10, 47, 'MORDER-2 Godok', 'MORDER-2 Godok'),
(1015, 10, 47, 'MULGAHT', 'MULGAHT'),
(1016, 10, 47, 'MUZDEH Khost Sub Centre', 'MUZDEH Khost Sub Centre'),
(1017, 10, 47, 'MUZUDUR Saht', 'MUZUDUR Saht'),
(1018, 10, 47, 'NASU DOOK Ghat', 'NASU DOOK Ghat'),
(1019, 10, 47, 'NILANDUR Saht', 'NILANDUR Saht'),
(1020, 10, 47, 'PANJIANTEK', 'PANJIANTEK'),
(1021, 10, 47, 'PINDORU GAZ Saht', 'PINDORU GAZ Saht'),
(1022, 10, 47, 'PISHALDURI', 'PISHALDURI'),
(1023, 10, 47, 'ROGHONDOK', 'ROGHONDOK'),
(1024, 10, 47, 'SANDRAGH', 'SANDRAGH'),
(1025, 10, 47, 'SEER BALA', 'SEER BALA'),
(1026, 10, 47, 'SEER Saht', 'SEER Saht'),
(1027, 10, 47, 'SHALDUR', 'SHALDUR'),
(1028, 10, 47, 'SHERANDUR Saht Sub Centre', 'SHERANDUR Saht Sub Centre'),
(1029, 10, 47, 'SHOKORMALI', 'SHOKORMALI'),
(1030, 10, 47, 'SHUNJOOR ANN', 'SHUNJOOR ANN'),
(1031, 10, 47, 'WARIJUNE Central', 'WARIJUNE Central'),
(1032, 10, 47, 'ZUNDRANGRAM Trech', 'ZUNDRANGRAM Trech'),
(1033, 10, 49, 'ADRAKH', 'ADRAKH'),
(1034, 10, 49, 'BOLASHT', 'BOLASHT'),
(1035, 10, 49, 'DUKAN-1 GoalKhot', 'DUKAN-1 GoalKhot'),
(1036, 10, 49, 'DUKAN-2', 'DUKAN-2'),
(1037, 10, 49, 'ISKHAN LASHT', 'ISKHAN LASHT'),
(1038, 10, 49, 'KISHNZOKH', 'KISHNZOKH'),
(1039, 10, 49, 'MU-RECH', 'MU-RECH'),
(1040, 10, 49, 'NIA-LASHT', 'NIA-LASHT'),
(1041, 10, 49, 'NISUR RECH Sub Centre', 'NISUR RECH Sub Centre'),
(1042, 10, 49, 'PA-RECH', 'PA-RECH'),
(1043, 10, 49, 'PURGRAM', 'PURGRAM'),
(1044, 10, 49, 'RAGH', 'RAGH'),
(1045, 10, 49, 'ROWA Rech', 'ROWA Rech'),
(1046, 10, 49, 'SALANDUR', 'SALANDUR'),
(1047, 10, 49, 'SHORQ', 'SHORQ'),
(1048, 10, 49, 'SIKUCH', 'SIKUCH'),
(1049, 10, 49, 'SU-RECH Sub Centre', 'SU-RECH Sub Centre'),
(1050, 10, 49, 'ZHANGOKHAM', 'ZHANGOKHAM'),
(1051, 10, 53, 'ALYABAD', 'ALYABAD'),
(1052, 10, 53, 'CHIKAR BROGHIL', 'CHIKAR BROGHIL'),
(1053, 10, 53, 'CHILMARABAD', 'CHILMARABAD'),
(1054, 10, 53, 'CHITISAR', 'CHITISAR'),
(1055, 10, 53, 'DUBARGAR', 'DUBARGAR'),
(1056, 10, 53, 'DURZU', 'DURZU'),
(1057, 10, 53, 'FAIZABAD Inchipun', 'FAIZABAD Inchipun'),
(1058, 10, 53, 'GARAMCHASHMA Sub Centre', 'GARAMCHASHMA Sub Centre'),
(1059, 10, 53, 'GAREL Broghil', 'GAREL Broghil'),
(1060, 10, 53, 'Ghairarum - 3 Hussainabad', 'Ghairarum - 3 Hussainabad'),
(1061, 10, 53, 'GHIRARUM-1', 'GHIRARUM-1'),
(1062, 10, 53, 'H. A. Inkeel', 'H. A. Inkeel'),
(1063, 10, 53, 'INKEP Paien', 'INKEP Paien'),
(1064, 10, 53, 'KARIMABAD Ghirarum-2', 'KARIMABAD Ghirarum-2'),
(1065, 10, 53, 'KHAIRABAD Youkshukut', 'KHAIRABAD Youkshukut'),
(1066, 10, 53, 'KHAN KHOON', 'KHAN KHOON'),
(1067, 10, 53, 'KHAND-1 Paien', 'KHAND-1 Paien'),
(1068, 10, 53, 'KHAND-2 Bala', 'KHAND-2 Bala'),
(1069, 10, 53, 'KISHMANJAH', 'KISHMANJAH'),
(1070, 10, 53, 'LASHKARGAZ', 'LASHKARGAZ'),
(1071, 10, 53, 'LOOTGAZ', 'LOOTGAZ'),
(1072, 10, 53, 'MAIDAN Broghil', 'MAIDAN Broghil'),
(1073, 10, 53, 'Mominabad', 'Mominabad'),
(1074, 10, 53, 'OWNAWUCH-1 Paien', 'OWNAWUCH-1 Paien'),
(1075, 10, 53, 'OWNAWUCH-2 Bala', 'OWNAWUCH-2 Bala'),
(1076, 10, 53, 'Pechoch Broghil', 'Pechoch Broghil'),
(1077, 10, 53, 'PUKTH', 'PUKTH'),
(1078, 10, 53, 'RAHIMABAD Shovest (S/C)', 'RAHIMABAD Shovest (S/C)'),
(1079, 10, 53, 'RUKUT', 'RUKUT'),
(1080, 10, 53, 'SALIMAHABAD Koi Chilmarabad', 'SALIMAHABAD Koi Chilmarabad'),
(1081, 10, 53, 'SHIKARWAZ Junali', 'SHIKARWAZ Junali'),
(1082, 10, 53, 'SKIRMUT', 'SKIRMUT'),
(1083, 10, 53, 'YAKHDAN', 'YAKHDAN'),
(1084, 10, 53, 'YARKHOON LASHT Centre', 'YARKHOON LASHT Centre'),
(1085, 10, 53, 'YOUSHKEST', 'YOUSHKEST');

-- --------------------------------------------------------

--
-- Table structure for table `landunit`
--

CREATE TABLE IF NOT EXISTS `landunit` (
  `uid` int(11) unsigned NOT NULL DEFAULT '0',
  `label` varchar(255) DEFAULT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `landunit`
--

INSERT INTO `landunit` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 :: Not Answered'),
(1, 'Acres', '1 : Acres'),
(2, 'Marla', '2 : Marla'),
(3, 'Yards', '3 : Yards'),
(4, 'Sq. Meters', '4 : Sq. Meters'),
(5, 'Square Inches', '5 : Square Inches'),
(6, 'Square Yards', '6 : Square Yards'),
(7, 'Kanaal', '7 : Kanaal'),
(8, 'Karam/Gatha', '8 : Karam/Gatha'),
(9, 'Killa', '9 : Killa'),
(10, 'Bigha', '10 : Bigha'),
(11, 'Murabba', '11 : Murabba'),
(12, 'Biswa', '12 : Biswa'),
(13, 'Bigha', '13 : Bigha'),
(14, 'Sarsahies', '14 : Sarsahies'),
(15, 'Hectare', '15 : Hectare'),
(16, 'Lat', '16 : Lat'),
(17, 'KM', '17 : KM'),
(18, 'Miles', '18 : Miles');

-- --------------------------------------------------------

--
-- Table structure for table `localcouncil`
--

CREATE TABLE IF NOT EXISTS `localcouncil` (
  `uid` int(11) unsigned NOT NULL,
  `regionalCouncilUid` int(11) unsigned DEFAULT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`),
  KEY `regionalCouncilUid` (`regionalCouncilUid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `localcouncil`
--

INSERT INTO `localcouncil` (`uid`, `regionalCouncilUid`, `label`, `uidLabel`) VALUES
(1, 1, 'Garden', 'Garden'),
(2, 1, 'Karimabad', 'Karimabad'),
(3, 1, 'Kharadar', 'Kharadar'),
(4, 1, 'Balochistan', 'Balochistan'),
(5, 2, 'Hyderabad', 'Hyderabad'),
(6, 2, 'Thatta', 'Thatta'),
(7, 2, 'Shahbandar', 'Shahbandar'),
(8, 2, 'Tando Turrail', 'Tando Turrail'),
(9, 3, 'Lahore', 'Lahore'),
(10, 3, 'Sialkot', 'Sialkot'),
(11, 3, 'Hafizabad', 'Hafizabad'),
(12, 3, 'Sargodha', 'Sargodha'),
(13, 3, 'Multan', 'Multan'),
(14, 3, 'Bhawalpur', 'Bhawalpur'),
(15, 4, 'Islamabad', 'Islamabad'),
(16, 4, 'Peshawar', 'Peshawar'),
(17, 5, 'Altit Karimabad', 'Altit Karimabad'),
(18, 5, 'Aliabad Hyderabad', 'Aliabad Hyderabad'),
(19, 5, 'Nasirabad Shinkani', 'Nasirabad Shinkani'),
(20, 5, 'Gulmit', 'Gulmit'),
(21, 5, 'Shimshal', 'Shimshal'),
(22, 5, 'Sost', 'Sost'),
(23, 5, 'Chipurson', 'Chipurson'),
(24, 6, 'Gilgit', 'Gilgit'),
(25, 6, 'Danyore', 'Danyore'),
(26, 6, 'Sakrdu', 'Sakrdu'),
(27, 7, 'Babur', 'Babur'),
(28, 7, 'Sherqilla', 'Sherqilla'),
(29, 7, 'Damas', 'Damas'),
(30, 7, 'Chatorkhand', 'Chatorkhand'),
(31, 7, 'Ishkoman', 'Ishkoman'),
(32, 7, 'Immit', 'Immit'),
(33, 8, 'Gupis', 'Gupis'),
(34, 8, 'Pingal', 'Pingal'),
(35, 8, 'Phunder', 'Phunder'),
(36, 8, 'Gulaghmuli', 'Gulaghmuli'),
(37, 8, 'Yasin', 'Yasin'),
(38, 8, 'Sultanabad', 'Sultanabad'),
(39, 8, 'Silgan', 'Silgan'),
(40, 8, 'Thoi', 'Thoi'),
(41, 9, 'Shoghor', 'Shoghor'),
(42, 9, 'Madaklasht', 'Madaklasht'),
(43, 9, 'Karimabad', 'Karimabad'),
(44, 9, 'Arkari', 'Arkari'),
(45, 9, 'Garamchashma', 'Garamchashma'),
(46, 10, 'Booni', 'Booni'),
(47, 10, 'Mulkow', 'Mulkow'),
(48, 10, 'Khot', 'Khot'),
(49, 10, 'Rech', 'Rech'),
(50, 10, 'Laspur', 'Laspur'),
(51, 10, 'Mastuj', 'Mastuj'),
(52, 10, 'Brep', 'Brep'),
(53, 10, 'Yarkhoon Lasht', 'Yarkhoon Lasht');

-- --------------------------------------------------------

--
-- Table structure for table `n1familyrelationship`
--

CREATE TABLE IF NOT EXISTS `n1familyrelationship` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n1familyrelationship`
--

INSERT INTO `n1familyrelationship` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Self', '1 : Self'),
(2, 'Husband', '2 : Husband'),
(3, 'Wife', '3 : Wife'),
(4, 'Son', '4 : Son'),
(5, 'Daughter', '5 : Daughter'),
(6, 'Father ', '6 : Father '),
(7, 'Mother ', '7 : Mother '),
(8, 'Brother', '8 : Brother'),
(9, 'Sister', '9 : Sister'),
(10, 'Grand children ', '10 : Grand children '),
(11, 'Daughter in law', '11 : Daughter in law'),
(12, 'Step Mother / Father ', '12 : Step Mother / Father '),
(13, 'Others ', '13 : Others ');

-- --------------------------------------------------------

--
-- Table structure for table `n2martialstauts`
--

CREATE TABLE IF NOT EXISTS `n2martialstauts` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n2martialstauts`
--

INSERT INTO `n2martialstauts` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Single', '1 : Single'),
(2, 'Married', '2 : Married'),
(3, 'Divorced', '3 : Divorced'),
(4, 'Widow', '4 : Widow'),
(5, 'Widower', '5 : Widower'),
(6, 'Separate (Not divorced)', '6 : Separate (Not divorced)');

-- --------------------------------------------------------

--
-- Table structure for table `n3educationstatus`
--

CREATE TABLE IF NOT EXISTS `n3educationstatus` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n3educationstatus`
--

INSERT INTO `n3educationstatus` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Never Enrolled / Uneducated', '1 : Never Enrolled / Uneducated'),
(2, 'Dropped out (Left studies)', '2 : Dropped out (Left studies)'),
(3, 'Currently enrolled ', '3 : Currently enrolled '),
(4, 'Completed studies.', '4 : Completed studies.');

-- --------------------------------------------------------

--
-- Table structure for table `n4recqualification`
--

CREATE TABLE IF NOT EXISTS `n4recqualification` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n4recqualification`
--

INSERT INTO `n4recqualification` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'Nursery', '2 : Nursery'),
(3, 'KG-1', '3 : KG-1'),
(4, 'KG-2', '4 : KG-2'),
(5, 'Class 1', '5 : Class 1'),
(6, 'Class 2', '6 : Class 2'),
(7, 'Class 3', '7 : Class 3'),
(8, 'Class 4', '8 : Class 4'),
(9, 'Class 5', '9 : Class 5'),
(10, 'Class 6', '10 : Class 6'),
(11, 'Class 7', '11 : Class 7'),
(12, 'Class 8', '12 : Class 8'),
(13, 'Class 9', '13 : Class 9'),
(14, 'Class 10', '14 : Class 10'),
(15, 'Class 11 / HRE I', '15 : Class 11 / HRE I'),
(16, 'Class 12 / HRE II', '16 : Class 12 / HRE II'),
(17, 'ARE ', '17 : ARE '),
(18, 'Advance courses like GIPSH, PTEP, IWTP, STEP etc.', '18 : Advance courses like GIPSH, PTEP, IWTP, STEP etc.');

-- --------------------------------------------------------

--
-- Table structure for table `n5educationstatusreason`
--

CREATE TABLE IF NOT EXISTS `n5educationstatusreason` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n5educationstatusreason`
--

INSERT INTO `n5educationstatusreason` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'Completed studies ', '2 : Completed studies '),
(3, 'Underage', '3 : Underage'),
(4, 'Distance of REC / Education institute ', '4 : Distance of REC / Education institute '),
(5, 'Timings of REC/ Education institute ', '5 : Timings of REC/ Education institute '),
(6, 'Low quality of teaching', '6 : Low quality of teaching'),
(7, 'Lack of interest / Awareness of Student', '7 : Lack of interest / Awareness of Student'),
(8, 'Lack of interest / Awareness of Parents', '8 : Lack of interest / Awareness of Parents'),
(9, 'Secular education pressure', '9 : Secular education pressure'),
(10, 'Females are not encouraged to attend ', '10 : Females are not encouraged to attend '),
(11, 'Females are not comfortable in studying from Male teachers or vice versa', '11 : Females are not comfortable in studying from Male teachers or vice versa'),
(12, 'Special Child', '12 : Special Child'),
(13, 'Quality of physical environment', '13 : Quality of physical environment'),
(14, 'Serving in some other Jamati Institution', '14 : Serving in some other Jamati Institution'),
(15, 'Over Age', '15 : Over Age'),
(16, 'Could not keep up with the class', '16 : Could not keep up with the class'),
(17, 'Engaged in some economic activity', '17 : Engaged in some economic activity'),
(18, 'Co-education', '18 : Co-education'),
(19, 'Unpleasant incident in past with anyone ', '19 : Unpleasant incident in past with anyone '),
(20, 'Taking care of siblings', '20 : Taking care of siblings'),
(21, 'Health related issues ', '21 : Health related issues '),
(22, 'Marriage', '22 : Marriage'),
(23, 'No Religious Education / secular institute ', '23 : No Religious Education / secular institute '),
(24, 'Security concerns ', '24 : Security concerns '),
(25, 'No Secular Education that’s why No Religious Education or vice versa ', '25 : No Secular Education that’s why No Religious Education or vice versa '),
(26, 'Affordability ', '26 : Affordability '),
(27, 'Went oversees for education ', '27 : Went oversees for education '),
(28, 'Housewife ', '28 : Housewife '),
(29, 'Retired ', '29 : Retired '),
(30, 'Others', '30 : Others');

-- --------------------------------------------------------

--
-- Table structure for table `n6jamatkhanaattendence`
--

CREATE TABLE IF NOT EXISTS `n6jamatkhanaattendence` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n6jamatkhanaattendence`
--

INSERT INTO `n6jamatkhanaattendence` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No response', '1 : No response'),
(2, 'Once a week', '2 : Once a week'),
(3, '(2-5 days a week)', '3 : (2-5 days a week)'),
(4, 'Daily ', '4 : Daily '),
(5, 'Only attend on occasions / Majlis', '5 : Only attend on occasions / Majlis'),
(6, 'Do not attend', '6 : Do not attend');

-- --------------------------------------------------------

--
-- Table structure for table `n7jamatkhanaattendencereason`
--

CREATE TABLE IF NOT EXISTS `n7jamatkhanaattendencereason` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n7jamatkhanaattendencereason`
--

INSERT INTO `n7jamatkhanaattendencereason` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'No response ', '2 : No response '),
(3, 'Leisure time or other priorities', '3 : Leisure time or other priorities'),
(4, 'Health related issues / caregiving to family members ', '4 : Health related issues / caregiving to family members '),
(5, 'Prefer taking rest', '5 : Prefer taking rest'),
(6, 'Education', '6 : Education'),
(7, 'Security issue', '7 : Security issue'),
(8, 'Long distance', '8 : Long distance'),
(9, 'Engaged in economic activity ', '9 : Engaged in economic activity '),
(10, 'Prior unpleasant experience with anyone ', '10 : Prior unpleasant experience with anyone '),
(11, 'Practice other sister community norms ', '11 : Practice other sister community norms ');

-- --------------------------------------------------------

--
-- Table structure for table `n8secularqualification`
--

CREATE TABLE IF NOT EXISTS `n8secularqualification` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n8secularqualification`
--

INSERT INTO `n8secularqualification` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Nursery', '1 : Nursery'),
(2, 'KG-1 (ECD-1)', '2 : KG-1 (ECD-1)'),
(3, 'KG-2 (ECD II)', '3 : KG-2 (ECD II)'),
(4, 'Class 1 (ECD III)', '4 : Class 1 (ECD III)'),
(5, 'Class 2 (ECD IV)', '5 : Class 2 (ECD IV)'),
(6, 'Class 3 (Jr. Sec. 1)', '6 : Class 3 (Jr. Sec. 1)'),
(7, 'Class 4 (Jr. Sec. 2)', '7 : Class 4 (Jr. Sec. 2)'),
(8, 'Class 5 (Jr. Sec. 3)', '8 : Class 5 (Jr. Sec. 3)'),
(9, 'Class 6 (Sr. Sec. 1)', '9 : Class 6 (Sr. Sec. 1)'),
(10, 'Class 7 (Sr. Sec. 2)', '10 : Class 7 (Sr. Sec. 2)'),
(11, 'Class 8 (Sr. Sec. 3)', '11 : Class 8 (Sr. Sec. 3)'),
(12, 'Class 9', '12 : Class 9'),
(13, 'Class 10', '13 : Class 10'),
(14, 'O levels (First Year )', '14 : O levels (First Year )'),
(15, 'O levels (Second Year )', '15 : O levels (Second Year )'),
(16, 'Class 11', '16 : Class 11'),
(17, 'Class 12', '17 : Class 12'),
(18, 'A levels (First year)', '18 : A levels (First year)'),
(19, 'A levels (Second year)', '19 : A levels (Second year)'),
(20, 'Bachelors Degree', '20 : Bachelors Degree'),
(21, 'Masters Degree ', '21 : Masters Degree '),
(22, 'PHD / Post Doctorate ', '22 : PHD / Post Doctorate '),
(23, '"Professional certificates (CA, ACCA, CIMA\netc.)"', '23 : "Professional certificates (CA, ACCA, CIMA\netc.)"'),
(24, 'Diplomas / Technical / Vocational Trainings ', '24 : Diplomas / Technical / Vocational Trainings '),
(25, 'No Education', '25 : No Education'),
(26, 'Drop out / not currently enrolled', '26 : Drop out / not currently enrolled');

-- --------------------------------------------------------

--
-- Table structure for table `n9educationinstitutecategory`
--

CREATE TABLE IF NOT EXISTS `n9educationinstitutecategory` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n9educationinstitutecategory`
--

INSERT INTO `n9educationinstitutecategory` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'Government', '2 : Government'),
(3, 'Private', '3 : Private'),
(4, 'AKES,  P / AKDN ', '4 : AKES,  P / AKDN '),
(5, 'Community Based School', '5 : Community Based School'),
(6, 'Coaching Centre', '6 : Coaching Centre'),
(7, 'Not -for-profit /NGO/ Trust (School / Collage / Universities)', '7 : Not -for-profit /NGO/ Trust (School / Collage / Universities)');

-- --------------------------------------------------------

--
-- Table structure for table `n10educationgroup`
--

CREATE TABLE IF NOT EXISTS `n10educationgroup` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n10educationgroup`
--

INSERT INTO `n10educationgroup` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'None / Not applicable ', '1 : None / Not applicable '),
(2, 'Science (Medical)', '2 : Science (Medical)'),
(3, 'Science (Engineering)', '3 : Science (Engineering)'),
(4, 'Science (General including IT related fields)', '4 : Science (General including IT related fields)'),
(5, 'Commerce - Finance ', '5 : Commerce - Finance '),
(6, 'Commerce (Others includes management, HR etc.) ', '6 : Commerce (Others includes management, HR etc.) '),
(7, 'Arts (Humanities / Graphics / Music etc.)', '7 : Arts (Humanities / Graphics / Music etc.)'),
(8, 'Education', '8 : Education'),
(9, 'Others ', '9 : Others ');

-- --------------------------------------------------------

--
-- Table structure for table `n11proficiency`
--

CREATE TABLE IF NOT EXISTS `n11proficiency` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n11proficiency`
--

INSERT INTO `n11proficiency` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No proficiency', '1 : No proficiency'),
(2, 'Fluent / Expert ', '2 : Fluent / Expert '),
(3, 'Moderate / Intermediate ', '3 : Moderate / Intermediate '),
(4, 'Basic', '4 : Basic');

-- --------------------------------------------------------

--
-- Table structure for table `n12occupationcategory`
--

CREATE TABLE IF NOT EXISTS `n12occupationcategory` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n12occupationcategory`
--

INSERT INTO `n12occupationcategory` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'AKDN services ', '2 : AKDN services '),
(3, 'Private Service full time ', '3 : Private Service full time '),
(4, 'Government Service', '4 : Government Service'),
(5, 'Wage worker / daily wager', '5 : Wage worker / daily wager'),
(6, 'Self Employed / Business / Partnership ', '6 : Self Employed / Business / Partnership '),
(7, 'Freelance (including tuition)', '7 : Freelance (including tuition)'),
(8, 'Part time employee / worker ', '8 : Part time employee / worker '),
(9, 'Unemployed ', '9 : Unemployed '),
(10, 'Student ', '10 : Student '),
(11, 'Retired ', '11 : Retired '),
(12, 'Housewife ', '12 : Housewife ');

-- --------------------------------------------------------

--
-- Table structure for table `n13income`
--

CREATE TABLE IF NOT EXISTS `n13income` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n13income`
--

INSERT INTO `n13income` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable (no income)', '1 : Not applicable (no income)'),
(2, 'Not willing to disclose ', '2 : Not willing to disclose '),
(3, ' Less than 5,000/-', '3 :  Less than 5,000/-'),
(4, ' 5,001/-  to 7500/-', '4 :  5,001/-  to 7500/-'),
(5, ' 7,501/- to  10,000/-', '5 :  7,501/- to  10,000/-'),
(6, '10,001/- to 15000/-', '6 : 10,001/- to 15000/-'),
(7, ' 15,001/-  to 20,000/-', '7 :  15,001/-  to 20,000/-'),
(8, ' 20,001/-  to 40,000/-', '8 :  20,001/-  to 40,000/-'),
(9, ' 40,001/-  to 100,000/-', '9 :  40,001/-  to 100,000/-'),
(10, ' 100,001/- to 200,000/-', '10 :  100,001/- to 200,000/-'),
(11, 'Above  200,000/-', '11 : Above  200,000/-');

-- --------------------------------------------------------

--
-- Table structure for table `n14unemployedfrom`
--

CREATE TABLE IF NOT EXISTS `n14unemployedfrom` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n14unemployedfrom`
--

INSERT INTO `n14unemployedfrom` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable (for Currently employed / student / housewife / retired / Infants)', '1 : Not applicable (for Currently employed / student / housewife / retired / Infants)'),
(2, 'Since one month ', '2 : Since one month '),
(3, 'Since three months ', '3 : Since three months '),
(4, 'Since 6 months ', '4 : Since 6 months '),
(5, 'Equal to or more than one year ', '5 : Equal to or more than one year ');

-- --------------------------------------------------------

--
-- Table structure for table `n15unemploymentreason`
--

CREATE TABLE IF NOT EXISTS `n15unemploymentreason` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n15unemploymentreason`
--

INSERT INTO `n15unemploymentreason` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'Un-educated / unskilled', '2 : Un-educated / unskilled'),
(3, 'Job market crises / Less job opportunities in field of studies /Undesirable salary package', '3 : Job market crises / Less job opportunities in field of studies /Undesirable salary package'),
(4, 'Family doesn’t allow', '4 : Family doesn’t allow'),
(5, 'Doesn''t like doing job / Business', '5 : Doesn''t like doing job / Business'),
(6, 'Doesn’t need to do job ', '6 : Doesn’t need to do job '),
(7, 'Student / housewife / priority over others commitments ', '7 : Student / housewife / priority over others commitments ');

-- --------------------------------------------------------

--
-- Table structure for table `n16disability`
--

CREATE TABLE IF NOT EXISTS `n16disability` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n16disability`
--

INSERT INTO `n16disability` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No disability / impairment (Not applicable)', '1 : No disability / impairment (Not applicable)'),
(2, 'Physical (includes visual /speech / Hearing)', '2 : Physical (includes visual /speech / Hearing)'),
(3, 'Mental ', '3 : Mental '),
(4, 'Both ', '4 : Both ');

-- --------------------------------------------------------

--
-- Table structure for table `n17illnessfrequency`
--

CREATE TABLE IF NOT EXISTS `n17illnessfrequency` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n17illnessfrequency`
--

INSERT INTO `n17illnessfrequency` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Regularly (continuous illness)', '1 : Regularly (continuous illness)'),
(2, 'Frequently (fall ill after little gap of time)', '2 : Frequently (fall ill after little gap of time)'),
(3, 'Occasional (fall ill after long gap of time)', '3 : Occasional (fall ill after long gap of time)'),
(4, 'Seldom (Rarely)', '4 : Seldom (Rarely)');

-- --------------------------------------------------------

--
-- Table structure for table `n18healthyfacilityaccessibility`
--

CREATE TABLE IF NOT EXISTS `n18healthyfacilityaccessibility` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n18healthyfacilityaccessibility`
--

INSERT INTO `n18healthyfacilityaccessibility` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Visit health facility during illness ', '1 : Visit health facility during illness '),
(2, 'Financial issue', '2 : Financial issue'),
(3, 'Too far', '3 : Too far'),
(4, 'Poor quality of care', '4 : Poor quality of care'),
(5, 'Cultural reasons', '5 : Cultural reasons'),
(6, 'Lack of time', '6 : Lack of time'),
(7, 'Went to traditional healer', '7 : Went to traditional healer'),
(8, 'Went to religious healer (e.g. Khalifas or Mullahs)', '8 : Went to religious healer (e.g. Khalifas or Mullahs)'),
(9, 'Self medication / Herbal', '9 : Self medication / Herbal'),
(10, 'let condition to be automatically rectified', '10 : let condition to be automatically rectified');

-- --------------------------------------------------------

--
-- Table structure for table `n19nutritionconsumption`
--

CREATE TABLE IF NOT EXISTS `n19nutritionconsumption` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n19nutritionconsumption`
--

INSERT INTO `n19nutritionconsumption` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable (for infants NA as well)', '1 : Not applicable (for infants NA as well)'),
(2, 'Daily ', '2 : Daily '),
(3, '(2-3) times a week', '3 : (2-3) times a week'),
(4, '(4 -6) times a week ', '4 : (4 -6) times a week '),
(5, 'Weekly ', '5 : Weekly '),
(6, 'Fortnightly or more ', '6 : Fortnightly or more '),
(7, 'Do not consume due to health issues ', '7 : Do not consume due to health issues '),
(8, 'Don''t like therefore do not consume ', '8 : Don''t like therefore do not consume '),
(9, 'Cant afford therefore do not consume ', '9 : Cant afford therefore do not consume ');

-- --------------------------------------------------------

--
-- Table structure for table `n20substanceabuse`
--

CREATE TABLE IF NOT EXISTS `n20substanceabuse` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n20substanceabuse`
--

INSERT INTO `n20substanceabuse` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Do not consume (Not applicable)', '1 : Do not consume (Not applicable)'),
(2, 'Highly addicted', '2 : Highly addicted'),
(3, 'Periodic / casual consumer ', '3 : Periodic / casual consumer '),
(4, 'Very low consumer (on events / gathering etc.)', '4 : Very low consumer (on events / gathering etc.)');

-- --------------------------------------------------------

--
-- Table structure for table `n21youthactivityaccessibility`
--

CREATE TABLE IF NOT EXISTS `n21youthactivityaccessibility` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n21youthactivityaccessibility`
--

INSERT INTO `n21youthactivityaccessibility` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable (for infants only)', '1 : Not applicable (for infants only)'),
(2, 'Less than 1 hour', '2 : Less than 1 hour'),
(3, '(1 - 5 hours)', '3 : (1 - 5 hours)'),
(4, '(5 - 10 Hours)', '4 : (5 - 10 Hours)'),
(5, '(More than 10   hours)', '5 : (More than 10   hours)'),
(6, 'No involvement due to No facility ', '6 : No involvement due to No facility '),
(7, 'No involvement due to No access', '7 : No involvement due to No access'),
(8, 'No involvement due to Lack of interest', '8 : No involvement due to Lack of interest'),
(9, 'No involvement due to  Priorities over other\ntasks', '9 : No involvement due to  Priorities over other\ntasks"'),
(10, 'No involvement due to Office burden ', '10 : No involvement due to Office burden '),
(11, 'No involvement due to Personal commitments', '11 : No involvement due to Personal commitments'),
(12, 'No involvement due to Financial issues', '12 : No involvement due to Financial issues'),
(13, 'No involvement due to health issues ', '13 : No involvement due to health issues ');

-- --------------------------------------------------------

--
-- Table structure for table `n22leisure`
--

CREATE TABLE IF NOT EXISTS `n22leisure` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n22leisure`
--

INSERT INTO `n22leisure` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No time at all', '1 : No time at all'),
(2, 'A lot of time', '2 : A lot of time'),
(3, 'Enough time', '3 : Enough time'),
(4, 'Little time', '4 : Little time');

-- --------------------------------------------------------

--
-- Table structure for table `n23qualitylife`
--

CREATE TABLE IF NOT EXISTS `n23qualitylife` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n23qualitylife`
--

INSERT INTO `n23qualitylife` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Excellent ', '1 : Excellent '),
(2, 'Good', '2 : Good'),
(3, 'Fair ', '3 : Fair '),
(4, 'Poor', '4 : Poor');

-- --------------------------------------------------------

--
-- Table structure for table `n24qualitylifestatus`
--

CREATE TABLE IF NOT EXISTS `n24qualitylifestatus` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n24qualitylifestatus`
--

INSERT INTO `n24qualitylifestatus` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Improved a lot', '1 : Improved a lot'),
(2, 'Improved a little', '2 : Improved a little'),
(3, 'Stayed the same', '3 : Stayed the same'),
(4, 'Became a little worse', '4 : Became a little worse'),
(5, 'Became a lot worse', '5 : Became a lot worse');

-- --------------------------------------------------------

--
-- Table structure for table `n25qualitylifechangereason`
--

CREATE TABLE IF NOT EXISTS `n25qualitylifechangereason` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n25qualitylifechangereason`
--

INSERT INTO `n25qualitylifechangereason` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable (who selected Qol status same)', '1 : Not applicable (who selected Qol status same)'),
(2, 'Economic ', '2 : Economic '),
(3, 'Health ', '3 : Health '),
(4, 'Housing ', '4 : Housing '),
(5, 'Education ', '5 : Education '),
(6, 'Family or outside conflicts ', '6 : Family or outside conflicts '),
(7, 'Security ', '7 : Security '),
(8, 'Natural disaster ', '8 : Natural disaster '),
(9, 'others ', '9 : others ');

-- --------------------------------------------------------

--
-- Table structure for table `n26homeconstruction`
--

CREATE TABLE IF NOT EXISTS `n26homeconstruction` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n26homeconstruction`
--

INSERT INTO `n26homeconstruction` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Kacha (mud / wooden made\nhome)', '1 : Kacha (mud / wooden made\nhome)'),
(2, 'Pacca Kacha (wall are proper but roof is not made of concrete)', '2 : Pacca Kacha (wall are proper but roof is not made of concrete)'),
(3, 'Pacca (resident building / bungalows)', '3 : Pacca (resident building / bungalows)');

-- --------------------------------------------------------

--
-- Table structure for table `n27cookingfuel`
--

CREATE TABLE IF NOT EXISTS `n27cookingfuel` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n27cookingfuel`
--

INSERT INTO `n27cookingfuel` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Coal', '1 : Coal'),
(2, 'Wood (fire wood)', '2 : Wood (fire wood)'),
(3, 'Cow dung', '3 : Cow dung'),
(4, 'Electricity', '4 : Electricity'),
(5, 'Gas (Pipe)', '5 : Gas (Pipe)'),
(6, 'Gas (Cylinder)', '6 : Gas (Cylinder)'),
(7, 'Don’t need heating during winter', '7 : Don’t need heating during winter'),
(8, 'Buy food from Outside', '8 : Buy food from Outside');

-- --------------------------------------------------------

--
-- Table structure for table `n28lightingsource`
--

CREATE TABLE IF NOT EXISTS `n28lightingsource` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n28lightingsource`
--

INSERT INTO `n28lightingsource` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Wood (fire wood)', '1 : Wood (fire wood)'),
(2, 'lantern / Candle', '2 : lantern / Candle'),
(3, 'Kerosene', '3 : Kerosene'),
(4, 'Electricity', '4 : Electricity'),
(5, 'Generator ', '5 : Generator '),
(6, 'No source ', '6 : No source ');

-- --------------------------------------------------------

--
-- Table structure for table `n29smokeoutlet`
--

CREATE TABLE IF NOT EXISTS `n29smokeoutlet` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n29smokeoutlet`
--

INSERT INTO `n29smokeoutlet` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No Ventilation', '1 : No Ventilation'),
(2, 'Open roof', '2 : Open roof'),
(3, 'Exhaust Fan', '3 : Exhaust Fan'),
(4, 'Pipe', '4 : Pipe'),
(5, 'Chimney', '5 : Chimney'),
(6, 'Cross Ventilation', '6 : Cross Ventilation'),
(7, 'Open Area', '7 : Open Area');

-- --------------------------------------------------------

--
-- Table structure for table `n30drinkingwatersource`
--

CREATE TABLE IF NOT EXISTS `n30drinkingwatersource` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n30drinkingwatersource`
--

INSERT INTO `n30drinkingwatersource` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Open well', '1 : Open well'),
(2, 'Stream / River / Canal / Lake', '2 : Stream / River / Canal / Lake'),
(3, 'Spring', '3 : Spring'),
(4, 'Tanker', '4 : Tanker'),
(5, 'Boring / Main line (Tap)', '5 : Boring / Main line (Tap)'),
(6, 'Tube well and pump', '6 : Tube well and pump'),
(7, 'Filter water', '7 : Filter water');

-- --------------------------------------------------------

--
-- Table structure for table `n31facilitydistance`
--

CREATE TABLE IF NOT EXISTS `n31facilitydistance` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n31facilitydistance`
--

INSERT INTO `n31facilitydistance` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable', '1 : Not applicable'),
(2, 'At premises', '2 : At premises'),
(3, 'Less than 1 KM ', '3 : Less than 1 KM '),
(4, '(1 - 5) KM', '4 : (1 - 5) KM'),
(5, '(5 - 10) KM', '5 : (5 - 10) KM'),
(6, '(10 - 20) KM', '6 : (10 - 20) KM'),
(7, '(20 - 50) KM', '7 : (20 - 50) KM'),
(8, 'More than 50 KM', '8 : More than 50 KM');

-- --------------------------------------------------------

--
-- Table structure for table `n32toiletfacility`
--

CREATE TABLE IF NOT EXISTS `n32toiletfacility` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n32toiletfacility`
--

INSERT INTO `n32toiletfacility` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Open field / bush (no toilet)', '1 : Open field / bush (no toilet)'),
(2, 'PIT (WC) / Commode (flush toilet) ', '2 : PIT (WC) / Commode (flush toilet) '),
(3, 'Water Privy (Traditional self made)', '3 : Water Privy (Traditional self made)'),
(4, 'Shared Toilet', '4 : Shared Toilet');

-- --------------------------------------------------------

--
-- Table structure for table `n33residancestatus`
--

CREATE TABLE IF NOT EXISTS `n33residancestatus` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n33residancestatus`
--

INSERT INTO `n33residancestatus` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Owned', '1 : Owned'),
(2, 'Rented / Pagdi', '2 : Rented / Pagdi'),
(3, 'Rent free ', '3 : Rent free ');

-- --------------------------------------------------------

--
-- Table structure for table `n34confilict`
--

CREATE TABLE IF NOT EXISTS `n34confilict` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n34confilict`
--

INSERT INTO `n34confilict` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No conflicts ', '1 : No conflicts '),
(2, 'Family conflicts ', '2 : Family conflicts '),
(3, 'Outside conflicts ', '3 : Outside conflicts '),
(4, 'Both ', '4 : Both ');

-- --------------------------------------------------------

--
-- Table structure for table `n35disputeresolution`
--

CREATE TABLE IF NOT EXISTS `n35disputeresolution` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n35disputeresolution`
--

INSERT INTO `n35disputeresolution` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'Conciliation and Arbitration Board', '2 : Conciliation and Arbitration Board'),
(3, 'Jamati Leaders (mukhi / Kamadia)', '3 : Jamati Leaders (mukhi / Kamadia)'),
(4, 'Local / Community leaders', '4 : Local / Community leaders'),
(5, 'Professional Counselors', '5 : Professional Counselors'),
(6, 'Legal advisor / lawyer / Court', '6 : Legal advisor / lawyer / Court'),
(7, 'Relatives / Friends', '7 : Relatives / Friends'),
(8, 'Local influential groups (Jirga / panchait )', '8 : Local influential groups (Jirga / panchait )'),
(9, 'Human Rights Group', '9 : Human Rights Group'),
(10, 'Resolve within family ', '10 : Resolve within family ');

-- --------------------------------------------------------

--
-- Table structure for table `n36seniorsliving`
--

CREATE TABLE IF NOT EXISTS `n36seniorsliving` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n36seniorsliving`
--

INSERT INTO `n36seniorsliving` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable', '1 : Not applicable'),
(2, 'Living independently separately ', '2 : Living independently separately '),
(3, 'Relatives', '3 : Relatives'),
(4, 'Others caring institution', '4 : Others caring institution'),
(5, 'Don’t know', '5 : Don’t know');

-- --------------------------------------------------------

--
-- Table structure for table `n37seniorsinvolmentreason`
--

CREATE TABLE IF NOT EXISTS `n37seniorsinvolmentreason` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n37seniorsinvolmentreason`
--

INSERT INTO `n37seniorsinvolmentreason` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Disability / on bed ', '1 : Disability / on bed '),
(2, 'No interest in socializing ', '2 : No interest in socializing '),
(3, 'No place for socializing ', '3 : No place for socializing '),
(4, 'Occupied in other home related activities ', '4 : Occupied in other home related activities '),
(5, 'Not applicable', '5 : Not applicable');

-- --------------------------------------------------------

--
-- Table structure for table `n38incomestatus`
--

CREATE TABLE IF NOT EXISTS `n38incomestatus` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n38incomestatus`
--

INSERT INTO `n38incomestatus` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Income over Expenditure (Surplus )', '1 : Income over Expenditure (Surplus )'),
(2, 'Expenditure over  Income (Deficit)', '2 : Expenditure over  Income (Deficit)'),
(3, 'Breakeven (Income equals expenditures)', '3 : Breakeven (Income equals expenditures)');

-- --------------------------------------------------------

--
-- Table structure for table `n39savingmode`
--

CREATE TABLE IF NOT EXISTS `n39savingmode` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n39savingmode`
--

INSERT INTO `n39savingmode` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable', '1 : Not applicable'),
(2, 'Micro Finance', '2 : Micro Finance'),
(3, 'Commercial Bank', '3 : Commercial Bank'),
(4, 'Cash at home', '4 : Cash at home'),
(5, 'Community Based Saving  Groups (LSO)', '5 : Community Based Saving  Groups (LSO)'),
(6, 'Credit Society', '6 : Credit Society'),
(7, 'Ornaments (jewellery gold, silver Etc.)', '7 : Ornaments (jewellery gold, silver Etc.)'),
(8, 'Saving Bonds, Saving Certificates etc.', '8 : Saving Bonds, Saving Certificates etc.'),
(9, 'Mutual funds', '9 : Mutual funds'),
(10, 'Stocks (shares)', '10 : Stocks (shares)'),
(11, 'Property', '11 : Property'),
(12, 'Informal Committee System (BISI)', '12 : Informal Committee System (BISI)');

-- --------------------------------------------------------

--
-- Table structure for table `n40deficitmanage`
--

CREATE TABLE IF NOT EXISTS `n40deficitmanage` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n40deficitmanage`
--

INSERT INTO `n40deficitmanage` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'Welfare Assistance (Jamati Institution)', '2 : Welfare Assistance (Jamati Institution)'),
(3, 'Welfare Assistance (Other faith based institutions)', '3 : Welfare Assistance (Other faith based institutions)'),
(4, 'Financial help within the family', '4 : Financial help within the family'),
(5, 'Interest bearing loans', '5 : Interest bearing loans'),
(6, 'Financial help from relatives / friends / neighbors ', '6 : Financial help from relatives / friends / neighbors ');

-- --------------------------------------------------------

--
-- Table structure for table `n41savingavaliblity`
--

CREATE TABLE IF NOT EXISTS `n41savingavaliblity` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n41savingavaliblity`
--

INSERT INTO `n41savingavaliblity` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, '(less than 3 months)', '1 : (less than 3 months)'),
(2, '(3 - 6 months)', '2 : (3 - 6 months)'),
(3, '(6 to 12 months)', '3 : (6 to 12 months)'),
(4, '(More than one year)', '4 : (More than one year)');

-- --------------------------------------------------------

--
-- Table structure for table `n42outstandingdebt`
--

CREATE TABLE IF NOT EXISTS `n42outstandingdebt` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n42outstandingdebt`
--

INSERT INTO `n42outstandingdebt` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Formal (legal ) channel (Banks, landing agencies and employer)', '1 : Formal (legal ) channel (Banks, landing agencies and employer)'),
(2, 'Informal channel  (shopkeeper, landlord, individual etc.)', '2 : Informal channel  (shopkeeper, landlord, individual etc.)'),
(3, 'No outstanding debt', '3 : No outstanding debt');

-- --------------------------------------------------------

--
-- Table structure for table `n43interestrate`
--

CREATE TABLE IF NOT EXISTS `n43interestrate` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n43interestrate`
--

INSERT INTO `n43interestrate` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable ', '1 : Not applicable '),
(2, 'less than 20%', '2 : less than 20%'),
(3, '(21-50%)', '3 : (21-50%)'),
(4, 'More than 50%', '4 : More than 50%'),
(5, 'Interest free ', '5 : Interest free ');

-- --------------------------------------------------------

--
-- Table structure for table `n44loanpurpose`
--

CREATE TABLE IF NOT EXISTS `n44loanpurpose` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n44loanpurpose`
--

INSERT INTO `n44loanpurpose` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Not applicable', '1 : Not applicable'),
(2, 'Purchase or construction of house', '2 : Purchase or construction of house'),
(3, 'Improving / refurbishing home', '3 : Improving / refurbishing home'),
(4, 'Business/trading', '4 : Business/trading'),
(5, 'Agriculture', '5 : Agriculture'),
(6, 'Education of children', '6 : Education of children'),
(7, 'Migration', '7 : Migration'),
(8, 'Health', '8 : Health'),
(9, 'Social obligations (e.g. dowry, wedding etc.)', '9 : Social obligations (e.g. dowry, wedding etc.)'),
(10, 'Durable goods', '10 : Durable goods'),
(11, 'Daily expenses', '11 : Daily expenses'),
(12, 'Loan or debt payments', '12 : Loan or debt payments'),
(13, 'Clothing', '13 : Clothing'),
(14, 'Utilities bills', '14 : Utilities bills'),
(15, 'Others ', '15 : Others ');

-- --------------------------------------------------------

--
-- Table structure for table `n45deliverycarriedby`
--

CREATE TABLE IF NOT EXISTS `n45deliverycarriedby` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n45deliverycarriedby`
--

INSERT INTO `n45deliverycarriedby` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Licensed Skilled Attendant (LSA) attendants includes Registered Doctors , LHV''s, Midwifery & RN. ', '1 : Licensed Skilled Attendant (LSA) attendants includes Registered Doctors , LHV''s, Midwifery & RN. '),
(2, 'Traditional Birth Attendants (TBA) includes trained dai''s (CHN''s / CHW)', '2 : Traditional Birth Attendants (TBA) includes trained dai''s (CHN''s / CHW)'),
(3, 'Unskilled Attendant (UA) includes all not license individual like untrained traditional birth attendant, older women', '3 : Unskilled Attendant (UA) includes all not license individual like untrained traditional birth attendant, older women');

-- --------------------------------------------------------

--
-- Table structure for table `n46businesstype`
--

CREATE TABLE IF NOT EXISTS `n46businesstype` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n46businesstype`
--

INSERT INTO `n46businesstype` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'Auto Mobile / Spare parts', '1 : Auto Mobile / Spare parts'),
(2, 'Bakery / Confectionery', '2 : Bakery / Confectionery'),
(3, 'Beauty Parlor / Salon', '3 : Beauty Parlor / Salon'),
(4, 'Dairy Products / Daily farm', '4 : Dairy Products / Daily farm'),
(5, 'Electronic & Electrical appliances', '5 : Electronic & Electrical appliances'),
(6, 'Entertainment', '6 : Entertainment'),
(7, 'Farming - Cereals / Grains', '7 : Farming - Cereals / Grains'),
(8, 'Farming - Fruits / Orchards', '8 : Farming - Fruits / Orchards'),
(9, 'Farming - Vegetables ', '9 : Farming - Vegetables '),
(10, 'Furniture / Timber / Interior Decoration', '10 : Furniture / Timber / Interior Decoration'),
(11, 'Garments / Hosiery / Linen & Textile', '11 : Garments / Hosiery / Linen & Textile'),
(12, 'General / Provision / Pan Shop', '12 : General / Provision / Pan Shop'),
(13, 'Gift items / Bookshop & Stationery', '13 : Gift items / Bookshop & Stationery'),
(14, 'Grocery / Vegetables', '14 : Grocery / Vegetables'),
(15, 'Handicrafts / Carpet', '15 : Handicrafts / Carpet'),
(16, 'Iron & Steel', '16 : Iron & Steel'),
(17, 'Jewelers / Gem stones', '17 : Jewelers / Gem stones'),
(18, 'Laundry', '18 : Laundry'),
(19, 'Leather / Foot Wear', '19 : Leather / Foot Wear'),
(20, 'Machinery Manufacturers', '20 : Machinery Manufacturers'),
(21, 'Paper / Printing', '21 : Paper / Printing'),
(22, 'Petrol Pump', '22 : Petrol Pump'),
(23, 'Pharmaceuticals / Medical Supplies', '23 : Pharmaceuticals / Medical Supplies'),
(24, 'Poultry /  Meat & Fish', '24 : Poultry /  Meat & Fish'),
(25, 'Real Estate & Property Rental', '25 : Real Estate & Property Rental'),
(26, 'Restaurant / Hotel & Food', '26 : Restaurant / Hotel & Food'),
(27, 'School / Coaching / Tuition Centre', '27 : School / Coaching / Tuition Centre'),
(28, 'Services - Clearing Forwarding', '28 : Services - Clearing Forwarding'),
(29, 'Services - Clinic / Hospital', '29 : Services - Clinic / Hospital'),
(30, 'Services - Financial / Stock / Legal', '30 : Services - Financial / Stock / Legal'),
(31, 'Goods & Services - I.T. (Hardware / Software)', '31 : Goods & Services - I.T. (Hardware / Software)'),
(32, 'Services - Transport', '32 : Services - Transport'),
(33, 'Sports Goods', '33 : Sports Goods'),
(34, 'Travel & Tourism', '34 : Travel & Tourism'),
(35, 'Workshop / Garage', '35 : Workshop / Garage'),
(36, 'Fertilizers / pesticides', '36 : Fertilizers / pesticides'),
(37, 'Others', '37 : Others');

-- --------------------------------------------------------

--
-- Table structure for table `n47financialrisk`
--

CREATE TABLE IF NOT EXISTS `n47financialrisk` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n47financialrisk`
--

INSERT INTO `n47financialrisk` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No risk', '1 : No risk'),
(2, 'Access to credit (insufficient funds)', '2 : Access to credit (insufficient funds)'),
(3, 'Robbery /loot / extortion money', '3 : Robbery /loot / extortion money'),
(4, 'Increasing debt / loan ratio ', '4 : Increasing debt / loan ratio '),
(5, 'Decreasing recovery ratio from customers ', '5 : Decreasing recovery ratio from customers ');

-- --------------------------------------------------------

--
-- Table structure for table `n48physicalrisk`
--

CREATE TABLE IF NOT EXISTS `n48physicalrisk` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `n48physicalrisk`
--

INSERT INTO `n48physicalrisk` (`uid`, `label`, `uidLabel`) VALUES
(0, 'Not Answered', '0 : Not Answered'),
(1, 'No risk ', '1 : No risk '),
(2, 'Structural hazard', '2 : Structural hazard'),
(3, 'Illegal /unregistered business area / space ', '3 : Illegal /unregistered business area / space '),
(4, 'Inappropriate political surrounding ', '4 : Inappropriate political surrounding ');

-- --------------------------------------------------------

--
-- Table structure for table `regionalcouncil`
--

CREATE TABLE IF NOT EXISTS `regionalcouncil` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  `uidLabel` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `regionalcouncil`
--

INSERT INTO `regionalcouncil` (`uid`, `label`, `uidLabel`) VALUES
(1, 'Karachi & Balochistan', 'Karachi & Balochistan'),
(2, 'Sindh', 'Sindh'),
(3, 'Punjab', 'Punjab'),
(4, 'RIMPA', 'RIMPA'),
(5, 'Hunza', 'Hunza'),
(6, 'Gilgit', 'Gilgit'),
(7, 'Ishkoman Puniyal', 'Ishkoman Puniyal'),
(8, 'Gupis Yasin', 'Gupis Yasin'),
(9, 'Lower Chitral', 'Lower Chitral'),
(10, 'Upper Chitral ', 'Upper Chitral ');

-- --------------------------------------------------------

--
-- Table structure for table `temp`
--

CREATE TABLE IF NOT EXISTS `temp` (
  `ruid` int(11) DEFAULT NULL,
  `regional` varchar(255) DEFAULT NULL,
  `luid` int(11) DEFAULT NULL,
  `local` varchar(255) DEFAULT NULL,
  `juid` int(11) DEFAULT NULL,
  `jamatkhana` varchar(255) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `temp`
--

INSERT INTO `temp` (`ruid`, `regional`, `luid`, `local`, `juid`, `jamatkhana`) VALUES
(6, 'Gilgit', 25, 'Danyore', 1, 'DANYORE1'),
(6, 'Gilgit', 25, 'Danyore', 2, 'DANYORE10'),
(6, 'Gilgit', 25, 'Danyore', 3, 'DANYORE11'),
(6, 'Gilgit', 25, 'Danyore', 4, 'DANYORE12'),
(6, 'Gilgit', 25, 'Danyore', 5, 'DANYORE2'),
(6, 'Gilgit', 25, 'Danyore', 6, 'DANYORE3'),
(6, 'Gilgit', 25, 'Danyore', 7, 'DANYORE4'),
(6, 'Gilgit', 25, 'Danyore', 8, 'DANYORE5'),
(6, 'Gilgit', 25, 'Danyore', 9, 'DANYORE6'),
(6, 'Gilgit', 25, 'Danyore', 10, 'DANYORE7'),
(6, 'Gilgit', 25, 'Danyore', 11, 'DANYORE8'),
(6, 'Gilgit', 25, 'Danyore', 12, 'DANYORE9'),
(6, 'Gilgit', 25, 'Danyore', 13, 'OSHIKHANDAS1'),
(6, 'Gilgit', 25, 'Danyore', 14, 'OSHIKHANDAS2'),
(6, 'Gilgit', 25, 'Danyore', 15, 'OSHIKHANDAS3'),
(6, 'Gilgit', 25, 'Danyore', 16, 'OSHIKHANDAS4'),
(6, 'Gilgit', 25, 'Danyore', 17, 'OSHIKHANDAS5'),
(6, 'Gilgit', 25, 'Danyore', 18, 'OSHIKHANDAS6'),
(6, 'Gilgit', 25, 'Danyore', 19, 'OSHIKHANDAS7'),
(6, 'Gilgit', 25, 'Danyore', 20, 'OSHIKHANDAS8'),
(6, 'Gilgit', 25, 'Danyore', 21, 'RAHIMABAD1'),
(6, 'Gilgit', 25, 'Danyore', 22, 'RAHIMABAD2'),
(6, 'Gilgit', 25, 'Danyore', 23, 'RAHIMABAD3'),
(6, 'Gilgit', 25, 'Danyore', 24, 'RAHIMABAD4'),
(6, 'Gilgit', 25, 'Danyore', 25, 'SULTANABAD1'),
(6, 'Gilgit', 25, 'Danyore', 26, 'SULTANABAD2'),
(6, 'Gilgit', 25, 'Danyore', 27, 'SULTANABAD3'),
(6, 'Gilgit', 25, 'Danyore', 28, 'SULTANABAD4'),
(6, 'Gilgit', 24, 'Gilgit', 29, 'AMYNABAD Jutiyal'),
(6, 'Gilgit', 24, 'Gilgit', 30, 'AMYNABAD1 NOMAL'),
(6, 'Gilgit', 24, 'Gilgit', 31, 'AMYNABAD2 NOMAL'),
(6, 'Gilgit', 24, 'Gilgit', 32, 'Baladul Karim JK'),
(6, 'Gilgit', 24, 'Gilgit', 33, 'GILGIT Central'),
(6, 'Gilgit', 24, 'Gilgit', 34, 'Hussainabad'),
(6, 'Gilgit', 24, 'Gilgit', 35, 'JUTIYAL2 UPPER'),
(6, 'Gilgit', 24, 'Gilgit', 36, 'KHOMER'),
(6, 'Gilgit', 24, 'Gilgit', 37, 'MADINATUL KARIM Nomal-4'),
(6, 'Gilgit', 24, 'Gilgit', 38, 'MUJAHID COLONY'),
(6, 'Gilgit', 24, 'Gilgit', 39, 'NOOR COLONY'),
(6, 'Gilgit', 24, 'Gilgit', 40, 'NOORABAD'),
(6, 'Gilgit', 24, 'Gilgit', 41, 'SADRUDDINABAD Nomal-3'),
(6, 'Gilgit', 24, 'Gilgit', 42, 'Satellite Colony'),
(6, 'Gilgit', 24, 'Gilgit', 43, 'SONIKOTE'),
(6, 'Gilgit', 24, 'Gilgit', 44, 'YASIN COLONY'),
(6, 'Gilgit', 24, 'Gilgit', 45, 'ZULFIQARABAD'),
(6, 'Gilgit', 26, 'Sakrdu', 46, 'KHAPLU, Baltistan'),
(6, 'Gilgit', 26, 'Sakrdu', 47, 'SATELLITE TOWN Skardu'),
(6, 'Gilgit', 26, 'Sakrdu', 48, 'SKARDU'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 49, 'ALYABAD Hundrap-3'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 50, 'BARSET-1'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 51, 'BARSET-2'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 52, 'BARSET-3'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 53, 'BARSET-4 Brundan'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 54, 'BARSET-5 Akhtobaring'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 55, 'Barset-6 Uthali'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 56, 'BOYANDEH'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 57, 'GHOLAGHMULLI Central'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 58, 'GHOLAGHTORI'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 59, 'HELTY'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 60, 'HUNDRAP-1 Paien'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 61, 'HUNDRAP-2 Bala'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 62, 'HUSSAINABAD'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 63, 'KARIMABAD Teru-4'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 64, 'KHONANDEH'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 65, 'SHAHIMAL'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 66, 'TERECH'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 67, 'TERU-1 Bahachi'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 68, 'TERU-2 Paien'),
(8, 'Gupis Yasin', 36, 'Gulaghmuli', 69, 'TERU-3 Bala'),
(8, 'Gupis Yasin', 33, 'Gupis', 70, 'ALi BASTI'),
(8, 'Gupis Yasin', 33, 'Gupis', 71, 'ALYABAD'),
(8, 'Gupis Yasin', 33, 'Gupis', 72, 'CHIRMAYUN'),
(8, 'Gupis Yasin', 33, 'Gupis', 73, 'DALTI'),
(8, 'Gupis Yasin', 33, 'Gupis', 74, 'DODOSHOT'),
(8, 'Gupis Yasin', 33, 'Gupis', 75, 'DROTE'),
(8, 'Gupis Yasin', 33, 'Gupis', 76, 'DURNAK'),
(8, 'Gupis Yasin', 33, 'Gupis', 77, 'GAMIS'),
(8, 'Gupis Yasin', 33, 'Gupis', 78, 'GUPIS Central'),
(8, 'Gupis Yasin', 33, 'Gupis', 79, 'GUPIS-1 Daas'),
(8, 'Gupis Yasin', 33, 'Gupis', 80, 'GUPIS2 BALA'),
(8, 'Gupis Yasin', 33, 'Gupis', 81, 'HAKIS'),
(8, 'Gupis Yasin', 33, 'Gupis', 82, 'HAMARDAS'),
(8, 'Gupis Yasin', 33, 'Gupis', 83, 'KARIMABAD-1 Sub Centre'),
(8, 'Gupis Yasin', 33, 'Gupis', 84, 'KARIMABAD-2 Bala'),
(8, 'Gupis Yasin', 33, 'Gupis', 85, 'KARIMABAD3 PAEEN'),
(8, 'Gupis Yasin', 33, 'Gupis', 86, 'KASHBAT HAJIABAD'),
(8, 'Gupis Yasin', 33, 'Gupis', 87, 'KHALTI'),
(8, 'Gupis Yasin', 33, 'Gupis', 88, 'MAULAABAD'),
(8, 'Gupis Yasin', 33, 'Gupis', 89, 'NOORABAD'),
(8, 'Gupis Yasin', 33, 'Gupis', 90, 'RAUSHAN'),
(8, 'Gupis Yasin', 33, 'Gupis', 91, 'SUMAL CENTRE'),
(8, 'Gupis Yasin', 33, 'Gupis', 92, 'TALIDAS'),
(8, 'Gupis Yasin', 33, 'Gupis', 93, 'YANGAL1 PAEEN'),
(8, 'Gupis Yasin', 33, 'Gupis', 94, 'YANGAL2 BALA'),
(8, 'Gupis Yasin', 33, 'Gupis', 95, 'Zulfiqar Abad'),
(8, 'Gupis Yasin', 35, 'Phunder', 96, 'ALYABAD Anotek'),
(8, 'Gupis Yasin', 35, 'Phunder', 97, 'ALYABAD Shamaran-4'),
(8, 'Gupis Yasin', 35, 'Phunder', 98, 'BEREST'),
(8, 'Gupis Yasin', 35, 'Phunder', 99, 'CHASHI-1 Bala'),
(8, 'Gupis Yasin', 35, 'Phunder', 100, 'CHASHI-2 Paien'),
(8, 'Gupis Yasin', 35, 'Phunder', 101, 'CHASHI-3 Tologh'),
(8, 'Gupis Yasin', 35, 'Phunder', 102, 'DALOMAL'),
(8, 'Gupis Yasin', 35, 'Phunder', 103, 'DALOMAL - 2'),
(8, 'Gupis Yasin', 35, 'Phunder', 104, 'DERBARKOLTI-1 Paien'),
(8, 'Gupis Yasin', 35, 'Phunder', 105, 'DERBARKOLTI-2 Bala'),
(8, 'Gupis Yasin', 35, 'Phunder', 106, 'GHOLOGH'),
(8, 'Gupis Yasin', 35, 'Phunder', 107, 'JAFFARABAD Joharbabad'),
(8, 'Gupis Yasin', 35, 'Phunder', 108, 'KINOTE'),
(8, 'Gupis Yasin', 35, 'Phunder', 109, 'MOMEENABAD Shamaran-5'),
(8, 'Gupis Yasin', 35, 'Phunder', 110, 'PHUNDER Central'),
(8, 'Gupis Yasin', 35, 'Phunder', 111, 'RAHIMABAD Derberkulti-3'),
(8, 'Gupis Yasin', 35, 'Phunder', 112, 'RAWAT - 2'),
(8, 'Gupis Yasin', 35, 'Phunder', 113, 'RAWAT Shamaran-3'),
(8, 'Gupis Yasin', 35, 'Phunder', 114, 'SERBAL'),
(8, 'Gupis Yasin', 35, 'Phunder', 115, 'SHAHABAD'),
(8, 'Gupis Yasin', 35, 'Phunder', 116, 'SHAMARAN-1'),
(8, 'Gupis Yasin', 35, 'Phunder', 117, 'SHAMARAN-2'),
(8, 'Gupis Yasin', 35, 'Phunder', 118, 'SHINGLAT'),
(8, 'Gupis Yasin', 34, 'Pingal', 119, 'DAHIMAL'),
(8, 'Gupis Yasin', 34, 'Pingal', 120, 'HAMER Rahimabad'),
(8, 'Gupis Yasin', 34, 'Pingal', 121, 'JIKARUM'),
(8, 'Gupis Yasin', 34, 'Pingal', 122, 'JOLIJAL'),
(8, 'Gupis Yasin', 34, 'Pingal', 123, 'KHALTI - 2 Paien'),
(8, 'Gupis Yasin', 34, 'Pingal', 124, 'KHASUNDER'),
(8, 'Gupis Yasin', 34, 'Pingal', 125, 'MATUJ LASHT'),
(8, 'Gupis Yasin', 34, 'Pingal', 126, 'NOGHOOR LASHT'),
(8, 'Gupis Yasin', 34, 'Pingal', 127, 'NOLTY'),
(8, 'Gupis Yasin', 34, 'Pingal', 128, 'PAYUKHUSH'),
(8, 'Gupis Yasin', 34, 'Pingal', 129, 'PINGAL'),
(8, 'Gupis Yasin', 34, 'Pingal', 130, 'SARALO KHOTU'),
(8, 'Gupis Yasin', 34, 'Pingal', 131, 'SOSOT'),
(8, 'Gupis Yasin', 34, 'Pingal', 132, 'THANGAI Sub-Centre'),
(8, 'Gupis Yasin', 39, 'Silgan', 133, 'BERKULTEY-1'),
(8, 'Gupis Yasin', 39, 'Silgan', 134, 'BERKULTEY-2'),
(8, 'Gupis Yasin', 39, 'Silgan', 135, 'BERKULTEY-3'),
(8, 'Gupis Yasin', 39, 'Silgan', 136, 'BERKULTEY-4'),
(8, 'Gupis Yasin', 39, 'Silgan', 137, 'BERKULTEY-5'),
(8, 'Gupis Yasin', 39, 'Silgan', 138, 'BERKULTEY-6'),
(8, 'Gupis Yasin', 39, 'Silgan', 139, 'BERKULTEY-7 Shaghitan'),
(8, 'Gupis Yasin', 39, 'Silgan', 140, 'BERKULTEY-8'),
(8, 'Gupis Yasin', 39, 'Silgan', 141, 'DARKUT-1'),
(8, 'Gupis Yasin', 39, 'Silgan', 142, 'DARKUT-2'),
(8, 'Gupis Yasin', 39, 'Silgan', 143, 'DARKUT-3'),
(8, 'Gupis Yasin', 39, 'Silgan', 144, 'DARKUT-4 Gamelti'),
(8, 'Gupis Yasin', 39, 'Silgan', 145, 'DARKUT-5 Gartenz'),
(8, 'Gupis Yasin', 39, 'Silgan', 146, 'DARKUT-6 Ghasum'),
(8, 'Gupis Yasin', 39, 'Silgan', 147, 'HUNDUR Central'),
(8, 'Gupis Yasin', 39, 'Silgan', 148, 'HUNDUR-2'),
(8, 'Gupis Yasin', 39, 'Silgan', 149, 'HUNDUR-3'),
(8, 'Gupis Yasin', 39, 'Silgan', 150, 'HUNDUR-4'),
(8, 'Gupis Yasin', 39, 'Silgan', 151, 'HUNDUR-5'),
(8, 'Gupis Yasin', 39, 'Silgan', 152, 'HUNDUR-6 Burakot'),
(8, 'Gupis Yasin', 39, 'Silgan', 153, 'HUNDUR-7 Terset'),
(8, 'Gupis Yasin', 39, 'Silgan', 154, 'ISTORDANI'),
(8, 'Gupis Yasin', 39, 'Silgan', 155, 'UMALSAT-1'),
(8, 'Gupis Yasin', 39, 'Silgan', 156, 'UMALSAT-2'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 157, 'CHIKMOJOOR'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 158, 'DALSANDEY'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 159, 'GOJALTI-1'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 160, 'GOJALTI-2'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 161, 'MAWLAABAD Sandey-4'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 162, 'QARKULTEY-1 Paien'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 163, 'QARKULTEY-2 Bala'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 164, 'QARKULTEY-3 Bardass'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 165, 'SANDEY - 1'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 166, 'SANDEY - 2 Paien'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 167, 'SANDEY - 3'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 168, 'SANDEY - 5'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 169, 'SULTANABAD-1 SubCentre'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 170, 'SULTANABAD-2'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 171, 'SULTANABAD-3'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 172, 'SULTANABAD-4'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 173, 'TAOUS-1'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 174, 'TAOUS-2'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 175, 'TAOUS-3'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 176, 'TAOUS-4'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 177, 'TAOUS-5'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 178, 'TAOUS-6'),
(8, 'Gupis Yasin', 38, 'Sultanabad', 179, 'TAOUS-7'),
(8, 'Gupis Yasin', 40, 'Thoi', 180, 'CHIRYAT'),
(8, 'Gupis Yasin', 40, 'Thoi', 181, 'DALKOI'),
(8, 'Gupis Yasin', 40, 'Thoi', 182, 'DAPIS'),
(8, 'Gupis Yasin', 40, 'Thoi', 183, 'DASS-1 Paien'),
(8, 'Gupis Yasin', 40, 'Thoi', 184, 'DASS-2 Bala'),
(8, 'Gupis Yasin', 40, 'Thoi', 185, 'DRACH'),
(8, 'Gupis Yasin', 40, 'Thoi', 186, 'DRASKIN'),
(8, 'Gupis Yasin', 40, 'Thoi', 187, 'GHAINCHEL'),
(8, 'Gupis Yasin', 40, 'Thoi', 188, 'HARF-2 Bala'),
(8, 'Gupis Yasin', 40, 'Thoi', 189, 'HARF-3 Paien'),
(8, 'Gupis Yasin', 40, 'Thoi', 190, 'ISHKAIBAR'),
(8, 'Gupis Yasin', 40, 'Thoi', 191, 'ISHKAMDAS'),
(8, 'Gupis Yasin', 40, 'Thoi', 192, 'KARIMABAD'),
(8, 'Gupis Yasin', 40, 'Thoi', 193, 'KUNO'),
(8, 'Gupis Yasin', 40, 'Thoi', 194, 'NALTY - 1'),
(8, 'Gupis Yasin', 40, 'Thoi', 195, 'NALTY -2'),
(8, 'Gupis Yasin', 40, 'Thoi', 196, 'QADAMABAD'),
(8, 'Gupis Yasin', 40, 'Thoi', 197, 'RAHIMABAD'),
(8, 'Gupis Yasin', 40, 'Thoi', 198, 'SHOTE'),
(8, 'Gupis Yasin', 40, 'Thoi', 199, 'THELTI'),
(8, 'Gupis Yasin', 40, 'Thoi', 200, 'THOI Central Harf-1'),
(8, 'Gupis Yasin', 37, 'Yasin', 201, 'ATKASH'),
(8, 'Gupis Yasin', 37, 'Yasin', 202, 'BARKHAI'),
(8, 'Gupis Yasin', 37, 'Yasin', 203, 'BOJAYOUT'),
(8, 'Gupis Yasin', 37, 'Yasin', 204, 'DAMALGUN'),
(8, 'Gupis Yasin', 37, 'Yasin', 205, 'Gindai Bala'),
(8, 'Gupis Yasin', 37, 'Yasin', 206, 'GINDAI-1'),
(8, 'Gupis Yasin', 37, 'Yasin', 207, 'GINDAI-2'),
(8, 'Gupis Yasin', 37, 'Yasin', 208, 'HELTER'),
(8, 'Gupis Yasin', 37, 'Yasin', 209, 'MAHSHER'),
(8, 'Gupis Yasin', 37, 'Yasin', 210, 'MURKAH'),
(8, 'Gupis Yasin', 37, 'Yasin', 211, 'NAZBAR-1'),
(8, 'Gupis Yasin', 37, 'Yasin', 212, 'NAZBAR-2'),
(8, 'Gupis Yasin', 37, 'Yasin', 213, 'NAZBAR-3'),
(8, 'Gupis Yasin', 37, 'Yasin', 214, 'NAZBAR-4'),
(8, 'Gupis Yasin', 37, 'Yasin', 215, 'NOOH-1 Bala'),
(8, 'Gupis Yasin', 37, 'Yasin', 216, 'NOOH-2 Paien'),
(8, 'Gupis Yasin', 37, 'Yasin', 217, 'YASIN Central'),
(8, 'Gupis Yasin', 37, 'Yasin', 218, 'YASIN-2'),
(8, 'Gupis Yasin', 37, 'Yasin', 219, 'YASIN-3'),
(8, 'Gupis Yasin', 37, 'Yasin', 220, 'YASIN-4 Manich'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 221, 'AGAKHANABAD'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 222, 'ALYABAD Central'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 223, 'BERBER'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 224, 'BROONGSHAL'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 225, 'CHUMER KHAND'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 226, 'DOOR KHUN'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 227, 'FARMANABAD MURTAZAABAD-3'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 228, 'GARELTH'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 229, 'HASSANABAD1'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 230, 'HASSANABAD2'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 231, 'HASSANABAD3'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 232, 'HASSANABAD4'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 233, 'HYDERABAD Central'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 234, 'KHURU KHUSHAL'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 235, 'MURTAZAABAD1'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 236, 'MURTAZAABAD2'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 237, 'MURTAZAABAD4'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 238, 'RAHIMABAD'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 239, 'SHAHABAD'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 240, 'SHERAZ'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 241, 'SINA KHAN'),
(5, 'Hunza', 18, 'Aliabad Hyderabad', 242, 'SULTANABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 243, 'AGA KHAN TEKKERY Doiker'),
(5, 'Hunza', 17, 'Altit Karimabad', 244, 'AHMEDABAD1 PAYEEN'),
(5, 'Hunza', 17, 'Altit Karimabad', 245, 'AHMEDABAD2 BALA'),
(5, 'Hunza', 17, 'Altit Karimabad', 246, 'ALMUSTAFABAD  BRUMBUN'),
(5, 'Hunza', 17, 'Altit Karimabad', 247, 'ALTIT Central'),
(5, 'Hunza', 17, 'Altit Karimabad', 248, 'AMYNABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 249, 'ATTAABAD1 BALA'),
(5, 'Hunza', 17, 'Altit Karimabad', 250, 'ATTAABAD2 PAYEEN'),
(5, 'Hunza', 17, 'Altit Karimabad', 251, 'BARASHAL'),
(5, 'Hunza', 17, 'Altit Karimabad', 252, 'BROONGSHAL'),
(5, 'Hunza', 17, 'Altit Karimabad', 253, 'CHOBI KHUSHAL'),
(5, 'Hunza', 17, 'Altit Karimabad', 254, 'EMANABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 255, 'FAIZABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 256, 'GANESH Kalan'),
(5, 'Hunza', 17, 'Altit Karimabad', 257, 'GARA BEREZ'),
(5, 'Hunza', 17, 'Altit Karimabad', 258, 'GHAMESH'),
(5, 'Hunza', 17, 'Altit Karimabad', 259, 'HERCHI'),
(5, 'Hunza', 17, 'Altit Karimabad', 260, 'IMAMYARABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 261, 'KARIMABAD Central'),
(5, 'Hunza', 17, 'Altit Karimabad', 262, 'KARIMABAD2'),
(5, 'Hunza', 17, 'Altit Karimabad', 263, 'MOHAMMEDABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 264, 'MOMINABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 265, 'RAHIMABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 266, 'SALMANABAD1 BALA'),
(5, 'Hunza', 17, 'Altit Karimabad', 267, 'SALMANABAD-2 Paien'),
(5, 'Hunza', 17, 'Altit Karimabad', 268, 'SARAT SUB CENTRE'),
(5, 'Hunza', 17, 'Altit Karimabad', 269, 'SHANO KHUSHAL'),
(5, 'Hunza', 17, 'Altit Karimabad', 270, 'SULTANABAD'),
(5, 'Hunza', 17, 'Altit Karimabad', 271, 'ZIARAT Astana'),
(5, 'Hunza', 23, 'Chipurson', 272, 'AMYNABAD Yezerich'),
(5, 'Hunza', 23, 'Chipurson', 273, 'ISPANGE'),
(5, 'Hunza', 23, 'Chipurson', 274, 'KHAIRABAD'),
(5, 'Hunza', 23, 'Chipurson', 275, 'KHIL1'),
(5, 'Hunza', 23, 'Chipurson', 276, 'KHIL-2 Chupersan'),
(5, 'Hunza', 23, 'Chipurson', 277, 'KIRMIN1 RAHIMABAD'),
(5, 'Hunza', 23, 'Chipurson', 278, 'KIRMIN2 AMYNABAD'),
(5, 'Hunza', 23, 'Chipurson', 279, 'KIRMIN3 NOORABAD'),
(5, 'Hunza', 23, 'Chipurson', 280, 'RESHIT Central'),
(5, 'Hunza', 23, 'Chipurson', 281, 'SHAR-E-SABZ'),
(5, 'Hunza', 23, 'Chipurson', 282, 'SHET MERG'),
(5, 'Hunza', 23, 'Chipurson', 283, 'ZOOD KHUN'),
(5, 'Hunza', 20, 'Gulmit', 284, 'AYEENABAD'),
(5, 'Hunza', 20, 'Gulmit', 285, 'BORETH1 PAEEN'),
(5, 'Hunza', 20, 'Gulmit', 286, 'BORETH-2 Bala'),
(5, 'Hunza', 20, 'Gulmit', 287, 'CHAMAN GUL'),
(5, 'Hunza', 20, 'Gulmit', 288, 'DALGIRAM'),
(5, 'Hunza', 20, 'Gulmit', 289, 'GHULKIN'),
(5, 'Hunza', 20, 'Gulmit', 290, 'GOZE'),
(5, 'Hunza', 20, 'Gulmit', 291, 'GULMIT Central'),
(5, 'Hunza', 20, 'Gulmit', 292, 'HUSSAINI'),
(5, 'Hunza', 20, 'Gulmit', 293, 'KAMARIS'),
(5, 'Hunza', 20, 'Gulmit', 294, 'MURADABAD'),
(5, 'Hunza', 20, 'Gulmit', 295, 'NAZIMABAD1 BALA'),
(5, 'Hunza', 20, 'Gulmit', 296, 'NAZIMABAD2 DARMIYAN'),
(5, 'Hunza', 20, 'Gulmit', 297, 'NAZIMABAD3 PAEEN'),
(5, 'Hunza', 20, 'Gulmit', 298, 'PASSU Janabad'),
(5, 'Hunza', 20, 'Gulmit', 299, 'UDBER'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 300, 'HOON KOOT'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 301, 'HUSSAINABAD1'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 302, 'HUSSAINABAD2'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 303, 'HUSSAINABAD3'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 304, 'HUSSAINABAD4'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 305, 'KHANABAD1'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 306, 'KHANABAD2 TEKRY'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 307, 'KHANABAD3 MASHRAQI'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 308, 'KHIZARABAD'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 309, 'MAYOON'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 310, 'NASIRABAD CENTRAL'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 311, 'NASIRABAD-2 Bala'),
(5, 'Hunza', 19, 'Nasirabad Shinkani', 312, 'REHMATABAD'),
(5, 'Hunza', 21, 'Shimshal', 313, 'AMYNABAD Shemshal'),
(5, 'Hunza', 21, 'Shimshal', 314, 'FARMANABAD Shemshal'),
(5, 'Hunza', 21, 'Shimshal', 315, 'KHIZARABAD Shemshal'),
(5, 'Hunza', 21, 'Shimshal', 316, 'SHEMSHAL Sub-Centre'),
(5, 'Hunza', 22, 'Sost', 317, 'ABGARCH'),
(5, 'Hunza', 22, 'Sost', 318, 'AMYNABAD Sost Paien'),
(5, 'Hunza', 22, 'Sost', 319, 'DHI KUNJERAB'),
(5, 'Hunza', 22, 'Sost', 320, 'GALAPAN'),
(5, 'Hunza', 22, 'Sost', 321, 'GIRCHA'),
(5, 'Hunza', 22, 'Sost', 322, 'HUSSAINABAD'),
(5, 'Hunza', 22, 'Sost', 323, 'JAMALABAD'),
(5, 'Hunza', 22, 'Sost', 324, 'KHUDAABAD Central'),
(5, 'Hunza', 22, 'Sost', 325, 'KHUDAABAD2 KARIMABAD'),
(5, 'Hunza', 22, 'Sost', 326, 'KHUDAABAD3 IMAMABAD'),
(5, 'Hunza', 22, 'Sost', 327, 'KHYBER1'),
(5, 'Hunza', 22, 'Sost', 328, 'KHYBER-2 Imamabad'),
(5, 'Hunza', 22, 'Sost', 329, 'MISGAR1'),
(5, 'Hunza', 22, 'Sost', 330, 'MISGAR2'),
(5, 'Hunza', 22, 'Sost', 331, 'MOORKHUN'),
(5, 'Hunza', 22, 'Sost', 332, 'NAZIMABAD Sost Bala'),
(5, 'Hunza', 22, 'Sost', 333, 'SERTEZ'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 334, 'BUBAR4 CENTER'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 335, 'BUBUR1 PAIEN'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 336, 'BUBUR-2 Bala'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 337, 'GEECH'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 338, 'GOHARABAD'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 339, 'GULMUTI'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 340, 'GURUNJER1 PAIEN'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 341, 'GURUNJER2 BALA'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 342, 'GURUNJER-3 Bala'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 343, 'HUSSAINABAD Bubur-3 Bala'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 344, 'KUTOOM1'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 345, 'KUTOOM2'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 346, 'SINGAL Central'),
(7, 'Ishkoman Puniyal', 27, 'Babur', 347, 'THINGDAS'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 348, 'AMYNABAD'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 349, 'BIRGALE'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 350, 'CHATOORKHAND Central'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 351, 'CHATOORKHAND PAEEN'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 352, 'CHATOORKHAND-3'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 353, 'CHINAR'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 354, 'DAIEN - 6 Paien'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 355, 'DAIEN1 PAEEN'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 356, 'DAIEN2 BALA'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 357, 'DAIEN3 GOCHAR'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 358, 'DAIEN4 DOOK'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 359, 'DAIEN5 GOOL'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 360, 'Dain Sholja'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 361, 'FAMANI'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 362, 'KOCHDEH'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 363, 'KOCHDEH - 2'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 364, 'MAWLAABAD, Daien'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 365, 'PAKHORA1'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 366, 'PAKHORA2'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 367, 'PAKORA - 3'),
(7, 'Ishkoman Puniyal', 30, 'Chatorkhand', 368, 'RAHIMABAD Shunas'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 369, 'AIYESH1'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 370, 'AIYESH2'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 371, 'AIYESH3'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 372, 'ANWERABAD'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 373, 'DAMAS Central'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 374, 'DAMAS2 BALA'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 375, 'GAHKUCH1 BALA'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 376, 'GAHKUCH2 BALA'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 377, 'GAHKUCH3 PAEEN'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 378, 'GAHKUCH4 PAEEN'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 379, 'GAHKUCH5 BALA'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 380, 'HASIS1 PAIEN'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 381, 'HASIS2 BALA'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 382, 'HASIS2 BIDIRI KHARI'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 383, 'HATOON1'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 384, 'HATOON2 SUBCENTRE'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 385, 'HATOON3'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 386, 'HATOON4 DASS'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 387, 'HATOON5 MAYOON'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 388, 'JAMALABAD'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 389, 'JK 1 Golodass'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 390, 'JK 2 Golodass'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 391, 'KUTOOLKEY'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 392, 'NOORABAD Damas'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 393, 'SARAYOT'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 394, 'SILPI1'),
(7, 'Ishkoman Puniyal', 29, 'Damas', 395, 'SILPI2'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 396, 'ALYABAD'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 397, 'BARSWAT'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 398, 'BAZAR KUTOO'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 399, 'BILHANZ-1'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 400, 'BILHANZ-2'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 401, 'BOARTH'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 402, 'BOOQ SHAMSABAD'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 403, 'DOWERDAS'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 404, 'GISH GISH1'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 405, 'GISH GISH2'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 406, 'IMMIT Central'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 407, 'KUNJ Ganjabad'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 408, 'MOHTRAM DAS'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 409, 'MUJAWAR'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 410, 'NASIRABAD'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 411, 'NAUBAHAR'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 412, 'RAHIMABAD Bar Jungle'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 413, 'REHMATABAD-1 Mujawar'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 414, 'REHMATABAD-2 Paien'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 415, 'SHAMSABAD Kakunekay'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 416, 'SULTANABAD'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 417, 'TESHNALOT'),
(7, 'Ishkoman Puniyal', 32, 'Immit', 418, 'TUNG SHAMSABAD (Seasonal)'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 419, 'ALYBASTI TUSHKIN - 2'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 420, 'DALTI'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 421, 'DAWOODABAD'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 422, 'FAIZABAD'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 423, 'FAIZABAD - 2 Bala'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 424, 'ISKHOMAN Central'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 425, 'JALALABAD'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 426, 'MOMINABAD'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 427, 'RAHIMABAD Gotolti'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 428, 'SHENIKEY'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 429, 'TAPUSHKIN'),
(7, 'Ishkoman Puniyal', 31, 'Ishkoman', 430, 'TUSHKIN'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 431, 'DAS JAPUK AMYNABAD'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 432, 'DERYANI'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 433, 'HAMUCHAL'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 434, 'JAPUKY'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 435, 'KARIMABAD Sherqilla-Sub-Centre'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 436, 'Pray Hall Ghullapur'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 437, 'SHERQILLA2 HUSSAINABAD'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 438, 'SHERQILLA3 RAHIMABAD'),
(7, 'Ishkoman Puniyal', 28, 'Sherqilla', 439, 'SHERQILLA4 SULTANABAD'),
(1, 'Karachi & Balochistan', 4, 'Balochistan', 440, 'GWADAR'),
(1, 'Karachi & Balochistan', 4, 'Balochistan', 441, 'ORMARA'),
(1, 'Karachi & Balochistan', 4, 'Balochistan', 442, 'PASNI'),
(1, 'Karachi & Balochistan', 1, 'Garden', 443, 'AMYNABAD'),
(1, 'Karachi & Balochistan', 1, 'Garden', 444, 'DARKHANA'),
(1, 'Karachi & Balochistan', 1, 'Garden', 445, 'EVERSHINE'),
(1, 'Karachi & Balochistan', 1, 'Garden', 446, 'GULZAR-E-RAHIM'),
(1, 'Karachi & Balochistan', 1, 'Garden', 447, 'KARIM NAGAR'),
(1, 'Karachi & Balochistan', 1, 'Garden', 448, 'MALIR'),
(1, 'Karachi & Balochistan', 1, 'Garden', 449, 'NAZIMABAD'),
(1, 'Karachi & Balochistan', 1, 'Garden', 450, 'NOORABAD'),
(1, 'Karachi & Balochistan', 1, 'Garden', 451, 'PASK COLONY'),
(1, 'Karachi & Balochistan', 1, 'Garden', 452, 'PLATINUM'),
(1, 'Karachi & Balochistan', 1, 'Garden', 453, 'REHMANI GARDEN'),
(1, 'Karachi & Balochistan', 1, 'Garden', 454, 'SULTANABAD'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 455, 'AL AZHAR GARDEN'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 456, 'AL NOOR'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 457, 'ALI JIWANI'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 458, 'ALYABAD'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 459, 'GULSHAN E NOOR'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 460, 'KARIMABAD'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 461, 'NIZARI'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 462, 'RAHIMABAD'),
(1, 'Karachi & Balochistan', 2, 'Karimabad', 463, 'SALIMAHABAD'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 464, 'CHASHMA GOTH'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 465, 'CLIFTON'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 466, 'EBRAHIM HAIDERY No. 1'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 467, 'KHADIJAHABAD'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 468, 'KHARADAR'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 469, 'KORANGI'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 470, 'LASSI'),
(1, 'Karachi & Balochistan', 3, 'Kharadar', 471, 'RANCHORELINE'),
(9, 'Lower Chitral', 44, 'Arkari', 472, 'AFZALABAD'),
(9, 'Lower Chitral', 44, 'Arkari', 473, 'ALYABAD Arkari-Central'),
(9, 'Lower Chitral', 44, 'Arkari', 474, 'BESTI - 1 Paien'),
(9, 'Lower Chitral', 44, 'Arkari', 475, 'BESTI - 2 Bala'),
(9, 'Lower Chitral', 44, 'Arkari', 476, 'DIR GOOL'),
(9, 'Lower Chitral', 44, 'Arkari', 477, 'OWEER-1'),
(9, 'Lower Chitral', 44, 'Arkari', 478, 'OWEER-2 Lasht'),
(9, 'Lower Chitral', 44, 'Arkari', 479, 'PURPUNI'),
(9, 'Lower Chitral', 44, 'Arkari', 480, 'RABAT'),
(9, 'Lower Chitral', 44, 'Arkari', 481, 'RABAT MOKHI'),
(9, 'Lower Chitral', 44, 'Arkari', 482, 'RAHIMABAD'),
(9, 'Lower Chitral', 44, 'Arkari', 483, 'SADDAM'),
(9, 'Lower Chitral', 44, 'Arkari', 484, 'SAFEED ARKARI'),
(9, 'Lower Chitral', 44, 'Arkari', 485, 'SHOL'),
(9, 'Lower Chitral', 44, 'Arkari', 486, 'SHUNJUR GHUCH'),
(9, 'Lower Chitral', 44, 'Arkari', 487, 'SULTANABAD'),
(9, 'Lower Chitral', 45, 'Garamchashma', 488, 'ALYABAD Amerdin-2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 489, 'AMERDIN'),
(9, 'Lower Chitral', 45, 'Garamchashma', 490, 'BASHQIR-3 PaienSub Centre'),
(9, 'Lower Chitral', 45, 'Garamchashma', 491, 'BEGHUSHT1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 492, 'BEGHUSHT2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 493, 'BEGHUSHT3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 494, 'BEGHUSHT4'),
(9, 'Lower Chitral', 45, 'Garamchashma', 495, 'BEGHUSHT5'),
(9, 'Lower Chitral', 45, 'Garamchashma', 496, 'BEHMI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 497, 'BEKHI1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 498, 'BEKHI2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 499, 'BESHQER1 BALA'),
(9, 'Lower Chitral', 45, 'Garamchashma', 500, 'BESHQER2 BALA'),
(9, 'Lower Chitral', 45, 'Garamchashma', 501, 'BESIRI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 502, 'BIRZEEN1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 503, 'BIRZEEN2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 504, 'BIRZEEN3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 505, 'BURBUNO'),
(9, 'Lower Chitral', 45, 'Garamchashma', 506, 'CHERWIL'),
(9, 'Lower Chitral', 45, 'Garamchashma', 507, 'DIRI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 508, 'DROSHP'),
(9, 'Lower Chitral', 45, 'Garamchashma', 509, 'EIZH'),
(9, 'Lower Chitral', 45, 'Garamchashma', 510, 'ERJIEK'),
(9, 'Lower Chitral', 45, 'Garamchashma', 511, 'GAJAL'),
(9, 'Lower Chitral', 45, 'Garamchashma', 512, 'GHOLAKI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 513, 'GISTINI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 514, 'GOHIK'),
(9, 'Lower Chitral', 45, 'Garamchashma', 515, 'GUFTI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 516, 'GULUGH'),
(9, 'Lower Chitral', 45, 'Garamchashma', 517, 'HAZAI (NEW)'),
(9, 'Lower Chitral', 45, 'Garamchashma', 518, 'JITUR SANIQ'),
(9, 'Lower Chitral', 45, 'Garamchashma', 519, 'JITUR-1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 520, 'JITUR-2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 521, 'KANDUJAL1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 522, 'KANDUJAL2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 523, 'KANDUJAL3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 524, 'KHATINJ'),
(9, 'Lower Chitral', 45, 'Garamchashma', 525, 'KHOGHIK'),
(9, 'Lower Chitral', 45, 'Garamchashma', 526, 'KOCH'),
(9, 'Lower Chitral', 45, 'Garamchashma', 527, 'LOHOK'),
(9, 'Lower Chitral', 45, 'Garamchashma', 528, 'MAIDAN'),
(9, 'Lower Chitral', 45, 'Garamchashma', 529, 'MAIDAN-2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 530, 'MEGIGRAM1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 531, 'MEGIGRAM2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 532, 'MEGIGRAM3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 533, 'MOGH1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 534, 'MOGH2 NIZH'),
(9, 'Lower Chitral', 45, 'Garamchashma', 535, 'MUNOOR1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 536, 'MUNOOR2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 537, 'MUNOOR3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 538, 'MUNOOR-4 Phalam'),
(9, 'Lower Chitral', 45, 'Garamchashma', 539, 'MURDAN'),
(9, 'Lower Chitral', 45, 'Garamchashma', 540, 'NARKURAT1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 541, 'NARKURAT2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 542, 'NARKURAT3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 543, 'NARKURAT4 ERGO'),
(9, 'Lower Chitral', 45, 'Garamchashma', 544, 'NASIRABAD Central Ziarat'),
(9, 'Lower Chitral', 45, 'Garamchashma', 545, 'NIWEST'),
(9, 'Lower Chitral', 45, 'Garamchashma', 546, 'NOGH PATI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 547, 'OCHIO LASHT'),
(9, 'Lower Chitral', 45, 'Garamchashma', 548, 'OCHU GOOL-1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 549, 'OSHIAK MUSHEEN'),
(9, 'Lower Chitral', 45, 'Garamchashma', 550, 'OUGHUTI'),
(9, 'Lower Chitral', 45, 'Garamchashma', 551, 'OVIRK1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 552, 'OVIRK2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 553, 'PARABEG Central'),
(9, 'Lower Chitral', 45, 'Garamchashma', 554, 'Postaki 3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 555, 'POSTAKI1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 556, 'POSTAKI2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 557, 'PURTO'),
(9, 'Lower Chitral', 45, 'Garamchashma', 558, 'ROYEE1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 559, 'ROYEE2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 560, 'ROYEE3'),
(9, 'Lower Chitral', 45, 'Garamchashma', 561, 'SANIK'),
(9, 'Lower Chitral', 45, 'Garamchashma', 562, 'Sanik Payeen'),
(9, 'Lower Chitral', 45, 'Garamchashma', 563, 'SEPOKHT'),
(9, 'Lower Chitral', 45, 'Garamchashma', 564, 'SHAGRAM1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 565, 'SHAGRAM2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 566, 'TUNIK'),
(9, 'Lower Chitral', 45, 'Garamchashma', 567, 'UCHO GOAL1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 568, 'UCHO GOAL2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 569, 'UTRAIE-1'),
(9, 'Lower Chitral', 45, 'Garamchashma', 570, 'UTRAIE-2'),
(9, 'Lower Chitral', 45, 'Garamchashma', 571, 'WAHAT'),
(9, 'Lower Chitral', 45, 'Garamchashma', 572, 'YOURJOGH'),
(9, 'Lower Chitral', 43, 'Karimabad', 573, 'AJARANDEH'),
(9, 'Lower Chitral', 43, 'Karimabad', 574, 'BISH NOGHOR'),
(9, 'Lower Chitral', 43, 'Karimabad', 575, 'BRESHGRAM Sub Centre'),
(9, 'Lower Chitral', 43, 'Karimabad', 576, 'DALMEER'),
(9, 'Lower Chitral', 43, 'Karimabad', 577, 'DARDAI'),
(9, 'Lower Chitral', 43, 'Karimabad', 578, 'DARDAI KULUM'),
(9, 'Lower Chitral', 43, 'Karimabad', 579, 'DOOK DUR'),
(9, 'Lower Chitral', 43, 'Karimabad', 580, 'DRONIL'),
(9, 'Lower Chitral', 43, 'Karimabad', 581, 'GALEH'),
(9, 'Lower Chitral', 43, 'Karimabad', 582, 'GREE Sub-Centre'),
(9, 'Lower Chitral', 43, 'Karimabad', 583, 'HINJIL-1'),
(9, 'Lower Chitral', 43, 'Karimabad', 584, 'HINJIL-2'),
(9, 'Lower Chitral', 43, 'Karimabad', 585, 'HINJIL-3'),
(9, 'Lower Chitral', 43, 'Karimabad', 586, 'KARIMABAD Susum Central'),
(9, 'Lower Chitral', 43, 'Karimabad', 587, 'KIYAR'),
(9, 'Lower Chitral', 43, 'Karimabad', 588, 'KULUM BRESHGRAM'),
(9, 'Lower Chitral', 43, 'Karimabad', 589, 'LORIGRAM'),
(9, 'Lower Chitral', 43, 'Karimabad', 590, 'MADASHIL-1'),
(9, 'Lower Chitral', 43, 'Karimabad', 591, 'MADASHIL-2'),
(9, 'Lower Chitral', 43, 'Karimabad', 592, 'ORGHUCH'),
(9, 'Lower Chitral', 43, 'Karimabad', 593, 'ORLAGH'),
(9, 'Lower Chitral', 43, 'Karimabad', 594, 'PASTURAGH'),
(9, 'Lower Chitral', 43, 'Karimabad', 595, 'PITRAGRAM-1'),
(9, 'Lower Chitral', 43, 'Karimabad', 596, 'PITRAGRAM-2'),
(9, 'Lower Chitral', 43, 'Karimabad', 597, 'SHAH'),
(9, 'Lower Chitral', 43, 'Karimabad', 598, 'SHAHNURUN'),
(9, 'Lower Chitral', 43, 'Karimabad', 599, 'SHUTH-1'),
(9, 'Lower Chitral', 43, 'Karimabad', 600, 'SHUTH-2'),
(9, 'Lower Chitral', 43, 'Karimabad', 601, 'SHUTH-3'),
(9, 'Lower Chitral', 43, 'Karimabad', 602, 'SUSUM LASHT'),
(9, 'Lower Chitral', 43, 'Karimabad', 603, 'TASHQAR-1'),
(9, 'Lower Chitral', 43, 'Karimabad', 604, 'TASHQAR-2'),
(9, 'Lower Chitral', 43, 'Karimabad', 605, 'THOK JAL'),
(9, 'Lower Chitral', 42, 'Madaklasht', 606, 'MADAKLASHT-1'),
(9, 'Lower Chitral', 42, 'Madaklasht', 607, 'MADAKLASHT-2, Sub-Centre'),
(9, 'Lower Chitral', 42, 'Madaklasht', 608, 'MADAKLASHT-3'),
(9, 'Lower Chitral', 42, 'Madaklasht', 609, 'MADAKLASHT-4'),
(9, 'Lower Chitral', 42, 'Madaklasht', 610, 'MADAKLASHT5 KASHANKOAL REC'),
(9, 'Lower Chitral', 41, 'Shoghor', 611, 'ARGESH Parsan'),
(9, 'Lower Chitral', 41, 'Shoghor', 612, 'AWI'),
(9, 'Lower Chitral', 41, 'Shoghor', 613, 'BALACH'),
(9, 'Lower Chitral', 41, 'Shoghor', 614, 'BILBIL Parsan'),
(9, 'Lower Chitral', 41, 'Shoghor', 615, 'BILPUK'),
(9, 'Lower Chitral', 41, 'Shoghor', 616, 'BODYOUGH'),
(9, 'Lower Chitral', 41, 'Shoghor', 617, 'BUHTULI DEH'),
(9, 'Lower Chitral', 41, 'Shoghor', 618, 'BUHTULI DOOK'),
(9, 'Lower Chitral', 41, 'Shoghor', 619, 'BUHTULI GOOL-1'),
(9, 'Lower Chitral', 41, 'Shoghor', 620, 'BUHTULI GOOL-2'),
(9, 'Lower Chitral', 41, 'Shoghor', 621, 'CHINAR SOCIETY'),
(9, 'Lower Chitral', 41, 'Shoghor', 622, 'GHORO GOOL'),
(9, 'Lower Chitral', 41, 'Shoghor', 623, 'HASANABAD'),
(9, 'Lower Chitral', 41, 'Shoghor', 624, 'HERENI'),
(9, 'Lower Chitral', 41, 'Shoghor', 625, 'HUSSAINABAD'),
(9, 'Lower Chitral', 41, 'Shoghor', 626, 'ISHPADER'),
(9, 'Lower Chitral', 41, 'Shoghor', 627, 'KASETH'),
(9, 'Lower Chitral', 41, 'Shoghor', 628, 'KILISHP'),
(9, 'Lower Chitral', 41, 'Shoghor', 629, 'KIRCHUM'),
(9, 'Lower Chitral', 41, 'Shoghor', 630, 'KRING'),
(9, 'Lower Chitral', 41, 'Shoghor', 631, 'KUCH GAH Shuljah'),
(9, 'Lower Chitral', 41, 'Shoghor', 632, 'LASHT DEH Parsan'),
(9, 'Lower Chitral', 41, 'Shoghor', 633, 'MAJIMI Parsan'),
(9, 'Lower Chitral', 41, 'Shoghor', 634, 'MIRLAN DEH Parsan'),
(9, 'Lower Chitral', 41, 'Shoghor', 635, 'MOMI-1 Paien'),
(9, 'Lower Chitral', 41, 'Shoghor', 636, 'MOMI-2 Bala'),
(9, 'Lower Chitral', 41, 'Shoghor', 637, 'MOMI-3 Khamjenandeh'),
(9, 'Lower Chitral', 41, 'Shoghor', 638, 'MOMOON'),
(9, 'Lower Chitral', 41, 'Shoghor', 639, 'OGHDER'),
(9, 'Lower Chitral', 41, 'Shoghor', 640, 'PECHUCH'),
(9, 'Lower Chitral', 41, 'Shoghor', 641, 'RUJI'),
(9, 'Lower Chitral', 41, 'Shoghor', 642, 'RUNI'),
(9, 'Lower Chitral', 41, 'Shoghor', 643, 'SEEN LASHT'),
(9, 'Lower Chitral', 41, 'Shoghor', 644, 'SHALEE'),
(9, 'Lower Chitral', 41, 'Shoghor', 645, 'SHERSHAL'),
(9, 'Lower Chitral', 41, 'Shoghor', 646, 'SHOGHORE Central'),
(9, 'Lower Chitral', 41, 'Shoghor', 647, 'SHOGHORE GHARI'),
(9, 'Lower Chitral', 41, 'Shoghor', 648, 'SIWAHTH-2'),
(9, 'Lower Chitral', 41, 'Shoghor', 649, 'SIWATH-1'),
(9, 'Lower Chitral', 41, 'Shoghor', 650, 'SUNICH'),
(9, 'Lower Chitral', 41, 'Shoghor', 651, 'Teyoli'),
(9, 'Lower Chitral', 41, 'Shoghor', 652, 'TILGRAM Parsan'),
(9, 'Lower Chitral', 41, 'Shoghor', 653, 'UCHETUR'),
(3, 'Punjab', 14, 'Bhawalpur', 654, 'AHMEDPUR EAST'),
(3, 'Punjab', 14, 'Bhawalpur', 655, 'ALLAHABAD'),
(3, 'Punjab', 14, 'Bhawalpur', 656, 'BAHAWALPUR'),
(3, 'Punjab', 14, 'Bhawalpur', 657, 'KHANPUR'),
(3, 'Punjab', 14, 'Bhawalpur', 658, 'RAHIMYAR KHAN'),
(3, 'Punjab', 14, 'Bhawalpur', 659, 'SADIQABAD'),
(3, 'Punjab', 14, 'Bhawalpur', 660, 'UCH SHARIF'),
(3, 'Punjab', 11, 'Hafizabad', 661, 'CHINYOUT'),
(3, 'Punjab', 11, 'Hafizabad', 662, 'HAFIZABAD'),
(3, 'Punjab', 11, 'Hafizabad', 663, 'JALALPUR BHATIAN'),
(3, 'Punjab', 11, 'Hafizabad', 664, 'KARIMABAD Hafizabad'),
(3, 'Punjab', 11, 'Hafizabad', 665, 'KHANPUR KALAY, Sheikupur'),
(3, 'Punjab', 11, 'Hafizabad', 666, 'KOLO TARAR'),
(3, 'Punjab', 11, 'Hafizabad', 667, 'PINDI BHATIAN'),
(3, 'Punjab', 11, 'Hafizabad', 668, 'SALOKI'),
(3, 'Punjab', 11, 'Hafizabad', 669, 'THATI BHILOLPUR'),
(3, 'Punjab', 9, 'Lahore', 670, 'BADOMALI'),
(3, 'Punjab', 9, 'Lahore', 671, 'FAISALABAD'),
(3, 'Punjab', 9, 'Lahore', 672, 'GUJRANWALA'),
(3, 'Punjab', 9, 'Lahore', 673, 'LAHORE'),
(3, 'Punjab', 9, 'Lahore', 674, 'LALA MUSA'),
(3, 'Punjab', 9, 'Lahore', 675, 'SHADRA'),
(3, 'Punjab', 9, 'Lahore', 676, 'SHAH ALAM'),
(3, 'Punjab', 9, 'Lahore', 677, 'TALWANDI'),
(3, 'Punjab', 13, 'Multan', 678, 'BAKHAR'),
(3, 'Punjab', 13, 'Multan', 679, 'KARIMABAD, Multan'),
(3, 'Punjab', 13, 'Multan', 680, 'MULTAN'),
(3, 'Punjab', 13, 'Multan', 681, 'MUMTAZABAD'),
(3, 'Punjab', 13, 'Multan', 682, 'SAHIWAL'),
(3, 'Punjab', 13, 'Multan', 683, 'VEHARI'),
(3, 'Punjab', 12, 'Sargodha', 684, 'ATHAR'),
(3, 'Punjab', 12, 'Sargodha', 685, 'BHAGTANWALA'),
(3, 'Punjab', 12, 'Sargodha', 686, 'BHALWAL'),
(3, 'Punjab', 12, 'Sargodha', 687, 'BHEKO'),
(3, 'Punjab', 12, 'Sargodha', 688, 'BHERA'),
(3, 'Punjab', 12, 'Sargodha', 689, 'CHAK NO.47'),
(3, 'Punjab', 12, 'Sargodha', 690, 'CHOHA SAIDEN SHAH'),
(3, 'Punjab', 12, 'Sargodha', 691, 'HARANPUR'),
(3, 'Punjab', 12, 'Sargodha', 692, 'JAWARIAN'),
(3, 'Punjab', 12, 'Sargodha', 693, 'KALRA ESTATE'),
(3, 'Punjab', 12, 'Sargodha', 694, 'KHOTI ALLAH SHEIKIAN'),
(3, 'Punjab', 12, 'Sargodha', 695, 'KORRAY KOT'),
(3, 'Punjab', 12, 'Sargodha', 696, 'KOT MOMIN'),
(3, 'Punjab', 12, 'Sargodha', 697, 'LILYANI'),
(3, 'Punjab', 12, 'Sargodha', 698, 'MANDI BAHAUDIN'),
(3, 'Punjab', 12, 'Sargodha', 699, 'MIANWAL'),
(3, 'Punjab', 12, 'Sargodha', 700, 'MIDH RANJHA'),
(3, 'Punjab', 12, 'Sargodha', 701, 'NASEERPUR KALAN'),
(3, 'Punjab', 12, 'Sargodha', 702, 'PHULARWAN'),
(3, 'Punjab', 12, 'Sargodha', 703, 'PIND DADAN KHAN'),
(3, 'Punjab', 12, 'Sargodha', 704, 'PINDI SAIDPUR'),
(3, 'Punjab', 12, 'Sargodha', 705, 'SARGODHA'),
(3, 'Punjab', 10, 'Sialkot', 706, 'SIALKOT'),
(4, 'RIMPA', 15, 'Islamabad', 707, 'ABBOTABAD'),
(4, 'RIMPA', 15, 'Islamabad', 708, 'ATTOCK'),
(4, 'RIMPA', 15, 'Islamabad', 709, 'BURBAN'),
(4, 'RIMPA', 15, 'Islamabad', 710, 'ISLAMABAD'),
(4, 'RIMPA', 15, 'Islamabad', 711, 'KARIMABAD, Islamabad'),
(4, 'RIMPA', 15, 'Islamabad', 712, 'Muzaffarabad'),
(4, 'RIMPA', 15, 'Islamabad', 713, 'RAWALPINDI'),
(4, 'RIMPA', 15, 'Islamabad', 714, 'TAXILA'),
(4, 'RIMPA', 16, 'Peshawar', 715, 'AMIN COLONY'),
(4, 'RIMPA', 16, 'Peshawar', 716, 'HAZRO'),
(4, 'RIMPA', 16, 'Peshawar', 717, 'MARDAN'),
(4, 'RIMPA', 16, 'Peshawar', 718, 'SIKANDER TOWN'),
(2, 'Sindh', 5, 'Hyderabad', 719, 'ALYABAD'),
(2, 'Sindh', 5, 'Hyderabad', 720, 'AMYNABAD'),
(2, 'Sindh', 5, 'Hyderabad', 721, 'JHUDO'),
(2, 'Sindh', 5, 'Hyderabad', 722, 'KHYBER'),
(2, 'Sindh', 5, 'Hyderabad', 723, 'MUBARAK'),
(2, 'Sindh', 5, 'Hyderabad', 724, 'PRINCE ALY ROAD'),
(2, 'Sindh', 5, 'Hyderabad', 725, 'RAHIMABAD, Tando Allahyar'),
(2, 'Sindh', 5, 'Hyderabad', 726, 'SHAH ALY'),
(2, 'Sindh', 5, 'Hyderabad', 727, 'SUKKUR'),
(2, 'Sindh', 5, 'Hyderabad', 728, 'SULTANABAD 1'),
(2, 'Sindh', 5, 'Hyderabad', 729, 'SULTANABAD 2'),
(2, 'Sindh', 5, 'Hyderabad', 730, 'SULTANABAD 3'),
(2, 'Sindh', 5, 'Hyderabad', 731, 'TANDO ALLAHYAR'),
(2, 'Sindh', 7, 'Shahbandar', 732, 'HUSSAINABAD'),
(2, 'Sindh', 7, 'Shahbandar', 733, 'JATI'),
(2, 'Sindh', 7, 'Shahbandar', 734, 'SUJAWAL'),
(2, 'Sindh', 7, 'Shahbandar', 735, 'TARR KHAWAJA'),
(2, 'Sindh', 8, 'Tando Turrail', 736, 'BADIN'),
(2, 'Sindh', 8, 'Tando Turrail', 737, 'KHADO'),
(2, 'Sindh', 8, 'Tando Turrail', 738, 'NAUABAD'),
(2, 'Sindh', 8, 'Tando Turrail', 739, 'SHAH KAPOOR'),
(2, 'Sindh', 8, 'Tando Turrail', 740, 'SONDA'),
(2, 'Sindh', 8, 'Tando Turrail', 741, 'TALHAR'),
(2, 'Sindh', 8, 'Tando Turrail', 742, 'TANDO BAGO'),
(2, 'Sindh', 8, 'Tando Turrail', 743, 'TANDO MOHAMMED KHAN'),
(2, 'Sindh', 6, 'Thatta', 744, 'ALIJAH RAJAB ALI GOTH'),
(2, 'Sindh', 6, 'Thatta', 745, 'ALIJAH WALI GOTH'),
(2, 'Sindh', 6, 'Thatta', 746, 'ALIJAHA BACHAL GOTH'),
(2, 'Sindh', 6, 'Thatta', 747, 'ALYABAD'),
(2, 'Sindh', 6, 'Thatta', 748, 'BOHARA'),
(2, 'Sindh', 6, 'Thatta', 749, 'CHACH SARKO(KAKO VILLAGE)'),
(2, 'Sindh', 6, 'Thatta', 750, 'DINABAD NO.2'),
(2, 'Sindh', 6, 'Thatta', 751, 'GHULAMULLAH'),
(2, 'Sindh', 6, 'Thatta', 752, 'KARIMABAD'),
(2, 'Sindh', 6, 'Thatta', 753, 'MIRPUR SAKRO'),
(2, 'Sindh', 6, 'Thatta', 754, 'MOHAMMEDABAD'),
(2, 'Sindh', 6, 'Thatta', 755, 'NAU GOTH'),
(2, 'Sindh', 6, 'Thatta', 756, 'RAHIMABAD'),
(2, 'Sindh', 6, 'Thatta', 757, 'VUR'),
(10, 'Upper Chitral ', 46, 'Booni', 758, 'AWI BAND-1'),
(10, 'Upper Chitral ', 46, 'Booni', 759, 'AWI BAND-2'),
(10, 'Upper Chitral ', 46, 'Booni', 760, 'AWI GUCHDUR'),
(10, 'Upper Chitral ', 46, 'Booni', 761, 'AWI LASHT Sub Centre'),
(10, 'Upper Chitral ', 46, 'Booni', 762, 'AWI LURANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 763, 'AWI MANDAGH'),
(10, 'Upper Chitral ', 46, 'Booni', 764, 'AWI SINGANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 765, 'BAZANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 766, 'BOLAN LASHT Mulgram'),
(10, 'Upper Chitral ', 46, 'Booni', 767, 'BOONI Centre'),
(10, 'Upper Chitral ', 46, 'Booni', 768, 'BOZIR'),
(10, 'Upper Chitral ', 46, 'Booni', 769, 'BUTAKHAN DEH'),
(10, 'Upper Chitral ', 46, 'Booni', 770, 'Centre JK Awi'),
(10, 'Upper Chitral ', 46, 'Booni', 771, 'CHARUN OVEER'),
(10, 'Upper Chitral ', 46, 'Booni', 772, 'CHARUN Sub Centre'),
(10, 'Upper Chitral ', 46, 'Booni', 773, 'CHARVELANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 774, 'DARIYANO'),
(10, 'Upper Chitral ', 46, 'Booni', 775, 'DEH AVI CENTRE'),
(10, 'Upper Chitral ', 46, 'Booni', 776, 'DUKANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 777, 'GOOL GUCH'),
(10, 'Upper Chitral ', 46, 'Booni', 778, 'GREEN LASHT'),
(10, 'Upper Chitral ', 46, 'Booni', 779, 'HUSSAINABAD Junali Koch'),
(10, 'Upper Chitral ', 46, 'Booni', 780, 'ISHPARDER Krui Junali'),
(10, 'Upper Chitral ', 46, 'Booni', 781, 'JINALIKOACH BALA 1'),
(10, 'Upper Chitral ', 46, 'Booni', 782, 'JUNALI KUCH-1 Paien'),
(10, 'Upper Chitral ', 46, 'Booni', 783, 'JUNALI KUCH-2 Bala'),
(10, 'Upper Chitral ', 46, 'Booni', 784, 'JUNALI KUCH-3 Paien'),
(10, 'Upper Chitral ', 46, 'Booni', 785, 'KHORA GOOL'),
(10, 'Upper Chitral ', 46, 'Booni', 786, 'KRUI GOLOGH AWI'),
(10, 'Upper Chitral ', 46, 'Booni', 787, 'KRUI JUNALI HOON'),
(10, 'Upper Chitral ', 46, 'Booni', 788, 'KRUICHAT PARWAK Sub Centre'),
(10, 'Upper Chitral ', 46, 'Booni', 789, 'KRUIJINALI 2'),
(10, 'Upper Chitral ', 46, 'Booni', 790, 'KURAGH'),
(10, 'Upper Chitral ', 46, 'Booni', 791, 'LASHT BOONI'),
(10, 'Upper Chitral ', 46, 'Booni', 792, 'LUQANDEH Lutdur'),
(10, 'Upper Chitral ', 46, 'Booni', 793, 'Meragram Sonoghur'),
(10, 'Upper Chitral ', 46, 'Booni', 794, 'MERAGRAM-1'),
(10, 'Upper Chitral ', 46, 'Booni', 795, 'MERAGRAM-2 Gool'),
(10, 'Upper Chitral ', 46, 'Booni', 796, 'MUGHLANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 797, 'NISUR GOOL Bala'),
(10, 'Upper Chitral ', 46, 'Booni', 798, 'NIYATABAD sakolasht'),
(10, 'Upper Chitral ', 46, 'Booni', 799, 'PANAN DEH'),
(10, 'Upper Chitral ', 46, 'Booni', 800, 'PARSANIK'),
(10, 'Upper Chitral ', 46, 'Booni', 801, 'Parwak Bala'),
(10, 'Upper Chitral ', 46, 'Booni', 802, 'PARWAK-1 Paien'),
(10, 'Upper Chitral ', 46, 'Booni', 803, 'PARWAK-2 Paien'),
(10, 'Upper Chitral ', 46, 'Booni', 804, 'QASIMANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 805, 'QAZIANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 806, 'QUDRATABAD Nisur Gool'),
(10, 'Upper Chitral ', 46, 'Booni', 807, 'RAHIMABAD PARWAK Bala'),
(10, 'Upper Chitral ', 46, 'Booni', 808, 'RAISANDEH Hisandeh'),
(10, 'Upper Chitral ', 46, 'Booni', 809, 'RESHUN RAGHEEN Sub Centre'),
(10, 'Upper Chitral ', 46, 'Booni', 810, 'SHADER'),
(10, 'Upper Chitral ', 46, 'Booni', 811, 'SHAGUNAN DEH'),
(10, 'Upper Chitral ', 46, 'Booni', 812, 'SHAKARANDEH'),
(10, 'Upper Chitral ', 46, 'Booni', 813, 'SONOGHOR GOOL'),
(10, 'Upper Chitral ', 46, 'Booni', 814, 'SONOGHORE Sub Centre'),
(10, 'Upper Chitral ', 46, 'Booni', 815, 'SULTANABAD Morigar'),
(10, 'Upper Chitral ', 46, 'Booni', 816, 'SYEDABAD PARWAK Bala'),
(10, 'Upper Chitral ', 46, 'Booni', 817, 'TEK-LASHT'),
(10, 'Upper Chitral ', 46, 'Booni', 818, 'ZAIET'),
(10, 'Upper Chitral ', 52, 'Brep', 819, 'ALYABAD Bang-3'),
(10, 'Upper Chitral ', 52, 'Brep', 820, 'ALYABAD Brep'),
(10, 'Upper Chitral ', 52, 'Brep', 821, 'BANG-1 Darmiyan SubCentre'),
(10, 'Upper Chitral ', 52, 'Brep', 822, 'BANG-2 Paien'),
(10, 'Upper Chitral ', 52, 'Brep', 823, 'BIRZOZE-1'),
(10, 'Upper Chitral ', 52, 'Brep', 824, 'CHATU DOOK'),
(10, 'Upper Chitral ', 52, 'Brep', 825, 'DEH BREP'),
(10, 'Upper Chitral ', 52, 'Brep', 826, 'DEWSEER'),
(10, 'Upper Chitral ', 52, 'Brep', 827, 'DEWSEER - 2 Paien'),
(10, 'Upper Chitral ', 52, 'Brep', 828, 'DIWAN GOOL'),
(10, 'Upper Chitral ', 52, 'Brep', 829, 'DIZG'),
(10, 'Upper Chitral ', 52, 'Brep', 830, 'DONICH'),
(10, 'Upper Chitral ', 52, 'Brep', 831, 'DOOK Brep'),
(10, 'Upper Chitral ', 52, 'Brep', 832, 'EMIT'),
(10, 'Upper Chitral ', 52, 'Brep', 833, 'GAZAN'),
(10, 'Upper Chitral ', 52, 'Brep', 834, 'GAZEEN-1'),
(10, 'Upper Chitral ', 52, 'Brep', 835, 'GAZEEN-2'),
(10, 'Upper Chitral ', 52, 'Brep', 836, 'HUSSAINABAD - 2'),
(10, 'Upper Chitral ', 52, 'Brep', 837, 'HUSSAINABAD Birzoze-2'),
(10, 'Upper Chitral ', 52, 'Brep', 838, 'HYDERABAD'),
(10, 'Upper Chitral ', 52, 'Brep', 839, 'ISTACH'),
(10, 'Upper Chitral ', 52, 'Brep', 840, 'JAMLASHT-1'),
(10, 'Upper Chitral ', 52, 'Brep', 841, 'JAMLASHT-2  Zandrandeh'),
(10, 'Upper Chitral ', 52, 'Brep', 842, 'KARIMABAD Hoon-Bap'),
(10, 'Upper Chitral ', 52, 'Brep', 843, 'KHANJARADEH'),
(10, 'Upper Chitral ', 52, 'Brep', 844, 'Khudabad Pashk'),
(10, 'Upper Chitral ', 52, 'Brep', 845, 'KHURUZG'),
(10, 'Upper Chitral ', 52, 'Brep', 846, 'LASHDAN'),
(10, 'Upper Chitral ', 52, 'Brep', 847, 'MAHTING Paien'),
(10, 'Upper Chitral ', 52, 'Brep', 848, 'MAHTING-1 Bala'),
(10, 'Upper Chitral ', 52, 'Brep', 849, 'MERAGRAM-1 Paien'),
(10, 'Upper Chitral ', 52, 'Brep', 850, 'MERAGRAM-2 Bala'),
(10, 'Upper Chitral ', 52, 'Brep', 851, 'MOMEENABAD Bang'),
(10, 'Upper Chitral ', 52, 'Brep', 852, 'MOULAABAD'),
(10, 'Upper Chitral ', 52, 'Brep', 853, 'MUHAMMADABAD'),
(10, 'Upper Chitral ', 52, 'Brep', 854, 'MULDEH'),
(10, 'Upper Chitral ', 52, 'Brep', 855, 'MUSTAFAABAD'),
(10, 'Upper Chitral ', 52, 'Brep', 856, 'NICHHAGH'),
(10, 'Upper Chitral ', 52, 'Brep', 857, 'NURABAD'),
(10, 'Upper Chitral ', 52, 'Brep', 858, 'PARDAN'),
(10, 'Upper Chitral ', 52, 'Brep', 859, 'PASHK'),
(10, 'Upper Chitral ', 52, 'Brep', 860, 'PATRANGAZ'),
(10, 'Upper Chitral ', 52, 'Brep', 861, 'PAWER-1 Uchu Hoon'),
(10, 'Upper Chitral ', 52, 'Brep', 862, 'PAWER-2'),
(10, 'Upper Chitral ', 52, 'Brep', 863, 'RAHIMABAD Brep Central'),
(10, 'Upper Chitral ', 52, 'Brep', 864, 'RAHMANABAD'),
(10, 'Upper Chitral ', 52, 'Brep', 865, 'RATHENI'),
(10, 'Upper Chitral ', 52, 'Brep', 866, 'RUSTAMANDEH Kuch'),
(10, 'Upper Chitral ', 52, 'Brep', 867, 'SHICH'),
(10, 'Upper Chitral ', 52, 'Brep', 868, 'SHOL KUCH'),
(10, 'Upper Chitral ', 52, 'Brep', 869, 'SULTANABAD (KHOTAN LASHT)'),
(10, 'Upper Chitral ', 52, 'Brep', 870, 'URKAN'),
(10, 'Upper Chitral ', 52, 'Brep', 871, 'Waghalabad Bang'),
(10, 'Upper Chitral ', 52, 'Brep', 872, 'WASUM Bala Sub Centre'),
(10, 'Upper Chitral ', 52, 'Brep', 873, 'WASUM-2 Paien'),
(10, 'Upper Chitral ', 52, 'Brep', 874, 'ZUPU'),
(10, 'Upper Chitral ', 48, 'Khot', 875, 'ANDERGHECH'),
(10, 'Upper Chitral ', 48, 'Khot', 876, 'BARKATABAD Boop'),
(10, 'Upper Chitral ', 48, 'Khot', 877, 'BUZUND Sub Centre'),
(10, 'Upper Chitral ', 48, 'Khot', 878, 'CHAT GAZ Sub Centre'),
(10, 'Upper Chitral ', 48, 'Khot', 879, 'DERGHERANDUR'),
(10, 'Upper Chitral ', 48, 'Khot', 880, 'DUKAN'),
(10, 'Upper Chitral ', 48, 'Khot', 881, 'FARDAGH'),
(10, 'Upper Chitral ', 48, 'Khot', 882, 'GOAL KHOT'),
(10, 'Upper Chitral ', 48, 'Khot', 883, 'HOON KHOT'),
(10, 'Upper Chitral ', 48, 'Khot', 884, 'Jumbai Shal'),
(10, 'Upper Chitral ', 48, 'Khot', 885, 'KISH WAHT'),
(10, 'Upper Chitral ', 48, 'Khot', 886, 'KISHEK'),
(10, 'Upper Chitral ', 48, 'Khot', 887, 'Kishim'),
(10, 'Upper Chitral ', 48, 'Khot', 888, 'Kishiwaht 2'),
(10, 'Upper Chitral ', 48, 'Khot', 889, 'LAMKUSHUM-1'),
(10, 'Upper Chitral ', 48, 'Khot', 890, 'LAMKUSHUM-2'),
(10, 'Upper Chitral ', 48, 'Khot', 891, 'LASHTDUR-1 Sub Centre'),
(10, 'Upper Chitral ', 48, 'Khot', 892, 'LASHTDUR-2'),
(10, 'Upper Chitral ', 48, 'Khot', 893, 'LOOGAR'),
(10, 'Upper Chitral ', 48, 'Khot', 894, 'LUNGAR'),
(10, 'Upper Chitral ', 48, 'Khot', 895, 'Mahtin 2'),
(10, 'Upper Chitral ', 48, 'Khot', 896, 'MAHTING Washich SubCentre'),
(10, 'Upper Chitral ', 48, 'Khot', 897, 'MALANDUR'),
(10, 'Upper Chitral ', 48, 'Khot', 898, 'MARANDEH'),
(10, 'Upper Chitral ', 48, 'Khot', 899, 'MAYAR'),
(10, 'Upper Chitral ', 48, 'Khot', 900, 'NICHAGH-1'),
(10, 'Upper Chitral ', 48, 'Khot', 901, 'NICHAGH-2'),
(10, 'Upper Chitral ', 48, 'Khot', 902, 'NOGHORAN TEK'),
(10, 'Upper Chitral ', 48, 'Khot', 903, 'PUCHUNG-1'),
(10, 'Upper Chitral ', 48, 'Khot', 904, 'PUCHUNG-2'),
(10, 'Upper Chitral ', 48, 'Khot', 905, 'PURKHOT-1'),
(10, 'Upper Chitral ', 48, 'Khot', 906, 'PURKHOT-2'),
(10, 'Upper Chitral ', 48, 'Khot', 907, 'RABAT-1'),
(10, 'Upper Chitral ', 48, 'Khot', 908, 'RABAT-2'),
(10, 'Upper Chitral ', 48, 'Khot', 909, 'SHAGRAM-1 Budandur'),
(10, 'Upper Chitral ', 48, 'Khot', 910, 'SHAGRAM-2 Uch Waht'),
(10, 'Upper Chitral ', 48, 'Khot', 911, 'SHAGRAM-3 (Mayar)'),
(10, 'Upper Chitral ', 48, 'Khot', 912, 'SHALANSUR'),
(10, 'Upper Chitral ', 48, 'Khot', 913, 'SHARASTOON'),
(10, 'Upper Chitral ', 48, 'Khot', 914, 'SHOOL'),
(10, 'Upper Chitral ', 48, 'Khot', 915, 'SHOT MELP'),
(10, 'Upper Chitral ', 48, 'Khot', 916, 'SHOTKHAR Central'),
(10, 'Upper Chitral ', 48, 'Khot', 917, 'SHOTKHAR-2'),
(10, 'Upper Chitral ', 48, 'Khot', 918, 'SHUCHAN LASHT'),
(10, 'Upper Chitral ', 48, 'Khot', 919, 'WAZUM'),
(10, 'Upper Chitral ', 48, 'Khot', 920, 'YAKDIZ-4'),
(10, 'Upper Chitral ', 48, 'Khot', 921, 'YAKHDIZ-1'),
(10, 'Upper Chitral ', 48, 'Khot', 922, 'YAKHDIZ-2'),
(10, 'Upper Chitral ', 48, 'Khot', 923, 'YAKHDIZ-3'),
(10, 'Upper Chitral ', 50, 'Laspur', 924, 'BALIM-1'),
(10, 'Upper Chitral ', 50, 'Laspur', 925, 'BALIM-2'),
(10, 'Upper Chitral ', 50, 'Laspur', 926, 'BALIM-3 Dook'),
(10, 'Upper Chitral ', 50, 'Laspur', 927, 'BALIM-4 Tordeh');
INSERT INTO `temp` (`ruid`, `regional`, `luid`, `local`, `juid`, `jamatkhana`) VALUES
(10, 'Upper Chitral ', 50, 'Laspur', 928, 'BROOK-1'),
(10, 'Upper Chitral ', 50, 'Laspur', 929, 'BROOK-2'),
(10, 'Upper Chitral ', 50, 'Laspur', 930, 'CHATORGHONI'),
(10, 'Upper Chitral ', 50, 'Laspur', 931, 'Dok Dur'),
(10, 'Upper Chitral ', 50, 'Laspur', 932, 'DOWKHAN Balim-5'),
(10, 'Upper Chitral ', 50, 'Laspur', 933, 'GADAR SULASPUR'),
(10, 'Upper Chitral ', 50, 'Laspur', 934, 'GASHT PAEEN'),
(10, 'Upper Chitral ', 50, 'Laspur', 935, 'GASHT-1'),
(10, 'Upper Chitral ', 50, 'Laspur', 936, 'GASHT-2'),
(10, 'Upper Chitral ', 50, 'Laspur', 937, 'GUCHGALL'),
(10, 'Upper Chitral ', 50, 'Laspur', 938, 'HARCHIN3'),
(10, 'Upper Chitral ', 50, 'Laspur', 939, 'HERCHIN Central'),
(10, 'Upper Chitral ', 50, 'Laspur', 940, 'HUJUNE'),
(10, 'Upper Chitral ', 50, 'Laspur', 941, 'IMAMABAD BALIM-6,'),
(10, 'Upper Chitral ', 50, 'Laspur', 942, 'JUBILEEABAD Balim - 7'),
(10, 'Upper Chitral ', 50, 'Laspur', 943, 'LASHT-1 Herchin'),
(10, 'Upper Chitral ', 50, 'Laspur', 944, 'LASHT-2 Herchin'),
(10, 'Upper Chitral ', 50, 'Laspur', 945, 'MAYOON LASHT'),
(10, 'Upper Chitral ', 50, 'Laspur', 946, 'NOORABAD Ownshot'),
(10, 'Upper Chitral ', 50, 'Laspur', 947, 'OWNSHOT'),
(10, 'Upper Chitral ', 50, 'Laspur', 948, 'PARGRAM-1'),
(10, 'Upper Chitral ', 50, 'Laspur', 949, 'PARGRAM-2'),
(10, 'Upper Chitral ', 50, 'Laspur', 950, 'PORTH-1'),
(10, 'Upper Chitral ', 50, 'Laspur', 951, 'PORTH-2'),
(10, 'Upper Chitral ', 50, 'Laspur', 952, 'RAMAN-1 Guchdur'),
(10, 'Upper Chitral ', 50, 'Laspur', 953, 'RAMAN-2 Kochdur'),
(10, 'Upper Chitral ', 50, 'Laspur', 954, 'RAMAN-3 Doshandeh'),
(10, 'Upper Chitral ', 50, 'Laspur', 955, 'RAMAN-4 Shaghnandeh'),
(10, 'Upper Chitral ', 50, 'Laspur', 956, 'REJUNE'),
(10, 'Upper Chitral ', 50, 'Laspur', 957, 'REJUNE - 2'),
(10, 'Upper Chitral ', 50, 'Laspur', 958, 'SHAIDAS'),
(10, 'Upper Chitral ', 50, 'Laspur', 959, 'SHAIDAS-2'),
(10, 'Upper Chitral ', 50, 'Laspur', 960, 'SUR-LASPUR'),
(10, 'Upper Chitral ', 50, 'Laspur', 961, 'Toor Jal'),
(10, 'Upper Chitral ', 51, 'Mastuj', 962, 'ALI ABAD LAKHAB'),
(10, 'Upper Chitral ', 51, 'Mastuj', 963, 'Aliabad Hundur'),
(10, 'Upper Chitral ', 51, 'Mastuj', 964, 'ALYABAD Chapali'),
(10, 'Upper Chitral ', 51, 'Mastuj', 965, 'ALYABAD Chunj-2 Bala'),
(10, 'Upper Chitral ', 51, 'Mastuj', 966, 'CHAPALI Bala'),
(10, 'Upper Chitral ', 51, 'Mastuj', 967, 'Chapali Guch'),
(10, 'Upper Chitral ', 51, 'Mastuj', 968, 'CHINAR Central'),
(10, 'Upper Chitral ', 51, 'Mastuj', 969, 'CHUINJ-1 Paien'),
(10, 'Upper Chitral ', 51, 'Mastuj', 970, 'DARALOT Mastuj'),
(10, 'Upper Chitral ', 51, 'Mastuj', 971, 'GHORU BALA'),
(10, 'Upper Chitral ', 51, 'Mastuj', 972, 'GHORU PAIEN'),
(10, 'Upper Chitral ', 51, 'Mastuj', 973, 'Hasratabad Parkusap'),
(10, 'Upper Chitral ', 51, 'Mastuj', 974, 'HOON Mastuj'),
(10, 'Upper Chitral ', 51, 'Mastuj', 975, 'ISTURDENI'),
(10, 'Upper Chitral ', 51, 'Mastuj', 976, 'KARGIN-1 Bala'),
(10, 'Upper Chitral ', 51, 'Mastuj', 977, 'KARGIN-2'),
(10, 'Upper Chitral ', 51, 'Mastuj', 978, 'KHUZ-1 Paien'),
(10, 'Upper Chitral ', 51, 'Mastuj', 979, 'KHUZ-2 Darmiyan'),
(10, 'Upper Chitral ', 51, 'Mastuj', 980, 'KHUZ-3 Bala'),
(10, 'Upper Chitral ', 51, 'Mastuj', 981, 'LAKAP'),
(10, 'Upper Chitral ', 51, 'Mastuj', 982, 'MUHAMMADABAD GhoruLasht'),
(10, 'Upper Chitral ', 51, 'Mastuj', 983, 'NISUR'),
(10, 'Upper Chitral ', 51, 'Mastuj', 984, 'PARKUSAB-1 Muzhdeh'),
(10, 'Upper Chitral ', 51, 'Mastuj', 985, 'PARKUSAB-2 Parimali'),
(10, 'Upper Chitral ', 51, 'Mastuj', 986, 'PASUM'),
(10, 'Upper Chitral ', 51, 'Mastuj', 987, 'RAHIMABAD Chuinj-3'),
(10, 'Upper Chitral ', 51, 'Mastuj', 988, 'SARGHOZE-1'),
(10, 'Upper Chitral ', 51, 'Mastuj', 989, 'SARGHOZE-2'),
(10, 'Upper Chitral ', 51, 'Mastuj', 990, 'TOOQ - 2 Bala'),
(10, 'Upper Chitral ', 51, 'Mastuj', 991, 'TOOQ Mastuj'),
(10, 'Upper Chitral ', 47, 'Mulkow', 992, 'ALYABAD Morder-3'),
(10, 'Upper Chitral ', 47, 'Mulkow', 993, 'AWNERDER'),
(10, 'Upper Chitral ', 47, 'Mulkow', 994, 'BAHRIANDUR'),
(10, 'Upper Chitral ', 47, 'Mulkow', 995, 'BARCHAT'),
(10, 'Upper Chitral ', 47, 'Mulkow', 996, 'BLOGH'),
(10, 'Upper Chitral ', 47, 'Mulkow', 997, 'BODO DOK Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 998, 'BUMBAGH'),
(10, 'Upper Chitral ', 47, 'Mulkow', 999, 'DILSHAD KHUM'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1000, 'DONO Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1001, 'DOOKDUR Ghat, Bala'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1002, 'GAHT PAEEN'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1003, 'ISTARI Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1004, 'KHALANI'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1005, 'KHOL BRUNZK Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1006, 'KHORA GOLE'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1007, 'KOHDOKE'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1008, 'KUSHUM'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1009, 'LOONE-1 Sub Centre'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1010, 'LOONE-2 Krui Deru'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1011, 'LOONE-3 Auzakh'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1012, 'LOONE-4 Pishili'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1013, 'MORDER-1 Paien'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1014, 'MORDER-2 Godok'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1015, 'MULGAHT'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1016, 'MUZDEH Khost Sub Centre'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1017, 'MUZUDUR Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1018, 'NASU DOOK Ghat'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1019, 'NILANDUR Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1020, 'PANJIANTEK'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1021, 'PINDORU GAZ Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1022, 'PISHALDURI'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1023, 'ROGHONDOK'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1024, 'SANDRAGH'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1025, 'SEER BALA'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1026, 'SEER Saht'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1027, 'SHALDUR'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1028, 'SHERANDUR Saht Sub Centre'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1029, 'SHOKORMALI'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1030, 'SHUNJOOR ANN'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1031, 'WARIJUNE Central'),
(10, 'Upper Chitral ', 47, 'Mulkow', 1032, 'ZUNDRANGRAM Trech'),
(10, 'Upper Chitral ', 49, 'Rech', 1033, 'ADRAKH'),
(10, 'Upper Chitral ', 49, 'Rech', 1034, 'BOLASHT'),
(10, 'Upper Chitral ', 49, 'Rech', 1035, 'DUKAN-1 GoalKhot'),
(10, 'Upper Chitral ', 49, 'Rech', 1036, 'DUKAN-2'),
(10, 'Upper Chitral ', 49, 'Rech', 1037, 'ISKHAN LASHT'),
(10, 'Upper Chitral ', 49, 'Rech', 1038, 'KISHNZOKH'),
(10, 'Upper Chitral ', 49, 'Rech', 1039, 'MU-RECH'),
(10, 'Upper Chitral ', 49, 'Rech', 1040, 'NIA-LASHT'),
(10, 'Upper Chitral ', 49, 'Rech', 1041, 'NISUR RECH Sub Centre'),
(10, 'Upper Chitral ', 49, 'Rech', 1042, 'PA-RECH'),
(10, 'Upper Chitral ', 49, 'Rech', 1043, 'PURGRAM'),
(10, 'Upper Chitral ', 49, 'Rech', 1044, 'RAGH'),
(10, 'Upper Chitral ', 49, 'Rech', 1045, 'ROWA Rech'),
(10, 'Upper Chitral ', 49, 'Rech', 1046, 'SALANDUR'),
(10, 'Upper Chitral ', 49, 'Rech', 1047, 'SHORQ'),
(10, 'Upper Chitral ', 49, 'Rech', 1048, 'SIKUCH'),
(10, 'Upper Chitral ', 49, 'Rech', 1049, 'SU-RECH Sub Centre'),
(10, 'Upper Chitral ', 49, 'Rech', 1050, 'ZHANGOKHAM'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1051, 'ALYABAD'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1052, 'CHIKAR BROGHIL'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1053, 'CHILMARABAD'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1054, 'CHITISAR'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1055, 'DUBARGAR'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1056, 'DURZU'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1057, 'FAIZABAD Inchipun'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1058, 'GARAMCHASHMA Sub Centre'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1059, 'GAREL Broghil'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1060, 'Ghairarum - 3 Hussainabad'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1061, 'GHIRARUM-1'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1062, 'H. A. Inkeel'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1063, 'INKEP Paien'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1064, 'KARIMABAD Ghirarum-2'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1065, 'KHAIRABAD Youkshukut'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1066, 'KHAN KHOON'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1067, 'KHAND-1 Paien'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1068, 'KHAND-2 Bala'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1069, 'KISHMANJAH'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1070, 'LASHKARGAZ'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1071, 'LOOTGAZ'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1072, 'MAIDAN Broghil'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1073, 'Mominabad'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1074, 'OWNAWUCH-1 Paien'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1075, 'OWNAWUCH-2 Bala'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1076, 'Pechoch Broghil'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1077, 'PUKTH'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1078, 'RAHIMABAD Shovest (S/C)'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1079, 'RUKUT'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1080, 'SALIMAHABAD Koi Chilmarabad'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1081, 'SHIKARWAZ Junali'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1082, 'SKIRMUT'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1083, 'YAKHDAN'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1084, 'YARKHOON LASHT Centre'),
(10, 'Upper Chitral ', 53, 'Yarkhoon Lasht', 1085, 'YOUSHKEST');

-- --------------------------------------------------------

--
-- Table structure for table `trash_n2familyresiding`
--

CREATE TABLE IF NOT EXISTS `trash_n2familyresiding` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trash_n28watertoiletdistance`
--

CREATE TABLE IF NOT EXISTS `trash_n28watertoiletdistance` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trash_n30securityreason`
--

CREATE TABLE IF NOT EXISTS `trash_n30securityreason` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `trash_n31saftyreason`
--

CREATE TABLE IF NOT EXISTS `trash_n31saftyreason` (
  `uid` int(11) unsigned NOT NULL,
  `label` varchar(255) NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE IF NOT EXISTS `user` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `username` varchar(128) NOT NULL,
  `password` varchar(128) NOT NULL,
  `userRole` int(5) unsigned NOT NULL,
  `accountStatus` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `loginName` (`username`),
  KEY `userRole` (`userRole`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=98 ;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`id`, `username`, `password`, `userRole`, `accountStatus`) VALUES
(10, 'admin', 'admin&*^', 1, 1),
(53, 'operator1', 'operator1', 7, 0),
(54, 'operator2', 'operator2', 7, 0),
(55, 'operator3', 'operator3', 7, 0),
(56, 'operator4', 'operator4', 7, 0),
(57, 'operator5', 'operator5', 7, 0),
(58, 'operator6', 'operator6', 7, 0),
(59, 'operator7', 'operator7', 7, 0),
(60, 'operator8', 'operator8', 7, 0),
(61, 'operator9', 'operator9', 7, 0),
(62, 'operator10', 'operator10', 7, 0),
(63, 'operator11', 'operator11', 7, 0),
(64, 'operator12', 'operator12', 7, 0),
(65, 'operator13', 'operator13', 7, 0),
(66, 'operator14', 'operator14', 7, 0),
(67, 'operator15', 'operator15', 7, 0),
(68, 'operator16', 'operator16', 7, 0),
(69, 'operator17', 'operator17', 7, 0),
(70, 'operator18', 'operator18', 7, 0),
(71, 'operator19', 'operator19', 7, 0),
(72, 'operator20', 'operator20', 7, 0),
(73, 'operator21', 'operator21', 7, 0),
(74, 'operator22', 'operator22', 7, 0),
(75, 'operator23', 'operator23', 7, 0),
(76, 'operator24', 'operator24', 7, 0),
(77, 'operator25', 'operator25', 7, 0),
(78, 'operator26', 'operator26', 7, 0),
(79, 'operator27', 'operator27', 7, 0),
(80, 'operator28', 'operator28', 7, 0),
(81, 'operator29', 'operator29', 7, 0),
(82, 'operator30', 'operator30', 7, 0),
(83, 'operator31', 'operator31', 7, 0),
(84, 'operator32', 'operator32', 7, 0),
(85, 'operator33', 'operator33', 7, 0),
(86, 'operator34', 'operator34', 7, 0),
(87, 'operator35', 'operator35', 7, 0),
(88, 'operator36', 'operator36', 7, 0),
(89, 'operator37', 'operator37', 7, 0),
(90, 'operator38', 'operator38', 7, 0),
(91, 'operator39', 'operator39', 7, 0),
(92, 'operator40', 'operator40', 7, 0),
(93, 'supervisor1', 'supervisor1', 3, 0),
(94, 'supervisor2', 'supervisor2', 3, 0),
(95, 'supervisor3', 'supervisor3', 3, 0),
(96, 'supervisor4', 'supervisor4', 3, 0),
(97, 'supervisor5', 'supervisor5', 3, 0);

-- --------------------------------------------------------

--
-- Table structure for table `userrole`
--

CREATE TABLE IF NOT EXISTS `userrole` (
  `id` int(11) unsigned NOT NULL,
  `title` varchar(200) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `userrole`
--

INSERT INTO `userrole` (`id`, `title`) VALUES
(1, 'Admin'),
(3, 'Data Entry Supervisor'),
(7, 'Data Entry Operator');

--
-- Constraints for dumped tables
--

--
-- Constraints for table `familymemberentrepreneur`
--
ALTER TABLE `familymemberentrepreneur`
  ADD CONSTRAINT `familymemberentrepreneur_ibfk_12` FOREIGN KEY (`householdUid`) REFERENCES `household` (`uid`),
  ADD CONSTRAINT `familymemberentrepreneur_ibfk_13` FOREIGN KEY (`businessTypeUid`) REFERENCES `n46businesstype` (`uid`),
  ADD CONSTRAINT `familymemberentrepreneur_ibfk_14` FOREIGN KEY (`businessSustainabilityUid`) REFERENCES `n38incomestatus` (`uid`),
  ADD CONSTRAINT `familymemberentrepreneur_ibfk_15` FOREIGN KEY (`financialRiskUid`) REFERENCES `n47financialrisk` (`uid`),
  ADD CONSTRAINT `familymemberentrepreneur_ibfk_16` FOREIGN KEY (`physicalRiskUid`) REFERENCES `n48physicalrisk` (`uid`);

--
-- Constraints for table `familymemberlivingaway`
--
ALTER TABLE `familymemberlivingaway`
  ADD CONSTRAINT `familymemberlivingaway_ibfk_1` FOREIGN KEY (`familyMemberUid`) REFERENCES `familymember` (`uid`);

--
-- Constraints for table `familymembermaternalhealth`
--
ALTER TABLE `familymembermaternalhealth`
  ADD CONSTRAINT `familymembermaternalhealth_ibfk_3` FOREIGN KEY (`deliveryCarriedByUid`) REFERENCES `n45deliverycarriedby` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `familymembermaternalhealth_ibfk_4` FOREIGN KEY (`childGenderUid`) REFERENCES `gender` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `familymembermaternalhealth_ibfk_5` FOREIGN KEY (`familyMemberMotherUid`) REFERENCES `familymember` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `familymembermaternalhealth_ibfk_6` FOREIGN KEY (`familyMemberChildUid`) REFERENCES `familymember` (`uid`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Constraints for table `familymemberseniors`
--
ALTER TABLE `familymemberseniors`
  ADD CONSTRAINT `familymemberseniors_ibfk_1` FOREIGN KEY (`householdUid`) REFERENCES `household` (`uid`),
  ADD CONSTRAINT `familymemberseniors_ibfk_2` FOREIGN KEY (`livingOtherUid`) REFERENCES `n36seniorsliving` (`uid`),
  ADD CONSTRAINT `familymemberseniors_ibfk_3` FOREIGN KEY (`notInvolveSocialOrEconomicActivitiesUid`) REFERENCES `n37seniorsinvolmentreason` (`uid`);

--
-- Constraints for table `jamatkhana`
--
ALTER TABLE `jamatkhana`
  ADD CONSTRAINT `jamatkhana_ibfk_1` FOREIGN KEY (`regionalCouncilUid`) REFERENCES `regionalcouncil` (`uid`),
  ADD CONSTRAINT `jamatkhana_ibfk_2` FOREIGN KEY (`localCouncilUid`) REFERENCES `localcouncil` (`uid`);

--
-- Constraints for table `localcouncil`
--
ALTER TABLE `localcouncil`
  ADD CONSTRAINT `localcouncil_ibfk_1` FOREIGN KEY (`regionalCouncilUid`) REFERENCES `regionalcouncil` (`uid`);

--
-- Constraints for table `user`
--
ALTER TABLE `user`
  ADD CONSTRAINT `user_ibfk_1` FOREIGN KEY (`userRole`) REFERENCES `userrole` (`id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
