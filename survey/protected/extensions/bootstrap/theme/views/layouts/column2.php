<?php /* @var $this Controller */ ?>
<?php $this->beginContent('//layouts/main'); ?>
<div class="row">
    <div class="col-xs-12 text-right">
        <?php
            foreach($this->menu as $item)
			{
				$this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'link',
					'type'=>'primary',
					'label'=>$item['label'],				
					'url'=>$item['url'],				
					'htmlOptions'=>array('class'=>'btn-xs'),				
					'icon'=>$item['icon'],				
				));
			}
        ?>
    </div>
    <div class="col-xs-12">
		<?php echo $content; ?>
    </div>
</div>
<?php $this->endContent(); ?>