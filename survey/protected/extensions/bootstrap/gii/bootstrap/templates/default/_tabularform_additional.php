<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>

<table class="table">
<thead>
	<tr>
	<td class="col-sm-1">&nbsp;</td>
	<?php foreach($this->tableSchema->columns as $column) { ?>
	<?php if(!$column->isPrimaryKey) { ?>
		<td class="col-sm-1"><?php echo $column->name; ?></td>
	<?php } ?>
	<?php } ?>
	</tr>
</thead>
<tbody>
	<?php echo '<?php foreach($model',$this->modelClass,' as $i=>$item): ?>',"\n"; ?>
	<tr rowIndex="<?php echo '<?php echo $i; ?>' ?>">
	<td><a class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="btn-deleterow<?php echo strtolower($this->modelClass);?> glyphicon glyphicon-remove"></i></a></td>	
	<?php foreach($this->tableSchema->columns as $column) { ?>
	<?php 
		if($column->isPrimaryKey) { 	
			echo "\t<?php echo ".$this->generateTabularFormActiveRow($this->modelClass, $column, $this->tableSchema->foreignKeys)."; ?>"; 
		}
		else
		{
			echo "\t<td><?php echo ".$this->generateTabularFormActiveRow($this->modelClass, $column, $this->tableSchema->foreignKeys)."; ?></td>"; 
		} 
	?>
	
	<?php } ?>
	<?php echo "\n"; ?>
	</tr>
	<?php echo '<?php endforeach; ?>', "\n"; ?>
<tbody>
</table>