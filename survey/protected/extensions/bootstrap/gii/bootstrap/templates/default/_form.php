<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php echo "<?php \$form=\$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'".$this->class2id($this->modelClass)."-form',
	'enableAjaxValidation'=>false,
)); ?>\n"; ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo "<?php echo \$form->errorSummary(\$model); ?>\n"; ?>

<?php
foreach($this->tableSchema->columns as $column)
{
	if($column->autoIncrement)
		continue;
	
	echo "<?php echo ".$this->generateActiveRow($this->modelClass, $column, $this->tableSchema->foreignKeys)."; ?>\n"; 
}

echo "<?php echo \$this->renderPartial('application.views.".lcfirst($this->modelClass)."._tabularform', array('form'=>\$form, 'model{$this->modelClass}'=>\$model{$this->modelClass})); ?>\n"; 
?>

<div class="form-actions">
	<?php echo "<?php \$this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>\$model->isNewRecord ? 'Create' : 'Save',
	)); ?>\n"; ?>
</div>

<?php echo "<?php \$this->endWidget(); ?>\n"; ?>
