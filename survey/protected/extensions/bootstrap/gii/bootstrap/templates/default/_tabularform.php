<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>



<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6"><?php echo $this->modelClass; ?></b>
			<div class="col-xs-6 text-right">
				<a id="<?php echo 'btn-addrow', strtolower($this->modelClass);?>" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="<?php echo strtolower($this->modelClass),'-list';?>" class="panel-body">
		<?php echo '<?php echo $this->renderPartial(\'application.views.',lcfirst($this->modelClass),'._tabularform_additional\', array(\'model',$this->modelClass,'\'=>$model',$this->modelClass,')); ?>',"\n"; ?>
	</div>
</div>

<script>
	$('#<?php echo strtolower($this->modelClass),'-list'?>').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php echo 
'<?php
Yii::app()->clientScript->registerScript(\'addrow',strtolower($this->modelClass),'\', "
	$(\'#btn-addrow',strtolower($this->modelClass),'\').click(function(){
		$.ajax({
			url: \'".Yii::app()->urlManager->createUrl(\'',strtolower($this->modelClass),'/formaddrow\') . "\',
			type: \'post\',
			data: $(\'#" . $form->id . "\').serialize(),
			dataType: \'html\',
			success: function(data) {
				$(\'#',strtolower($this->modelClass),'-list\').html(data);
			},
			error: function() {
				alert(\'An error has occured while adding a new block.\');
			}
		});
	});
");
?>
';
?>

<?php echo '<?php'; ?>

Yii::app()->clientScript->registerScript('deleterow<?php echo strtolower($this->modelClass); ?>',"
	$('#<?php echo strtolower($this->modelClass); ?>-list').on('click', '.btn-deleterow<?php echo strtolower($this->modelClass); ?>', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('<?php echo strtolower($this->modelClass);?>/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#<?php echo strtolower($this->modelClass); ?>-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
<?php echo '?>'; ?>


