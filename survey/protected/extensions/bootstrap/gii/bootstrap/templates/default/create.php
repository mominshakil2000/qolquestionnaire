<?php
/**
 * The following variables are available in this template:
 * - $this: the BootCrudCode object
 */
?>
<?php
echo "<?php\n";
$label=$this->pluralize($this->class2name($this->modelClass));
echo "\$this->breadcrumbs=array(
	'$label'=>array('index'),
	'Create',
);\n";
?>

$this->menu=array(
	array('label'=>'List <?php echo $this->modelClass; ?>','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage <?php echo $this->modelClass; ?>','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create <?php echo $this->modelClass; ?></h1>
<?php # TODO : generate tabluar form on demand ?>
<?php echo "<?php echo \$this->renderPartial('_form', array('model'=>\$model, 'model{$this->modelClass}'=>\$model{$this->modelClass})); ?>"; ?>
