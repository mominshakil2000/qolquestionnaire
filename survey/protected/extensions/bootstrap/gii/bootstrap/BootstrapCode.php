<?php
/**
 * BootstrapCode class file.
 * @author Christoffer Niska <ChristofferNiska@gmail.com>
 * @copyright Copyright &copy; Christoffer Niska 2011-
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

Yii::import('gii.generators.crud.CrudCode');

class BootstrapCode extends CrudCode
{
	public function generateActiveRow($modelClass, $column, $foreignKeys)
	{
		if($column->dbType =='date'){
			$column->type = $column->dbType; 
		}
		if ($column->type === 'boolean')
			return "\$form->checkBoxRow(\$model,'{$column->name}')";
		else if ($column->type === 'date')
			return "\$form->textFieldRow(\$model,'{$column->name}',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control', 'inputGroupOptions'=>array('class'=>'date'), 'append'=>'<i class=\"glyphicon glyphicon-calendar\"></i>'))";
		else if($column->isForeignKey)
		{
			$foreignTable = $foreignKeys[$column->name][0];
			$foreignColumn = $foreignKeys[$column->name][1]; 
			return "\$form->dropDownListRow(\$model,'{$column->name}',CHtml::listData(".ucfirst(strtolower($foreignTable))."::model()->findAll(), '{$foreignColumn}', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control'))";
			
		}
		else if (stripos($column->dbType,'text') !== false)
			return "\$form->textAreaRow(\$model,'{$column->name}',array('rows'=>6, 'cols'=>50, 'class'=>'form-control'))";
		else
		{
			if (preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
				$inputField='passwordFieldRow';
			else
				$inputField='textFieldRow';

			if ($column->type!=='string' || $column->size===null)
				return "\$form->{$inputField}(\$model,'{$column->name}',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control'))";
			else
				return "\$form->{$inputField}(\$model,'{$column->name}',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>$column->size))";
		}
	}
	
	public function generateTabularFormActiveRow($modelClass, $column, $foreignKeys)
	{
		if($column->isPrimaryKey)
			return "CHtml::activeHiddenField(\$item,\"[\$i]{$column->name}\")";
		
		if($column->dbType =='date'){
			$column->type = $column->dbType; 
		}
		
		if ($column->type === 'boolean')
			return "\$form->checkBoxRow(\$model,'{$column->name}')";
		else if ($column->type === 'date')
			return '\'<div class="input-group date">\','."CHtml::activeTextField(\$item,\"[\$i]{$column->name}\",array('class'=>'form-control'))".',\'<span class="input-group-addon"><i class="glyphicon glyphicon-calendar"></i></span></div>\'';
		else if($column->isForeignKey)
		{
			$foreignTable = $foreignKeys[$column->name][0];
			$foreignColumn = $foreignKeys[$column->name][1]; 
			return "CHtml::dropDownList(\"[\$i]$column->name\", \$item->{$column->name}, CHtml::listData(".strtolower($foreignTable)."::model()->findAll(), '{$foreignColumn}', 'title'),array('class'=>'form-control'))";
		}
		else if (stripos($column->dbType,'text') !== false)
			return "\$form->textAreaRow(\$model,'{$column->name}',array('rows'=>6, 'cols'=>50, 'class'=>'form-control'),array('class'=>'form-control'))";
		else
		{
			if (preg_match('/^(password|pass|passwd|passcode)$/i',$column->name))
				$inputField='passwordFieldRow';
			else
				$inputField='activeTextField';

			if ($column->type!=='string' || $column->size===null)
				return "CHtml::{$inputField}(\$item,'[\$i]{$column->name}',array('class'=>'form-control'))";
			else
				return "CHtml::{$inputField}(\$item,\"[\$i]{$column->name}\",array('class'=>'form-control'))";
		}
	}
}
