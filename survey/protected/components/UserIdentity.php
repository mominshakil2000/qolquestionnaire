<?php

/**
 * UserIdentity represents the data needed to identity a user.
 * It contains the authentication method that checks if the provided
 * data can identity the user.
 */
class UserIdentity extends CUserIdentity
{
	private $_id;
	
	/**
	 * Authenticates a user.
	 * The example implementation makes sure if the username and password
	 * are both 'demo'.
	 * In practical applications, this should be changed to authenticate
	 * against some persistent user identity storage (e.g. database).
	 * @return boolean whether authentication succeeds.
	 */
	public function authenticate()
	{
			
			
			$user = User::model()->findByAttributes(array('username'=>$this->username));
			
			if($user === NULL || $user->accountStatus == 0)
				$this->errorCode = self::ERROR_USERNAME_INVALID;
			else if($this->password !== $user->password)
				$this->errorCode = self::ERROR_PASSWORD_INVALID;
			else
			{
				$userRole = UserRole::model()->findByPk($user->userRole);
				
				$this->_id = $user->id;
				$this->setState('title', $user->username);
				$this->setState('userRoleId', $userRole->id);
				
				$this->setState('userRole', strtolower($userRole->title));

				$auth=Yii::app()->authManager;
				if(!$auth->isAssigned(strtolower(str_replace(' ', '', $userRole->title)), $this->_id))
				{
					if($auth->assign(strtolower(str_replace(' ', '', $userRole->title)), $this->_id))
					{
						// Yii::app()->authManager->save();
					}
				}

				$this->errorCode=self::ERROR_NONE;
			}

			return !$this->errorCode;
	}
	
	public function getId()
    {
        return $this->_id;
    }
}