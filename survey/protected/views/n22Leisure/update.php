<?php
$this->breadcrumbs=array(
	'N22 Leisures'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N22Leisure','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N22Leisure','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N22Leisure','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N22Leisure','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N22Leisure <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>