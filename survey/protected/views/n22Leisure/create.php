<?php
$this->breadcrumbs=array(
	'N22 Leisures'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N22Leisure','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N22Leisure','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N22Leisure</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN22Leisure'=>$modelN22Leisure)); ?>