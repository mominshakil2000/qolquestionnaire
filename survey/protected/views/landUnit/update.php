<?php
$this->breadcrumbs=array(
	'Land Units'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List LandUnit','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create LandUnit','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View LandUnit','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage LandUnit','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update LandUnit <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>