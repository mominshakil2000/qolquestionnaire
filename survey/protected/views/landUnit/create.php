<?php
$this->breadcrumbs=array(
	'Land Units'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List LandUnit','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage LandUnit','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create LandUnit</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelLandUnit'=>$modelLandUnit)); ?>