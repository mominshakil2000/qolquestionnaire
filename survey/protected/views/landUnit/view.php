<?php
$this->breadcrumbs=array(
	'Land Units'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List LandUnit','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create LandUnit','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update LandUnit','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete LandUnit','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage LandUnit','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View LandUnit #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
		'uidLabel',
	),
)); ?>
