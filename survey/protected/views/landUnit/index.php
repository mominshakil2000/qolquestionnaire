<?php
$this->breadcrumbs=array(
	'Land Units',
);

$this->menu=array(
	array('label'=>'Create LandUnit','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage LandUnit','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Land Units</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
