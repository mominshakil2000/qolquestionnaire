<?php
$this->breadcrumbs=array(
	'N39 Saving Modes',
);

$this->menu=array(
	array('label'=>'Create N39SavingMode','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N39SavingMode','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N39 Saving Modes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
