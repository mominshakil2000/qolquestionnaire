<?php
$this->breadcrumbs=array(
	'N39 Saving Modes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N39SavingMode','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N39SavingMode','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N39SavingMode</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN39SavingMode'=>$modelN39SavingMode)); ?>