


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N39SavingMode</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown39savingmode" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n39savingmode-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n39SavingMode._tabularform_additional', array('modelN39SavingMode'=>$modelN39SavingMode)); ?>
	</div>
</div>

<script>
	$('#n39savingmode-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown39savingmode', "
	$('#btn-addrown39savingmode').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n39savingmode/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n39savingmode-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown39savingmode',"
	$('#n39savingmode-list').on('click', '.btn-deleterown39savingmode', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n39savingmode/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n39savingmode-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

