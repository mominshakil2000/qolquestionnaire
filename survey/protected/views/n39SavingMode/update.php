<?php
$this->breadcrumbs=array(
	'N39 Saving Modes'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N39SavingMode','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N39SavingMode','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N39SavingMode','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N39SavingMode','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N39SavingMode <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>