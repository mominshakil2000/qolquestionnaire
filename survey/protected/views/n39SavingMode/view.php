<?php
$this->breadcrumbs=array(
	'N39 Saving Modes'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N39SavingMode','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N39SavingMode','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N39SavingMode','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N39SavingMode','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N39SavingMode','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N39SavingMode #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
