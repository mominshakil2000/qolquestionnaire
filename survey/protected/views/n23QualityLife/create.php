<?php
$this->breadcrumbs=array(
	'N23 Quality Lives'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N23QualityLife','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N23QualityLife','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N23QualityLife</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN23QualityLife'=>$modelN23QualityLife)); ?>