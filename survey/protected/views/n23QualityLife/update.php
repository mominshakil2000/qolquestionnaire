<?php
$this->breadcrumbs=array(
	'N23 Quality Lives'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N23QualityLife','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N23QualityLife','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N23QualityLife','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N23QualityLife','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N23QualityLife <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>