


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N23QualityLife</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown23qualitylife" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n23qualitylife-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n23QualityLife._tabularform_additional', array('modelN23QualityLife'=>$modelN23QualityLife)); ?>
	</div>
</div>

<script>
	$('#n23qualitylife-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown23qualitylife', "
	$('#btn-addrown23qualitylife').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n23qualitylife/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n23qualitylife-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown23qualitylife',"
	$('#n23qualitylife-list').on('click', '.btn-deleterown23qualitylife', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n23qualitylife/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n23qualitylife-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

