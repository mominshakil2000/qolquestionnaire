<?php
$this->breadcrumbs=array(
	'N23 Quality Lives',
);

$this->menu=array(
	array('label'=>'Create N23QualityLife','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N23QualityLife','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N23 Quality Lives</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
