<?php
$this->breadcrumbs=array(
	'N23 Quality Lives'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N23QualityLife','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N23QualityLife','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N23QualityLife','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N23QualityLife','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N23QualityLife','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N23QualityLife #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
