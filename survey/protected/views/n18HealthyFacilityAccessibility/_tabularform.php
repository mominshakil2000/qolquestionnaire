


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N18HealthyFacilityAccessibility</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown18healthyfacilityaccessibility" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n18healthyfacilityaccessibility-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n18HealthyFacilityAccessibility._tabularform_additional', array('modelN18HealthyFacilityAccessibility'=>$modelN18HealthyFacilityAccessibility)); ?>
	</div>
</div>

<script>
	$('#n18healthyfacilityaccessibility-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown18healthyfacilityaccessibility', "
	$('#btn-addrown18healthyfacilityaccessibility').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n18healthyfacilityaccessibility/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n18healthyfacilityaccessibility-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown18healthyfacilityaccessibility',"
	$('#n18healthyfacilityaccessibility-list').on('click', '.btn-deleterown18healthyfacilityaccessibility', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n18healthyfacilityaccessibility/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n18healthyfacilityaccessibility-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

