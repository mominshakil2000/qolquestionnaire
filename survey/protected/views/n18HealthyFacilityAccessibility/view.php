<?php
$this->breadcrumbs=array(
	'N18 Healthy Facility Accessibilities'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N18HealthyFacilityAccessibility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N18HealthyFacilityAccessibility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N18HealthyFacilityAccessibility','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N18HealthyFacilityAccessibility','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N18HealthyFacilityAccessibility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N18HealthyFacilityAccessibility #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
