<?php
$this->breadcrumbs=array(
	'N18 Healthy Facility Accessibilities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N18HealthyFacilityAccessibility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N18HealthyFacilityAccessibility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N18HealthyFacilityAccessibility</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN18HealthyFacilityAccessibility'=>$modelN18HealthyFacilityAccessibility)); ?>