<?php
$this->breadcrumbs=array(
	'N18 Healthy Facility Accessibilities'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N18HealthyFacilityAccessibility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N18HealthyFacilityAccessibility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N18HealthyFacilityAccessibility','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N18HealthyFacilityAccessibility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N18HealthyFacilityAccessibility <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>