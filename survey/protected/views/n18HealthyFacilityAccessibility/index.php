<?php
$this->breadcrumbs=array(
	'N18 Healthy Facility Accessibilities',
);

$this->menu=array(
	array('label'=>'Create N18HealthyFacilityAccessibility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N18HealthyFacilityAccessibility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N18 Healthy Facility Accessibilities</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
