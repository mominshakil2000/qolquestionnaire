<?php
$this->breadcrumbs=array(
	'N21 Youth Activity Accessibilities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N21YouthActivityAccessibility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N21YouthActivityAccessibility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N21YouthActivityAccessibility</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN21YouthActivityAccessibility'=>$modelN21YouthActivityAccessibility)); ?>