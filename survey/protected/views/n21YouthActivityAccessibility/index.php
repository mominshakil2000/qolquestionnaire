<?php
$this->breadcrumbs=array(
	'N21 Youth Activity Accessibilities',
);

$this->menu=array(
	array('label'=>'Create N21YouthActivityAccessibility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N21YouthActivityAccessibility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N21 Youth Activity Accessibilities</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
