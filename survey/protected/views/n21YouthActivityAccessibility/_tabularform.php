


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N21YouthActivityAccessibility</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown21youthactivityaccessibility" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n21youthactivityaccessibility-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n21YouthActivityAccessibility._tabularform_additional', array('modelN21YouthActivityAccessibility'=>$modelN21YouthActivityAccessibility)); ?>
	</div>
</div>

<script>
	$('#n21youthactivityaccessibility-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown21youthactivityaccessibility', "
	$('#btn-addrown21youthactivityaccessibility').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n21youthactivityaccessibility/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n21youthactivityaccessibility-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown21youthactivityaccessibility',"
	$('#n21youthactivityaccessibility-list').on('click', '.btn-deleterown21youthactivityaccessibility', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n21youthactivityaccessibility/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n21youthactivityaccessibility-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

