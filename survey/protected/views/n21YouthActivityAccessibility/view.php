<?php
$this->breadcrumbs=array(
	'N21 Youth Activity Accessibilities'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N21YouthActivityAccessibility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N21YouthActivityAccessibility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N21YouthActivityAccessibility','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N21YouthActivityAccessibility','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N21YouthActivityAccessibility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N21YouthActivityAccessibility #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
