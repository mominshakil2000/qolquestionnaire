<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'uid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'householdUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'familyMemberUid',CHtml::listData(Familymember::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'residenceCountry',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'currentCity',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'currentJamatKhana',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'residenceStatus',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'workStatus',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'residenceRelativesStatus',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'year',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>4)); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
			'htmlOptions'=> array('class'=>'btn btn-primary btn-sm'),				
			'icon'=>'glyphicon glyphicon-search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
