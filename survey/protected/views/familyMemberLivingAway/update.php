<?php
$this->breadcrumbs=array(
	'Family Member Living Aways'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List FamilyMemberLivingAway','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberLivingAway','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View FamilyMemberLivingAway','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage FamilyMemberLivingAway','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update FamilyMemberLivingAway <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>