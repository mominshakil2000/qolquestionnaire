<?php
$this->breadcrumbs=array(
	'Family Member Living Aways'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FamilyMemberLivingAway','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FamilyMemberLivingAway','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FamilyMemberLivingAway</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFamilyMemberLivingAway'=>$modelFamilyMemberLivingAway)); ?>