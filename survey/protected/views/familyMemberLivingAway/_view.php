<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid),array('view','id'=>$data->uid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('householdUid')); ?>:</b>
	<?php echo CHtml::encode($data->householdUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('familyMemberUid')); ?>:</b>
	<?php echo CHtml::encode($data->familyMemberUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residenceCountry')); ?>:</b>
	<?php echo CHtml::encode($data->residenceCountry); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currentCity')); ?>:</b>
	<?php echo CHtml::encode($data->currentCity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('currentJamatKhana')); ?>:</b>
	<?php echo CHtml::encode($data->currentJamatKhana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residenceStatus')); ?>:</b>
	<?php echo CHtml::encode($data->residenceStatus); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('workStatus')); ?>:</b>
	<?php echo CHtml::encode($data->workStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residenceRelativesStatus')); ?>:</b>
	<?php echo CHtml::encode($data->residenceRelativesStatus); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('year')); ?>:</b>
	<?php echo CHtml::encode($data->year); ?>
	<br />

	*/ ?>

</div>