<?php
$this->breadcrumbs=array(
	'Family Member Living Aways'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List FamilyMemberLivingAway','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberLivingAway','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update FamilyMemberLivingAway','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete FamilyMemberLivingAway','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage FamilyMemberLivingAway','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View FamilyMemberLivingAway #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'householdUid',
		'familyMemberUid',
		'residenceCountry',
		'currentCity',
		'currentJamatKhana',
		'residenceStatus',
		'workStatus',
		'residenceRelativesStatus',
		'year',
	),
)); ?>
