<?php
$this->breadcrumbs=array(
	'Family Member Living Aways',
);

$this->menu=array(
	array('label'=>'Create FamilyMemberLivingAway','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage FamilyMemberLivingAway','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Family Member Living Aways</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
