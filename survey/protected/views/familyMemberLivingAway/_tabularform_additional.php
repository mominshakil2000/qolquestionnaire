
<table class="table">

<tbody>

	<tr>
		<td class="col-sm-2">&nbsp;</td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<?php echo CHtml::activeHiddenField($item,"[$i]uid"); ?>	
			<td rowIndex="<?php echo $i; ?>"><a class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="btn-deleterowfamilymemberlivingaway glyphicon glyphicon-remove"></i></a></td>	
		<?php endforeach; ?>	
		<?php } ?>	
	</tr>
	<tr>
		<td class="col-sm-2">Family Member <span class="required">*</span></td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberLivingAway[$i][familyMemberUid]", $item->familyMemberUid, CHtml::listData(FamilyMember::model()->findAll(array('condition'=>'householdUid=:householdUid', 'params'=>array(':householdUid'=>$item->householdUid))), 'uid', 'firstName'),array('empty'=>'Not Applicable','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'familyMemberUid'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
		
	</tr>
	<tr>
		<td class="col-sm-2">Country of Residence </td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::activeTextField($item,"[$i]residenceCountry",array('class'=>'form-control')); ?></td>	
				<?php echo CHtml::error($item, 'residenceCountry'); ?>
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-2">Current City</td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::activeTextField($item,"[$i]currentCity",array('class'=>'form-control')); ?></td>	
				<?php echo CHtml::error($item, 'currentCity'); ?>
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-2">Current Jamat khana attending </td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::activeTextField($item,"[$i]currentJamatKhana",array('class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'currentJamatKhana'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-2">Permanent (P) / Temporary (T)</td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberLivingAway[$i][residenceStatus]", $item->residenceStatus, CHtml::listData(Lookup::residenceStatus(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'residenceStatus'); ?>
			</td>
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-2">Study(S) / work(W) / Relocated (R) </td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberLivingAway[$i][workStatus]", $item->workStatus, CHtml::listData(Lookup::residencePurpose(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'workStatus'); ?>
			</td>
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-2">Living separately(S) / relatives(R) / other(O)</td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberLivingAway[$i][residenceRelativesStatus]", $item->residenceRelativesStatus, CHtml::listData(Lookup::livingWith(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'residenceRelativesStatus'); ?>
			</td>
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-2">Since Year</td>
		<?php if(is_array($modelFamilyMemberLivingAway)) { ?>
		<?php foreach($modelFamilyMemberLivingAway as $i=>$item): ?>
			<td>
				<?php echo CHtml::activeTextField($item,"[$i]year",array('class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'year'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
<tbody>
</table>