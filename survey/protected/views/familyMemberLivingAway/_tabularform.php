


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">Family members living away from home</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowfamilymemberlivingaway" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="familymemberlivingaway-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.familyMemberLivingAway._tabularform_additional', array('modelFamilyMemberLivingAway'=>$modelFamilyMemberLivingAway)); ?>
	</div>
</div>

<script>
	$('#familymemberlivingaway-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowfamilymemberlivingaway', "
	$('#btn-addrowfamilymemberlivingaway').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMemberLivingAway/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#familymemberlivingaway-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowfamilymemberlivingaway',"
	$('#familymemberlivingaway-list').on('click', '.btn-deleterowfamilymemberlivingaway', function(){
		rowIndex = $(this).closest('td').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMemberLivingAway/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#familymemberlivingaway-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

