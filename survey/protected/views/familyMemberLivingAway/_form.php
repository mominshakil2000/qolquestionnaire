<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'family-member-living-away-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model,'householdUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->dropDownListRow($model,'familyMemberUid',CHtml::listData(Familymember::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->textFieldRow($model,'residenceCountry',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
<?php echo $form->textFieldRow($model,'currentCity',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
<?php echo $form->textFieldRow($model,'currentJamatKhana',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
<?php echo $form->textFieldRow($model,'residenceStatus',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'workStatus',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'residenceRelativesStatus',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'year',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>4)); ?>
<?php echo $this->renderPartial('application.views.familyMemberLivingAway._tabularform', array('form'=>$form, 'modelFamilyMemberLivingAway'=>$modelFamilyMemberLivingAway)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
