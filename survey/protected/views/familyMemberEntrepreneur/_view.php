<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid),array('view','id'=>$data->uid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('householdUid')); ?>:</b>
	<?php echo CHtml::encode($data->householdUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('familyMemberUid')); ?>:</b>
	<?php echo CHtml::encode($data->familyMemberUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('businessTypeUid')); ?>:</b>
	<?php echo CHtml::encode($data->businessTypeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('businessSustainabilityUid')); ?>:</b>
	<?php echo CHtml::encode($data->businessSustainabilityUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('financialRiskUid')); ?>:</b>
	<?php echo CHtml::encode($data->financialRiskUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('physicalRiskUid')); ?>:</b>
	<?php echo CHtml::encode($data->physicalRiskUid); ?>
	<br />


</div>