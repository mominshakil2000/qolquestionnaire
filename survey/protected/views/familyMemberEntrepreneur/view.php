<?php
$this->breadcrumbs=array(
	'Family Member Entrepreneurs'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List FamilyMemberEntrepreneur','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberEntrepreneur','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update FamilyMemberEntrepreneur','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete FamilyMemberEntrepreneur','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage FamilyMemberEntrepreneur','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View FamilyMemberEntrepreneur #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'householdUid',
		'familyMemberUid',
		'businessTypeUid',
		'businessSustainabilityUid',
		'financialRiskUid',
		'physicalRiskUid',
	),
)); ?>
