<?php
$this->breadcrumbs=array(
	'Family Member Entrepreneurs'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List FamilyMemberEntrepreneur','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberEntrepreneur','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View FamilyMemberEntrepreneur','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage FamilyMemberEntrepreneur','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update FamilyMemberEntrepreneur <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>