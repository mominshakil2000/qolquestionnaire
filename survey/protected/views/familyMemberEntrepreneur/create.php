<?php
$this->breadcrumbs=array(
	'Family Member Entrepreneurs'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FamilyMemberEntrepreneur','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FamilyMemberEntrepreneur','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FamilyMemberEntrepreneur</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFamilyMemberEntrepreneur'=>$modelFamilyMemberEntrepreneur)); ?>