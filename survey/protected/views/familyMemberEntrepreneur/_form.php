<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'family-member-entrepreneur-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListRow($model,'householdUid',CHtml::listData(Household::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->dropDownListRow($model,'familyMemberUid',CHtml::listData(Familymember::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->dropDownListRow($model,'businessTypeUid',CHtml::listData(N46businesstype::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->dropDownListRow($model,'businessSustainabilityUid',CHtml::listData(N38incomestatus::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->dropDownListRow($model,'financialRiskUid',CHtml::listData(N47financialrisk::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->dropDownListRow($model,'physicalRiskUid',CHtml::listData(N48physicalrisk::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $this->renderPartial('application.views.familyMemberEntrepreneur._tabularform', array('form'=>$form, 'modelFamilyMemberEntrepreneur'=>$modelFamilyMemberEntrepreneur)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
