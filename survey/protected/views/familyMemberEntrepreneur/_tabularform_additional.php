
<table class="table">
<thead>
	<tr>
		<td class="col-sm-1">&nbsp;</td>
		<td class="col-sm-1">Business type</td>
		<td class="col-sm-1">Business sustainability</td>
		<td class="col-sm-1">Financial risk</td>
		<td class="col-sm-1">Physical risk</td>
	</tr>
</thead>
<tbody>
	<?php foreach($modelFamilyMemberEntrepreneur as $i=>$item): ?>
	<tr rowIndex="<?php echo $i; ?>">
		<td><a class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="btn-deleterowfamilymemberentrepreneur glyphicon glyphicon-remove"></i></a></td>	
		<?php echo CHtml::activeHiddenField($item,"[$i]uid"); ?>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberEntrepreneur[$i][businessTypeUid]", $item->businessTypeUid, CHtml::listData(N46BusinessType::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
			<?php echo CHtml::error($item, 'businessTypeUid'); ?>
		</td>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberEntrepreneur[$i][businessSustainabilityUid]", $item->businessSustainabilityUid, CHtml::listData(N38IncomeStatus::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
			<?php echo CHtml::error($item, 'businessSustainabilityUid'); ?>
		</td>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberEntrepreneur[$i][financialRiskUid]", $item->financialRiskUid, CHtml::listData(N47FinancialRisk::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>	
			<?php echo CHtml::error($item, 'financialRiskUid'); ?>
		</td>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberEntrepreneur[$i][physicalRiskUid]", $item->physicalRiskUid, CHtml::listData(N48PhysicalRisk::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>	
			<?php echo CHtml::error($item, 'physicalRiskUid'); ?>
		</td>	
	</tr>
	<?php endforeach; ?>
<tbody>
</table>