


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">For any of the family member(s) who is/are an entrepreneur or associated in partnership business? If 'no' skip this section</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowfamilymemberentrepreneur" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="familymemberentrepreneur-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.familyMemberEntrepreneur._tabularform_additional', array('modelFamilyMemberEntrepreneur'=>$modelFamilyMemberEntrepreneur)); ?>
	</div>
</div>

<script>
	$('#familymemberentrepreneur-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowfamilymemberentrepreneur', "
	$('#btn-addrowfamilymemberentrepreneur').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMemberEntrepreneur/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#familymemberentrepreneur-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowfamilymemberentrepreneur',"
	$('#familymemberentrepreneur-list').on('click', '.btn-deleterowfamilymemberentrepreneur', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMemberEntrepreneur/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#familymemberentrepreneur-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

