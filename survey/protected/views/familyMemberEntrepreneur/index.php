<?php
$this->breadcrumbs=array(
	'Family Member Entrepreneurs',
);

$this->menu=array(
	array('label'=>'Create FamilyMemberEntrepreneur','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage FamilyMemberEntrepreneur','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Family Member Entrepreneurs</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
