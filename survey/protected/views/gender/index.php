<?php
$this->breadcrumbs=array(
	'Genders',
);

$this->menu=array(
	array('label'=>'Create Gender','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage Gender','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Genders</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
