<?php
$this->breadcrumbs=array(
	'Genders'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Gender','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage Gender','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create Gender</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelGender'=>$modelGender)); ?>