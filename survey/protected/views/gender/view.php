<?php
$this->breadcrumbs=array(
	'Genders'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List Gender','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Gender','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update Gender','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete Gender','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage Gender','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View Gender #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
