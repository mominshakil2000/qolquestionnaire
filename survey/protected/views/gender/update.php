<?php
$this->breadcrumbs=array(
	'Genders'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List Gender','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Gender','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View Gender','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage Gender','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update Gender <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>