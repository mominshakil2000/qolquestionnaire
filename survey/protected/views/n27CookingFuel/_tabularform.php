


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N27CookingFuel</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown27cookingfuel" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n27cookingfuel-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n27CookingFuel._tabularform_additional', array('modelN27CookingFuel'=>$modelN27CookingFuel)); ?>
	</div>
</div>

<script>
	$('#n27cookingfuel-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown27cookingfuel', "
	$('#btn-addrown27cookingfuel').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n27cookingfuel/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n27cookingfuel-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown27cookingfuel',"
	$('#n27cookingfuel-list').on('click', '.btn-deleterown27cookingfuel', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n27cookingfuel/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n27cookingfuel-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

