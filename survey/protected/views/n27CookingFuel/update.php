<?php
$this->breadcrumbs=array(
	'N27 Cooking Fuels'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N27CookingFuel','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N27CookingFuel','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N27CookingFuel','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N27CookingFuel','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N27CookingFuel <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>