<?php
$this->breadcrumbs=array(
	'N27 Cooking Fuels'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N27CookingFuel','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N27CookingFuel','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N27CookingFuel','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N27CookingFuel','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N27CookingFuel','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N27CookingFuel #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
