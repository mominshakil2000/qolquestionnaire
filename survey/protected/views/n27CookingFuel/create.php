<?php
$this->breadcrumbs=array(
	'N27 Cooking Fuels'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N27CookingFuel','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N27CookingFuel','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N27CookingFuel</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN27CookingFuel'=>$modelN27CookingFuel)); ?>