<?php
$this->breadcrumbs=array(
	'N27 Cooking Fuels',
);

$this->menu=array(
	array('label'=>'Create N27CookingFuel','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N27CookingFuel','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N27 Cooking Fuels</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
