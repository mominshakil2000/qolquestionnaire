<?php
$this->breadcrumbs=array(
	'N33 Residance Statuses',
);

$this->menu=array(
	array('label'=>'Create N33ResidanceStatus','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N33ResidanceStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N33 Residance Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
