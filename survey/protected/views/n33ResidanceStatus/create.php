<?php
$this->breadcrumbs=array(
	'N33 Residance Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N33ResidanceStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N33ResidanceStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N33ResidanceStatus</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN33ResidanceStatus'=>$modelN33ResidanceStatus)); ?>