


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N33ResidanceStatus</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown33residancestatus" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n33residancestatus-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n33ResidanceStatus._tabularform_additional', array('modelN33ResidanceStatus'=>$modelN33ResidanceStatus)); ?>
	</div>
</div>

<script>
	$('#n33residancestatus-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown33residancestatus', "
	$('#btn-addrown33residancestatus').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n33residancestatus/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n33residancestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown33residancestatus',"
	$('#n33residancestatus-list').on('click', '.btn-deleterown33residancestatus', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n33residancestatus/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n33residancestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

