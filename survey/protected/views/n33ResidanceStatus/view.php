<?php
$this->breadcrumbs=array(
	'N33 Residance Statuses'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N33ResidanceStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N33ResidanceStatus','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N33ResidanceStatus','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N33ResidanceStatus','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N33ResidanceStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N33ResidanceStatus #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
