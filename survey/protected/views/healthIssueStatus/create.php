<?php
$this->breadcrumbs=array(
	'Health Issue Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List HealthIssueStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage HealthIssueStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create HealthIssueStatus</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelHealthIssueStatus'=>$modelHealthIssueStatus)); ?>