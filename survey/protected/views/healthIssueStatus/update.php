<?php
$this->breadcrumbs=array(
	'Health Issue Statuses'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List HealthIssueStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create HealthIssueStatus','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View HealthIssueStatus','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage HealthIssueStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update HealthIssueStatus <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>