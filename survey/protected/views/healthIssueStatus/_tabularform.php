


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">HealthIssueStatus</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowhealthissuestatus" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="healthissuestatus-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.healthIssueStatus._tabularform_additional', array('modelHealthIssueStatus'=>$modelHealthIssueStatus)); ?>
	</div>
</div>

<script>
	$('#healthissuestatus-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowhealthissuestatus', "
	$('#btn-addrowhealthissuestatus').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('healthissuestatus/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#healthissuestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowhealthissuestatus',"
	$('#healthissuestatus-list').on('click', '.btn-deleterowhealthissuestatus', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('healthissuestatus/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#healthissuestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

