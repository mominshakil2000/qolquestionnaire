<?php
$this->breadcrumbs=array(
	'N17 Illness Frequencies'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N17IllnessFrequency','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N17IllnessFrequency','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N17IllnessFrequency','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N17IllnessFrequency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N17IllnessFrequency <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>