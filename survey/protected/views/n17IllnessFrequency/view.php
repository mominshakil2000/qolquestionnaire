<?php
$this->breadcrumbs=array(
	'N17 Illness Frequencies'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N17IllnessFrequency','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N17IllnessFrequency','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N17IllnessFrequency','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N17IllnessFrequency','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N17IllnessFrequency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N17IllnessFrequency #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
