


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N17IllnessFrequency</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown17illnessfrequency" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n17illnessfrequency-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n17IllnessFrequency._tabularform_additional', array('modelN17IllnessFrequency'=>$modelN17IllnessFrequency)); ?>
	</div>
</div>

<script>
	$('#n17illnessfrequency-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown17illnessfrequency', "
	$('#btn-addrown17illnessfrequency').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n17illnessfrequency/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n17illnessfrequency-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown17illnessfrequency',"
	$('#n17illnessfrequency-list').on('click', '.btn-deleterown17illnessfrequency', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n17illnessfrequency/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n17illnessfrequency-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

