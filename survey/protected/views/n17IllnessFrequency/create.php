<?php
$this->breadcrumbs=array(
	'N17 Illness Frequencies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N17IllnessFrequency','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N17IllnessFrequency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N17IllnessFrequency</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN17IllnessFrequency'=>$modelN17IllnessFrequency)); ?>