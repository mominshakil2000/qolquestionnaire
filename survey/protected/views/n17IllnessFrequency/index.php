<?php
$this->breadcrumbs=array(
	'N17 Illness Frequencies',
);

$this->menu=array(
	array('label'=>'Create N17IllnessFrequency','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N17IllnessFrequency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N17 Illness Frequencies</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
