<?php
$this->breadcrumbs=array(
	'N12 Occupation Categories'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N12OccupationCategory','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N12OccupationCategory','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N12OccupationCategory','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N12OccupationCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N12OccupationCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N12OccupationCategory #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
