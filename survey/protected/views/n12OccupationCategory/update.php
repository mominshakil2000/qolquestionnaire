<?php
$this->breadcrumbs=array(
	'N12 Occupation Categories'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N12OccupationCategory','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N12OccupationCategory','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N12OccupationCategory','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N12OccupationCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N12OccupationCategory <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>