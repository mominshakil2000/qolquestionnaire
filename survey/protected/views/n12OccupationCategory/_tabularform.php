


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N12OccupationCategory</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown12occupationcategory" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n12occupationcategory-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n12OccupationCategory._tabularform_additional', array('modelN12OccupationCategory'=>$modelN12OccupationCategory)); ?>
	</div>
</div>

<script>
	$('#n12occupationcategory-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown12occupationcategory', "
	$('#btn-addrown12occupationcategory').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n12occupationcategory/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n12occupationcategory-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown12occupationcategory',"
	$('#n12occupationcategory-list').on('click', '.btn-deleterown12occupationcategory', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n12occupationcategory/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n12occupationcategory-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

