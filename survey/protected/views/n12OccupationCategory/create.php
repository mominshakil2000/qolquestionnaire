<?php
$this->breadcrumbs=array(
	'N12 Occupation Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N12OccupationCategory','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N12OccupationCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N12OccupationCategory</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN12OccupationCategory'=>$modelN12OccupationCategory)); ?>