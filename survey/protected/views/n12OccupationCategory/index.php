<?php
$this->breadcrumbs=array(
	'N12 Occupation Categories',
);

$this->menu=array(
	array('label'=>'Create N12OccupationCategory','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N12OccupationCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N12 Occupation Categories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
