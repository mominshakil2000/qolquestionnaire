<?php
$this->breadcrumbs=array(
	'N28 Lighting Sources'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N28LightingSource','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N28LightingSource','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N28LightingSource</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN28LightingSource'=>$modelN28LightingSource)); ?>