<?php
$this->breadcrumbs=array(
	'N28 Lighting Sources',
);

$this->menu=array(
	array('label'=>'Create N28LightingSource','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N28LightingSource','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N28 Lighting Sources</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
