<?php
$this->breadcrumbs=array(
	'N28 Lighting Sources'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N28LightingSource','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N28LightingSource','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N28LightingSource','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N28LightingSource','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N28LightingSource','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N28LightingSource #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
