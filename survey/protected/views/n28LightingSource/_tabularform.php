


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N28LightingSource</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown28lightingsource" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n28lightingsource-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n28LightingSource._tabularform_additional', array('modelN28LightingSource'=>$modelN28LightingSource)); ?>
	</div>
</div>

<script>
	$('#n28lightingsource-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown28lightingsource', "
	$('#btn-addrown28lightingsource').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n28lightingsource/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n28lightingsource-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown28lightingsource',"
	$('#n28lightingsource-list').on('click', '.btn-deleterown28lightingsource', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n28lightingsource/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n28lightingsource-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

