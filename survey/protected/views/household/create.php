<?php
$this->breadcrumbs=array(
	'Households'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List Survey Form','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage Survey Form','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create Survey Form</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFamilyMember'=>$modelFamilyMember, 'modelFamilyMemberLivingAway'=>$modelFamilyMemberLivingAway, 'modelFamilyMemberMaternalHealth'=>$modelFamilyMemberMaternalHealth, 'modelFamilyMemberSeniors'=>$modelFamilyMemberSeniors, 'modelFamilyMemberEntrepreneur'=>$modelFamilyMemberEntrepreneur)); ?>