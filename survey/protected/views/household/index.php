<?php
$this->breadcrumbs=array(
	'Households',
);

$this->menu=array(
	array('label'=>'Create Survey Form','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage Survey Form','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Survey Form</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
