<?php
$this->breadcrumbs=array(
	'Households'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	# array('label'=>'List Survey Form','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Survey Form','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	# array('label'=>'View Survey Form','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage Survey Form','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<div class="row">
	<div class="col-xs-10 col-md-offset-2" >
		<h1>Update Survey Form # <?php echo $model->uid; ?></h1>
		<?php echo $this->renderPartial('_form',array('model'=>$model, 'modelFamilyMember'=>$modelFamilyMember, 'modelFamilyMemberLivingAway'=>$modelFamilyMemberLivingAway, 'modelFamilyMemberMaternalHealth'=>$modelFamilyMemberMaternalHealth, 'modelFamilyMemberSeniors'=>$modelFamilyMemberSeniors, 'modelFamilyMemberEntrepreneur'=>$modelFamilyMemberEntrepreneur)); ?>
	</div>
</div>