<?php
$this->breadcrumbs=array(
	'Households'=>array('index'),
	'Manage',
);

$this->menu=array(
	# array('label'=>'List Survey Form','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Survey Form','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('household-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Survey Form</h1>
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
-->
<div class="panel panel-default">
	<div class="panel-heading">
		Advance Search
		<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'link',
					'type'=>'primary',
					'label'=>'',				
					'url'=>'#',				
					'htmlOptions'=> array('class'=>'search-button btn btn-primary btn-sm'),				
					'icon'=>'glyphicon glyphicon-search',				
				));?>
	</div>
		<div class="panel-body search-form" style="display:none">	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div>
	</div><!-- panel -->
<?php 
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'household-admin-form',
		'action' => Yii::app()->createUrl('Household/adminUpdate'),
		'enableAjaxValidation'=>false,
	)); 
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'household-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'class' => 'CCheckBoxColumn',
			// enable selection multiple checkboxes
			'selectableRows' => 2,
			'checkBoxHtmlOptions' => array(
				// array where the ids will be collected
				'name' => 'uid[]',
			),
		),
		'uid',
		'formNum',
		array(
			'filter'=> CHtml::listData(FormType::model()->findAll(), 'uid', 'uidLabel') ,
			'name'=>'formTypeUid',
			'value'=>'FormType::model()->FindByPk($data->formTypeUid)->uidLabel',
		),
		array(
			'filter'=> CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id', 'username') ,
			'name'=>'formAddedByUser',
			'value'=>'User::model()->FindByPk($data->formAddedByUser)->username',
		),
		array(
			'filter'=> CHtml::listData(User::model()->findAll(array('order'=>'username')), 'id', 'username') ,
			'name'=>'formLastUpdatedByUser',
			'value'=>'User::model()->FindByPk($data->formLastUpdatedByUser)->username',
		),
		array(
			'filter'=> CHtml::listData(FormPostStatus::model()->findAll(), 'uid', 'uidLabel') ,
			'name'=>'formPostStatus',
			'value'=>'FormPostStatus::model()->FindByPk($data->formPostStatus)->uidLabel',
		),
		array(
			'filter'=> CHtml::listData(FormFillStatus::model()->findAll(), 'uid', 'uidLabel') ,
			'name'=>'formFillStatus',
			'value'=>'FormFillStatus::model()->FindByPk($data->formFillStatus)->uidLabel',
		),
		'createDate',
		'lastUpdateDate',
		'firstName',
		array(
			'filter'=> CHtml::listData(RegionalCouncil::model()->findAll(array('order'=>'uidLabel')), 'uid', 'uidLabel') ,
			'name'=>'reginalCouncilUid',
			'value'=>'RegionalCouncil::model()->FindByPk($data->reginalCouncilUid)->uidLabel',
		),
		array(
			'filter'=> CHtml::listData(LocalCouncil::model()->findAll(array('order'=>'uidLabel')), 'uid', 'uidLabel') ,
			'name'=>'localCouncilUid',
			'value'=>'LocalCouncil::model()->FindByPk($data->localCouncilUid)->uidLabel',
		),
		array(
			'filter'=> CHtml::listData(JamatKhana::model()->findAll(array('order'=>'uidLabel')), 'uid', 'uidLabel') ,
			'name'=>'nearestJamatKhanaUid',
			'value'=>'JamatKhana::model()->FindByPk($data->nearestJamatKhanaUid)->uidLabel',
		),
		
		/*
		'genderUid',
		'nic',
		'totalHouseHoldMember',
		'isHouseShared',
		'respondentName',
		'respondentHeadRealtionUid',
		'numResidingBloodFamilies',
		'numResidingUnbloodMember',
		'totalFamilyMember',
		'addressHouseNo',
		'addressStreet',
		'addressColony',
		'addressArea',
		'addressVillageCity',
		'addressProvince',
		'addressPrimaryPhone',
		'addressSecondartPhone',
		'houseConstructTypeUid',
		'houseTotalRoom',
		'cookingFuelUid',
		'lightingSourceUid',
		'winterHeatingSystemFuelUid',
		'smokeOutletUid',
		'drinkingWaterSourceUid',
		'recDistanceUid',
		'jkDistanceUid',
		'toiletFacilityUid',
		'physicalThreatInHouseHold',
		'physicalThreatInNeighbourhood',
		'satisfyWithLivingSpace',
		'internetServiceAvailable',
		'residenceStatusUid',
		'confilictInHouseHoldUid',
		'confilictResolutionUid',
		'numberOfSeniorsInHome',
		'numberOfSeniorsInOther',
		'foodShortageInLastMonth',
		'monthlyFamilyIncomeStatusUid',
		'savingUid',
		'deficitUid',
		'savingAvailableUid',
		'outstandingDebitUid',
		'outstandingDebitInterestRateUid',
		'loanPurposeUid',
		'faJamatiHealth',
		'faJamatiEducation',
		'faJamatiHousing',
		'faJamatiFood',
		'faJamatiSeniorCitizen',
		'faJamatiSpecialChild',
		'faExternalHealth',
		'faExternalEducation',
		'faExternalHousing',
		'faExternalFood',
		'faExternalSeniorCitizen',
		'faExternalSpecialChild',
		'hhIsTelvision',
		'hhQtyTelvision',
		'hhIsDishOrCabel',
		'hhQtyDishOrCabel',
		'hhIsWashingMachine',
		'hhQtyWashingMachine',
		'hhIsElectricFan',
		'hhQtyElectricFan',
		'hhIsAc',
		'hhQtyAc',
		'hhIsRefrigerator',
		'hhQtyRefrigerator',
		'hhIsStove',
		'hhQtyStove',
		'hhIsOven',
		'hhQtyOven',
		'hhIsloadshadingBackup',
		'hhQtyloadshadingBackup',
		'hhIsGeyser',
		'hhQtyGeyser',
		'hhIsHeater',
		'hhQtyHeater',
		'hhIsMobilePhone',
		'hhQtyMobilePhone',
		'hhIsLivestock',
		'hhQtyLivestock',
		'hhIsHouse',
		'hhQtyHouse',
		'hhIsMotoryCycle',
		'hhQtyMotoryCycle',
		'hhIsCommercialProperty',
		'hhQtyCommercialProperty',
		'hhIsComputer',
		'hhQtyComputer',
		'hhIsFruitTrees',
		'hhQtyFruitTrees',
		'hhIsAgricultureAsset',
		'hhQtyAgricultureAsset',
		'hhIsCar',
		'hhQtyCar',
		'hhIsTruckBusVan',
		'hhQtyTruckBusVan',
		'hhIsTractor',
		'hhQtyTractor',
		'hhIsAgricultureLand',
		'hhQtyAgricultureLand',
		'hhIsNonAgricultureLand',
		'hhQtyNonAgricultureLand',
		'comments',
		'filledBy',
		'reviewedBy',
		'filledDate',
		'formFilledMinutes',
		'surveyorEmail',
		'surveyorContactNumber',
		'isEducationInstituteAffordable',
		'educationInstituteIsAffordable',
		'educationInstituteIsAccessible',
		'educationInstituteIsQuality',
		'educationInstituteDistanceUid',
		'primaryHealthServiceIsAffordable',
		'primaryHealthServiceIsAccessible',
		'primaryHealthServiceIsQuality',
		'primaryHealthServiceDistanceUid',
		'secondaryHealthServiceIsAffordable',
		'secondaryHealthServiceIsAccessible',
		'secondaryHealthServiceIsQuality',
		'secondaryHealthServiceDistanceUid',
		'drinkingWaterIsAffordable',
		'drinkingWaterIsAccessible',
		'drinkingWaterIsQuality',
		'drinkingWaterDistanceUid',
		'marketIsAffordable',
		'marketIsAccessible',
		'marketIsQuality',
		'marketDistanceUid',
		'parkIsAffordable',
		'parkIsAccessible',
		'parkIsQuality',
		'parkDistanceUid',
		'electricityIsAffordable',
		'electricityIsAccessible',
		'electricityIsQuality',
		'electricityDistanceUid',
		'gasIsAffordable',
		'gasIsAccessible',
		'gasIsQuality',
		'gasDistanceUid',
		'libraryIsAffordable',
		'libraryIsAccessible',
		'libraryIsQuality',
		'libraryDistanceUid',
		'cinemaIsAffordable',
		'cinemaIsAccessible',
		'cinemaIsQuality',
		'cinemaDistanceUid',
		'transportationIsAffordable',
		'transportationIsAccessible',
		'transportationIsQuality',
		'transportationDistanceUid',
		'telephoneIsAffordable',
		'telephoneIsAccessible',
		'telephoneIsQuality',
		'telephoneDistanceUid',
		'isRoadsafty',
		'isUnsafeHouse',
		'isNaturalDisaster',
		'isLawOrder',
		'isInappropriatePolitical',
		'isChildAbuse',
		'isReligiousIntolerance',
		'isLandGrabbing',
		'isRansom',
		'isRiskBusinessWindup',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{update} {delete}',
		),
	),
)); 
?> 
<?php 	
	echo CHtml::activeLabel($model,'uid',array('label'=>'<b>Form Save As :</b>')); 
	echo CHtml::dropDownList("formPostStatus", $model->formPostStatus, CHtml::listData(FormPostStatus::findAllByUserRole(Yii::app()->user->userRoleId), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); 
	$this->widget('bootstrap.widgets.TbButton', array( 'buttonType'=>'submit', 'type'=>'primary', 'label'=>'Save', )); 
?>
<?php $this->endWidget(); ?>