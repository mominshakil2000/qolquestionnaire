<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'formTypeUid',CHtml::listData(FormType::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'firstName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'middleName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'lastName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'genderUid',CHtml::listData(Gender::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'nic',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'totalHouseHoldMember',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isHouseShared',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'respondentName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'respondentHeadRealtionUid',CHtml::listData(N1FamilyRelationship::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'nearestJamatKhanaUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'localCouncilUid',CHtml::listData(LocalCouncil::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'reginalCouncilUid',CHtml::listData(RegionalCouncil::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'numResidingBloodFamilies',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'numResidingUnbloodMember',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'totalFamilyMember',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressHouseNo',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressStreet',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressColony',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressArea',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressVillageCity',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressProvince',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressPrimaryPhone',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'addressSecondartPhone',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'houseConstructTypeUid',CHtml::listData(N26HomeConstruction::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'houseTotalRoom',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'cookingFuelUid',CHtml::listData(N27CookingFuel::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'lightingSourceUid',CHtml::listData(N28LightingSource::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'winterHeatingSystemFuelUid',CHtml::listData(N27CookingFuel::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'smokeOutletUid',CHtml::listData(N29SmokeOutlet::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'drinkingWaterSourceUid',CHtml::listData(N30DrinkingWaterSource::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'recDistanceUid',CHtml::listData(N31FacilityDistance::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'jkDistanceUid',CHtml::listData(N31FacilityDistance::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'toiletFacilityUid',CHtml::listData(N32ToiletFacility::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'physicalThreatInHouseHold',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'physicalThreatInNeighbourhood',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'satisfyWithLivingSpace',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'internetServiceAvailable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'residenceStatusUid',CHtml::listData(N33ResidanceStatus::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'confilictInHouseHoldUid',CHtml::listData(N34Confilict::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'confilictResolutionUid',CHtml::listData(N35DisputeResolution::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'numberOfSeniorsInHome',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'numberOfSeniorsInOther',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'foodShortageInLastMonth',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'monthlyFamilyIncomeStatusUid',CHtml::listData(N38IncomeStatus::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'savingUid',CHtml::listData(N39SavingMode::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'deficitUid',CHtml::listData(N40DeficitManage::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'savingAvailableUid',CHtml::listData(N41SavingAvaliblity::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'outstandingDebitUid',CHtml::listData(N42OutstandingDebt::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'outstandingDebitInterestRateUid',CHtml::listData(N43InterestRate::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'loanPurposeUid',CHtml::listData(N44LoanPurpose::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faJamatiHealth',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faJamatiEducation',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faJamatiHousing',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faJamatiFood',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faJamatiSeniorCitizen',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faJamatiSpecialChild',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faExternalHealth',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faExternalEducation',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faExternalHousing',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faExternalFood',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faExternalSeniorCitizen',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'faExternalSpecialChild',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsTelvision',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyTelvision',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsDishOrCabel',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyDishOrCabel',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsWashingMachine',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyWashingMachine',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsElectricFan',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyElectricFan',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsAc',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyAc',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsRefrigerator',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyRefrigerator',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsStove',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyStove',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsOven',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyOven',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsloadshadingBackup',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyloadshadingBackup',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsGeyser',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyGeyser',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsHeater',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyHeater',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsMobilePhone',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyMobilePhone',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsLivestock',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyLivestock',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsHouse',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyHouse',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsMotoryCycle',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyMotoryCycle',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsCommercialProperty',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyCommercialProperty',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsComputer',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyComputer',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsFruitTrees',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyFruitTrees',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsAgricultureAsset',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyAgricultureAsset',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsCar',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyCar',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsTruckBusVan',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyTruckBusVan',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsTractor',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyTractor',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsAgricultureLand',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyAgricultureLand',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhIsNonAgricultureLand',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'hhQtyNonAgricultureLand',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textAreaRow($model,'comments',array('rows'=>6, 'cols'=>50, 'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'filledBy',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'reviewedBy',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'filledDate',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control', 'inputGroupOptions'=>array('class'=>'date'), 'append'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'formFilledMinutes',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control', 'inputGroupOptions'=>array('class'=>'date'), 'append'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'surveyorEmail',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'surveyorContactNumber',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isEducationInstituteAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'educationInstituteIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'educationInstituteIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'educationInstituteIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'educationInstituteDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'primaryHealthServiceIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'primaryHealthServiceIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'primaryHealthServiceIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'primaryHealthServiceDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'secondaryHealthServiceIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'secondaryHealthServiceIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'secondaryHealthServiceIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'secondaryHealthServiceDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'drinkingWaterIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'drinkingWaterIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'drinkingWaterIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'drinkingWaterDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'marketIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'marketIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'marketIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'marketDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'parkIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'parkIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'parkIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'parkDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'electricityIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'electricityIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'electricityIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'electricityDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'gasIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'gasIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'gasIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'gasDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'libraryIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'libraryIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'libraryIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'libraryDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'cinemaIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'cinemaIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'cinemaIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'cinemaDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'transportationIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'transportationIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'transportationIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'transportationDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'telephoneIsAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'telephoneIsAccessible',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'telephoneIsQuality',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'telephoneDistanceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isRoadsafty',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isUnsafeHouse',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isNaturalDisaster',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isLawOrder',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isInappropriatePolitical',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isChildAbuse',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isReligiousIntolerance',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isLandGrabbing',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isRansom',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'isRiskBusinessWindup',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
			'htmlOptions'=> array('class'=>'btn btn-primary btn-sm'),				
			'icon'=>'glyphicon glyphicon-search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
