<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'household-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>
<div class="panel panel-default">
	<!--
	<div class="panel-heading">
		<div class="row">
		</div>			
	</div>
	-->
	<div id="household-detail-group1" class="panel-body">
		<?php echo CHtml::activeHiddenField($model,"uid"); ?>
		<?php echo $form->textFieldRow($model,'formNum',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->dropDownListRow($model,'formTypeUid',CHtml::listData(FormType::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
		<?php echo $form->textFieldRow($model,'firstName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'middleName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'lastName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->dropDownListRow($model,'genderUid',CHtml::listData(Gender::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'),'empty'=>'','class'=>'form-control')); ?>
		<?php echo $form->textFieldRow($model,'nic',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'familyNumberNic',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_isHouseShared">House shared (Y) or not shared (N)</label>
			<div class="col-sm-9">
				<?php echo CHtml::dropDownList("Household[isHouseShared]", $model->isHouseShared, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'isHouseShared'); ?>
			</div>
		</div>
		<?php echo $form->textFieldRow($model,'respondentName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->dropDownListRow($model,'respondentHeadRealtionUid',CHtml::listData(N1FamilyRelationship::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'), 'empty'=>'', 'class'=>'form-control')); ?>
	</div>
</div>
<div class="panel panel-default">
	<!--
	<div class="panel-heading">
		<div class="row">
		</div>			
	</div>
	-->
	<div id="household-detail-group2" class="panel-body">
		<?php echo $form->dropDownListRow($model,'reginalCouncilUid',CHtml::listData(RegionalCouncil::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'), 'empty'=>'', 'class'=>'form-control')); ?>
		<?php echo $form->dropDownListRow($model,'localCouncilUid',CHtml::listData(LocalCouncil::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'), 'empty'=>'', 'class'=>'form-control')); ?>
		<?php echo $form->dropDownListRow($model,'nearestJamatKhanaUid',CHtml::listData(JamatKhana::model()->findAll(), 'uid', 'uidLabel'), array('labelOptions'=>array('class'=>'col-sm-3'), 'empty'=>'', 'class'=>'form-control')); ?>
		<?php echo $form->textFieldRow($model,'numResidingBloodFamilies',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
		<?php echo $form->textFieldRow($model,'numResidingUnbloodFamilies',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
		<?php echo $form->textFieldRow($model,'totalFamilyMember',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
		<?php echo $form->textFieldRow($model,'numResidingUnbloodMember',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>
</div>

<div class="panel panel-default">
	<!--
	<div class="panel-heading">
		<div class="row">
		</div>			
	</div>
	-->
	<div id="household-detail-group3" class="panel-body">
		<?php echo $form->textFieldRow($model,'addressHouseNo',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'addressStreet',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'addressColony',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'addressArea',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'addressVillageCity',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'addressProvince',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'addressPrimaryPhone',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'addressSecondartPhone',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>
</div>

<div id="household-detail-group4"></div>
<?php echo $this->renderPartial('application.views.familyMember._tabularform', array('form'=>$form, 'modelFamilyMember'=>$modelFamilyMember)); ?>

<div id="household-detail-group5"></div>
<?php echo $this->renderPartial('application.views.familyMemberLivingAway._tabularform', array('form'=>$form, 'modelFamilyMemberLivingAway'=>$modelFamilyMemberLivingAway)); ?>


<div id="household-detail-group11" class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b>Income / Deficit</b>
		</div>
	</div>
	<div id="household-list" class="panel-body">
		<table class="table">
		<tbody>
			<tr>
				<td class="col-sm-6">1. What is your monthly family income status?</td>
				<td class="col-sm-6">
					<?php echo CHtml::dropDownList("Household[monthlyFamilyIncomeStatusUid]", $model->monthlyFamilyIncomeStatusUid, CHtml::listData(N38IncomeStatus::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'monthlyFamilyIncomeStatusUid'); ?>
				</td>
			</tr>
			<tr>
				<td>2. If family status is surplus? What is the mode of saving / Investment (Most / Preferred mode of the investment)</td>
				<td><?php echo CHtml::dropDownList("Household[savingUid]", $model->savingUid, CHtml::listData(N39SavingMode::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'savingUid'); ?>
				</td>
			</tr>
			<tr>
				<td>3. If family is experiencing deficit, how is it being managed?</td>
				<td><?php echo CHtml::dropDownList("Household[deficitUid]", $model->deficitUid, CHtml::listData(N40DeficitManage::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'deficitUid'); ?>
				</td>
			</tr>
			<tr>
				<td>4. In case if main source of income discontinued, Is your saving enough to cover your monthly expenses for months/years? (Refer note)</td>
				<td><?php echo CHtml::dropDownList("Household[savingAvailableUid]", $model->savingAvailableUid, CHtml::listData(N41SavingAvaliblity::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'savingAvailableUid'); ?>
				</td>
			</tr>
			<tr>
				<td>5. Outstanding debt from? (Major borrowing)</td>
				<td><?php echo CHtml::dropDownList("Household[outstandingDebitUid]", $model->outstandingDebitUid, CHtml::listData(N42OutstandingDebt::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'outstandingDebitUid'); ?>
				</td>
			</tr>
			<tr>
				<td>6. Average Interest rate on outstanding debt (Per anum)?</td>
				<td><?php echo CHtml::dropDownList("Household[outstandingDebitInterestRateUid]", $model->outstandingDebitInterestRateUid, CHtml::listData(N43InterestRate::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'outstandingDebitInterestRateUid'); ?>
				</td>
			</tr>
			<tr>
				<td>7. What was the purpose of loan money? (For major borrowing)</td>
				<td><?php echo CHtml::dropDownList("Household[loanPurposeUid]", $model->loanPurposeUid, CHtml::listData(N44LoanPurpose::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'loanPurposeUid'); ?>
				</td>
			</tr>
		
		<tbody>
		</table>
	</div>
</div>

<div id="household-detail-group7" class="panel panel-default">
	<div id="household-facility-group1" class="panel-body">
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_houseConstructTypeUid">Main Type Construction of Dwelling (Home)</label>
			<div class="col-sm-9">
				<?php echo CHtml::dropDownList("Household[houseConstructTypeUid]", $model->houseConstructTypeUid, CHtml::listData(N26HomeConstruction::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'houseConstructTypeUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_houseTotalRoom">No. of rooms (excld Store, Kitchen & Washroom)</label>
			<div class="col-sm-9"><?php echo CHtml::activeTextField($model,"houseTotalRoom",array('class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'houseTotalRoom'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_cookingFuelUid">Fuel used for cooking</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[cookingFuelUid]", $model->cookingFuelUid, CHtml::listData(N27CookingFuel::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'cookingFuelUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_lightingSourceUid">Main source of lighting</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[lightingSourceUid]", $model->lightingSourceUid, CHtml::listData(N28LightingSource::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'lightingSourceUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_winterHeatingSystemFuelUid">Fuel for heating system during winter</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[winterHeatingSystemFuelUid]", $model->winterHeatingSystemFuelUid, CHtml::listData(N27CookingFuel::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'winterHeatingSystemFuelUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_smokeOutletUid">Smoke outlet in house </label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[smokeOutletUid]", $model->smokeOutletUid, CHtml::listData(N29SmokeOutlet::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'smokeOutletUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_">Main source of drinking water </label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[drinkingWaterSourceUid]", $model->drinkingWaterSourceUid, CHtml::listData(N30DrinkingWaterSource::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'drinkingWaterSourceUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_">Distance to reach REC</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[recDistanceUid]", $model->recDistanceUid, CHtml::listData(N31FacilityDistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'recDistanceUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_">Distance to reach Jamat khana</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[jkDistanceUid]", $model->jkDistanceUid, CHtml::listData(N31FacilityDistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'jkDistanceUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_">Toilet facilities</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[toiletFacilityUid]", $model->toiletFacilityUid, CHtml::listData(N32ToiletFacility::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'toiletFacilityUid'); ?>
			</div>
		</div>
	</div>
</div>

<div id="household-detail-group8" class="panel panel-default">
	<div id="household-facility-group2" class="panel-body">
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_houseConstructTypeUid">Does any HH member feel insecure in house? (Y/N)</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[physicalThreatInHouseHold]", $model->physicalThreatInHouseHold, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'physicalThreatInHouseHold'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_physicalThreatInNeighbourhood">Does any HH member feel insecure in Neighbourhood? (Y/N)</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[physicalThreatInNeighbourhood]", $model->physicalThreatInNeighbourhood, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'physicalThreatInNeighbourhood'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_satisfyWithLivingSpace">Are you satisfied with your living Space ?</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[satisfyWithLivingSpace]", $model->satisfyWithLivingSpace, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'satisfyWithLivingSpace'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_internetServiceAvailable">Do you have internet service available in your village / area? (Y/N)</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[internetServiceAvailable]", $model->internetServiceAvailable, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'internetServiceAvailable'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_residenceStatusUid">What is the status of  residence you live in?</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[residenceStatusUid]", $model->residenceStatusUid, CHtml::listData(N33ResidanceStatus::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'residenceStatusUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_confilictInHouseHoldUid">Any conflicts resulting in stress in household? (most relevant)</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[confilictInHouseHoldUid]", $model->confilictInHouseHoldUid, CHtml::listData(N34Confilict::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'confilictInHouseHoldUid'); ?>
			</div>
		</div>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_confilictResolutionUid">For any dispute / conflict resolution who do you approach?</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[confilictResolutionUid]", $model->confilictResolutionUid, CHtml::listData(N35DisputeResolution::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($model, 'confilictResolutionUid'); ?>
			</div>
		</div>
	</div>
</div>

<div id="household-detail-group9" class="panel panel-default">
	<!--
	<div class="panel-heading">
		<div class="row">
		</div>			
	</div>
	-->
	<div id="household-facility-group3" class="panel-body">
		<?php echo $form->textFieldRow($model,'numberOfSeniorsInHome',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
		<?php echo $form->textFieldRow($model,'numberOfSeniorsInOther',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>
</div>

<div id="household-detail-group10"></div>
<?php  echo $this->renderPartial('application.views.familyMemberSeniors._tabularform', array('form'=>$form, 'modelFamilyMemberSeniors'=>$modelFamilyMemberSeniors)); ?>

<div id="household-detail-group9" class="panel panel-default">
	<!--
	<div class="panel-heading">
		<div class="row">
		</div>			
	</div>
	-->
	<div class="panel-body">
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_isHouseShared">Did you face any food shortage in last month?</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[foodShortageInLastMonth]", $model->foodShortageInLastMonth, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?></div>
		</div>
		
	</div>
</div>

<div id="household-detail-group12"></div>
<?php echo $this->renderPartial('application.views.familyMemberEntrepreneur._tabularform', array('form'=>$form, 'modelFamilyMemberEntrepreneur'=>$modelFamilyMemberEntrepreneur)); ?>

<div id="household-detail-group13" class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b>For which of the following do you use financial assistance?(Y/N)</b>
		</div>
	</div>
	<div id="household-list" class="panel-body">
		<table class="table">
		<thead>
			<tr>
				<td colspan="2"><b>Welfare / Jamati</b></td>
				<td colspan="2"><b>Non Jamati / Extenal</b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td class="col-sm-2">Health</td>
				<td class="col-sm-4"><?php echo CHtml::dropDownList("Household[faJamatiHealth]", $model->faJamatiHealth, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faJamatiHealth'); ?>
				</td>
				<td class="col-sm-2">Health</td>
				<td class="col-sm-4"><?php echo CHtml::dropDownList("Household[faExternalHealth]", $model->faExternalHealth, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faExternalHealth'); ?>
				</td>
			</tr>
			<tr>
				<td>Education</td>
				<td><?php echo CHtml::dropDownList("Household[faJamatiEducation]", $model->faJamatiEducation, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faJamatiEducation'); ?>
				</td>
				<td>Education</td>
				<td><?php echo CHtml::dropDownList("Household[faExternalEducation]", $model->faExternalEducation, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faExternalEducation'); ?>
				</td>
			</tr>
			<tr>
				<td>Housing </td>
				<td><?php echo CHtml::dropDownList("Household[faJamatiHousing]", $model->faJamatiHousing, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faJamatiHousing'); ?>
				</td>
				<td>Housing </td>
				<td><?php echo CHtml::dropDownList("Household[faExternalHousing]", $model->faExternalHousing, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faExternalHousing'); ?>
				</td>
			</tr>
			<tr>
				<td>Food / Ration</td>
				<td><?php echo CHtml::dropDownList("Household[faJamatiFood]", $model->faJamatiFood, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faJamatiFood'); ?>
				</td>
				<td>Food / Ration</td>
				<td><?php echo CHtml::dropDownList("Household[faExternalFood]", $model->faExternalFood, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faExternalFood'); ?>
				</td>
			</tr>
			<tr>
				<td>Senior Citizen</td>
				<td><?php echo CHtml::dropDownList("Household[faJamatiSeniorCitizen]", $model->faJamatiSeniorCitizen, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faJamatiSeniorCitizen'); ?>
				</td>
				<td>Senior Citizen</td>
				<td><?php echo CHtml::dropDownList("Household[faExternalSeniorCitizen]", $model->faExternalSeniorCitizen, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faExternalSeniorCitizen'); ?>
				</td>
			</tr>
			<tr>
				<td>Special child</td>
				<td><?php echo CHtml::dropDownList("Household[faJamatiSpecialChild]", $model->faJamatiSpecialChild, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faJamatiSpecialChild'); ?>
				</td>
				<td>Special child</td>
				<td><?php echo CHtml::dropDownList("Household[faExternalSpecialChild]", $model->faExternalSpecialChild, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'faExternalSpecialChild'); ?>
				</td>
			</tr>		
		<tbody>
		</table>
	</div>
</div>

<div id="household-detail-group6"></div>
<?php  echo $this->renderPartial('application.views.familyMemberMaternalHealth._tabularform', array('form'=>$form, 'modelFamilyMemberMaternalHealth'=>$modelFamilyMemberMaternalHealth)); ?>


<div id="household-detail-group14"  class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b>Does the household have the following?</b>
			<p>(for assets which are measureable in length specify length with unit of measurement like Acres, Marla, yards, sq. meters etc. in quantity section)<p>
		</div>
	</div>
	<div id="household-list" class="panel-body">
		<table class="table">
		<thead>
			<tr>
				<td class="col-sm-2"><b>Assets</b></td>
				<td class="col-sm-1"><b>(Y/N)</b></td>
				<td class="col-sm-1"><b>Qty</b></td>
				<td class="col-sm-2"><b>Assets</b></td>
				<td class="col-sm-1"><b>(Y/N)</b></td>
				<td class="col-sm-1"><b>Qty</b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Television</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsTelvision]", $model->hhIsTelvision, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsTelvision'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyTelvision",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyTelvision'); ?>
				</td>
				<td>Livestock</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsLivestock]", $model->hhIsLivestock, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsLivestock'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyLivestock",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyLivestock'); ?>
				</td>
			</tr>
			<tr>
				<td>Dish Antenna / TV Cabel</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsDishOrCabel]", $model->hhIsDishOrCabel, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsDishOrCabel'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyDishOrCabel",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyDishOrCabel'); ?>
				</td>
				<td>House / flat / bungalows</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsHouse]", $model->hhIsHouse, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsHouse'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyHouse",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyHouse'); ?>
				</td>
			</tr>
			<tr>
				<td>Washing Machine</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsWashingMachine]", $model->hhIsWashingMachine, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsWashingMachine'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyWashingMachine",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyWashingMachine'); ?>
				</td>
				<td>Motorcycle</td> 
				<td><?php echo CHtml::dropDownList("Household[hhIsMotoryCycle]", $model->hhIsMotoryCycle, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsMotoryCycle'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyMotoryCycle",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyMotoryCycle'); ?>
				</td>
			</tr>
			<tr>
				<td>Electric Fan</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsElectricFan]", $model->hhIsElectricFan, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsElectricFan'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyElectricFan",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyElectricFan'); ?>
				</td>
				<td>Commercial property</td> 
				<td><?php echo CHtml::dropDownList("Household[hhIsCommercialProperty]", $model->hhIsCommercialProperty, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsCommercialProperty'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyCommercialProperty",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyCommercialProperty'); ?>
				</td>
			</tr>
			<tr>
				<td>Air-Conditioner / Air Cooler</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsAc]", $model->hhIsAc, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsAc'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyAc",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyAc'); ?>
				</td>	
				<td>Computer / laptop / tablets</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsComputer]", $model->hhIsComputer, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsComputer'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyComputer",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyComputer'); ?>
				</td>
			</tr>
			<tr>
				<td>Refrigerator / Freezer </td>
				<td><?php echo CHtml::dropDownList("Household[hhIsRefrigerator]", $model->hhIsRefrigerator, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsRefrigerator'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyRefrigerator",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyRefrigerator'); ?>
				</td>
				<td>Fruit trees for business</td> 
				<td><?php echo CHtml::dropDownList("Household[hhIsFruitTrees]", $model->hhIsFruitTrees, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsFruitTrees'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyFruitTrees",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyFruitTrees'); ?>
				</td>
			</tr>
			<tr>
				<td>Cooking Stove </td>
				<td><?php echo CHtml::dropDownList("Household[hhIsStove]", $model->hhIsStove, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsStove'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyStove",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyStove'); ?>
				</td>
				<td>Agriculture related assets</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsAgricultureAsset]", $model->hhIsAgricultureAsset, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsAgricultureAsset'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyAgricultureAsset",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyAgricultureAsset'); ?>
				</td>
			</tr>
			<tr>
				<td>Microwave / Oven </td>
				<td><?php echo CHtml::dropDownList("Household[hhIsOven]", $model->hhIsOven, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsOven'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyOven",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyOven'); ?>
				</td>
				<td>Car</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsCar]", $model->hhIsCar, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsCar'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyCar",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyCar'); ?>
				</td>
			</tr>					
			<tr>
				<td>Generator /UPS /Solar backup</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsloadshadingBackup]", $model->hhIsloadshadingBackup, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsloadshadingBackup'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyloadshadingBackup",array('class'=>'form-control')); ?></td>
				<td>Truck / Minibus /Vans </td>
				<td><?php echo CHtml::dropDownList("Household[hhIsTruckBusVan]", $model->hhIsTruckBusVan, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsTruckBusVan'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyTruckBusVan",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyTruckBusVan'); ?>
				</td>
			</tr>	
			<tr>
				<td>Geyser</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsGeyser]", $model->hhIsGeyser, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsGeyser'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyGeyser",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyGeyser'); ?>
				</td>
				<td>Tractor</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsTractor]", $model->hhIsTractor, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsTractor'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyTractor",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyTractor'); ?>
				</td>
			</tr>
			<tr>
				<td>Gas / Electric Heater</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsHeater]", $model->hhIsHeater, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsHeater'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyHeater",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyHeater'); ?>
				</td>					
				<td>Agriculture land</td> 
				<td><?php echo CHtml::dropDownList("Household[hhIsAgricultureLand]", $model->hhIsAgricultureLand, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsAgricultureLand'); ?>
				</td>
				<td>
					<?php echo CHtml::activeTextField($model,"hhQtyAgricultureLand",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyAgricultureLand'); ?>
					<?php echo CHtml::dropDownList("Household[hhQtyAgricultureLandUnitUid]", $model->hhQtyAgricultureLandUnitUid, CHtml::listData(LandUnit::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyAgricultureLandUnitUid'); ?>
				</td>
			</tr>
			<tr>
				<td>Mobile Phone</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsMobilePhone]", $model->hhIsMobilePhone, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsMobilePhone'); ?>
				</td>
				<td><?php echo CHtml::activeTextField($model,"hhQtyMobilePhone",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyMobilePhone'); ?>
				</td>			
				<td>Non agricultural Land</td>
				<td><?php echo CHtml::dropDownList("Household[hhIsNonAgricultureLand]", $model->hhIsNonAgricultureLand, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhIsNonAgricultureLand'); ?>
				</td>
				<td>
					<?php echo CHtml::activeTextField($model,"hhQtyNonAgricultureLand",array('class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyNonAgricultureLand'); ?>
					<?php echo CHtml::dropDownList("Household[hhQtyNonAgricultureLandUnitUid]", $model->hhQtyNonAgricultureLandUnitUid, CHtml::listData(LandUnit::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'hhQtyNonAgricultureLandUnitUid'); ?>
				</td>
			</tr>			
		<tbody>
		</table>
	</div>
</div>

<div id="household-detail-group15" class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<p>Based on your experiences and perceptions [For Northern areas (GBC) - winter season,  others (south) - round the year], please give your response to the following categories in [Y / N / NA (Not available/applicable) ] except for distance (D)</p>
		</div>
	</div>
	<div id="household-list" class="panel-body">
		<table class="table">
		<thead>
			<tr>
				<td class="col-sm-1"> </td>
				<td class="col-sm-1"><b>Educational institute</b></td>
				<td class="col-sm-1"><b>Primary health care services</b></td>
				<td class="col-sm-1"><b>Secondary / Tertiary  health care services</b></td>
				<td class="col-sm-1"><b>Drinking Water supply</b></td>
				<td class="col-sm-1"><b>Market (grocery, commodities etc.)</b></td>
				<td class="col-sm-1"><b>Local park / Green spaces</b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Affordable (A)</td>
				
				<td><?php echo CHtml::dropDownList("Household[educationInstituteIsAffordable]", $model->educationInstituteIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'educationInstituteIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[primaryHealthServiceIsAffordable]", $model->primaryHealthServiceIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'primaryHealthServiceIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[secondaryHealthServiceIsAffordable]", $model->secondaryHealthServiceIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'secondaryHealthServiceIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[drinkingWaterIsAffordable]", $model->drinkingWaterIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'drinkingWaterIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[marketIsAffordable]", $model->marketIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'marketIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[parkIsAffordable]", $model->parkIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'parkIsAffordable'); ?>
				</td>
			</tr>

			<tr>
				<td>Easily accessible (E)</td>
				<td><?php echo CHtml::dropDownList("Household[educationInstituteIsAccessible]", $model->educationInstituteIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'educationInstituteIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[primaryHealthServiceIsAccessible]", $model->primaryHealthServiceIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'primaryHealthServiceIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[secondaryHealthServiceIsAccessible]", $model->secondaryHealthServiceIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'secondaryHealthServiceIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[drinkingWaterIsAccessible]", $model->drinkingWaterIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'drinkingWaterIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[marketIsAccessible]", $model->marketIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'marketIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[parkIsAccessible]", $model->parkIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'parkIsAccessible'); ?>
				</td>
			</tr>

			<tr>
				<td>Quality (Q)</td>
				<td><?php echo CHtml::dropDownList("Household[educationInstituteIsQuality]", $model->educationInstituteIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'educationInstituteIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[primaryHealthServiceIsQuality]", $model->primaryHealthServiceIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'primaryHealthServiceIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[secondaryHealthServiceIsQuality]", $model->secondaryHealthServiceIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'secondaryHealthServiceIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[drinkingWaterIsQuality]", $model->drinkingWaterIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'drinkingWaterIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[marketIsQuality]", $model->marketIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'marketIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[parkIsQuality]", $model->parkIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'parkIsQuality'); ?>
				</td>
			</tr>
			<tr>
				<td>Distance (D)</td>
				<td><?php echo CHtml::dropDownList("Household[educationInstituteDistanceUid]", $model->educationInstituteDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'educationInstituteDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[primaryHealthServiceDistanceUid]", $model->primaryHealthServiceDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'primaryHealthServiceDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[secondaryHealthServiceDistanceUid]", $model->secondaryHealthServiceDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'secondaryHealthServiceDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[drinkingWaterDistanceUid]", $model->drinkingWaterDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'drinkingWaterDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[marketDistanceUid]", $model->marketDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'marketDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[parkDistanceUid]", $model->parkDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'parkDistanceUid'); ?>
				</td>
			</tr>
		<tbody>
		</table>
		
		<table class="table">
		<thead>
			<tr>
				<td class="col-sm-1"> </td>
				<td class="col-sm-1"><b>Electricity supply</b></td>
				<td class="col-sm-1"><b>Gas supply / Firewood</b></td>
				<td class="col-sm-1"><b>Library Area</b></td>
				<td class="col-sm-1"><b>Cinemas / Art Gallery</b></td>
				<td class="col-sm-1"><b>Transportation</b></td>
				<td class="col-sm-1"><b>Telephone / Mobile</b></td>
			</tr>
		</thead>
		<tbody>
			<tr>
				<td>Affordable (A)</td>
				<td><?php echo CHtml::dropDownList("Household[electricityIsAffordable]", $model->electricityIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'electricityIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[gasIsAffordable]", $model->gasIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'gasIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[libraryIsAffordable]", $model->libraryIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'libraryIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[cinemaIsAffordable]", $model->cinemaIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'cinemaIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[transportationIsAffordable]", $model->transportationIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'transportationIsAffordable'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[telephoneIsAffordable]", $model->telephoneIsAffordable, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'telephoneIsAffordable'); ?>
				</td>
			</tr>
			<tr>
				<td>Easily accessible (E)</td>
				<td><?php echo CHtml::dropDownList("Household[electricityIsAccessible]", $model->electricityIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'electricityIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[gasIsAccessible]", $model->gasIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'gasIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[libraryIsAccessible]", $model->libraryIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'libraryIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[cinemaIsAccessible]", $model->cinemaIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'cinemaIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[transportationIsAccessible]", $model->transportationIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'transportationIsAccessible'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[telephoneIsAccessible]", $model->telephoneIsAccessible, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'telephoneIsAccessible'); ?>
				</td>
			</tr>
			<tr>
				<td>Quality (Q)</td>
				<td><?php echo CHtml::dropDownList("Household[electricityIsQuality]", $model->electricityIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'electricityIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[gasIsQuality]", $model->gasIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'gasIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[libraryIsQuality]", $model->libraryIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'libraryIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[cinemaIsQuality]", $model->cinemaIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'cinemaIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[transportationIsQuality]", $model->transportationIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'transportationIsQuality'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[telephoneIsQuality]", $model->telephoneIsQuality, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'telephoneIsQuality'); ?>
				</td>
			</tr>
			<tr>
				<td>Distance (D)</td>
				<td><?php echo CHtml::dropDownList("Household[electricityDistanceUid]", $model->electricityDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'electricityDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[gasDistanceUid]", $model->gasDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'gasDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[libraryDistanceUid]", $model->libraryDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'libraryDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[cinemaDistanceUid]", $model->cinemaDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'cinemaDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[transportationDistanceUid]", $model->transportationDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'transportationDistanceUid'); ?>
				</td>
				<td><?php echo CHtml::dropDownList("Household[telephoneDistanceUid]", $model->telephoneDistanceUid, CHtml::listData(N31facilitydistance::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'telephoneDistanceUid'); ?>
				</td>
			</tr>
		<tbody>
		</table>
	</div>
</div>

<div id="household-detail-group16" class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b>In general, does your household have the following safety and security concerns (excluding irrelevant external factors which don't affect individual's or HH as a whole)?  [ Y / N / NA (Not applicable)].</b>
		</div>
	</div>
	<div id="household-list" class="panel-body">
		<table class="table">
		<tbody>
			<tr>
				<td>Road Safety</td>
				<td><?php echo CHtml::dropDownList("Household[isRoadsafty]", $model->isRoadsafty, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isRoadsafty'); ?>
				</td>
				<td>Child Abuse</td>
				<td><?php echo CHtml::dropDownList("Household[isChildAbuse]", $model->isChildAbuse, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isChildAbuse'); ?>
				</td>
			</tr>
			<tr>
				<td>Unsafe House Infrastructure</td>
				<td><?php echo CHtml::dropDownList("Household[isUnsafeHouse]", $model->isUnsafeHouse, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isUnsafeHouse'); ?>
				</td>
				<td>Religious Intolerance</td>
				<td><?php echo CHtml::dropDownList("Household[isReligiousIntolerance]", $model->isReligiousIntolerance, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isReligiousIntolerance'); ?>
				</td>
			</tr>
			<tr>
				<td>Natural disaster</td>
				<td><?php echo CHtml::dropDownList("Household[isNaturalDisaster]", $model->isNaturalDisaster, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isNaturalDisaster'); ?>
				</td>
				<td>Land Grabbing</td>
				<td><?php echo CHtml::dropDownList("Household[isLandGrabbing]", $model->isLandGrabbing, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isLandGrabbing'); ?>
				</td>
			</tr>
			<tr>
				<td>General Law &amps; Order</td>
				<td><?php echo CHtml::dropDownList("Household[isLawOrder]", $model->isLawOrder, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isLawOrder'); ?>
				</td>
				<td>Ransom</td>
				<td><?php echo CHtml::dropDownList("Household[isRansom]", $model->isRansom, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isRansom'); ?>
				</td>
			</tr>
			<tr>
				<td>Inappropriate Political Surrounding</td>
				<td><?php echo CHtml::dropDownList("Household[isInappropriatePolitical]", $model->isInappropriatePolitical, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isInappropriatePolitical'); ?>
				</td>
				<td>Risk Of Winding Up Of Business</td>
				<td><?php echo CHtml::dropDownList("Household[isRiskBusinessWindup]", $model->isRiskBusinessWindup, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
					<?php echo CHtml::error($model, 'isRiskBusinessWindup'); ?>
				</td>
			</tr>		
		<tbody>
		</table>
	</div>
</div> 

<?php // echo $form->textFieldRow($model,'isEducationInstituteAffordable',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>

<div id="household-detail-group17" class="panel panel-default">
	<!--
	<div class="panel-heading">
		<div class="row">
		</div>			
	</div>
	-->
	<div id="form-filledby" class="panel-body">
		<?php echo $form->textAreaRow($model,'comments',array('rows'=>6, 'cols'=>50, 'labelOptions'=>array('class'=>'col-sm-3'), 'class'=>'form-control')); ?>
		<?php echo $form->textFieldRow($model,'filledBy',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'reviewedBy',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'filledDate',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control', 'inputGroupOptions'=>array('class'=>'date'), 'append'=>'<i class="glyphicon glyphicon-calendar"></i>')); ?>
		<?php echo $form->textFieldRow($model,'formFilledMinutes',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
		<?php echo $form->textFieldRow($model,'surveyorEmail',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php echo $form->textFieldRow($model,'surveyorContactNumber',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
		<?php // TODO :: handle it using access rights  ?>
		<?php // show only for Admin Supervisor ?>
		<?php if(Yii::app()->user->userRoleId == 2) { ?>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_isHouseShared">Form Added By</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[formAddedByUser]", $model->formAddedByUser, CHtml::listData(User::model()->findAll(), 'id', 'username'),array('class'=>'form-control')); ?></div>
		</div>
		<?php } ?>
		<div class="form-group ">
			<label class="col-sm-3 control-label" for="Household_isHouseShared">Form Status</label>
			<div class="col-sm-9"><?php echo CHtml::dropDownList("Household[formFillStatus]", $model->formFillStatus, CHtml::listData(FormFillStatus::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?></div>
		</div>
	</div> 
</div>
<br/>
<br/>
<br/>
<br/>
<div class="form-actions navbar navbar-default navbar-fixed-bottom">
	<div class="container-fluid text-center">
		<div class="col-sm-1">
			<?php $this->widget('bootstrap.widgets.TbButton', array(
				'buttonType'=>'submit',
				'type'=>'primary',
				'label'=>$model->isNewRecord ? 'Save as' : 'Save as',
			)); ?>
		</div>
		<div class="col-sm-4"><?php echo CHtml::dropDownList("Household[formPostStatus]", $model->formPostStatus, CHtml::listData(FormPostStatus::findAllByUserRole(Yii::app()->user->userRoleId), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?></div>
	</div>
</div>

<?php $jamatKhanaList = JamatKhana::model()->findAll();?>
<?php $localCouncilList = LocalCouncil::model()->findAll();?>
<script>
		// block back button
		// window.onbeforeunload = function() { return "You work will be lost."; };
		
		// add some validation on submit.
		
		/* create json object like [{regionalUid:0, localUid:0}] */
		var localCouncilList = [
		<?php foreach ($localCouncilList as $item) { ?>
			<?php echo "{'regionalUid' : '$item->regionalCouncilUid', 'localUid' : '$item->uid', 'uidLabel' : '$item->uidLabel'},"; ?>
		<?php } ?>
		];
		
		var jamatKhanaList = [
		<?php foreach ($jamatKhanaList as $item) { ?>
			<?php echo "{'regionalUid' : '$item->regionalCouncilUid', 'localUid' : '$item->localCouncilUid', 'jamatKhanaUid' : '$item->uid', 'uidLabel' : '$item->uidLabel'},"; ?>
		<?php } ?>
		];
		var selectedJamatKhanaUid = '<?php echo $model->nearestJamatKhanaUid; ?>';
		/* On change of regional council selection, hide local councils which are not in selected regional council. */
		jQuery('#Household_reginalCouncilUid').change(function(){
			var selectedRegionalUid = jQuery(this).val();
			var selectedLocalUid = jQuery('#Household_localCouncilUid').val();
			console.log('selectedRegionalUid='+selectedRegionalUid);
			jQuery("#Household_localCouncilUid").val('');
			jQuery("#Household_localCouncilUid option[value!='']").remove();
			jQuery("#Household_nearestJamatKhanaUid").val('');
			jQuery("#Household_nearestJamatKhanaUid option[value!='']").remove();
			jQuery.each(localCouncilList, function(i, item) {
				if(selectedRegionalUid == item.regionalUid){
					//console.log(localCouncil[item.localUid]);
					jQuery("#Household_localCouncilUid").append('<option value="'+item.localUid+'" '+(selectedLocalUid == item.localUid ? 'selected=selected' : '')+'>'+item.uidLabel+'</option>');
				}
			});
		});
		
		/* On change of local council selection, hide jamatkhana which are not in selected local council. */
		jQuery('#Household_localCouncilUid').change(function(){
			var selectedLocalUid = jQuery(this).val();
			// var selectedJamatKhanaUid = jQuery('#Household_nearestJamatKhanaUid').val();
			// console.log('selectedLocalUid='+selectedLocalUid);
			jQuery("#Household_nearestJamatKhanaUid").val('');
			jQuery("#Household_nearestJamatKhanaUid option[value!='']").remove();
			jQuery.each(jamatKhanaList, function(i, item) {
				if(selectedLocalUid == item.localUid){
					jQuery("#Household_nearestJamatKhanaUid").append('<option value="'+item.jamatKhanaUid+'" '+(selectedJamatKhanaUid == item.jamatKhanaUid ? 'selected=selected' : '')+'>'+item.uidLabel+'</option>');
				}
			});
		});
		jQuery('#Household_reginalCouncilUid').change();
		jQuery('#Household_localCouncilUid').change();
	</script>
<?php $this->endWidget(); ?>
