<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid),array('view','id'=>$data->uid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('formTypeUid')); ?>:</b>
	<?php echo CHtml::encode($data->formTypeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('middleName')); ?>:</b>
	<?php echo CHtml::encode($data->middleName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lastName')); ?>:</b>
	<?php echo CHtml::encode($data->lastName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('genderUid')); ?>:</b>
	<?php echo CHtml::encode($data->genderUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nic')); ?>:</b>
	<?php echo CHtml::encode($data->nic); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('totalHouseHoldMember')); ?>:</b>
	<?php echo CHtml::encode($data->totalHouseHoldMember); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isHouseShared')); ?>:</b>
	<?php echo CHtml::encode($data->isHouseShared); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('respondentName')); ?>:</b>
	<?php echo CHtml::encode($data->respondentName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('respondentHeadRealtionUid')); ?>:</b>
	<?php echo CHtml::encode($data->respondentHeadRealtionUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nearestJamatKhana')); ?>:</b>
	<?php echo CHtml::encode($data->nearestJamatKhana); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('localCouncilUid')); ?>:</b>
	<?php echo CHtml::encode($data->localCouncilUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reginalCouncilUid')); ?>:</b>
	<?php echo CHtml::encode($data->reginalCouncilUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numResidingBloodFamilies')); ?>:</b>
	<?php echo CHtml::encode($data->numResidingBloodFamilies); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numResidingUnbloodMember')); ?>:</b>
	<?php echo CHtml::encode($data->numResidingUnbloodMember); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('totalFamilyMember')); ?>:</b>
	<?php echo CHtml::encode($data->totalFamilyMember); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressHouseNo')); ?>:</b>
	<?php echo CHtml::encode($data->addressHouseNo); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressStreet')); ?>:</b>
	<?php echo CHtml::encode($data->addressStreet); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressColony')); ?>:</b>
	<?php echo CHtml::encode($data->addressColony); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressArea')); ?>:</b>
	<?php echo CHtml::encode($data->addressArea); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressVillageCity')); ?>:</b>
	<?php echo CHtml::encode($data->addressVillageCity); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressProvince')); ?>:</b>
	<?php echo CHtml::encode($data->addressProvince); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressPrimaryPhone')); ?>:</b>
	<?php echo CHtml::encode($data->addressPrimaryPhone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('addressSecondartPhone')); ?>:</b>
	<?php echo CHtml::encode($data->addressSecondartPhone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('houseConstructTypeUid')); ?>:</b>
	<?php echo CHtml::encode($data->houseConstructTypeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('houseTotalRoom')); ?>:</b>
	<?php echo CHtml::encode($data->houseTotalRoom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cookingFuelUid')); ?>:</b>
	<?php echo CHtml::encode($data->cookingFuelUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('lightingSourceUid')); ?>:</b>
	<?php echo CHtml::encode($data->lightingSourceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('winterHeatingSystemFuelUid')); ?>:</b>
	<?php echo CHtml::encode($data->winterHeatingSystemFuelUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('smokeOutletUid')); ?>:</b>
	<?php echo CHtml::encode($data->smokeOutletUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('drinkingWaterSourceUid')); ?>:</b>
	<?php echo CHtml::encode($data->drinkingWaterSourceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->recDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('jkDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->jkDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('toiletFacilityUid')); ?>:</b>
	<?php echo CHtml::encode($data->toiletFacilityUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('physicalThreatInHouseHold')); ?>:</b>
	<?php echo CHtml::encode($data->physicalThreatInHouseHold); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('physicalThreatInNeighbourhood')); ?>:</b>
	<?php echo CHtml::encode($data->physicalThreatInNeighbourhood); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('satisfyWithLivingSpace')); ?>:</b>
	<?php echo CHtml::encode($data->satisfyWithLivingSpace); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('internetServiceAvailable')); ?>:</b>
	<?php echo CHtml::encode($data->internetServiceAvailable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('residenceStatusUid')); ?>:</b>
	<?php echo CHtml::encode($data->residenceStatusUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('confilictInHouseHoldUid')); ?>:</b>
	<?php echo CHtml::encode($data->confilictInHouseHoldUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('confilictResolutionUid')); ?>:</b>
	<?php echo CHtml::encode($data->confilictResolutionUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numberOfSeniorsInHome')); ?>:</b>
	<?php echo CHtml::encode($data->numberOfSeniorsInHome); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('numberOfSeniorsInOther')); ?>:</b>
	<?php echo CHtml::encode($data->numberOfSeniorsInOther); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('foodShortageInLastMonth')); ?>:</b>
	<?php echo CHtml::encode($data->foodShortageInLastMonth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monthlyFamilyIncomeStatusUid')); ?>:</b>
	<?php echo CHtml::encode($data->monthlyFamilyIncomeStatusUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('savingUid')); ?>:</b>
	<?php echo CHtml::encode($data->savingUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deficitUid')); ?>:</b>
	<?php echo CHtml::encode($data->deficitUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('savingAvailableUid')); ?>:</b>
	<?php echo CHtml::encode($data->savingAvailableUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('outstandingDebitUid')); ?>:</b>
	<?php echo CHtml::encode($data->outstandingDebitUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('outstandingDebitInterestRateUid')); ?>:</b>
	<?php echo CHtml::encode($data->outstandingDebitInterestRateUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('loanPurposeUid')); ?>:</b>
	<?php echo CHtml::encode($data->loanPurposeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faJamatiHealth')); ?>:</b>
	<?php echo CHtml::encode($data->faJamatiHealth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faJamatiEducation')); ?>:</b>
	<?php echo CHtml::encode($data->faJamatiEducation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faJamatiHousing')); ?>:</b>
	<?php echo CHtml::encode($data->faJamatiHousing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faJamatiFood')); ?>:</b>
	<?php echo CHtml::encode($data->faJamatiFood); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faJamatiSeniorCitizen')); ?>:</b>
	<?php echo CHtml::encode($data->faJamatiSeniorCitizen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faJamatiSpecialChild')); ?>:</b>
	<?php echo CHtml::encode($data->faJamatiSpecialChild); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faExternalHealth')); ?>:</b>
	<?php echo CHtml::encode($data->faExternalHealth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faExternalEducation')); ?>:</b>
	<?php echo CHtml::encode($data->faExternalEducation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faExternalHousing')); ?>:</b>
	<?php echo CHtml::encode($data->faExternalHousing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faExternalFood')); ?>:</b>
	<?php echo CHtml::encode($data->faExternalFood); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faExternalSeniorCitizen')); ?>:</b>
	<?php echo CHtml::encode($data->faExternalSeniorCitizen); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('faExternalSpecialChild')); ?>:</b>
	<?php echo CHtml::encode($data->faExternalSpecialChild); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsTelvision')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsTelvision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyTelvision')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyTelvision); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsDishOrCabel')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsDishOrCabel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyDishOrCabel')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyDishOrCabel); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsWashingMachine')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsWashingMachine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyWashingMachine')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyWashingMachine); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsElectricFan')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsElectricFan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyElectricFan')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyElectricFan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsAc')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsAc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyAc')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyAc); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsRefrigerator')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsRefrigerator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyRefrigerator')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyRefrigerator); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsStove')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsStove); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyStove')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyStove); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsOven')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsOven); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyOven')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyOven); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsloadshadingBackup')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsloadshadingBackup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyloadshadingBackup')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyloadshadingBackup); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsGeyser')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsGeyser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyGeyser')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyGeyser); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsHeater')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsHeater); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyHeater')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyHeater); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsMobilePhone')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsMobilePhone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyMobilePhone')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyMobilePhone); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsLivestock')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsLivestock); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyLivestock')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyLivestock); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsHouse')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsHouse); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyHouse')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyHouse); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsMotoryCycle')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsMotoryCycle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyMotoryCycle')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyMotoryCycle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsCommercialProperty')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsCommercialProperty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyCommercialProperty')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyCommercialProperty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsComputer')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsComputer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyComputer')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyComputer); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsFruitTrees')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsFruitTrees); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyFruitTrees')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyFruitTrees); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsAgricultureAsset')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsAgricultureAsset); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyAgricultureAsset')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyAgricultureAsset); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsCar')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsCar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyCar')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyCar); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsTruckBusVan')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsTruckBusVan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyTruckBusVan')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyTruckBusVan); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsTractor')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsTractor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyTractor')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyTractor); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsAgricultureLand')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsAgricultureLand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyAgricultureLand')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyAgricultureLand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhIsNonAgricultureLand')); ?>:</b>
	<?php echo CHtml::encode($data->hhIsNonAgricultureLand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('hhQtyNonAgricultureLand')); ?>:</b>
	<?php echo CHtml::encode($data->hhQtyNonAgricultureLand); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('comments')); ?>:</b>
	<?php echo CHtml::encode($data->comments); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('filledBy')); ?>:</b>
	<?php echo CHtml::encode($data->filledBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('reviewedBy')); ?>:</b>
	<?php echo CHtml::encode($data->reviewedBy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('filledDate')); ?>:</b>
	<?php echo CHtml::encode($data->filledDate); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('formFilledMinutes')); ?>:</b>
	<?php echo CHtml::encode($data->formFilledMinutes); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('surveyorEmail')); ?>:</b>
	<?php echo CHtml::encode($data->surveyorEmail); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('surveyorContactNumber')); ?>:</b>
	<?php echo CHtml::encode($data->surveyorContactNumber); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isEducationInstituteAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->isEducationInstituteAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('educationInstituteIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->educationInstituteIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('educationInstituteIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->educationInstituteIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('educationInstituteIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->educationInstituteIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('educationInstituteDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->educationInstituteDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primaryHealthServiceIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->primaryHealthServiceIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primaryHealthServiceIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->primaryHealthServiceIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primaryHealthServiceIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->primaryHealthServiceIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('primaryHealthServiceDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->primaryHealthServiceDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secondaryHealthServiceIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->secondaryHealthServiceIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secondaryHealthServiceIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->secondaryHealthServiceIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secondaryHealthServiceIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->secondaryHealthServiceIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secondaryHealthServiceDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->secondaryHealthServiceDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('drinkingWaterIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->drinkingWaterIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('drinkingWaterIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->drinkingWaterIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('drinkingWaterIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->drinkingWaterIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('drinkingWaterDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->drinkingWaterDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->marketIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->marketIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->marketIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('marketDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->marketDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parkIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->parkIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parkIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->parkIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parkIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->parkIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('parkDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->parkDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electricityIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->electricityIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electricityIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->electricityIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electricityIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->electricityIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('electricityDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->electricityDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gasIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->gasIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gasIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->gasIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gasIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->gasIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('gasDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->gasDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('libraryIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->libraryIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('libraryIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->libraryIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('libraryIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->libraryIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('libraryDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->libraryDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cinemaIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->cinemaIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cinemaIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->cinemaIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cinemaIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->cinemaIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('cinemaDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->cinemaDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transportationIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->transportationIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transportationIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->transportationIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transportationIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->transportationIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('transportationDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->transportationDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephoneIsAffordable')); ?>:</b>
	<?php echo CHtml::encode($data->telephoneIsAffordable); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephoneIsAccessible')); ?>:</b>
	<?php echo CHtml::encode($data->telephoneIsAccessible); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephoneIsQuality')); ?>:</b>
	<?php echo CHtml::encode($data->telephoneIsQuality); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('telephoneDistanceUid')); ?>:</b>
	<?php echo CHtml::encode($data->telephoneDistanceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isRoadsafty')); ?>:</b>
	<?php echo CHtml::encode($data->isRoadsafty); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isUnsafeHouse')); ?>:</b>
	<?php echo CHtml::encode($data->isUnsafeHouse); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isNaturalDisaster')); ?>:</b>
	<?php echo CHtml::encode($data->isNaturalDisaster); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isLawOrder')); ?>:</b>
	<?php echo CHtml::encode($data->isLawOrder); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isInappropriatePolitical')); ?>:</b>
	<?php echo CHtml::encode($data->isInappropriatePolitical); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isChildAbuse')); ?>:</b>
	<?php echo CHtml::encode($data->isChildAbuse); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isReligiousIntolerance')); ?>:</b>
	<?php echo CHtml::encode($data->isReligiousIntolerance); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isLandGrabbing')); ?>:</b>
	<?php echo CHtml::encode($data->isLandGrabbing); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isRansom')); ?>:</b>
	<?php echo CHtml::encode($data->isRansom); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isRiskBusinessWindup')); ?>:</b>
	<?php echo CHtml::encode($data->isRiskBusinessWindup); ?>
	<br />

	*/ ?>

</div>