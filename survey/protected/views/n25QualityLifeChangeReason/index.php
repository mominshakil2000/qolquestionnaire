<?php
$this->breadcrumbs=array(
	'N25 Quality Life Change Reasons',
);

$this->menu=array(
	array('label'=>'Create N25QualityLifeChangeReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N25QualityLifeChangeReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N25 Quality Life Change Reasons</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
