<?php
$this->breadcrumbs=array(
	'N25 Quality Life Change Reasons'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N25QualityLifeChangeReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N25QualityLifeChangeReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N25QualityLifeChangeReason','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N25QualityLifeChangeReason','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N25QualityLifeChangeReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N25QualityLifeChangeReason #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
