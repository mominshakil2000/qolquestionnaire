<?php
$this->breadcrumbs=array(
	'N25 Quality Life Change Reasons'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N25QualityLifeChangeReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N25QualityLifeChangeReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N25QualityLifeChangeReason','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N25QualityLifeChangeReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N25QualityLifeChangeReason <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>