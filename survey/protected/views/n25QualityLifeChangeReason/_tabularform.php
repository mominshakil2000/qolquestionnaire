


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N25QualityLifeChangeReason</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown25qualitylifechangereason" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n25qualitylifechangereason-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n25QualityLifeChangeReason._tabularform_additional', array('modelN25QualityLifeChangeReason'=>$modelN25QualityLifeChangeReason)); ?>
	</div>
</div>

<script>
	$('#n25qualitylifechangereason-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown25qualitylifechangereason', "
	$('#btn-addrown25qualitylifechangereason').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n25qualitylifechangereason/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n25qualitylifechangereason-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown25qualitylifechangereason',"
	$('#n25qualitylifechangereason-list').on('click', '.btn-deleterown25qualitylifechangereason', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n25qualitylifechangereason/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n25qualitylifechangereason-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

