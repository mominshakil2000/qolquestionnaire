<?php
$this->breadcrumbs=array(
	'N25 Quality Life Change Reasons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N25QualityLifeChangeReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N25QualityLifeChangeReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N25QualityLifeChangeReason</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN25QualityLifeChangeReason'=>$modelN25QualityLifeChangeReason)); ?>