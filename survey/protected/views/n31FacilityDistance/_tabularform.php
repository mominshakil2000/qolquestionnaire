


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N31FacilityDistance</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown31facilitydistance" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n31facilitydistance-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n31FacilityDistance._tabularform_additional', array('modelN31FacilityDistance'=>$modelN31FacilityDistance)); ?>
	</div>
</div>

<script>
	$('#n31facilitydistance-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown31facilitydistance', "
	$('#btn-addrown31facilitydistance').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n31facilitydistance/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n31facilitydistance-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown31facilitydistance',"
	$('#n31facilitydistance-list').on('click', '.btn-deleterown31facilitydistance', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n31facilitydistance/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n31facilitydistance-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

