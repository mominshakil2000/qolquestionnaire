<?php
$this->breadcrumbs=array(
	'N31 Facility Distances'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N31FacilityDistance','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N31FacilityDistance','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N31FacilityDistance</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN31FacilityDistance'=>$modelN31FacilityDistance)); ?>