<?php
$this->breadcrumbs=array(
	'N31 Facility Distances',
);

$this->menu=array(
	array('label'=>'Create N31FacilityDistance','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N31FacilityDistance','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N31 Facility Distances</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
