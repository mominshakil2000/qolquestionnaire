<?php
$this->breadcrumbs=array(
	'N31 Facility Distances'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N31FacilityDistance','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N31FacilityDistance','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N31FacilityDistance','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N31FacilityDistance','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N31FacilityDistance','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N31FacilityDistance #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
