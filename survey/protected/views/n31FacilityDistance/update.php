<?php
$this->breadcrumbs=array(
	'N31 Facility Distances'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N31FacilityDistance','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N31FacilityDistance','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N31FacilityDistance','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N31FacilityDistance','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N31FacilityDistance <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>