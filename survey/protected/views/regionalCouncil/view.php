<?php
$this->breadcrumbs=array(
	'Regional Councils'=>array('index'),
	$model->uid,
);

$this->menu=array(
	// array('label'=>'List Regional Council','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Regional Council','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update Regional Council','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete Regional Council','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage Regional Council','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View RegionalCouncil #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
		'uidLabel',
	),
)); ?>
