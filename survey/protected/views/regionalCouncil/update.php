<?php
$this->breadcrumbs=array(
	'Regional Councils'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	// array('label'=>'List RegionalCouncil','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create RegionalCouncil','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	// array('label'=>'View RegionalCouncil','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage RegionalCouncil','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update Regional Council <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>