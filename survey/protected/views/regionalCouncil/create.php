<?php
$this->breadcrumbs=array(
	'Regional Councils'=>array('index'),
	'Create',
);

$this->menu=array(
	// array('label'=>'List RegionalCouncil','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage Regional Council','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create Regional Council</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelRegionalCouncil'=>$modelRegionalCouncil)); ?>