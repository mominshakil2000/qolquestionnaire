<?php
$this->breadcrumbs=array(
	'Regional Councils',
);

$this->menu=array(
	array('label'=>'Create RegionalCouncil','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage RegionalCouncil','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Regional Councils</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
