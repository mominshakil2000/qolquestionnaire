


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">RegionalCouncil</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowregionalcouncil" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="regionalcouncil-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.regionalCouncil._tabularform_additional', array('modelRegionalCouncil'=>$modelRegionalCouncil)); ?>
	</div>
</div>

<script>
	$('#regionalcouncil-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowregionalcouncil', "
	$('#btn-addrowregionalcouncil').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('regionalcouncil/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#regionalcouncil-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowregionalcouncil',"
	$('#regionalcouncil-list').on('click', '.btn-deleterowregionalcouncil', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('regionalcouncil/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#regionalcouncil-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

