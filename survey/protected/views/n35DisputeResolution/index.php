<?php
$this->breadcrumbs=array(
	'N35 Dispute Resolutions',
);

$this->menu=array(
	array('label'=>'Create N35DisputeResolution','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N35DisputeResolution','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N35 Dispute Resolutions</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
