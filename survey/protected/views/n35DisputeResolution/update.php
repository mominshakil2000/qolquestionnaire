<?php
$this->breadcrumbs=array(
	'N35 Dispute Resolutions'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N35DisputeResolution','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N35DisputeResolution','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N35DisputeResolution','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N35DisputeResolution','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N35DisputeResolution <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>