<?php
$this->breadcrumbs=array(
	'N35 Dispute Resolutions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N35DisputeResolution','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N35DisputeResolution','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N35DisputeResolution</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN35DisputeResolution'=>$modelN35DisputeResolution)); ?>