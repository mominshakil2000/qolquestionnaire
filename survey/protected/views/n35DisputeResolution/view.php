<?php
$this->breadcrumbs=array(
	'N35 Dispute Resolutions'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N35DisputeResolution','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N35DisputeResolution','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N35DisputeResolution','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N35DisputeResolution','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N35DisputeResolution','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N35DisputeResolution #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
