<?php
$this->breadcrumbs=array(
	'N46 Business Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N46BusinessType','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N46BusinessType','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N46BusinessType</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN46BusinessType'=>$modelN46BusinessType)); ?>