


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N46BusinessType</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown46businesstype" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n46businesstype-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n46BusinessType._tabularform_additional', array('modelN46BusinessType'=>$modelN46BusinessType)); ?>
	</div>
</div>

<script>
	$('#n46businesstype-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown46businesstype', "
	$('#btn-addrown46businesstype').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n46businesstype/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n46businesstype-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown46businesstype',"
	$('#n46businesstype-list').on('click', '.btn-deleterown46businesstype', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n46businesstype/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n46businesstype-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

