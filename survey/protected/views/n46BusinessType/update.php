<?php
$this->breadcrumbs=array(
	'N46 Business Types'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N46BusinessType','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N46BusinessType','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N46BusinessType','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N46BusinessType','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N46BusinessType <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>