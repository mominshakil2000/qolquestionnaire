<?php
$this->breadcrumbs=array(
	'N44 Loan Purposes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N44LoanPurpose','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N44LoanPurpose','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N44LoanPurpose</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN44LoanPurpose'=>$modelN44LoanPurpose)); ?>