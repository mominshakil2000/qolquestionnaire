<?php
$this->breadcrumbs=array(
	'N44 Loan Purposes'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N44LoanPurpose','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N44LoanPurpose','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N44LoanPurpose','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N44LoanPurpose','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N44LoanPurpose <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>