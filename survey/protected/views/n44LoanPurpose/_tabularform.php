


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N44LoanPurpose</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown44loanpurpose" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n44loanpurpose-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n44LoanPurpose._tabularform_additional', array('modelN44LoanPurpose'=>$modelN44LoanPurpose)); ?>
	</div>
</div>

<script>
	$('#n44loanpurpose-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown44loanpurpose', "
	$('#btn-addrown44loanpurpose').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n44loanpurpose/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n44loanpurpose-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown44loanpurpose',"
	$('#n44loanpurpose-list').on('click', '.btn-deleterown44loanpurpose', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n44loanpurpose/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n44loanpurpose-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

