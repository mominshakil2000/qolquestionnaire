<?php
$this->breadcrumbs=array(
	'N44 Loan Purposes'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N44LoanPurpose','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N44LoanPurpose','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N44LoanPurpose','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N44LoanPurpose','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N44LoanPurpose','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N44LoanPurpose #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
