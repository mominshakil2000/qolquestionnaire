<?php
$this->breadcrumbs=array(
	'N44 Loan Purposes',
);

$this->menu=array(
	array('label'=>'Create N44LoanPurpose','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N44LoanPurpose','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N44 Loan Purposes</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
