<?php
$this->breadcrumbs=array(
	'Family Members'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FamilyMember','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FamilyMember','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FamilyMember</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFamilyMember'=>$modelFamilyMember)); ?>