


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">For all residing family members (Include migrated / internally displaced members and exclude separated families)  </b>
			<div class="col-xs-6 text-right">
				<a id="btn-saverowsfamilymember" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-floppy-disk"></i> Save All Items</a>			
				<a id="btn-addrowfamilymember" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="familymember-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.familyMember._tabularform_additional', array('modelFamilyMember'=>$modelFamilyMember)); ?>
	</div>
</div>

<script>
	$('#familymember-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowfamilymember', "
	$('#btn-addrowfamilymember').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMember/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#familymember-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>
<?php
Yii::app()->clientScript->registerScript('saverowsfamilymember', "
	$('#btn-saverowsfamilymember').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMember/formsaverows') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#familymember-list').html(data);
			},
			error: function() {
				alert('An error has occured while saving.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowfamilymember',"
	$('#familymember-list').on('click', '.btn-deleterowfamilymember', function(){
		rowIndex = $(this).closest('td').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMember/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#familymember-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

