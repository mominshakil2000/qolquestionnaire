<?php 
	$tabLimit = 10;
	$itemCount = intval(count($modelFamilyMember)/$tabLimit); 
	echo 'Items=',count($modelFamilyMember);
	if(count($modelFamilyMember) % $tabLimit > 0)
		$itemCount ++;
	$itemCount --;

?>
<ul class="nav nav-tabs" role="tablist" id="modelFamilyMemberTab">
	<?php for($i=0; $i <= $itemCount; $i++) { ?>
		<li role="presentation" class="<?php echo ($i==1 ? 'active': '');?>"><a href="<?php echo '#tab-', $i; ?>" aria-controls="<?php echo 'tab-', $i; ?>" role="tab" data-toggle="tab"><?php echo $i*$tabLimit + 1, ' - ', ($i+1)*$tabLimit; ?></a></li>
	<?php } ?>
</ul>
<div class="tab-content">
	<?php for($i=0; $i <= $itemCount; $i++) { 
			$startIndex = $i*$tabLimit;
			$endIndex = ($i+1)*$tabLimit;
			if($endIndex > count($modelFamilyMember))
				$endIndex = count($modelFamilyMember) ;
	?>
	<div role="tabpanel" class="tab-pane active" id="<?php echo 'tab-', $i; ?>">
		<table class="table">
			<tbody>
				<tr>
					<td class="col-sm-1">&nbsp;</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<?php echo CHtml::activeHiddenField($modelFamilyMember[$j],"[$j]uid"); ?>	
						<td rowIndex="<?php echo $j; ?>"><a class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="btn-deleterowfamilymember glyphicon glyphicon-remove"></i></a>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>

				<tr> 
					<td class="col-sm-1">First Name <span class="required">*</span></td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::activeTextField($modelFamilyMember[$j],"[$j]firstName",array('class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'firstName'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>

				<tr>	
					<td class="col-sm-1">Relationship with Head of Family <span class="required">*</span></td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][relationshipWithHeadUid]]", $modelFamilyMember[$j]->relationshipWithHeadUid, CHtml::listData(N1FamilyRelationship::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'relationshipWithHeadUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>

				<tr>
					<td class="col-sm-1">Marital Status <span class="required">*</span></td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td>
							<?php echo CHtml::dropDownList("FamilyMember[$j][martialStatusUid]]", $modelFamilyMember[$j]->martialStatusUid, CHtml::listData(N2MartialStauts::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'martialStatusUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>

				<tr>
					<td class="col-sm-1">Gender <span class="required">*</span></td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td>
							<?php echo CHtml::dropDownList("FamilyMember[$j][genderUid]]", $modelFamilyMember[$j]->genderUid, CHtml::listData(Gender::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'genderUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Current Age <span class="required">*</span></td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::activeTextField($modelFamilyMember[$j],"[$j]birthYear",array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'birthYear'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>"><h4>REC</h4></td>
				</tr>
				<tr>
					<td class="col-sm-1">REC Status?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td>
							<?php echo CHtml::dropDownList("FamilyMember[$j][recEducationStatusUid]]", $modelFamilyMember[$j]->recEducationStatusUid, CHtml::listData(N3EducationStatus::model()->findAll(), 'uid', 'uidLabel'), array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'recEducationStatusUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1">Current Class</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][recCurrentGradeUid]]", $modelFamilyMember[$j]->recCurrentGradeUid, CHtml::listData(N4RecQualification::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'recCurrentGradeUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>If Not enrolled, completed studies or Dropout</b></td>	
				</tr>	
				<tr>
					<td class="col-sm-1"> &nbsp; - Last Class Completed / highest qualification</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][recLastGradeUid]]", $modelFamilyMember[$j]->recLastGradeUid, CHtml::listData(N4RecQualification::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'recLastGradeUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Major reason for <b>NOT</b> being Enrolled now</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][recEducationStatusReasonUid]]", $modelFamilyMember[$j]->recEducationStatusReasonUid, CHtml::listData(N5EducationStatusReason::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'recEducationStatusReasonUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>"><h4>Faith</h4></td>	
				</tr>	
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>Are you benefiting from Jamati Programme</b></td>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Jamati Institutions</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][isBenefitingJamatiInstitution]]", $modelFamilyMember[$j]->isBenefitingJamatiInstitution, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'isBenefitingJamatiInstitution'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - ITREB,P / REC</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][isBenefitingItreb]]", $modelFamilyMember[$j]->isBenefitingItreb, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'isBenefitingItreb'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1">How often do you attend evening Jamatkhana? (including any voluntary duty)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][eveningJkAttendenceUid]]", $modelFamilyMember[$j]->eveningJkAttendenceUid, CHtml::listData(N6JamatKhanaAttendence::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'eveningJkAttendenceUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">If not attending evening Jamat khana for at least 2 days a week, reason</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][eveningJkNotAttendenceReasonUid]]", $modelFamilyMember[$j]->eveningJkNotAttendenceReasonUid, CHtml::listData(N7JamatKhanaAttendenceReason::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'eveningJkNotAttendenceReasonUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1">How often do you attend morning</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][morningJkAttendenceUid]]", $modelFamilyMember[$j]->morningJkAttendenceUid, CHtml::listData(N6JamatKhanaAttendence::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'morningJkAttendenceUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1">If not attending morning  Jamat khana for atleast (2 days a week), reason</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][morningJkNotAttendenceReasonUid]]", $modelFamilyMember[$j]->morningJkNotAttendenceReasonUid, CHtml::listData(N7JamatKhanaAttendenceReason::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'morningJkNotAttendenceReasonUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>"><h4>Secular Education</h4></td>	
				</tr>
				<tr>
					<td class="col-sm-1">Secular Education status ?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularEducationStatusUid]]", $modelFamilyMember[$j]->secularEducationStatusUid, CHtml::listData(N3EducationStatus::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularEducationStatusUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
					
				<tr>
					<td class="col-sm-1">Current Class / qualification</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularCurrentGradeUid]]", $modelFamilyMember[$j]->secularCurrentGradeUid, CHtml::listData(N8SecularQualification::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularCurrentGradeUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
					
				<tr>
					<td class="col-sm-1">Are you Satisfied with current education institute </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularEducationIsSatisfy]]", $modelFamilyMember[$j]->secularEducationIsSatisfy, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularEducationIsSatisfy'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Last Class Completed / highest qualification </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularLastGradeUid]", $modelFamilyMember[$j]->secularLastGradeUid, CHtml::listData(N8SecularQualification::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularLastGradeUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>If Not enrolled, completed studies or Dropout</b></td>	
				</tr>	
				<tr>
					<td class="col-sm-1"> &nbsp; - Major reason for NOT being Enrolled now</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularNotEnrolledReasonUid]", $modelFamilyMember[$j]->secularNotEnrolledReasonUid, CHtml::listData(N5EducationStatusReason::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularNotEnrolledReasonUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Dropout year or (N/A)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::activeTextField($modelFamilyMember[$j],"[$j]secularDropoutYear",array('class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularDropoutYear'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Category of Current / last school attended (upto 10 class)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularUpto10InstitueCategoryUid]", $modelFamilyMember[$j]->secularUpto10InstitueCategoryUid, CHtml::listData(N9EducationInstituteCategory::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularUpto10InstitueCategoryUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Category of College / university attended / attending </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularEducationGroupUid]", $modelFamilyMember[$j]->secularEducationGroupUid, CHtml::listData(N9EducationInstituteCategory::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularEducationGroupUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Current / Last Group (class 9 & above)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][secularAbove10InstitueCategoryUid]", $modelFamilyMember[$j]->secularAbove10InstitueCategoryUid, CHtml::listData(N10EducationGroup::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'secularAbove10InstitueCategoryUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>Language Proficiency</b></td>	
				</tr>		
				<tr>
					<td class="col-sm-1"> &nbsp; - English Writing</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][englishProficiencyWritingUid]", $modelFamilyMember[$j]->englishProficiencyWritingUid, CHtml::listData(N11Proficiency::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'englishProficiencyWritingUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
					
				<tr>
					<td class="col-sm-1"> &nbsp; - English Reading</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][englishProficiencyReadingUid]", $modelFamilyMember[$j]->englishProficiencyReadingUid, CHtml::listData(N11Proficiency::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'englishProficiencyReadingUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - English Speaking</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][englishProficiencySpeakingUid]", $modelFamilyMember[$j]->englishProficiencySpeakingUid, CHtml::listData(N11Proficiency::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'englishProficiencySpeakingUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>IT Proficiency</b></td>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Office Suite (Word, excel, PPT)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][itProficiencyOfficeSuitUid]", $modelFamilyMember[$j]->itProficiencyOfficeSuitUid, CHtml::listData(N11Proficiency::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'itProficiencyOfficeSuitUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
					
				<tr>
					<td class="col-sm-1"> &nbsp; - Internet Browsing </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][itProficiencyInternetUid]", $modelFamilyMember[$j]->itProficiencyInternetUid, CHtml::listData(N11Proficiency::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'itProficiencyInternetUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>"><h4>Occupation</h4></td>	
				</tr>	
				<tr>
					<td class="col-sm-1">Occupation</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][occupationUid]", $modelFamilyMember[$j]->occupationUid, CHtml::listData(N12OccupationCategory::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'occupationUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1">Current monthly Income</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][monthlyIncomeUid]", $modelFamilyMember[$j]->monthlyIncomeUid, CHtml::listData(N13Income::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'monthlyIncomeUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Unemployed since</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][unemployedSinceUid]", $modelFamilyMember[$j]->unemployedSinceUid, CHtml::listData(N14UnemployedFrom::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'unemployedSinceUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">If unemployed, specify major reason</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][unemployedReasonUid]", $modelFamilyMember[$j]->unemployedReasonUid, CHtml::listData(N15UnemploymentReason::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'unemployedReasonUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>

				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>"><h4>Insurance</h4></td>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>Are you insured for the following (Including reimbursements)? [Self]</b></td>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Health</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredSelfHealth]", $modelFamilyMember[$j]->insuredSelfHealth, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredSelfHealth'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Life</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredSelfLife]", $modelFamilyMember[$j]->insuredSelfLife, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredSelfLife'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Live stock/ Assets / Crops</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredSelfAsset]", $modelFamilyMember[$j]->insuredSelfAsset, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredSelfAsset'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Vehicle</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredSelfVehicle]", $modelFamilyMember[$j]->insuredSelfVehicle, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredSelfVehicle'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Business</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredSelfBusiness]", $modelFamilyMember[$j]->insuredSelfBusiness, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredSelfBusiness'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Education</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredSelfEducation]", $modelFamilyMember[$j]->insuredSelfEducation, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredSelfEducation'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>Are you insured for the following (Including reimbursements)? [Employer]</b></td>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Life Insurance</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredEmployerLife]", $modelFamilyMember[$j]->insuredEmployerLife, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredEmployerLife'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Health Insurance</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][insuredEmployerHealth]", $modelFamilyMember[$j]->insuredEmployerHealth, CHtml::listData(Lookup::yesNoNa(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'insuredEmployerHealth'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>

				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>">
						<h4>Health</h4>
					</td>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>Are you having following health related issues?</b></td>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Diabetes</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusDiabetesUid]", $modelFamilyMember[$j]->healthIssueStatusDiabetesUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusDiabetesUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Arthritis (Joint pain)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueArthritisUid]", $modelFamilyMember[$j]->healthIssueArthritisUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueArthritisUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Cancer</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusCancerUid]", $modelFamilyMember[$j]->healthIssueStatusCancerUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusCancerUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - High blood pressure</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusBpUid]", $modelFamilyMember[$j]->healthIssueStatusBpUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusBpUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Heart related diseases</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusHeartUid]", $modelFamilyMember[$j]->healthIssueStatusHeartUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusHeartUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Mental Illness (depression etc.)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusMentalIllnessUid]", $modelFamilyMember[$j]->healthIssueStatusMentalIllnessUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusMentalIllnessUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Tuberculosis</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusTuberculosisUid]", $modelFamilyMember[$j]->healthIssueStatusTuberculosisUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusTuberculosisUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Kidney related</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusKidneyUid]", $modelFamilyMember[$j]->healthIssueStatusKidneyUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusKidneyUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
					
				<tr>
					<td class="col-sm-1"> &nbsp; - Stroke (Falij)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusStrokeUid]", $modelFamilyMember[$j]->healthIssueStatusStrokeUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusStrokeUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Hepatitis B & C</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusHepatitis]", $modelFamilyMember[$j]->healthIssueStatusHepatitis, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusHepatitis'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Asthma</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusAsthmaUid]", $modelFamilyMember[$j]->healthIssueStatusAsthmaUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusAsthmaUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Others</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueStatusOthersUid]", $modelFamilyMember[$j]->healthIssueStatusOthersUid, CHtml::listData(Lookup::yesNoNi(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueStatusOthersUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Do you have any disability?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueDisabilityUid]", $modelFamilyMember[$j]->healthIssueDisabilityUid, CHtml::listData(N16Disability::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueDisabilityUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">How frequently do you fall ill / unhealthy</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueIllFrequencyUid]", $modelFamilyMember[$j]->healthIssueIllFrequencyUid, CHtml::listData(N17IllnessFrequency::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueIllFrequencyUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Do you visit any health facilities during illness? (If no, reason)</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][healthIssueNoHealthFacilityReasonUid]", $modelFamilyMember[$j]->healthIssueNoHealthFacilityReasonUid, CHtml::listData(N18HealthyFacilityAccessibility::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'healthIssueNoHealthFacilityReasonUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>">
						<h4>Nutrition</h4>
					</td>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>Consumption frequency of the following</b></td>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Red Meat or Chicken</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][nutritionConsumptionFrequencyMeatUid]", $modelFamilyMember[$j]->nutritionConsumptionFrequencyMeatUid, CHtml::listData(N19NutritionConsumption::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'nutritionConsumptionFrequencyMeatUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Fruits</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][nutritionConsumptionFrequencyFruitUid]", $modelFamilyMember[$j]->nutritionConsumptionFrequencyFruitUid, CHtml::listData(N19NutritionConsumption::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'nutritionConsumptionFrequencyFruitUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Milk / Milk products</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][nutritionConsumptionFrequencyMilkUid]", $modelFamilyMember[$j]->nutritionConsumptionFrequencyMilkUid, CHtml::listData(N19NutritionConsumption::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'nutritionConsumptionFrequencyMilkUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>">
						<h4>Substance abuse</h4>
					</td>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>Do you consume the following?</b></td>	
				</tr>
					
				<tr>
					<td class="col-sm-1"> &nbsp; - Alcohol / Mo?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][substanceAbuseAlcoholUid]", $modelFamilyMember[$j]->substanceAbuseAlcoholUid, CHtml::listData(N20SubstanceAbuse::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'substanceAbuseAlcoholUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Chewing tobacco/Gutka / Naswar?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][substanceAbuseTobaccoUid]", $modelFamilyMember[$j]->substanceAbuseTobaccoUid, CHtml::listData(N20SubstanceAbuse::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'substanceAbuseTobaccoUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Cigarettes / Shisha?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][substanceAbuseCigrateUid]", $modelFamilyMember[$j]->substanceAbuseCigrateUid, CHtml::listData(N20SubstanceAbuse::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'substanceAbuseCigrateUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1"> &nbsp; - Illicit Drugs</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][substanceAbuseIllicitDrugUid]", $modelFamilyMember[$j]->substanceAbuseIllicitDrugUid, CHtml::listData(N20SubstanceAbuse::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'substanceAbuseIllicitDrugUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Pan</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][substanceAbusePanUid]", $modelFamilyMember[$j]->substanceAbusePanUid, CHtml::listData(N20SubstanceAbuse::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'substanceAbusePanUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="text-center" colspan="<?php echo count($modelFamilyMember)+1;?>">
						<h4>Social Activity</h4> 
					</td>	
				</tr>
				<tr>
					<td colspan="<?php echo count($modelFamilyMember)+1;?>"><b>How much time do you spend in the following per week</b></td>	
				</tr>	
				<tr>
					<td class="col-sm-1">&nbsp; - Social Services / Khidmat</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][socialKhidmatUid]", $modelFamilyMember[$j]->socialKhidmatUid, CHtml::listData(N21YouthActivityAccessibility::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'socialKhidmatUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">&nbsp; - Sports (During Holidays) </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][socialSportsHolidayUid]", $modelFamilyMember[$j]->socialSportsHolidayUid, CHtml::listData(N21YouthActivityAccessibility::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'socialSportsHolidayUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">&nbsp; - Sports (During working days) </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][socialSportsWorkingDayUid]", $modelFamilyMember[$j]->socialSportsWorkingDayUid, CHtml::listData(N21YouthActivityAccessibility::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'socialSportsWorkingDayUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">&nbsp; - Physical exercise </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][socialExcerciseUid]", $modelFamilyMember[$j]->socialExcerciseUid, CHtml::listData(N21YouthActivityAccessibility::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'socialExcerciseUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1"> &nbsp; - Recreational activities </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][socialRecreationActivityUid]", $modelFamilyMember[$j]->socialRecreationActivityUid, CHtml::listData(N21YouthActivityAccessibility::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'socialRecreationActivityUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
					
				<tr>
					<td class="col-sm-1">Do you feel empowered to make your own decisions?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][takSelfDecisionMaking]", $modelFamilyMember[$j]->takSelfDecisionMaking, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'takSelfDecisionMaking'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">How much time does each individual have for leisure?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][leisureTimeUid]", $modelFamilyMember[$j]->leisureTimeUid, CHtml::listData(N22Leisure::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'leisureTimeUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				<tr>
					<td class="col-sm-1">How would individuals in your family rate their Quality of Life?</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][qolUid]", $modelFamilyMember[$j]->qolUid, CHtml::listData(N23QualityLife::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'qolUid'); ?>
						</td>
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">How has the QoL for each individual changed in the past 3 years</td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][qolChangePast3YearsUid]", $modelFamilyMember[$j]->qolChangePast3YearsUid, CHtml::listData(N24QualityLifeStatus::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'qolChangePast3YearsUid'); ?>
						</td>	
					<?php } ?>	
					<?php } ?>	
				</tr>
				
				<tr>
					<td class="col-sm-1">Major factor contributing towards change in status of QOL </td>
					<?php if(is_array($modelFamilyMember)) { ?>
					<?php for($j=$startIndex; $j < $endIndex; $j++){ ?>
						<td><?php echo CHtml::dropDownList("FamilyMember[$j][qolChangeStatusUid]", $modelFamilyMember[$j]->qolChangeStatusUid, CHtml::listData(N25QualityLifeChangeReason::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
							<?php echo CHtml::error($modelFamilyMember[$j], 'qolChangeStatusUid'); ?>
						</td>
					<?php } ?>	
					<?php } ?>
				</tr>
			<tbody>
		</table>
	</div>
	<?php } ?>
</div>

<script>
  $(function () {
    $('#modelFamilyMemberTab a:first').tab('show')
  })
</script>