<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid),array('view','id'=>$data->uid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('firstName')); ?>:</b>
	<?php echo CHtml::encode($data->firstName); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('relationshipWithHeadUid')); ?>:</b>
	<?php echo CHtml::encode($data->relationshipWithHeadUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('martialStatusUid')); ?>:</b>
	<?php echo CHtml::encode($data->martialStatusUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('genderUid')); ?>:</b>
	<?php echo CHtml::encode($data->genderUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('birthYear')); ?>:</b>
	<?php echo CHtml::encode($data->birthYear); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recEducationStatusUid')); ?>:</b>
	<?php echo CHtml::encode($data->recEducationStatusUid); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('recCurrentGradeUid')); ?>:</b>
	<?php echo CHtml::encode($data->recCurrentGradeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recLastGradeUid')); ?>:</b>
	<?php echo CHtml::encode($data->recLastGradeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('recEducationStatusReasonUid')); ?>:</b>
	<?php echo CHtml::encode($data->recEducationStatusReasonUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isBenefitingJamatiInstitution')); ?>:</b>
	<?php echo CHtml::encode($data->isBenefitingJamatiInstitution); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('isBenefitingItreb')); ?>:</b>
	<?php echo CHtml::encode($data->isBenefitingItreb); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eveningJkAttendenceUid')); ?>:</b>
	<?php echo CHtml::encode($data->eveningJkAttendenceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('eveningJkNotAttendenceReasonUid')); ?>:</b>
	<?php echo CHtml::encode($data->eveningJkNotAttendenceReasonUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('morningJkAttendenceUid')); ?>:</b>
	<?php echo CHtml::encode($data->morningJkAttendenceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('morningJkNotAttendenceReasonUid')); ?>:</b>
	<?php echo CHtml::encode($data->morningJkNotAttendenceReasonUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularEducationStatusUid')); ?>:</b>
	<?php echo CHtml::encode($data->secularEducationStatusUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularCurrentGradeUid')); ?>:</b>
	<?php echo CHtml::encode($data->secularCurrentGradeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularEducationIsSatisfy')); ?>:</b>
	<?php echo CHtml::encode($data->secularEducationIsSatisfy); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularLastGradeUid')); ?>:</b>
	<?php echo CHtml::encode($data->secularLastGradeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularNotEnrolledReasonUid')); ?>:</b>
	<?php echo CHtml::encode($data->secularNotEnrolledReasonUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularDropoutYear')); ?>:</b>
	<?php echo CHtml::encode($data->secularDropoutYear); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularUpto10InstitueCategoryUid')); ?>:</b>
	<?php echo CHtml::encode($data->secularUpto10InstitueCategoryUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularAbove10InstitueCategoryUid')); ?>:</b>
	<?php echo CHtml::encode($data->secularAbove10InstitueCategoryUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('secularEducationGroupUid')); ?>:</b>
	<?php echo CHtml::encode($data->secularEducationGroupUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('englishProficiencyReadingUid')); ?>:</b>
	<?php echo CHtml::encode($data->englishProficiencyReadingUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('englishProficiencyWritingUid')); ?>:</b>
	<?php echo CHtml::encode($data->englishProficiencyWritingUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('englishProficiencySpeakingUid')); ?>:</b>
	<?php echo CHtml::encode($data->englishProficiencySpeakingUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itProficiencyOfficeSuitUid')); ?>:</b>
	<?php echo CHtml::encode($data->itProficiencyOfficeSuitUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('itProficiencyInternetUid')); ?>:</b>
	<?php echo CHtml::encode($data->itProficiencyInternetUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('occupationUid')); ?>:</b>
	<?php echo CHtml::encode($data->occupationUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('monthlyIncomeUid')); ?>:</b>
	<?php echo CHtml::encode($data->monthlyIncomeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unemployedSinceUid')); ?>:</b>
	<?php echo CHtml::encode($data->unemployedSinceUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('unemployedReasonUid')); ?>:</b>
	<?php echo CHtml::encode($data->unemployedReasonUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredSelfHealth')); ?>:</b>
	<?php echo CHtml::encode($data->insuredSelfHealth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredSelfLife')); ?>:</b>
	<?php echo CHtml::encode($data->insuredSelfLife); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredSelfAsset')); ?>:</b>
	<?php echo CHtml::encode($data->insuredSelfAsset); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredSelfVehicle')); ?>:</b>
	<?php echo CHtml::encode($data->insuredSelfVehicle); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredSelfBusiness')); ?>:</b>
	<?php echo CHtml::encode($data->insuredSelfBusiness); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredSelfEducation')); ?>:</b>
	<?php echo CHtml::encode($data->insuredSelfEducation); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredEmployerLife')); ?>:</b>
	<?php echo CHtml::encode($data->insuredEmployerLife); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('insuredEmployerHealth')); ?>:</b>
	<?php echo CHtml::encode($data->insuredEmployerHealth); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueArthritisUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueArthritisUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusCancerUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusCancerUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusBpUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusBpUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusHeartUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusHeartUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusTuberculosisUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusTuberculosisUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusKidneyUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusKidneyUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusStrokeUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusStrokeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusHepatitis')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusHepatitis); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusAsthmaUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusAsthmaUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueStatusOthersUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueStatusOthersUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueDisabilityUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueDisabilityUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueIllFrequencyUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueIllFrequencyUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('healthIssueNoHealthFacilityReasonUid')); ?>:</b>
	<?php echo CHtml::encode($data->healthIssueNoHealthFacilityReasonUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nutritionConsumptionFrequencyMeatUid')); ?>:</b>
	<?php echo CHtml::encode($data->nutritionConsumptionFrequencyMeatUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nutritionConsumptionFrequencyFruitUid')); ?>:</b>
	<?php echo CHtml::encode($data->nutritionConsumptionFrequencyFruitUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nutritionConsumptionFrequencyMilkUid')); ?>:</b>
	<?php echo CHtml::encode($data->nutritionConsumptionFrequencyMilkUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('substanceAbuseAlcoholUid')); ?>:</b>
	<?php echo CHtml::encode($data->substanceAbuseAlcoholUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('substanceAbuseTobaccoUid')); ?>:</b>
	<?php echo CHtml::encode($data->substanceAbuseTobaccoUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('substanceAbuseIllicitDrugUid')); ?>:</b>
	<?php echo CHtml::encode($data->substanceAbuseIllicitDrugUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('substanceAbusePanUid')); ?>:</b>
	<?php echo CHtml::encode($data->substanceAbusePanUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('socialKhidmatUid')); ?>:</b>
	<?php echo CHtml::encode($data->socialKhidmatUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('socialSportsHolidayUid')); ?>:</b>
	<?php echo CHtml::encode($data->socialSportsHolidayUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('socialSportsWorkingDayUid')); ?>:</b>
	<?php echo CHtml::encode($data->socialSportsWorkingDayUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('socialExcerciseUid')); ?>:</b>
	<?php echo CHtml::encode($data->socialExcerciseUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('socialRecreationActivityUid')); ?>:</b>
	<?php echo CHtml::encode($data->socialRecreationActivityUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('takSelfDecisionMaking')); ?>:</b>
	<?php echo CHtml::encode($data->takSelfDecisionMaking); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('leisureTimeUid')); ?>:</b>
	<?php echo CHtml::encode($data->leisureTimeUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qolUid')); ?>:</b>
	<?php echo CHtml::encode($data->qolUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qolChangePast3YearsUid')); ?>:</b>
	<?php echo CHtml::encode($data->qolChangePast3YearsUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('qolChangeStatusUid')); ?>:</b>
	<?php echo CHtml::encode($data->qolChangeStatusUid); ?>
	<br />

	*/ ?>

</div>