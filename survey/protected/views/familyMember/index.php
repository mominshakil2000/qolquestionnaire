<?php
$this->breadcrumbs=array(
	'Family Members',
);

$this->menu=array(
	array('label'=>'Create FamilyMember','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage FamilyMember','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Family Members</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
