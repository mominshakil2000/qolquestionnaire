<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'family-member-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model,'uid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'firstName',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
<?php echo $form->textFieldRow($model,'relationshipWithHeadUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
<?php echo $form->textFieldRow($model,'martialStatusUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->textFieldRow($model,'genderUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'birthYear',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
<?php echo $form->textFieldRow($model,'recEducationStatusUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'recCurrentGradeUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'recLastGradeUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'recEducationStatusReasonUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'isBenefitingJamatiInstitution',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'isBenefitingItreb',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'eveningJkAttendenceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'eveningJkNotAttendenceReasonUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'morningJkAttendenceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'morningJkNotAttendenceReasonUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'secularEducationStatusUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'secularCurrentGradeUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'secularEducationIsSatisfy',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'secularLastGradeUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'secularNotEnrolledReasonUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'secularDropoutYear',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
<?php echo $form->textFieldRow($model,'secularUpto10InstitueCategoryUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'secularAbove10InstitueCategoryUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'secularEducationGroupUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'englishProficiencyReadingUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'englishProficiencyWritingUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'englishProficiencySpeakingUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'itProficiencyOfficeSuitUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'itProficiencyInternetUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'occupationUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'monthlyIncomeUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'unemployedSinceUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'unemployedReasonUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'insuredSelfHealth',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'insuredSelfLife',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'insuredSelfAsset',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'insuredSelfVehicle',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'insuredSelfBusiness',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'insuredSelfEducation',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'insuredEmployerLife',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'insuredEmployerHealth',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'healthIssueArthritisUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusCancerUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusBpUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusHeartUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusTuberculosisUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusKidneyUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusStrokeUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusHepatitis',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusAsthmaUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueStatusOthersUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueDisabilityUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueIllFrequencyUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'healthIssueNoHealthFacilityReasonUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'nutritionConsumptionFrequencyMeatUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'nutritionConsumptionFrequencyFruitUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'nutritionConsumptionFrequencyMilkUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'substanceAbuseAlcoholUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'substanceAbuseTobaccoUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'substanceAbuseIllicitDrugUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'substanceAbusePanUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'socialKhidmatUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'socialSportsHolidayUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'socialSportsWorkingDayUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'socialExcerciseUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'socialRecreationActivityUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'takSelfDecisionMaking',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'leisureTimeUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'qolUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'qolChangePast3YearsUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'qolChangeStatusUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $this->renderPartial('application.views.familyMember._tabularform', array('form'=>$form, 'modelFamilyMember'=>$modelFamilyMember)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
