<?php
$this->breadcrumbs=array(
	'N15 Unemployment Reasons'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N15UnemploymentReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N15UnemploymentReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N15UnemploymentReason','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N15UnemploymentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N15UnemploymentReason <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>