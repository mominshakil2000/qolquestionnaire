<?php
$this->breadcrumbs=array(
	'N15 Unemployment Reasons',
);

$this->menu=array(
	array('label'=>'Create N15UnemploymentReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N15UnemploymentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N15 Unemployment Reasons</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
