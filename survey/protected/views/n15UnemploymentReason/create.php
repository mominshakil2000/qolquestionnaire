<?php
$this->breadcrumbs=array(
	'N15 Unemployment Reasons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N15UnemploymentReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N15UnemploymentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N15UnemploymentReason</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN15UnemploymentReason'=>$modelN15UnemploymentReason)); ?>