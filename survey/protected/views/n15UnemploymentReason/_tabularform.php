


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N15UnemploymentReason</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown15unemploymentreason" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n15unemploymentreason-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n15UnemploymentReason._tabularform_additional', array('modelN15UnemploymentReason'=>$modelN15UnemploymentReason)); ?>
	</div>
</div>

<script>
	$('#n15unemploymentreason-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown15unemploymentreason', "
	$('#btn-addrown15unemploymentreason').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n15unemploymentreason/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n15unemploymentreason-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown15unemploymentreason',"
	$('#n15unemploymentreason-list').on('click', '.btn-deleterown15unemploymentreason', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n15unemploymentreason/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n15unemploymentreason-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

