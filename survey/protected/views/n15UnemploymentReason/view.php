<?php
$this->breadcrumbs=array(
	'N15 Unemployment Reasons'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N15UnemploymentReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N15UnemploymentReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N15UnemploymentReason','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N15UnemploymentReason','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N15UnemploymentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N15UnemploymentReason #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
