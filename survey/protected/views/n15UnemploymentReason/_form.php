<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'n15-unemployment-reason-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model,'uid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->textFieldRow($model,'label',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
<?php echo $this->renderPartial('application.views.n15UnemploymentReason._tabularform', array('form'=>$form, 'modelN15UnemploymentReason'=>$modelN15UnemploymentReason)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
