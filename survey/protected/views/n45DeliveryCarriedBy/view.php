<?php
$this->breadcrumbs=array(
	'N45 Delivery Carried Bies'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N45DeliveryCarriedBy','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N45DeliveryCarriedBy','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N45DeliveryCarriedBy','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N45DeliveryCarriedBy','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N45DeliveryCarriedBy','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N45DeliveryCarriedBy #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
