<?php
$this->breadcrumbs=array(
	'N45 Delivery Carried Bies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N45DeliveryCarriedBy','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N45DeliveryCarriedBy','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N45DeliveryCarriedBy</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN45DeliveryCarriedBy'=>$modelN45DeliveryCarriedBy)); ?>