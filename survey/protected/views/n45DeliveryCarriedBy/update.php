<?php
$this->breadcrumbs=array(
	'N45 Delivery Carried Bies'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N45DeliveryCarriedBy','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N45DeliveryCarriedBy','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N45DeliveryCarriedBy','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N45DeliveryCarriedBy','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N45DeliveryCarriedBy <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>