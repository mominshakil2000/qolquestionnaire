<?php
$this->breadcrumbs=array(
	'N45 Delivery Carried Bies',
);

$this->menu=array(
	array('label'=>'Create N45DeliveryCarriedBy','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N45DeliveryCarriedBy','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N45 Delivery Carried Bies</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
