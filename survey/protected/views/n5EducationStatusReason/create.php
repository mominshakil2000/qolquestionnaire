<?php
$this->breadcrumbs=array(
	'N5 Education Status Reasons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N5EducationStatusReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N5EducationStatusReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N5EducationStatusReason</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN5EducationStatusReason'=>$modelN5EducationStatusReason)); ?>