<?php
$this->breadcrumbs=array(
	'N5 Education Status Reasons'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N5EducationStatusReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N5EducationStatusReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N5EducationStatusReason','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N5EducationStatusReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N5EducationStatusReason <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>