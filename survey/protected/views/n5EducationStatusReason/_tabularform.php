


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N5EducationStatusReason</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown5educationstatusreason" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n5educationstatusreason-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n5EducationStatusReason._tabularform_additional', array('modelN5EducationStatusReason'=>$modelN5EducationStatusReason)); ?>
	</div>
</div>

<script>
	$('#n5educationstatusreason-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown5educationstatusreason', "
	$('#btn-addrown5educationstatusreason').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n5educationstatusreason/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n5educationstatusreason-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown5educationstatusreason',"
	$('#n5educationstatusreason-list').on('click', '.btn-deleterown5educationstatusreason', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n5educationstatusreason/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n5educationstatusreason-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

