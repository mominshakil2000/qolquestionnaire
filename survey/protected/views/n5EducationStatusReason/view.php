<?php
$this->breadcrumbs=array(
	'N5 Education Status Reasons'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N5EducationStatusReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N5EducationStatusReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N5EducationStatusReason','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N5EducationStatusReason','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N5EducationStatusReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N5EducationStatusReason #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
