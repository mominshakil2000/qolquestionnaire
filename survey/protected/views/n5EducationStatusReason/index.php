<?php
$this->breadcrumbs=array(
	'N5 Education Status Reasons',
);

$this->menu=array(
	array('label'=>'Create N5EducationStatusReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N5EducationStatusReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N5 Education Status Reasons</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
