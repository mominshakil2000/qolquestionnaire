<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid),array('view','id'=>$data->uid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('householdUid')); ?>:</b>
	<?php echo CHtml::encode($data->householdUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('livingOtherUid')); ?>:</b>
	<?php echo CHtml::encode($data->livingOtherUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('involveSocialOrEconomicActivities')); ?>:</b>
	<?php echo CHtml::encode($data->involveSocialOrEconomicActivities); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('notInvolveSocialOrEconomicActivitiesUid')); ?>:</b>
	<?php echo CHtml::encode($data->notInvolveSocialOrEconomicActivitiesUid); ?>
	<br />


</div>