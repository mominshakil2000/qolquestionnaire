<?php
$this->breadcrumbs=array(
	'Family Member Seniors'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List FamilyMemberSeniors','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberSeniors','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View FamilyMemberSeniors','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage FamilyMemberSeniors','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update FamilyMemberSeniors <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>