<?php
$this->breadcrumbs=array(
	'Family Member Seniors'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FamilyMemberSeniors','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FamilyMemberSeniors','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FamilyMemberSeniors</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFamilyMemberSeniors'=>$modelFamilyMemberSeniors)); ?>