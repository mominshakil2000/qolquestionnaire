<?php
$this->breadcrumbs=array(
	'Family Member Seniors'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List FamilyMemberSeniors','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberSeniors','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update FamilyMemberSeniors','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete FamilyMemberSeniors','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage FamilyMemberSeniors','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View FamilyMemberSeniors #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'householdUid',
		'livingOtherUid',
		'involveSocialOrEconomicActivities',
		'notInvolveSocialOrEconomicActivitiesUid',
	),
)); ?>
