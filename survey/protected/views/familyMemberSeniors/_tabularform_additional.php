
<table class="table">
<thead>
	<tr>
		<td class="col-sm-1">&nbsp;</td>
		<td class="col-sm-1">Living with in Home / Others</td>
		<td class="col-sm-1">Senior living with others</td>
		<td class="col-sm-2">Are seniors actively involved in social and economic activities like senior citizen get-together, Social get together etc.?</td>
		<td class="col-sm-3">If No </td>
	</tr>
</thead>
<tbody>
	<?php foreach($modelFamilyMemberSeniors as $i=>$item): ?>
	<tr rowIndex="<?php echo $i; ?>">
		<?php echo CHtml::activeHiddenField($item,"[$i]uid"); ?>	
		<td><a class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="btn-deleterowfamilymemberseniors glyphicon glyphicon-remove"></i></a></td>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberSeniors[$i][livingWithUid]", $item->livingWithUid, CHtml::listData(Lookup::seniorsLivingWith(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
			<?php echo CHtml::error($item, 'livingWithUid'); ?>
		</td>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberSeniors[$i][livingOtherUid]", $item->livingOtherUid, CHtml::listData(N36SeniorsLiving::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
			<?php echo CHtml::error($item, 'livingOtherUid'); ?>
		</td>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberSeniors[$i][involveSocialOrEconomicActivities]", $item->involveSocialOrEconomicActivities, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
			<?php echo CHtml::error($item, 'involveSocialOrEconomicActivities'); ?>
		</td>	
		<td>
			<?php echo CHtml::dropDownList("FamilyMemberSeniors[$i][notInvolveSocialOrEconomicActivitiesUid]", $item->notInvolveSocialOrEconomicActivitiesUid, CHtml::listData(N37SeniorsInvolmentReason::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'', 'class'=>'form-control')); ?>
			<?php echo CHtml::error($item, 'notInvolveSocialOrEconomicActivitiesUid'); ?>
		</td>	
		
	</tr>
	<?php endforeach; ?>
<tbody>
</table>