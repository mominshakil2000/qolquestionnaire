<?php
$this->breadcrumbs=array(
	'Family Member Seniors',
);

$this->menu=array(
	array('label'=>'Create FamilyMemberSeniors','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage FamilyMemberSeniors','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Family Member Seniors</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
