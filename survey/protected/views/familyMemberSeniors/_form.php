<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'family-member-seniors-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->dropDownListRow($model,'householdUid',CHtml::listData(Household::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->dropDownListRow($model,'livingOtherUid',CHtml::listData(N36seniorsliving::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->textFieldRow($model,'involveSocialOrEconomicActivities',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->dropDownListRow($model,'notInvolveSocialOrEconomicActivitiesUid',CHtml::listData(N37seniorsinvolmentreason::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $this->renderPartial('application.views.familyMemberSeniors._tabularform', array('form'=>$form, 'modelFamilyMemberSeniors'=>$modelFamilyMemberSeniors)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
