


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N10EducationGroup</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown10educationgroup" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n10educationgroup-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n10EducationGroup._tabularform_additional', array('modelN10EducationGroup'=>$modelN10EducationGroup)); ?>
	</div>
</div>

<script>
	$('#n10educationgroup-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown10educationgroup', "
	$('#btn-addrown10educationgroup').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n10educationgroup/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n10educationgroup-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown10educationgroup',"
	$('#n10educationgroup-list').on('click', '.btn-deleterown10educationgroup', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n10educationgroup/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n10educationgroup-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

