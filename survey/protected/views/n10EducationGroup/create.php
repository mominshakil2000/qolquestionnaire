<?php
$this->breadcrumbs=array(
	'N10 Education Groups'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N10EducationGroup','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N10EducationGroup','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N10EducationGroup</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN10EducationGroup'=>$modelN10EducationGroup)); ?>