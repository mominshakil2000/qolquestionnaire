<?php
$this->breadcrumbs=array(
	'N10 Education Groups'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N10EducationGroup','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N10EducationGroup','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N10EducationGroup','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N10EducationGroup','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N10EducationGroup <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>