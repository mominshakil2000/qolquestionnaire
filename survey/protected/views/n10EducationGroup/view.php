<?php
$this->breadcrumbs=array(
	'N10 Education Groups'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N10EducationGroup','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N10EducationGroup','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N10EducationGroup','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N10EducationGroup','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N10EducationGroup','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N10EducationGroup #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
