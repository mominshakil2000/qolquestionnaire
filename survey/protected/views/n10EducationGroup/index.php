<?php
$this->breadcrumbs=array(
	'N10 Education Groups',
);

$this->menu=array(
	array('label'=>'Create N10EducationGroup','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N10EducationGroup','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N10 Education Groups</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
