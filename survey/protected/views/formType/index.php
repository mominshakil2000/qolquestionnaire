<?php
$this->breadcrumbs=array(
	'Form Types',
);

$this->menu=array(
	array('label'=>'Create FormType','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage FormType','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Form Types</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
