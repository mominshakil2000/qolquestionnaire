<?php
$this->breadcrumbs=array(
	'Form Types'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List FormType','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FormType','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update FormType','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete FormType','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage FormType','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View FormType #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
