<?php
$this->breadcrumbs=array(
	'Form Types'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List FormType','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FormType','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View FormType','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage FormType','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update FormType <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>