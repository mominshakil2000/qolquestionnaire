<?php
$this->breadcrumbs=array(
	'Form Types'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FormType','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FormType','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FormType</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFormType'=>$modelFormType)); ?>