<?php
$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	$model->title=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	array('label'=>'List UserRole','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create UserRole','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View UserRole','url'=>array('view','id'=>$model->id),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage UserRole','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update UserRole <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>