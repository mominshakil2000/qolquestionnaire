<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-role-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldRow($model,'id',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	
		<?php echo $form->textFieldRow($model,'title',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>200)); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
