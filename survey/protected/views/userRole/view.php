<?php
$this->breadcrumbs=array(
	'User Roles'=>array('index'),
	$model->title,
);

$this->menu=array(
	// array('label'=>'List UserRole','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create UserRole','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update UserRole','url'=>array('update','id'=>$model->id),'icon'=>'glyphicon glyphicon-pencil'),
	// array('label'=>'Delete UserRole','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage UserRole','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View UserRole #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'title',
	),
)); ?>
