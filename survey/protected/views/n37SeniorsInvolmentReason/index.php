<?php
$this->breadcrumbs=array(
	'N37 Seniors Involment Reasons',
);

$this->menu=array(
	array('label'=>'Create N37SeniorsInvolmentReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N37SeniorsInvolmentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N37 Seniors Involment Reasons</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
