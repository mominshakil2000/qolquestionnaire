<?php
$this->breadcrumbs=array(
	'N37 Seniors Involment Reasons'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N37SeniorsInvolmentReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N37SeniorsInvolmentReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N37SeniorsInvolmentReason','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N37SeniorsInvolmentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N37SeniorsInvolmentReason <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>