<?php
$this->breadcrumbs=array(
	'N37 Seniors Involment Reasons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N37SeniorsInvolmentReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N37SeniorsInvolmentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N37SeniorsInvolmentReason</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN37SeniorsInvolmentReason'=>$modelN37SeniorsInvolmentReason)); ?>