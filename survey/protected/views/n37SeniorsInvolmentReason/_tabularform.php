


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N37SeniorsInvolmentReason</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown37seniorsinvolmentreason" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n37seniorsinvolmentreason-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n37SeniorsInvolmentReason._tabularform_additional', array('modelN37SeniorsInvolmentReason'=>$modelN37SeniorsInvolmentReason)); ?>
	</div>
</div>

<script>
	$('#n37seniorsinvolmentreason-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown37seniorsinvolmentreason', "
	$('#btn-addrown37seniorsinvolmentreason').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n37seniorsinvolmentreason/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n37seniorsinvolmentreason-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown37seniorsinvolmentreason',"
	$('#n37seniorsinvolmentreason-list').on('click', '.btn-deleterown37seniorsinvolmentreason', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n37seniorsinvolmentreason/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n37seniorsinvolmentreason-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

