<?php
$this->breadcrumbs=array(
	'N37 Seniors Involment Reasons'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N37SeniorsInvolmentReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N37SeniorsInvolmentReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N37SeniorsInvolmentReason','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N37SeniorsInvolmentReason','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N37SeniorsInvolmentReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N37SeniorsInvolmentReason #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
