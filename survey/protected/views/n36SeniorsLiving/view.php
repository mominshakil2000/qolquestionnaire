<?php
$this->breadcrumbs=array(
	'N36 Seniors Livings'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N36SeniorsLiving','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N36SeniorsLiving','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N36SeniorsLiving','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N36SeniorsLiving','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N36SeniorsLiving','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N36SeniorsLiving #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
