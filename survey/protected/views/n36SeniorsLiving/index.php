<?php
$this->breadcrumbs=array(
	'N36 Seniors Livings',
);

$this->menu=array(
	array('label'=>'Create N36SeniorsLiving','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N36SeniorsLiving','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N36 Seniors Livings</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
