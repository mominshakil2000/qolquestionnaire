<?php
$this->breadcrumbs=array(
	'N36 Seniors Livings'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N36SeniorsLiving','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N36SeniorsLiving','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N36SeniorsLiving</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN36SeniorsLiving'=>$modelN36SeniorsLiving)); ?>