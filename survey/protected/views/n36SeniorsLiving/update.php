<?php
$this->breadcrumbs=array(
	'N36 Seniors Livings'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N36SeniorsLiving','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N36SeniorsLiving','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N36SeniorsLiving','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N36SeniorsLiving','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N36SeniorsLiving <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>