


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N1FamilyRelationship</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown1familyrelationship" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n1familyrelationship-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n1FamilyRelationship._tabularform_additional', array('modelN1FamilyRelationship'=>$modelN1FamilyRelationship)); ?>
	</div>
</div>

<script>
	$('#n1familyrelationship-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown1familyrelationship', "
	$('#btn-addrown1familyrelationship').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n1familyrelationship/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n1familyrelationship-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown1familyrelationship',"
	$('#n1familyrelationship-list').on('click', '.btn-deleterown1familyrelationship', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n1familyrelationship/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n1familyrelationship-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

