<?php
$this->breadcrumbs=array(
	'N1 Family Relationships'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N1FamilyRelationship','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N1FamilyRelationship','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N1FamilyRelationship','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N1FamilyRelationship','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N1FamilyRelationship','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N1FamilyRelationship #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
