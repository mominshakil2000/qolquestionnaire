<?php
$this->breadcrumbs=array(
	'N1 Family Relationships'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N1FamilyRelationship','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N1FamilyRelationship','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N1FamilyRelationship</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN1FamilyRelationship'=>$modelN1FamilyRelationship)); ?>