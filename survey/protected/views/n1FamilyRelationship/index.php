<?php
$this->breadcrumbs=array(
	'N1 Family Relationships',
);

$this->menu=array(
	array('label'=>'Create N1FamilyRelationship','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N1FamilyRelationship','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N1 Family Relationships</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
