<?php
$this->breadcrumbs=array(
	'N1 Family Relationships'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N1FamilyRelationship','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N1FamilyRelationship','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N1FamilyRelationship','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N1FamilyRelationship','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N1FamilyRelationship <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>