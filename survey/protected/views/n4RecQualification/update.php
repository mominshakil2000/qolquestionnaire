<?php
$this->breadcrumbs=array(
	'N4 Rec Qualifications'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N4RecQualification','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N4RecQualification','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N4RecQualification','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N4RecQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N4RecQualification <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>