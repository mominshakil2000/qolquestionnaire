<?php
$this->breadcrumbs=array(
	'N4 Rec Qualifications'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N4RecQualification','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N4RecQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N4RecQualification</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN4RecQualification'=>$modelN4RecQualification)); ?>