<?php
$this->breadcrumbs=array(
	'N4 Rec Qualifications'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N4RecQualification','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N4RecQualification','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N4RecQualification','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N4RecQualification','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N4RecQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N4RecQualification #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
