


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N4RecQualification</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown4recqualification" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n4recqualification-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n4RecQualification._tabularform_additional', array('modelN4RecQualification'=>$modelN4RecQualification)); ?>
	</div>
</div>

<script>
	$('#n4recqualification-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown4recqualification', "
	$('#btn-addrown4recqualification').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n4recqualification/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n4recqualification-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown4recqualification',"
	$('#n4recqualification-list').on('click', '.btn-deleterown4recqualification', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n4recqualification/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n4recqualification-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

