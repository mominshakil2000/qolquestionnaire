<?php
$this->breadcrumbs=array(
	'N4 Rec Qualifications',
);

$this->menu=array(
	array('label'=>'Create N4RecQualification','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N4RecQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N4 Rec Qualifications</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
