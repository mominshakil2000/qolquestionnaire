<?php
$this->breadcrumbs=array(
	'N9 Education Institute Categories',
);

$this->menu=array(
	array('label'=>'Create N9EducationInstituteCategory','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N9EducationInstituteCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N9 Education Institute Categories</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
