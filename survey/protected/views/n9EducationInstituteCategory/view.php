<?php
$this->breadcrumbs=array(
	'N9 Education Institute Categories'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N9EducationInstituteCategory','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N9EducationInstituteCategory','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N9EducationInstituteCategory','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N9EducationInstituteCategory','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N9EducationInstituteCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N9EducationInstituteCategory #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
