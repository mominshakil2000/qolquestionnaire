<?php
$this->breadcrumbs=array(
	'N9 Education Institute Categories'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N9EducationInstituteCategory','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N9EducationInstituteCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N9EducationInstituteCategory</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN9EducationInstituteCategory'=>$modelN9EducationInstituteCategory)); ?>