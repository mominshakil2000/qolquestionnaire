<?php
$this->breadcrumbs=array(
	'N9 Education Institute Categories'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N9EducationInstituteCategory','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N9EducationInstituteCategory','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N9EducationInstituteCategory','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N9EducationInstituteCategory','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N9EducationInstituteCategory <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>