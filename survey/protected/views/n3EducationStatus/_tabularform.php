


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N3EducationStatus</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown3educationstatus" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n3educationstatus-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n3EducationStatus._tabularform_additional', array('modelN3EducationStatus'=>$modelN3EducationStatus)); ?>
	</div>
</div>

<script>
	$('#n3educationstatus-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown3educationstatus', "
	$('#btn-addrown3educationstatus').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n3educationstatus/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n3educationstatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown3educationstatus',"
	$('#n3educationstatus-list').on('click', '.btn-deleterown3educationstatus', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n3educationstatus/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n3educationstatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

