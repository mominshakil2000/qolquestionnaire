<?php
$this->breadcrumbs=array(
	'N3 Education Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N3EducationStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N3EducationStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N3EducationStatus</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN3EducationStatus'=>$modelN3EducationStatus)); ?>