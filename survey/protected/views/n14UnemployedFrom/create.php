<?php
$this->breadcrumbs=array(
	'N14 Unemployed Froms'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N14UnemployedFrom','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N14UnemployedFrom','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N14UnemployedFrom</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN14UnemployedFrom'=>$modelN14UnemployedFrom)); ?>