


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N14UnemployedFrom</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown14unemployedfrom" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n14unemployedfrom-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n14UnemployedFrom._tabularform_additional', array('modelN14UnemployedFrom'=>$modelN14UnemployedFrom)); ?>
	</div>
</div>

<script>
	$('#n14unemployedfrom-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown14unemployedfrom', "
	$('#btn-addrown14unemployedfrom').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n14unemployedfrom/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n14unemployedfrom-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown14unemployedfrom',"
	$('#n14unemployedfrom-list').on('click', '.btn-deleterown14unemployedfrom', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n14unemployedfrom/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n14unemployedfrom-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

