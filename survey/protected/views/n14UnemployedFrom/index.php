<?php
$this->breadcrumbs=array(
	'N14 Unemployed Froms',
);

$this->menu=array(
	array('label'=>'Create N14UnemployedFrom','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N14UnemployedFrom','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N14 Unemployed Froms</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
