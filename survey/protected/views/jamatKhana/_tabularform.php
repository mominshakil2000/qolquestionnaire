


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">JamatKhana</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowjamatkhana" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="jamatkhana-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.jamatKhana._tabularform_additional', array('modelJamatKhana'=>$modelJamatKhana)); ?>
	</div>
</div>

<script>
	$('#jamatkhana-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowjamatkhana', "
	$('#btn-addrowjamatkhana').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('jamatkhana/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#jamatkhana-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowjamatkhana',"
	$('#jamatkhana-list').on('click', '.btn-deleterowjamatkhana', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('jamatkhana/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#jamatkhana-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

