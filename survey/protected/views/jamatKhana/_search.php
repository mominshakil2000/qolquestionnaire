<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'uid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'regionalCouncilUid',CHtml::listData(RegionalCouncil::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'localCouncilUid',CHtml::listData(LocalCouncil::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'label',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'uidLabel',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>255)); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
			'htmlOptions'=> array('class'=>'btn btn-primary btn-sm'),				
			'icon'=>'glyphicon glyphicon-search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
