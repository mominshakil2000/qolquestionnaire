<?php
$this->breadcrumbs=array(
	'Jamat Khanas'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	// array('label'=>'List Jamat Khana','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Jamat Khana','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	// array('label'=>'View Jamat Khana','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage Jamat Khana','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update Jamat Khana <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>