<?php
$this->breadcrumbs=array(
	'Jamat Khanas',
);

$this->menu=array(
	array('label'=>'Create JamatKhana','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage JamatKhana','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Jamat Khanas</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
