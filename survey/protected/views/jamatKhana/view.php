<?php
$this->breadcrumbs=array(
	'Jamat Khanas'=>array('index'),
	$model->uid,
);

$this->menu=array(
	// array('label'=>'List Jamat Khana','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Jamat Khana','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update Jamat Khana','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	// array('label'=>'Delete JamatKhana','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage Jamat Khana','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View JamatKhana #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'regionalCouncilUid',
		'localCouncilUid',
		'label',
		'uidLabel',
	),
)); ?>
