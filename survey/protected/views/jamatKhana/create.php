<?php
$this->breadcrumbs=array(
	'Jamat Khanas'=>array('index'),
	'Create',
);

$this->menu=array(
	// array('label'=>'List Jamat Khana','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage Jamat Khana','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create Jamat Khana</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelJamatKhana'=>$modelJamatKhana)); ?>