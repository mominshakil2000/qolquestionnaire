


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N47FinancialRisk</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown47financialrisk" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n47financialrisk-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n47FinancialRisk._tabularform_additional', array('modelN47FinancialRisk'=>$modelN47FinancialRisk)); ?>
	</div>
</div>

<script>
	$('#n47financialrisk-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown47financialrisk', "
	$('#btn-addrown47financialrisk').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n47financialrisk/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n47financialrisk-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown47financialrisk',"
	$('#n47financialrisk-list').on('click', '.btn-deleterown47financialrisk', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n47financialrisk/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n47financialrisk-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

