<?php
$this->breadcrumbs=array(
	'N47 Financial Risks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N47FinancialRisk','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N47FinancialRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N47FinancialRisk</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN47FinancialRisk'=>$modelN47FinancialRisk)); ?>