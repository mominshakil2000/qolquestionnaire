<?php
$this->breadcrumbs=array(
	'N47 Financial Risks'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N47FinancialRisk','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N47FinancialRisk','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N47FinancialRisk','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N47FinancialRisk','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N47FinancialRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N47FinancialRisk #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
