<?php
$this->breadcrumbs=array(
	'N47 Financial Risks',
);

$this->menu=array(
	array('label'=>'Create N47FinancialRisk','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N47FinancialRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N47 Financial Risks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
