<?php
$this->breadcrumbs=array(
	'N47 Financial Risks'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N47FinancialRisk','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N47FinancialRisk','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N47FinancialRisk','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N47FinancialRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N47FinancialRisk <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>