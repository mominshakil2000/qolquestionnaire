<?php
$this->breadcrumbs=array(
	'N8 Secular Qualifications'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N8SecularQualification','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N8SecularQualification','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N8SecularQualification','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N8SecularQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N8SecularQualification <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>