<?php
$this->breadcrumbs=array(
	'N8 Secular Qualifications'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N8SecularQualification','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N8SecularQualification','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N8SecularQualification','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N8SecularQualification','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N8SecularQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N8SecularQualification #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
