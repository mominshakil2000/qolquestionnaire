


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N8SecularQualification</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown8secularqualification" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n8secularqualification-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n8SecularQualification._tabularform_additional', array('modelN8SecularQualification'=>$modelN8SecularQualification)); ?>
	</div>
</div>

<script>
	$('#n8secularqualification-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown8secularqualification', "
	$('#btn-addrown8secularqualification').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n8secularqualification/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n8secularqualification-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown8secularqualification',"
	$('#n8secularqualification-list').on('click', '.btn-deleterown8secularqualification', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n8secularqualification/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n8secularqualification-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

