<?php
$this->breadcrumbs=array(
	'N8 Secular Qualifications'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N8SecularQualification','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N8SecularQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N8SecularQualification</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN8SecularQualification'=>$modelN8SecularQualification)); ?>