<?php
$this->breadcrumbs=array(
	'N8 Secular Qualifications',
);

$this->menu=array(
	array('label'=>'Create N8SecularQualification','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N8SecularQualification','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N8 Secular Qualifications</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
