<?php
$this->breadcrumbs=array(
	'Family Member Maternal Healths'=>array('index'),
	'Manage',
);

$this->menu=array(
	array('label'=>'List FamilyMemberMaternalHealth','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberMaternalHealth','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('family-member-maternal-health-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Family Member Maternal Healths</h1>
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
-->
<div class="panel panel-default">
	<div class="panel-heading">
		Advance Search
		<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'link',
					'type'=>'primary',
					'label'=>'',				
					'url'=>'#',				
					'htmlOptions'=> array('class'=>'search-button btn btn-primary btn-sm'),				
					'icon'=>'glyphicon glyphicon-search',				
				));?>
	</div>
		<div class="panel-body search-form" style="display:none">	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div>
	</div><!-- panel -->

<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'family-member-maternal-health-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		'uid',
		'householdUid',
		'familyMemberMotherUid',
		'familyMemberChildUid',
		'deliveryYear',
		'deliveryCarriedByUid',
		/*
		'duringDeliveryChildAlive',
		'duringDeliveryMotherAlive',
		'nowChildAlive',
		'nowMotherAlive',
		'miscarriaged',
		'motherDiedAge',
		'childDiedAge',
		'childGenderUid',
		*/
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
		),
	),
)); ?>
