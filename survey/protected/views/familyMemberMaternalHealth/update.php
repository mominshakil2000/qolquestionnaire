<?php
$this->breadcrumbs=array(
	'Family Member Maternal Healths'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List FamilyMemberMaternalHealth','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberMaternalHealth','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View FamilyMemberMaternalHealth','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage FamilyMemberMaternalHealth','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update FamilyMemberMaternalHealth <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>