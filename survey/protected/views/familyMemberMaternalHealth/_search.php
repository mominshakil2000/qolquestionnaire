<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'action'=>Yii::app()->createUrl($this->route),
	'method'=>'get',
)); ?>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'uid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'householdUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'familyMemberMotherUid',CHtml::listData(Familymember::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'familyMemberChildUid',CHtml::listData(Familymember::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'deliveryYear',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>4)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'deliveryCarriedByUid',CHtml::listData(N45deliverycarriedby::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'duringDeliveryChildAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'duringDeliveryMotherAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'nowChildAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'nowMotherAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'miscarriaged',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'motherDiedAge',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->textFieldRow($model,'childDiedAge',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
	</div>

	<div class="form-group col-xs-12">
		<?php echo $form->dropDownListRow($model,'childGenderUid',CHtml::listData(Gender::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	</div>

	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>'Search',
			'htmlOptions'=> array('class'=>'btn btn-primary btn-sm'),				
			'icon'=>'glyphicon glyphicon-search',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
