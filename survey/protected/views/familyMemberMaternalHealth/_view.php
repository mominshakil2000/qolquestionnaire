<div class="view">

	<b><?php echo CHtml::encode($data->getAttributeLabel('uid')); ?>:</b>
	<?php echo CHtml::link(CHtml::encode($data->uid),array('view','id'=>$data->uid)); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('householdUid')); ?>:</b>
	<?php echo CHtml::encode($data->householdUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('familyMemberMotherUid')); ?>:</b>
	<?php echo CHtml::encode($data->familyMemberMotherUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('familyMemberChildUid')); ?>:</b>
	<?php echo CHtml::encode($data->familyMemberChildUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deliveryYear')); ?>:</b>
	<?php echo CHtml::encode($data->deliveryYear); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('deliveryCarriedByUid')); ?>:</b>
	<?php echo CHtml::encode($data->deliveryCarriedByUid); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('duringDeliveryChildAlive')); ?>:</b>
	<?php echo CHtml::encode($data->duringDeliveryChildAlive); ?>
	<br />

	<?php /*
	<b><?php echo CHtml::encode($data->getAttributeLabel('duringDeliveryMotherAlive')); ?>:</b>
	<?php echo CHtml::encode($data->duringDeliveryMotherAlive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nowChildAlive')); ?>:</b>
	<?php echo CHtml::encode($data->nowChildAlive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('nowMotherAlive')); ?>:</b>
	<?php echo CHtml::encode($data->nowMotherAlive); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('miscarriaged')); ?>:</b>
	<?php echo CHtml::encode($data->miscarriaged); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('motherDiedAge')); ?>:</b>
	<?php echo CHtml::encode($data->motherDiedAge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('childDiedAge')); ?>:</b>
	<?php echo CHtml::encode($data->childDiedAge); ?>
	<br />

	<b><?php echo CHtml::encode($data->getAttributeLabel('childGenderUid')); ?>:</b>
	<?php echo CHtml::encode($data->childGenderUid); ?>
	<br />

	*/ ?>

</div>