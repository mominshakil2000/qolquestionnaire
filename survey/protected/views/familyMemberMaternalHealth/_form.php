<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'family-member-maternal-health-form',
	'enableAjaxValidation'=>false,
)); ?>

<p class="help-block">Fields with <span class="required">*</span> are required.</p>

<?php echo $form->errorSummary($model); ?>

<?php echo $form->textFieldRow($model,'householdUid',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>11)); ?>
<?php echo $form->dropDownListRow($model,'familyMemberMotherUid',CHtml::listData(Familymember::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->dropDownListRow($model,'familyMemberChildUid',CHtml::listData(Familymember::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->textFieldRow($model,'deliveryYear',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>4)); ?>
<?php echo $form->dropDownListRow($model,'deliveryCarriedByUid',CHtml::listData(N45deliverycarriedby::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->textFieldRow($model,'duringDeliveryChildAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'duringDeliveryMotherAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $form->textFieldRow($model,'nowChildAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'nowMotherAlive',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'miscarriaged',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>1)); ?>
<?php echo $form->textFieldRow($model,'motherDiedAge',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
<?php echo $form->textFieldRow($model,'childDiedAge',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>5)); ?>
<?php echo $form->dropDownListRow($model,'childGenderUid',CHtml::listData(Gender::model()->findAll(), 'uid', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
<?php echo $this->renderPartial('application.views.familyMemberMaternalHealth._tabularform', array('form'=>$form, 'modelFamilyMemberMaternalHealth'=>$modelFamilyMemberMaternalHealth)); ?>

<div class="form-actions">
	<?php $this->widget('bootstrap.widgets.TbButton', array(
		'buttonType'=>'submit',
		'type'=>'primary',
		'label'=>$model->isNewRecord ? 'Create' : 'Save',
	)); ?>
</div>

<?php $this->endWidget(); ?>
