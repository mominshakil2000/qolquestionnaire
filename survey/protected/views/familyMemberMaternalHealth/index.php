<?php
$this->breadcrumbs=array(
	'Family Member Maternal Healths',
);

$this->menu=array(
	array('label'=>'Create FamilyMemberMaternalHealth','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage FamilyMemberMaternalHealth','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Family Member Maternal Healths</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
