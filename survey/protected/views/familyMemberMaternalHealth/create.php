<?php
$this->breadcrumbs=array(
	'Family Member Maternal Healths'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FamilyMemberMaternalHealth','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FamilyMemberMaternalHealth','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FamilyMemberMaternalHealth</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFamilyMemberMaternalHealth'=>$modelFamilyMemberMaternalHealth)); ?>