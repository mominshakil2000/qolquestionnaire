


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">Maternal & Child health (Details of Pregnancies for last five year)</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowfamilymembermaternalhealth" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="familymembermaternalhealth-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.familyMemberMaternalHealth._tabularform_additional', array('modelFamilyMemberMaternalHealth'=>$modelFamilyMemberMaternalHealth)); ?>
	</div>
</div>

<script>
	$('#familymembermaternalhealth-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowfamilymembermaternalhealth', "
	$('#btn-addrowfamilymembermaternalhealth').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMemberMaternalHealth/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#familymembermaternalhealth-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowfamilymembermaternalhealth',"
	$('#familymembermaternalhealth-list').on('click', '.btn-deleterowfamilymembermaternalhealth', function(){
		rowIndex = $(this).closest('td').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('FamilyMemberMaternalHealth/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#familymembermaternalhealth-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

