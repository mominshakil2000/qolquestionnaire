
<table class="table">
<tbody>	
	<tr>
		<td class="col-sm-2">&nbsp;</td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<?php echo CHtml::activeHiddenField($item,"[$i]uid"); ?>	
			<td rowIndex="<?php echo $i; ?>"><a class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="btn-deleterowfamilymembermaternalhealth glyphicon glyphicon-remove"></i></a></td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-1">Mother (Family Member) <span class="required">*</span></td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][familyMemberMotherUid]", $item->familyMemberMotherUid, CHtml::listData(FamilyMember::model()->findAll(array('condition'=>'householdUid=:householdUid', 'params'=>array(':householdUid'=>$item->householdUid))), 'uid', 'firstName'), array('empty'=>'Not Applicable', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'familyMemberMotherUid'); ?>
			</td>
		<?php endforeach; ?>	
		<?php } ?>
	</tr>

	<tr>
		<td class="col-sm-1">Child (Family Member) <span class="required">*</span></td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][familyMemberChildUid]", $item->familyMemberChildUid, CHtml::listData(FamilyMember::model()->findAll(array('condition'=>'householdUid=:householdUid', 'params'=>array(':householdUid'=>$item->householdUid))), 'uid', 'firstName'), array('empty'=>'Not Applicable', 'class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'familyMemberChildUid'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>

	<tr>
		<td class="col-sm-1">Delivery / Miscarriage Year </td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::activeTextField($item,"[$i]deliveryYear",array('class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'deliveryYear'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
		
	<tr>
		<td class="col-sm-1">Delivery carried out by?</td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][deliveryCarriedByUid]", $item->deliveryCarriedByUid, CHtml::listData(N45DeliveryCarriedBy::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'deliveryCarriedByUid'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td colspan="<?php echo count($modelFamilyMemberMaternalHealth)+1;?>"><b>During delivery</b></td>	
	</tr>
	<tr>
		<td class="col-sm-1">&nbsp; - Child </td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][duringDeliveryChildAlive]", $item->duringDeliveryChildAlive, CHtml::listData(Lookup::aliveStatus(), 'uid', 'uidLabel'),array('empty'=>'','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'duringDeliveryChildAlive'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>

	<tr>
		<td class="col-sm-1">&nbsp; - Mother </td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][duringDeliveryMotherAlive]", $item->duringDeliveryMotherAlive, CHtml::listData(Lookup::aliveStatus(), 'uid', 'uidLabel'),array('empty'=>'','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'duringDeliveryMotherAlive'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td colspan="<?php echo count($modelFamilyMemberMaternalHealth)+1;?>"><b>Now</b></td>	
	</tr>	
	<tr>
		<td class="col-sm-1">&nbsp; - Child </td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][nowChildAlive]", $item->nowChildAlive, CHtml::listData(Lookup::aliveStatus(), 'uid', 'uidLabel'),array('empty'=>'','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'nowChildAlive'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-1">&nbsp; - Mother </td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][nowMotherAlive]", $item->nowMotherAlive, CHtml::listData(Lookup::aliveStatus(), 'uid', 'uidLabel'),array('empty'=>'','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'nowMotherAlive'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>	
	<tr>
		<td class="col-sm-1">Miscarriage (Y/N)</td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][miscarriaged]", $item->miscarriaged, CHtml::listData(Lookup::yesNo(), 'uid', 'uidLabel'),array('empty'=>'','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'miscarriaged'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td colspan="<?php echo count($modelFamilyMemberMaternalHealth)+1;?>"><b>Age when died</b></td>	
	</tr>	
	<tr>
		<td class="col-sm-1">&nbsp; - Mother</td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::activeTextField($item,"[$i]motherDiedAge",array('class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'motherDiedAge'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	
	<tr>
		<td class="col-sm-1">&nbsp; - Child (under 5)</td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::activeTextField($item,"[$i]childDiedAge",array('class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'childDiedAge'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>
	<tr>
		<td class="col-sm-1">Baby Gender (M/F)</td>
		<?php if(is_array($modelFamilyMemberMaternalHealth)) { ?>
		<?php foreach($modelFamilyMemberMaternalHealth as $i=>$item): ?>
			<td>
				<?php echo CHtml::dropDownList("FamilyMemberMaternalHealth[$i][childGenderUid]", $item->childGenderUid, CHtml::listData(Gender::model()->findAll(), 'uid', 'uidLabel'),array('empty'=>'','class'=>'form-control')); ?>
				<?php echo CHtml::error($item, 'childGenderUid'); ?>
			</td>	
		<?php endforeach; ?>	
		<?php } ?>
	</tr>	
<tbody>
</table>