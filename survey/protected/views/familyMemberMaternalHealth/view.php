<?php
$this->breadcrumbs=array(
	'Family Member Maternal Healths'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List FamilyMemberMaternalHealth','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FamilyMemberMaternalHealth','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update FamilyMemberMaternalHealth','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete FamilyMemberMaternalHealth','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage FamilyMemberMaternalHealth','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View FamilyMemberMaternalHealth #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'householdUid',
		'familyMemberMotherUid',
		'familyMemberChildUid',
		'deliveryYear',
		'deliveryCarriedByUid',
		'duringDeliveryChildAlive',
		'duringDeliveryMotherAlive',
		'nowChildAlive',
		'nowMotherAlive',
		'miscarriaged',
		'motherDiedAge',
		'childDiedAge',
		'childGenderUid',
	),
)); ?>
