<?php
$this->breadcrumbs=array(
	'N34 Confilicts'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N34Confilict','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N34Confilict','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N34Confilict','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N34Confilict','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N34Confilict <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>