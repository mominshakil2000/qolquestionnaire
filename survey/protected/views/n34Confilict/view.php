<?php
$this->breadcrumbs=array(
	'N34 Confilicts'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N34Confilict','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N34Confilict','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N34Confilict','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N34Confilict','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N34Confilict','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N34Confilict #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
