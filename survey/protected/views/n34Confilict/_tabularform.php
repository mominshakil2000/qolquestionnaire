


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N34Confilict</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown34confilict" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n34confilict-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n34Confilict._tabularform_additional', array('modelN34Confilict'=>$modelN34Confilict)); ?>
	</div>
</div>

<script>
	$('#n34confilict-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown34confilict', "
	$('#btn-addrown34confilict').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n34confilict/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n34confilict-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown34confilict',"
	$('#n34confilict-list').on('click', '.btn-deleterown34confilict', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n34confilict/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n34confilict-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

