<?php
$this->breadcrumbs=array(
	'N34 Confilicts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N34Confilict','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N34Confilict','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N34Confilict</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN34Confilict'=>$modelN34Confilict)); ?>