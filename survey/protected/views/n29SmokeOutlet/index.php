<?php
$this->breadcrumbs=array(
	'N29 Smoke Outlets',
);

$this->menu=array(
	array('label'=>'Create N29SmokeOutlet','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N29SmokeOutlet','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N29 Smoke Outlets</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
