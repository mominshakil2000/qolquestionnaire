


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N29SmokeOutlet</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown29smokeoutlet" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n29smokeoutlet-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n29SmokeOutlet._tabularform_additional', array('modelN29SmokeOutlet'=>$modelN29SmokeOutlet)); ?>
	</div>
</div>

<script>
	$('#n29smokeoutlet-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown29smokeoutlet', "
	$('#btn-addrown29smokeoutlet').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n29smokeoutlet/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n29smokeoutlet-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown29smokeoutlet',"
	$('#n29smokeoutlet-list').on('click', '.btn-deleterown29smokeoutlet', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n29smokeoutlet/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n29smokeoutlet-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

