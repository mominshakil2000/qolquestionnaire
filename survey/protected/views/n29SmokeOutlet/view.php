<?php
$this->breadcrumbs=array(
	'N29 Smoke Outlets'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N29SmokeOutlet','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N29SmokeOutlet','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N29SmokeOutlet','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N29SmokeOutlet','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N29SmokeOutlet','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N29SmokeOutlet #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
