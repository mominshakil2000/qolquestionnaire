<?php
$this->breadcrumbs=array(
	'N29 Smoke Outlets'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N29SmokeOutlet','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N29SmokeOutlet','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N29SmokeOutlet</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN29SmokeOutlet'=>$modelN29SmokeOutlet)); ?>