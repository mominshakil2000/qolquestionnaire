<?php
$this->breadcrumbs=array(
	'N29 Smoke Outlets'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N29SmokeOutlet','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N29SmokeOutlet','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N29SmokeOutlet','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N29SmokeOutlet','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N29SmokeOutlet <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>