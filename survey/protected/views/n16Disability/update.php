<?php
$this->breadcrumbs=array(
	'N16 Disabilities'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N16Disability','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N16Disability','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N16Disability','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N16Disability','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N16Disability <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>