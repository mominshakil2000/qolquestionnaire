<?php
$this->breadcrumbs=array(
	'N16 Disabilities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N16Disability','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N16Disability','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N16Disability</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN16Disability'=>$modelN16Disability)); ?>