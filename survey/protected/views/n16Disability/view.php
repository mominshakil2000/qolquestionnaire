<?php
$this->breadcrumbs=array(
	'N16 Disabilities'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N16Disability','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N16Disability','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N16Disability','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N16Disability','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N16Disability','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N16Disability #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
