<?php
$this->breadcrumbs=array(
	'N13 Incomes'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N13Income','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N13Income','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N13Income','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N13Income','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N13Income <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>