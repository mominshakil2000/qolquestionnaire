<?php
$this->breadcrumbs=array(
	'N13 Incomes'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N13Income','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N13Income','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N13Income</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN13Income'=>$modelN13Income)); ?>