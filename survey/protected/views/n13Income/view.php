<?php
$this->breadcrumbs=array(
	'N13 Incomes'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N13Income','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N13Income','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N13Income','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N13Income','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N13Income','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N13Income #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
