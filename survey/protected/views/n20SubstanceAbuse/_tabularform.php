


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N20SubstanceAbuse</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown20substanceabuse" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n20substanceabuse-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n20SubstanceAbuse._tabularform_additional', array('modelN20SubstanceAbuse'=>$modelN20SubstanceAbuse)); ?>
	</div>
</div>

<script>
	$('#n20substanceabuse-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown20substanceabuse', "
	$('#btn-addrown20substanceabuse').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n20substanceabuse/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n20substanceabuse-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown20substanceabuse',"
	$('#n20substanceabuse-list').on('click', '.btn-deleterown20substanceabuse', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n20substanceabuse/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n20substanceabuse-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

