<?php
$this->breadcrumbs=array(
	'N20 Substance Abuses'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N20SubstanceAbuse','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N20SubstanceAbuse','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N20SubstanceAbuse','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N20SubstanceAbuse','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N20SubstanceAbuse <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>