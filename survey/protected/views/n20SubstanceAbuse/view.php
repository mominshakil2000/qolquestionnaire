<?php
$this->breadcrumbs=array(
	'N20 Substance Abuses'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N20SubstanceAbuse','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N20SubstanceAbuse','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N20SubstanceAbuse','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N20SubstanceAbuse','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N20SubstanceAbuse','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N20SubstanceAbuse #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
