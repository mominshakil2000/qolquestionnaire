<?php
$this->breadcrumbs=array(
	'N20 Substance Abuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N20SubstanceAbuse','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N20SubstanceAbuse','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N20SubstanceAbuse</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN20SubstanceAbuse'=>$modelN20SubstanceAbuse)); ?>