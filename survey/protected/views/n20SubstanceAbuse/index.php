<?php
$this->breadcrumbs=array(
	'N20 Substance Abuses',
);

$this->menu=array(
	array('label'=>'Create N20SubstanceAbuse','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N20SubstanceAbuse','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N20 Substance Abuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
