<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id=>array('view','id'=>$model->id),
	'Update',
);

$this->menu=array(
	// array('label'=>'List User','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create User','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View User','url'=>array('view','id'=>$model->id),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage User','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update User <?php echo $model->id; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>