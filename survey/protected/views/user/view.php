<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	$model->id,
);

$this->menu=array(
	// array('label'=>'List User','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create User','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update User','url'=>array('update','id'=>$model->id),'icon'=>'glyphicon glyphicon-pencil'),
	// array('label'=>'Delete User','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->id),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage User','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View User #<?php echo $model->id; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'id',
		'username',
		'password',
		'userRole',
		'accountStatus',
	),
)); ?>
