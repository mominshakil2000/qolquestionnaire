<?php
$this->breadcrumbs=array(
	'Users'=>array('index'),
	'Manage',
);

$this->menu=array(
	// array('label'=>'List User','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create User','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
);

Yii::app()->clientScript->registerScript('search', "
$('.search-button').click(function(){
	$('.search-form').toggle();
	return false;
});
$('.search-form form').submit(function(){
	$.fn.yiiGridView.update('user-grid', {
		data: $(this).serialize()
	});
	return false;
});
");
?>

<h1>Manage Users</h1>
<!--
<p>
You may optionally enter a comparison operator (<b>&lt;</b>, <b>&lt;=</b>, <b>&gt;</b>, <b>&gt;=</b>, <b>&lt;&gt;</b>
or <b>=</b>) at the beginning of each of your search values to specify how the comparison should be done.
</p>
-->
<div class="panel panel-default">
	<div class="panel-heading">
		Advance Search
		<?php $this->widget('bootstrap.widgets.TbButton', array(
					'buttonType'=>'link',
					'type'=>'primary',
					'label'=>'',				
					'url'=>'#',				
					'htmlOptions'=> array('class'=>'search-button btn btn-primary btn-sm'),				
					'icon'=>'glyphicon glyphicon-search',				
				));?>
	</div>
		<div class="panel-body search-form" style="display:none">	<?php $this->renderPartial('_search',array(
		'model'=>$model,
	)); ?>
	</div>
	</div><!-- panel -->
<?php 
	$form = $this->beginWidget('bootstrap.widgets.TbActiveForm',array(
		'id'=>'user-admin-form',
		'action' => Yii::app()->createUrl('User/adminUpdate'),
		'enableAjaxValidation'=>false,
	)); 
?>
<?php $this->widget('bootstrap.widgets.TbGridView',array(
	'id'=>'user-grid',
	'dataProvider'=>$model->search(),
	'filter'=>$model,
	'columns'=>array(
		array(
			'class' => 'CCheckBoxColumn',
			// enable selection multiple checkboxes
			'selectableRows' => 2,
			'checkBoxHtmlOptions' => array(
				// array where the ids will be collected
				'name' => 'id[]',
			),
		),
		'username',
		'password',
		'userRole',
		'accountStatus',
		array(
			'class'=>'bootstrap.widgets.TbButtonColumn',
			'template'=>'{view} {update}',
		),
	),
)); 
?>
<?php 	
	echo CHtml::activeLabel($model,'uid',array('label'=>'<b>Activate Account ?</b>')); 
	echo CHtml::dropDownList("accountStatus", $model->accountStatus, CHtml::listData(Lookup::yesNoOnly(), 'uid', 'uidLabel'),array('class'=>'form-control')); 
	$this->widget('bootstrap.widgets.TbButton', array( 'buttonType'=>'submit', 'type'=>'primary', 'label'=>'Save', )); 
?>
<?php $this->endWidget(); ?>
