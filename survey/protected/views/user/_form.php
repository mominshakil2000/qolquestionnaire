<?php $form=$this->beginWidget('bootstrap.widgets.TbActiveForm',array(
	'id'=>'user-form',
	'enableAjaxValidation'=>false,
)); ?>

	<p class="help-block">Fields with <span class="required">*</span> are required.</p>

	<?php echo $form->errorSummary($model); ?>

		<?php echo $form->textFieldRow($model,'username',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>128)); ?>
	
		<?php echo $form->passwordFieldRow($model,'password',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control','maxlength'=>128)); ?>
	
		<?php echo $form->dropDownListRow($model,'userRole',CHtml::listData(UserRole::model()->findAll(), 'id', 'title'), array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	
		<?php echo $form->textFieldRow($model,'accountStatus',array('labelOptions'=>array('class'=>'col-sm-3'),'class'=>'form-control')); ?>
	
	<div class="form-actions">
		<?php $this->widget('bootstrap.widgets.TbButton', array(
			'buttonType'=>'submit',
			'type'=>'primary',
			'label'=>$model->isNewRecord ? 'Create' : 'Save',
		)); ?>
	</div>

<?php $this->endWidget(); ?>
