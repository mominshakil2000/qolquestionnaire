<?php
$this->breadcrumbs=array(
	'Local Councils'=>array('index'),
	'Create',
);

$this->menu=array(
	// array('label'=>'List Local Council','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage Local Council','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create Local Council</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelLocalCouncil'=>$modelLocalCouncil)); ?>