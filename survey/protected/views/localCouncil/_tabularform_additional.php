
<table class="table">
<thead>
	<tr>
	<td class="col-sm-1">&nbsp;</td>
						<td class="col-sm-1">regionalCouncilUid</td>
					<td class="col-sm-1">label</td>
					<td class="col-sm-1">uidLabel</td>
			</tr>
</thead>
<tbody>
	<?php foreach($modelLocalCouncil as $i=>$item): ?>
	<tr rowIndex="<?php echo $i; ?>">
	<td><a class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="btn-deleterowlocalcouncil glyphicon glyphicon-remove"></i></a></td>	
			<?php echo CHtml::activeHiddenField($item,"[$i]uid"); ?>	
			<td><?php echo CHtml::dropDownList("[$i]regionalCouncilUid", $item->regionalCouncilUid, CHtml::listData(RegionalCouncil::model()->findAll(), 'uid', 'title'),array('class'=>'form-control')); ?></td>	
			<td><?php echo CHtml::activeTextField($item,"[$i]label",array('class'=>'form-control')); ?></td>	
			<td><?php echo CHtml::activeTextField($item,"[$i]uidLabel",array('class'=>'form-control')); ?></td>	
		
	</tr>
	<?php endforeach; ?>
<tbody>
</table>