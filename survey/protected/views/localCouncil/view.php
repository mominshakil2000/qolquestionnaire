<?php
$this->breadcrumbs=array(
	'Local Councils'=>array('index'),
	$model->uid,
);

$this->menu=array(
	// array('label'=>'List Local Council','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Local Council','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update Local Council','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete Local Council','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage Local Council','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View LocalCouncil #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'regionalCouncilUid',
		'label',
		'uidLabel',
	),
)); ?>
