<?php
$this->breadcrumbs=array(
	'Local Councils'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	// array('label'=>'List LocalCouncil','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create Local Council','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	// array('label'=>'View Local Council','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage Local Council','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update LocalCouncil <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>