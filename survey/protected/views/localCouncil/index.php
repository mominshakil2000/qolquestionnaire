<?php
$this->breadcrumbs=array(
	'Local Councils',
);

$this->menu=array(
	array('label'=>'Create LocalCouncil','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage LocalCouncil','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Local Councils</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
