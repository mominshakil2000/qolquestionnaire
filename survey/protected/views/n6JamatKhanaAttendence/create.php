<?php
$this->breadcrumbs=array(
	'N6 Jamat Khana Attendences'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N6JamatKhanaAttendence','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N6JamatKhanaAttendence','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N6JamatKhanaAttendence</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN6JamatKhanaAttendence'=>$modelN6JamatKhanaAttendence)); ?>