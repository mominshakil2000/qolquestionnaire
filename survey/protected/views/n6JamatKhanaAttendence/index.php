<?php
$this->breadcrumbs=array(
	'N6 Jamat Khana Attendences',
);

$this->menu=array(
	array('label'=>'Create N6JamatKhanaAttendence','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N6JamatKhanaAttendence','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N6 Jamat Khana Attendences</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
