<?php
$this->breadcrumbs=array(
	'N6 Jamat Khana Attendences'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N6JamatKhanaAttendence','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N6JamatKhanaAttendence','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N6JamatKhanaAttendence','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N6JamatKhanaAttendence','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N6JamatKhanaAttendence','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N6JamatKhanaAttendence #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
