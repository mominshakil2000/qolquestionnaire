<?php
$this->breadcrumbs=array(
	'N6 Jamat Khana Attendences'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N6JamatKhanaAttendence','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N6JamatKhanaAttendence','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N6JamatKhanaAttendence','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N6JamatKhanaAttendence','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N6JamatKhanaAttendence <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>