


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N6JamatKhanaAttendence</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown6jamatkhanaattendence" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n6jamatkhanaattendence-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n6JamatKhanaAttendence._tabularform_additional', array('modelN6JamatKhanaAttendence'=>$modelN6JamatKhanaAttendence)); ?>
	</div>
</div>

<script>
	$('#n6jamatkhanaattendence-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown6jamatkhanaattendence', "
	$('#btn-addrown6jamatkhanaattendence').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n6jamatkhanaattendence/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n6jamatkhanaattendence-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown6jamatkhanaattendence',"
	$('#n6jamatkhanaattendence-list').on('click', '.btn-deleterown6jamatkhanaattendence', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n6jamatkhanaattendence/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n6jamatkhanaattendence-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

