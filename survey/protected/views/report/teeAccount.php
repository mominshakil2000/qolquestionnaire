<h1>T-Account</h1>
<table class="table-bordered">
<thead>
	<tr>
		<th colspan="7" class="text-center" ><h4><?php echo $glData['codePrefix'], ' ', $glData['codePostfix'], ' ', $glData['title']; ?></h4></th>
	</tr>
</thead>
<tbody>

<?php 
	$totalDebitAmount = 0; 
	$totalCreditAmount = 0;  
	$i = 0;
	$i = 0;
	$debitRowCount =  count($debitData);
	$creditRowCount =  count($creditData);
	$maxRowCount = $debitRowCount > $creditRowCount ? $debitRowCount : $creditRowCount; 
?>
<?php for($i=0; $i < $maxRowCount; $i++) { ?>
<?php 
	
	$totalDebitAmount += $debitData[$i]['amount'];
	$totalCreditAmount += $creditData[$i]['amount'];;
	
?>
	<tr>
		<?php if($i < $debitRowCount) {?>
			<td nowrap class="col-sm-1"><?php echo Yii::app()->format->formatDate($debitData[$i]['createDate']); ?></td>
			<td class="col-sm-4"><?php echo $debitData[$i]['voucherPrefix'],'-',$debitData[$i]['voucherPostfix'], ' :: ',$debitData[$i]['narration']; ?></td>
			<td class="col-sm-1 text-right"><?php echo Yii::app()->format->formatNumber($debitData[$i]['amount']); ?></td>
		<?php } else {?>
			<td></td>
			<td></td>
			<td></td>
		<?php } ?>
		
		<?php if($i < $creditRowCount) {?>
			<td nowrap class="col-sm-1"><?php echo Yii::app()->format->formatDate($creditData[$i]['createDate']); ?></td>
			<td class="col-sm-4"><?php echo $creditData[$i]['voucherPrefix'],'-',$creditData[$i]['voucherPostfix'], ' :: ',$creditData[$i]['narration']; ?></td>
			<td class="col-sm-1 text-right"><?php echo Yii::app()->format->formatNumber($creditData[$i]['amount']); ?></td>
		<?php } else {?>
			<td></td>
			<td></td>
			<td></td>
		<?php } ?>

		</tr>
<?php } ?>
<tr>
	<td></td>
	<td></td>
	<td class="col-sm-1 text-right subtotal"><b><?php echo Yii::app()->format->formatNumber($totalDebitAmount); ?></b></td>
	<td class=""></td>
	<td class=""></td>
	<td class="col-sm-1 text-right subtotal"><b><?php echo Yii::app()->format->formatNumber($totalCreditAmount); ?></b></td>
</tr>
<?php if($totalDebitAmount < $totalCreditAmount){ ?>
<tr>
	<td class="" nowrap></td>
	<td class="col-sm-1"><b>Carried Down</b></td>
	<td class="col-sm-1 text-right"><b><?php echo Yii::app()->format->formatNumber($totalCreditAmount-$totalDebitAmount); ?></b></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<tr>
	<td nowrap></td>
	<td ></td>
	<td class="col-sm-1 total" ><b><?php echo Yii::app()->format->formatNumber($totalCreditAmount); ?></b></td>
	<td ></td>
	<td ></td>
	<td class="col-sm-1 total"><b><?php echo Yii::app()->format->formatNumber($totalCreditAmount); ?></b></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td></td>
	<td class="col-sm-1"><b>Brought Down</b></td>
	<td class="col-sm-1 text-right"><b><?php echo Yii::app()->format->formatNumber($totalCreditAmount-$totalDebitAmount); ?></b></td>
</tr>
<?php } else if($totalCreditAmount < $totalDebitAmount) { ?>
<tr>
	<td></td>
	<td></td>
	<td></td>
	<td class="" nowrap></td>
	<td class="col-sm-1"><b>Carried Down</b></td>
	<td class="col-sm-1 text-right"><b><?php echo Yii::app()->format->formatNumber($totalDebitAmount - $totalCreditAmount); ?></b></td>
</tr>
<tr>
	<td></td>
	<td></td>
	<td class="col-sm-1 total"><b><?php echo Yii::app()->format->formatNumber($totalDebitAmount); ?></b></td>
	<td></td>
	<td></td>
	<td class="col-sm-1 total" ><b><?php echo Yii::app()->format->formatNumber($totalDebitAmount); ?></b></td>
</tr>
<tr>
	<td></td>
	<td class="col-sm-1"><b>Brought Down</b></td>
	<td class="col-sm-1 text-right"><b><?php echo Yii::app()->format->formatNumber($totalDebitAmount-$totalCreditAmount); ?></b></td>
	<td></td>
	<td></td>
	<td></td>
</tr>
<?php } ?>
</tbody>
</table>