<h1>Trial Balance</h1>
<table class="table">
<thead>
	<tr>
		<th class="col-sm-1" >Code</th>
		<th>Title</th>
		<th class="col-sm-2 text-right">Debit</th>
		<th class="col-sm-2 text-right">Credit</th>
	</tr>
</thead>
<tbody>

<?php $totalDebitAmount = 0; $totalCreditAmount = 0;  ?>
<?php foreach($data as $i => $item) { ?>
<?php 
	$debitAmount = 0;
	$creditAmount = 0;
	if($item['debitAmount'] > $item['creditAmount'])
	{
		$debitAmount = $item['debitAmount'] - $item['creditAmount']; 
		$totalDebitAmount += $debitAmount; 
	}
	else
	{
		$creditAmount = $item['creditAmount'] - $item['debitAmount']; 
		$totalCreditAmount += $creditAmount; 
	}
	
	if($debitAmount == $creditAmount)
		continue;
?>
	<tr>
		<td><?php echo $item['codePrefix'], ' ', $item['codePostfix'];?></td>
		<td><?php echo $item['title'];?></td>
		<td class="text-right"><?php if($debitAmount > 0) echo Yii::app()->formatInteger->formatNumber($debitAmount); ?></td>
		<td class="text-right"><?php if($creditAmount > 0) echo Yii::app()->formatInteger->formatNumber($creditAmount); ?></td>
	</tr>
<?php } ?>
<tr>
	<td class="text-right" colspan="2"><b>Total</b></td>
	<td class="text-right"><b><?php echo Yii::app()->formatInteger->formatNumber($totalDebitAmount); ?></b></td>
	<td class="text-right"><b><?php echo Yii::app()->formatInteger->formatNumber($totalCreditAmount); ?></b></td>
</tr>
</tbody>
</table>