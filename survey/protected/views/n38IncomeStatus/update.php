<?php
$this->breadcrumbs=array(
	'N38 Income Statuses'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N38IncomeStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N38IncomeStatus','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N38IncomeStatus','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N38IncomeStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N38IncomeStatus <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>