


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N38IncomeStatus</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown38incomestatus" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n38incomestatus-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n38IncomeStatus._tabularform_additional', array('modelN38IncomeStatus'=>$modelN38IncomeStatus)); ?>
	</div>
</div>

<script>
	$('#n38incomestatus-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown38incomestatus', "
	$('#btn-addrown38incomestatus').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n38incomestatus/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n38incomestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown38incomestatus',"
	$('#n38incomestatus-list').on('click', '.btn-deleterown38incomestatus', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n38incomestatus/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n38incomestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

