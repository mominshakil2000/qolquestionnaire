<?php
$this->breadcrumbs=array(
	'N38 Income Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N38IncomeStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N38IncomeStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N38IncomeStatus</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN38IncomeStatus'=>$modelN38IncomeStatus)); ?>