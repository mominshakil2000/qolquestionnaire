<?php
$this->breadcrumbs=array(
	'Form Fill Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FormFillStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FormFillStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FormFillStatus</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFormFillStatus'=>$modelFormFillStatus)); ?>