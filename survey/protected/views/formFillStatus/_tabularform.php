


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">FormFillStatus</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowformfillstatus" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="formfillstatus-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.formFillStatus._tabularform_additional', array('modelFormFillStatus'=>$modelFormFillStatus)); ?>
	</div>
</div>

<script>
	$('#formfillstatus-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowformfillstatus', "
	$('#btn-addrowformfillstatus').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('formfillstatus/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#formfillstatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowformfillstatus',"
	$('#formfillstatus-list').on('click', '.btn-deleterowformfillstatus', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('formfillstatus/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#formfillstatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

