<?php
$this->breadcrumbs=array(
	'N40 Deficit Manages'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N40DeficitManage','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N40DeficitManage','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N40DeficitManage</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN40DeficitManage'=>$modelN40DeficitManage)); ?>