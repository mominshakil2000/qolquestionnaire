<?php
$this->breadcrumbs=array(
	'N40 Deficit Manages'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N40DeficitManage','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N40DeficitManage','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N40DeficitManage','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N40DeficitManage','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N40DeficitManage','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N40DeficitManage #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
