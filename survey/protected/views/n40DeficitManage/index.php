<?php
$this->breadcrumbs=array(
	'N40 Deficit Manages',
);

$this->menu=array(
	array('label'=>'Create N40DeficitManage','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N40DeficitManage','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N40 Deficit Manages</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
