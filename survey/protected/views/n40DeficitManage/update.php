<?php
$this->breadcrumbs=array(
	'N40 Deficit Manages'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N40DeficitManage','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N40DeficitManage','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N40DeficitManage','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N40DeficitManage','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N40DeficitManage <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>