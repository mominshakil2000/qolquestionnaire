


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N40DeficitManage</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown40deficitmanage" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n40deficitmanage-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n40DeficitManage._tabularform_additional', array('modelN40DeficitManage'=>$modelN40DeficitManage)); ?>
	</div>
</div>

<script>
	$('#n40deficitmanage-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown40deficitmanage', "
	$('#btn-addrown40deficitmanage').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n40deficitmanage/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n40deficitmanage-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown40deficitmanage',"
	$('#n40deficitmanage-list').on('click', '.btn-deleterown40deficitmanage', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n40deficitmanage/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n40deficitmanage-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

