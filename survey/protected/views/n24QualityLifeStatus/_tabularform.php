


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N24QualityLifeStatus</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown24qualitylifestatus" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n24qualitylifestatus-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n24QualityLifeStatus._tabularform_additional', array('modelN24QualityLifeStatus'=>$modelN24QualityLifeStatus)); ?>
	</div>
</div>

<script>
	$('#n24qualitylifestatus-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown24qualitylifestatus', "
	$('#btn-addrown24qualitylifestatus').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n24qualitylifestatus/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n24qualitylifestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown24qualitylifestatus',"
	$('#n24qualitylifestatus-list').on('click', '.btn-deleterown24qualitylifestatus', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n24qualitylifestatus/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n24qualitylifestatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

