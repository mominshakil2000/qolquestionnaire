<?php
$this->breadcrumbs=array(
	'N24 Quality Life Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N24QualityLifeStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N24QualityLifeStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N24QualityLifeStatus</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN24QualityLifeStatus'=>$modelN24QualityLifeStatus)); ?>