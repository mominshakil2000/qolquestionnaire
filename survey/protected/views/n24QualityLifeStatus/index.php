<?php
$this->breadcrumbs=array(
	'N24 Quality Life Statuses',
);

$this->menu=array(
	array('label'=>'Create N24QualityLifeStatus','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N24QualityLifeStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N24 Quality Life Statuses</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
