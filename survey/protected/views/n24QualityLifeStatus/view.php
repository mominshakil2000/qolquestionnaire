<?php
$this->breadcrumbs=array(
	'N24 Quality Life Statuses'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N24QualityLifeStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N24QualityLifeStatus','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N24QualityLifeStatus','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N24QualityLifeStatus','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N24QualityLifeStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N24QualityLifeStatus #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
