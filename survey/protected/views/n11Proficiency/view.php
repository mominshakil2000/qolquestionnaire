<?php
$this->breadcrumbs=array(
	'N11 Proficiencies'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N11Proficiency','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N11Proficiency','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N11Proficiency','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N11Proficiency','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N11Proficiency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N11Proficiency #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
