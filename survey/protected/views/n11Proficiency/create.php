<?php
$this->breadcrumbs=array(
	'N11 Proficiencies'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N11Proficiency','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N11Proficiency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N11Proficiency</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN11Proficiency'=>$modelN11Proficiency)); ?>