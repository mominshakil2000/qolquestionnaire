<?php
$this->breadcrumbs=array(
	'N11 Proficiencies'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N11Proficiency','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N11Proficiency','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N11Proficiency','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N11Proficiency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N11Proficiency <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>