


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N11Proficiency</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown11proficiency" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n11proficiency-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n11Proficiency._tabularform_additional', array('modelN11Proficiency'=>$modelN11Proficiency)); ?>
	</div>
</div>

<script>
	$('#n11proficiency-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown11proficiency', "
	$('#btn-addrown11proficiency').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n11proficiency/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n11proficiency-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown11proficiency',"
	$('#n11proficiency-list').on('click', '.btn-deleterown11proficiency', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n11proficiency/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n11proficiency-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

