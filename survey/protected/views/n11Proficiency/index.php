<?php
$this->breadcrumbs=array(
	'N11 Proficiencies',
);

$this->menu=array(
	array('label'=>'Create N11Proficiency','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N11Proficiency','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N11 Proficiencies</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
