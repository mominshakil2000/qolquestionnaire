


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N42OutstandingDebt</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown42outstandingdebt" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n42outstandingdebt-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n42OutstandingDebt._tabularform_additional', array('modelN42OutstandingDebt'=>$modelN42OutstandingDebt)); ?>
	</div>
</div>

<script>
	$('#n42outstandingdebt-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown42outstandingdebt', "
	$('#btn-addrown42outstandingdebt').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n42outstandingdebt/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n42outstandingdebt-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown42outstandingdebt',"
	$('#n42outstandingdebt-list').on('click', '.btn-deleterown42outstandingdebt', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n42outstandingdebt/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n42outstandingdebt-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

