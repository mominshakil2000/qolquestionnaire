<?php
$this->breadcrumbs=array(
	'N42 Outstanding Debts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N42OutstandingDebt','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N42OutstandingDebt','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N42OutstandingDebt</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN42OutstandingDebt'=>$modelN42OutstandingDebt)); ?>