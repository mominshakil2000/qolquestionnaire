<?php
$this->breadcrumbs=array(
	'N42 Outstanding Debts'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N42OutstandingDebt','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N42OutstandingDebt','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N42OutstandingDebt','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N42OutstandingDebt','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N42OutstandingDebt','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N42OutstandingDebt #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
