<?php
$this->breadcrumbs=array(
	'N42 Outstanding Debts'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N42OutstandingDebt','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N42OutstandingDebt','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N42OutstandingDebt','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N42OutstandingDebt','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N42OutstandingDebt <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>