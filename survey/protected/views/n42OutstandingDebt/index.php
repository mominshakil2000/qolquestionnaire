<?php
$this->breadcrumbs=array(
	'N42 Outstanding Debts',
);

$this->menu=array(
	array('label'=>'Create N42OutstandingDebt','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N42OutstandingDebt','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N42 Outstanding Debts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
