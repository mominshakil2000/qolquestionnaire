<?php
$this->breadcrumbs=array(
	'N32 Toilet Facilities',
);

$this->menu=array(
	array('label'=>'Create N32ToiletFacility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N32ToiletFacility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N32 Toilet Facilities</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
