<?php
$this->breadcrumbs=array(
	'N32 Toilet Facilities'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N32ToiletFacility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N32ToiletFacility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N32ToiletFacility','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N32ToiletFacility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N32ToiletFacility <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>