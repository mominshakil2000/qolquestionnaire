<?php
$this->breadcrumbs=array(
	'N32 Toilet Facilities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N32ToiletFacility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N32ToiletFacility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N32ToiletFacility</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN32ToiletFacility'=>$modelN32ToiletFacility)); ?>