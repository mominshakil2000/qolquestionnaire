<?php
$this->breadcrumbs=array(
	'N32 Toilet Facilities'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N32ToiletFacility','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N32ToiletFacility','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N32ToiletFacility','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N32ToiletFacility','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N32ToiletFacility','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N32ToiletFacility #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
