


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N32ToiletFacility</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown32toiletfacility" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n32toiletfacility-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n32ToiletFacility._tabularform_additional', array('modelN32ToiletFacility'=>$modelN32ToiletFacility)); ?>
	</div>
</div>

<script>
	$('#n32toiletfacility-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown32toiletfacility', "
	$('#btn-addrown32toiletfacility').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n32toiletfacility/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n32toiletfacility-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown32toiletfacility',"
	$('#n32toiletfacility-list').on('click', '.btn-deleterown32toiletfacility', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n32toiletfacility/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n32toiletfacility-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

