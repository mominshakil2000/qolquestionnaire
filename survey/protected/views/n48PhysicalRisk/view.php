<?php
$this->breadcrumbs=array(
	'N48 Physical Risks'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N48PhysicalRisk','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N48PhysicalRisk','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N48PhysicalRisk','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N48PhysicalRisk','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N48PhysicalRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N48PhysicalRisk #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
