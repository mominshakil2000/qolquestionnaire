<?php
$this->breadcrumbs=array(
	'N48 Physical Risks'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N48PhysicalRisk','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N48PhysicalRisk','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N48PhysicalRisk','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N48PhysicalRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N48PhysicalRisk <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>