


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N48PhysicalRisk</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown48physicalrisk" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n48physicalrisk-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n48PhysicalRisk._tabularform_additional', array('modelN48PhysicalRisk'=>$modelN48PhysicalRisk)); ?>
	</div>
</div>

<script>
	$('#n48physicalrisk-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown48physicalrisk', "
	$('#btn-addrown48physicalrisk').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n48physicalrisk/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n48physicalrisk-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown48physicalrisk',"
	$('#n48physicalrisk-list').on('click', '.btn-deleterown48physicalrisk', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n48physicalrisk/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n48physicalrisk-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

