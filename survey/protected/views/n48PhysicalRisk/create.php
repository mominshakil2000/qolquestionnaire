<?php
$this->breadcrumbs=array(
	'N48 Physical Risks'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N48PhysicalRisk','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N48PhysicalRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N48PhysicalRisk</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN48PhysicalRisk'=>$modelN48PhysicalRisk)); ?>