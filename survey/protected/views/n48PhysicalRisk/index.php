<?php
$this->breadcrumbs=array(
	'N48 Physical Risks',
);

$this->menu=array(
	array('label'=>'Create N48PhysicalRisk','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N48PhysicalRisk','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N48 Physical Risks</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
