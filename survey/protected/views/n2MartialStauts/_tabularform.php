


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N2MartialStauts</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown2martialstauts" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n2martialstauts-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n2MartialStauts._tabularform_additional', array('modelN2MartialStauts'=>$modelN2MartialStauts)); ?>
	</div>
</div>

<script>
	$('#n2martialstauts-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown2martialstauts', "
	$('#btn-addrown2martialstauts').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n2martialstauts/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n2martialstauts-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown2martialstauts',"
	$('#n2martialstauts-list').on('click', '.btn-deleterown2martialstauts', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n2martialstauts/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n2martialstauts-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

