<?php
$this->breadcrumbs=array(
	'N2 Martial Stauts'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N2MartialStauts','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N2MartialStauts','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N2MartialStauts','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N2MartialStauts','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N2MartialStauts <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>