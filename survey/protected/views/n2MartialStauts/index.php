<?php
$this->breadcrumbs=array(
	'N2 Martial Stauts',
);

$this->menu=array(
	array('label'=>'Create N2MartialStauts','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N2MartialStauts','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N2 Martial Stauts</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
