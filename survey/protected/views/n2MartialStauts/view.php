<?php
$this->breadcrumbs=array(
	'N2 Martial Stauts'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N2MartialStauts','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N2MartialStauts','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N2MartialStauts','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N2MartialStauts','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N2MartialStauts','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N2MartialStauts #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
