<?php
$this->breadcrumbs=array(
	'N2 Martial Stauts'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N2MartialStauts','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N2MartialStauts','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N2MartialStauts</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN2MartialStauts'=>$modelN2MartialStauts)); ?>