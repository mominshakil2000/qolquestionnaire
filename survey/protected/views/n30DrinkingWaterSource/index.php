<?php
$this->breadcrumbs=array(
	'N30 Drinking Water Sources',
);

$this->menu=array(
	array('label'=>'Create N30DrinkingWaterSource','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N30DrinkingWaterSource','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N30 Drinking Water Sources</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
