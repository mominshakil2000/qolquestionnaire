<?php
$this->breadcrumbs=array(
	'N30 Drinking Water Sources'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N30DrinkingWaterSource','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N30DrinkingWaterSource','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N30DrinkingWaterSource</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN30DrinkingWaterSource'=>$modelN30DrinkingWaterSource)); ?>