<?php
$this->breadcrumbs=array(
	'N30 Drinking Water Sources'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N30DrinkingWaterSource','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N30DrinkingWaterSource','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N30DrinkingWaterSource','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N30DrinkingWaterSource','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N30DrinkingWaterSource','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N30DrinkingWaterSource #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
