


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N30DrinkingWaterSource</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown30drinkingwatersource" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n30drinkingwatersource-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n30DrinkingWaterSource._tabularform_additional', array('modelN30DrinkingWaterSource'=>$modelN30DrinkingWaterSource)); ?>
	</div>
</div>

<script>
	$('#n30drinkingwatersource-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown30drinkingwatersource', "
	$('#btn-addrown30drinkingwatersource').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n30drinkingwatersource/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n30drinkingwatersource-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown30drinkingwatersource',"
	$('#n30drinkingwatersource-list').on('click', '.btn-deleterown30drinkingwatersource', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n30drinkingwatersource/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n30drinkingwatersource-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

