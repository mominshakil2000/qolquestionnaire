<?php
$this->breadcrumbs=array(
	'N43 Interest Rates'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N43InterestRate','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N43InterestRate','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N43InterestRate</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN43InterestRate'=>$modelN43InterestRate)); ?>