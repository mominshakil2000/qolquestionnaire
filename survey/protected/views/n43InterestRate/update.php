<?php
$this->breadcrumbs=array(
	'N43 Interest Rates'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N43InterestRate','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N43InterestRate','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N43InterestRate','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N43InterestRate','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N43InterestRate <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>