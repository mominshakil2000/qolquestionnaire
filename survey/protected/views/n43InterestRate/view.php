<?php
$this->breadcrumbs=array(
	'N43 Interest Rates'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N43InterestRate','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N43InterestRate','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N43InterestRate','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N43InterestRate','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N43InterestRate','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N43InterestRate #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
