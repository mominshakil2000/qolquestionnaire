<?php
$this->breadcrumbs=array(
	'N43 Interest Rates',
);

$this->menu=array(
	array('label'=>'Create N43InterestRate','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N43InterestRate','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N43 Interest Rates</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
