


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N43InterestRate</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown43interestrate" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n43interestrate-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n43InterestRate._tabularform_additional', array('modelN43InterestRate'=>$modelN43InterestRate)); ?>
	</div>
</div>

<script>
	$('#n43interestrate-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown43interestrate', "
	$('#btn-addrown43interestrate').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n43interestrate/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n43interestrate-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown43interestrate',"
	$('#n43interestrate-list').on('click', '.btn-deleterown43interestrate', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n43interestrate/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n43interestrate-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

