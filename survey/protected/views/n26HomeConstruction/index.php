<?php
$this->breadcrumbs=array(
	'N26 Home Constructions',
);

$this->menu=array(
	array('label'=>'Create N26HomeConstruction','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N26HomeConstruction','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N26 Home Constructions</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
