


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N26HomeConstruction</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown26homeconstruction" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n26homeconstruction-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n26HomeConstruction._tabularform_additional', array('modelN26HomeConstruction'=>$modelN26HomeConstruction)); ?>
	</div>
</div>

<script>
	$('#n26homeconstruction-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown26homeconstruction', "
	$('#btn-addrown26homeconstruction').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n26homeconstruction/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n26homeconstruction-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown26homeconstruction',"
	$('#n26homeconstruction-list').on('click', '.btn-deleterown26homeconstruction', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n26homeconstruction/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n26homeconstruction-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

