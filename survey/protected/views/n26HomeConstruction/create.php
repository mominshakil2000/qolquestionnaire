<?php
$this->breadcrumbs=array(
	'N26 Home Constructions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N26HomeConstruction','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N26HomeConstruction','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N26HomeConstruction</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN26HomeConstruction'=>$modelN26HomeConstruction)); ?>