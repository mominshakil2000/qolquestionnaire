<?php
$this->breadcrumbs=array(
	'N26 Home Constructions'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N26HomeConstruction','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N26HomeConstruction','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N26HomeConstruction','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N26HomeConstruction','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N26HomeConstruction','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N26HomeConstruction #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
