<?php
$this->breadcrumbs=array(
	'N26 Home Constructions'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N26HomeConstruction','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N26HomeConstruction','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N26HomeConstruction','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N26HomeConstruction','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N26HomeConstruction <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>