<?php
$this->breadcrumbs=array(
	'N41 Saving Avaliblities',
);

$this->menu=array(
	array('label'=>'Create N41SavingAvaliblity','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N41SavingAvaliblity','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N41 Saving Avaliblities</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
