


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N41SavingAvaliblity</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown41savingavaliblity" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n41savingavaliblity-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n41SavingAvaliblity._tabularform_additional', array('modelN41SavingAvaliblity'=>$modelN41SavingAvaliblity)); ?>
	</div>
</div>

<script>
	$('#n41savingavaliblity-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown41savingavaliblity', "
	$('#btn-addrown41savingavaliblity').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n41savingavaliblity/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n41savingavaliblity-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown41savingavaliblity',"
	$('#n41savingavaliblity-list').on('click', '.btn-deleterown41savingavaliblity', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n41savingavaliblity/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n41savingavaliblity-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

