<?php
$this->breadcrumbs=array(
	'N41 Saving Avaliblities'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N41SavingAvaliblity','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N41SavingAvaliblity','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N41SavingAvaliblity','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N41SavingAvaliblity','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N41SavingAvaliblity <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>