<?php
$this->breadcrumbs=array(
	'N41 Saving Avaliblities'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N41SavingAvaliblity','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N41SavingAvaliblity','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N41SavingAvaliblity</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN41SavingAvaliblity'=>$modelN41SavingAvaliblity)); ?>