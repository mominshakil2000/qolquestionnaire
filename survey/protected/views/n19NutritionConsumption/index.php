<?php
$this->breadcrumbs=array(
	'N19 Nutrition Consumptions',
);

$this->menu=array(
	array('label'=>'Create N19NutritionConsumption','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N19NutritionConsumption','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N19 Nutrition Consumptions</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
