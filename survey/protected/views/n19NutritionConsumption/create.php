<?php
$this->breadcrumbs=array(
	'N19 Nutrition Consumptions'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N19NutritionConsumption','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N19NutritionConsumption','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N19NutritionConsumption</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN19NutritionConsumption'=>$modelN19NutritionConsumption)); ?>