


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N19NutritionConsumption</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown19nutritionconsumption" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n19nutritionconsumption-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n19NutritionConsumption._tabularform_additional', array('modelN19NutritionConsumption'=>$modelN19NutritionConsumption)); ?>
	</div>
</div>

<script>
	$('#n19nutritionconsumption-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown19nutritionconsumption', "
	$('#btn-addrown19nutritionconsumption').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n19nutritionconsumption/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n19nutritionconsumption-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown19nutritionconsumption',"
	$('#n19nutritionconsumption-list').on('click', '.btn-deleterown19nutritionconsumption', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n19nutritionconsumption/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n19nutritionconsumption-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

