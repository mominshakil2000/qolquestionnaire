<?php
$this->breadcrumbs=array(
	'N19 Nutrition Consumptions'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N19NutritionConsumption','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N19NutritionConsumption','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N19NutritionConsumption','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N19NutritionConsumption','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N19NutritionConsumption <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>