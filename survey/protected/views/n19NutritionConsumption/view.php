<?php
$this->breadcrumbs=array(
	'N19 Nutrition Consumptions'=>array('index'),
	$model->uid,
);

$this->menu=array(
	array('label'=>'List N19NutritionConsumption','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N19NutritionConsumption','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Update N19NutritionConsumption','url'=>array('update','id'=>$model->uid),'icon'=>'glyphicon glyphicon-pencil'),
	array('label'=>'Delete N19NutritionConsumption','url'=>'#','linkOptions'=>array('submit'=>array('delete','id'=>$model->uid),'confirm'=>'Are you sure you want to delete this item?'),'icon'=>'glyphicon glyphicon-trash'),
	array('label'=>'Manage N19NutritionConsumption','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>View N19NutritionConsumption #<?php echo $model->uid; ?></h1>

<?php $this->widget('bootstrap.widgets.TbDetailView',array(
	'data'=>$model,
	'attributes'=>array(
		'uid',
		'label',
	),
)); ?>
