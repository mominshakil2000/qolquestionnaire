


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">N7JamatKhanaAttendenceReason</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrown7jamatkhanaattendencereason" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="n7jamatkhanaattendencereason-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.n7JamatKhanaAttendenceReason._tabularform_additional', array('modelN7JamatKhanaAttendenceReason'=>$modelN7JamatKhanaAttendenceReason)); ?>
	</div>
</div>

<script>
	$('#n7jamatkhanaattendencereason-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrown7jamatkhanaattendencereason', "
	$('#btn-addrown7jamatkhanaattendencereason').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n7jamatkhanaattendencereason/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#n7jamatkhanaattendencereason-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterown7jamatkhanaattendencereason',"
	$('#n7jamatkhanaattendencereason-list').on('click', '.btn-deleterown7jamatkhanaattendencereason', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('n7jamatkhanaattendencereason/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#n7jamatkhanaattendencereason-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

