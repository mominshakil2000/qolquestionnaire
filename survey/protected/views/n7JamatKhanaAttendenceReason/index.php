<?php
$this->breadcrumbs=array(
	'N7 Jamat Khana Attendence Reasons',
);

$this->menu=array(
	array('label'=>'Create N7JamatKhanaAttendenceReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'Manage N7JamatKhanaAttendenceReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>N7 Jamat Khana Attendence Reasons</h1>

<?php $this->widget('bootstrap.widgets.TbListView',array(
	'dataProvider'=>$dataProvider,
	'itemView'=>'_view',
)); ?>
