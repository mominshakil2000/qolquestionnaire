<?php
$this->breadcrumbs=array(
	'N7 Jamat Khana Attendence Reasons'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List N7JamatKhanaAttendenceReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create N7JamatKhanaAttendenceReason','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View N7JamatKhanaAttendenceReason','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage N7JamatKhanaAttendenceReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update N7JamatKhanaAttendenceReason <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>