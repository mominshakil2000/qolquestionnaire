<?php
$this->breadcrumbs=array(
	'N7 Jamat Khana Attendence Reasons'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List N7JamatKhanaAttendenceReason','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage N7JamatKhanaAttendenceReason','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create N7JamatKhanaAttendenceReason</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelN7JamatKhanaAttendenceReason'=>$modelN7JamatKhanaAttendenceReason)); ?>