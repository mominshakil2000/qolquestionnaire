<?php
$this->breadcrumbs=array(
	'Form Post Statuses'=>array('index'),
	'Create',
);

$this->menu=array(
	array('label'=>'List FormPostStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Manage FormPostStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Create FormPostStatus</h1>
<?php echo $this->renderPartial('_form', array('model'=>$model, 'modelFormPostStatus'=>$modelFormPostStatus)); ?>