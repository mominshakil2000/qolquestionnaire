


<div class="panel panel-default">
	<div class="panel-heading">
		<div class="row">
			<b class="col-xs-6">FormPostStatus</b>
			<div class="col-xs-6 text-right">
				<a id="btn-addrowformpoststatus" class="btn-sm btn btn-primary" href="javascript:void(0);"><i class="glyphicon glyphicon-plus"></i> Add Item</a>			
			</div>
		</div>
	</div>
	<div id="formpoststatus-list" class="panel-body">
		<?php echo $this->renderPartial('application.views.formPostStatus._tabularform_additional', array('modelFormPostStatus'=>$modelFormPostStatus)); ?>
	</div>
</div>

<script>
	$('#formpoststatus-list').on('click', '.input-group.date', function(){$('.input-group.date').datepicker({
		format: "dd/mm/yyyy",
		keyboardNavigation: false,
		autoclose: true,
		todayHighlight: true,
	});});
</script>

<?php
Yii::app()->clientScript->registerScript('addrowformpoststatus', "
	$('#btn-addrowformpoststatus').click(function(){
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('formpoststatus/formaddrow') . "',
			type: 'post',
			data: $('#" . $form->id . "').serialize(),
			dataType: 'html',
			success: function(data) {
				$('#formpoststatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while adding a new block.');
			}
		});
	});
");
?>

<?php
Yii::app()->clientScript->registerScript('deleterowformpoststatus',"
	$('#formpoststatus-list').on('click', '.btn-deleterowformpoststatus', function(){
		rowIndex = $(this).closest('tr').attr('rowIndex');
		$.ajax({
			url: '".Yii::app()->urlManager->createUrl('formpoststatus/formdeleterow')."',
			type: 'post',
			data: $('#".$form->id."').serialize()+'&rowIndex='+rowIndex,
			dataType: 'html',
			success: function(data) {
				$('#formpoststatus-list').html(data);
			},
			error: function() {
				alert('An error has occured while deleting a row.');
			}
		});
	});
");
?>

