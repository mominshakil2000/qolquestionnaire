<?php
$this->breadcrumbs=array(
	'Form Post Statuses'=>array('index'),
	$model->uid=>array('view','id'=>$model->uid),
	'Update',
);

$this->menu=array(
	array('label'=>'List FormPostStatus','url'=>array('index'),'icon'=>'glyphicon glyphicon-list'),
	array('label'=>'Create FormPostStatus','url'=>array('create'),'icon'=>'glyphicon glyphicon-plus'),
	array('label'=>'View FormPostStatus','url'=>array('view','id'=>$model->uid),'icon'=>'glyphicon glyphicon-eye-open'),
	array('label'=>'Manage FormPostStatus','url'=>array('admin'),'icon'=>'glyphicon glyphicon-list-alt'),
);
?>

<h1>Update FormPostStatus <?php echo $model->uid; ?></h1>

<?php echo $this->renderPartial('_form',array('model'=>$model)); ?>