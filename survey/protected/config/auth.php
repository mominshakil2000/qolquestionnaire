<?php
return array (
  'admin' => 
  array (
    'type' => 2,
    'description' => 'can perform all operations',
    'bizRule' => '',
    'data' => '',
	'assignments' => 
    array (
      10 => 
      array (
        'bizRule' => NULL,
        'data' => NULL,
      ),
    ),  
  ),
  'adminsupervisor' => 
  array (
    'type' => 2,
    'description' => 'Can perform all operations, including form posting',
    'bizRule' => '',
    'data' => '',
  ),
  'dataentrysupervisor' => 
  array (
    'type' => 2,
    'description' => 'Can perform all operations, including form posting',
    'bizRule' => '',
    'data' => '',
  ),
  'dataentryoperator' => 
  array (
    'type' => 2,
    'description' => 'can create and update form',
    'bizRule' => '',
    'data' => '',
  ),
);
